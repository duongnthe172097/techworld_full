package dal;

import dal.DBContext;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Cart;

public class CartDAO extends DBContext {

    public List<Cart> getAllCarts() {
        List<Cart> cartList = new ArrayList<>();

        try {
            String query = "SELECT * FROM cart";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Cart cart = new Cart();
                cart.setCartId(resultSet.getInt("cart_id"));
                cart.setUserId(resultSet.getInt("user_id"));
                cartList.add(cart);
            }

        } catch (SQLException e) {
            System.out.println("getAllCarts: " + e.getMessage());
        }

        return cartList;
    }

    public Cart getCartById(int cartId) {
        Cart cart = null;

        try {
            String query = "SELECT * FROM cart WHERE cart_id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, cartId);

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                cart = new Cart();
                cart.setCartId(resultSet.getInt("cart_id"));
                cart.setUserId(resultSet.getInt("user_id"));
            }

        } catch (SQLException e) {
            System.out.println("getCartById: " + e.getMessage());
        }

        return cart;
    }

    public boolean addCart(Cart cart) {
        try {
            String query = "INSERT INTO cart (user_id) VALUES (?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);

            preparedStatement.setInt(1, cart.getUserId());

            int rowsAffected = preparedStatement.executeUpdate();
            return rowsAffected > 0;

        } catch (SQLException e) {
            System.out.println("addCart: " + e.getMessage());
            return false;
        }
    }

    public boolean updateCart(Cart cart) {
        try {
            String query = "UPDATE cart SET user_id=? WHERE cart_id=?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);

            preparedStatement.setInt(1, cart.getUserId());
            preparedStatement.setInt(2, cart.getCartId());

            int rowsAffected = preparedStatement.executeUpdate();
            return rowsAffected > 0;

        } catch (SQLException e) {
            System.out.println("updateCart: " + e.getMessage());
            return false;
        }
    }

    public boolean deleteCart(int cartId) {
        try {
            String query = "DELETE FROM cart WHERE cart_id=?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);

            preparedStatement.setInt(1, cartId);

            int rowsAffected = preparedStatement.executeUpdate();
            return rowsAffected > 0;

        } catch (SQLException e) {
            System.out.println("deleteCart: " + e.getMessage());
            return false;
        }
    }

    public Cart getFirstCartByUserId(int userId) {
        Cart cart = null;

        try {
            String query = "SELECT * FROM cart WHERE user_id = ? LIMIT 1";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, userId);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                cart = new Cart();
                cart.setCartId(resultSet.getInt("cart_id"));
                cart.setUserId(resultSet.getInt("user_id"));
            }

        } catch (SQLException e) {
            System.out.println("getFirstCartByUserId: " + e.getMessage());
        }

        return cart;
    }

    public Cart getCartByUserId(int UserId) {
        Cart cart = null;

        try {
            String query = "SELECT * FROM cart WHERE user_id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, UserId);

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                cart = new Cart();
                cart.setCartId(resultSet.getInt("cart_id"));
                cart.setUserId(resultSet.getInt("user_id"));
            }

        } catch (SQLException e) {
            System.out.println("getCartById: " + e.getMessage());
        }

        return cart;
    }
    public int getCartIdByUserId( int id) {
        int cartId = -1; // Default value if cart not found or user has no cart
        
        String xSql = "SELECT cart_id FROM cart WHERE user_id = ?";
        
        try {
             PreparedStatement preparedStatement = connection.prepareStatement(xSql); 
            
            preparedStatement.setInt(1, id);
            ResultSet rs = preparedStatement.executeQuery();
            
            if (rs.next()) {
                cartId = rs.getInt("cart_id");
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
        
        return cartId;
    }
    
   
    
    public static void main(String[] args) {
        CartDAO c = new CartDAO();
        c.deleteCart(14);
    }
}
