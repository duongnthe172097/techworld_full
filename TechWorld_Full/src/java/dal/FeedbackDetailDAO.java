/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import model.FeedbackDetail;

/**
 *
 * @author HP
 */
public class FeedbackDetailDAO extends DAO {

    public FeedbackDetail getFeedbackDetails(int feedbackId) {
        FeedbackDetail feedbackDetails = new FeedbackDetail();
        xSql = "SELECT f.feedback_id, pi.image_url,  u.image, u.full_name, u.email, u.mobile, f.rated_star, f.content, f.status \n"
                + "FROM `user` u\n"
                + "INNER JOIN `feedback` f ON u.user_id = f.user_id\n"
                + "INNER JOIN `product` p ON f.product_id = p.product_id\n"
                + "INNER JOIN `product_image` pi ON p.product_id = pi.product_id\n"
                + "WHERE f.feedback_id = ?\n"
                + "LIMIT 1;";
        try {
            ps = con.prepareStatement(xSql);
            ps.setInt(1, feedbackId);
            rs = ps.executeQuery();
            while (rs.next()) {
                feedbackDetails.setFeedbackId(rs.getInt("feedback_id"));
                feedbackDetails.setImageUrl(rs.getString("image_url"));
                feedbackDetails.setImage(rs.getString("image"));
                feedbackDetails.setFullName(rs.getString("full_name"));
                feedbackDetails.setEmail(rs.getString("email"));
                feedbackDetails.setMobile(rs.getString("mobile"));
                feedbackDetails.setRatedStar(rs.getInt("rated_star"));
                feedbackDetails.setContent(rs.getString("content"));
                feedbackDetails.setStatus(rs.getBoolean("status"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return feedbackDetails;
    }

    public static void main(String[] args) {
        FeedbackDetailDAO dao = new FeedbackDetailDAO();
        FeedbackDetail feedback = dao.getFeedbackDetails(1);
        System.out.println(dao.getFeedbackDetails(1));

    }

}
