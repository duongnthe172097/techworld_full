/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.util.ArrayList;
import java.util.List;
import model.Comment;

/**
 *
 * @author admin
 */
public class CommentDAO extends DAO {

    public List<Comment> getCommentsByPostId(int id) {
        List<Comment> comments = new ArrayList<>();
        xSql = "select * from comment where post_id = " + id + " and reply_of is null";
        System.out.println(xSql);
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Comment comment = new Comment(
                        rs.getInt("comment_id"),
                        rs.getString("content"),
                        rs.getInt("post_id"),
                        rs.getInt("user_id"),
                        rs.getDate("updated_date")
                );
                comments.add(comment);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return comments;
    }

    public List<Comment> getReplyByCommentId(int id) {
        List<Comment> comments = new ArrayList<>();
        xSql = "select * from comment where reply_of = " + id;
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Comment comment = new Comment(
                        rs.getInt("comment_id"),
                        rs.getString("content"),
                        rs.getInt("post_id"),
                        rs.getInt("user_id"),
                        rs.getDate("updated_date")
                );
                comments.add(comment);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return comments;
    }

    public int insertComment(Comment comment) {
        xSql = "INSERT INTO `online_shop`.`comment`(`content`,`post_id`,`user_id`,`updated_date`)\n"
                + "VALUES(?, ?, ?, ?);";
        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, comment.getContent());
            ps.setInt(2, comment.getPostId());
            ps.setInt(3, comment.getUserId());
            ps.setDate(4, comment.getUpdatedDate());
            return ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
    
    public int insertReply(Comment comment) {
        xSql = "INSERT INTO `online_shop`.`comment`(`content`,`post_id`,`user_id`,`updated_date`, `reply_of`)\n"
                + "VALUES(?, ?, ?, ?, ?);";
        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, comment.getContent());
            ps.setInt(2, comment.getPostId());
            ps.setInt(3, comment.getUserId());
            ps.setDate(4, comment.getUpdatedDate());
            ps.setInt(5, comment.getReplyOf());
            return ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static void main(String[] args) {
        CommentDAO dao = new CommentDAO();
        List<Comment> comments = dao.getCommentsByPostId(1);
        System.out.println(comments.size());
    }
}
