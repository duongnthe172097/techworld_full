/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import model.Feedback;
import model.User;

/**
 *
 * @author admin
 */
public class FeedbackDAO extends DAO {

    public LinkedHashMap<Feedback, String[]> getNewlyFeedBacks(Date fromDate, Date toDate) {
        LinkedHashMap<Feedback, String[]> feedbacks = new LinkedHashMap<>();
        xSql = "select f.*, u.full_name, u.image , p.product_name\n"
                + "from feedback f join user u on f.user_id = u.user_id\n"
                + "	join product p on f.product_id = p.product_id\n"
                + "where f.updated_date between '" + fromDate + "' and '" + toDate + "'\n"
                + "order by f.updated_date desc";
        System.out.println(xSql);
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Feedback feedback = new Feedback(
                        rs.getInt("feedback_id"),
                        rs.getInt("user_id"),
                        rs.getInt("product_id"),
                        rs.getString("content"),
                        rs.getInt("rated_star"),
                        rs.getDate("updated_date")
                );
                String[] feedInfo = new String[3];
                feedInfo[0] = rs.getString("full_name");
                feedInfo[1] = rs.getString("image") == null ? "" : rs.getString("image");
                feedInfo[2] = rs.getString("product_name");
                feedbacks.put(feedback, feedInfo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return feedbacks;
    }

    public double getAvgRatedStarOfProductById(int id) {
        double avg = 0;
        xSql = "select avg(f.rated_star) as avg_rating\n"
                + "from product p join feedback f on p.product_id = f.product_id\n"
                + "where p.product_id = " + id + "\n"
                + "group by p.product_id;";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            if (rs.next()) {
                avg = rs.getDouble("avg_rating");
                avg = avg == (int) avg ? (int) avg : Math.round(avg * 10.0) / 10.0;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return avg;
    }

    public int getCountFeedbackOfProductById(int id) {
        int count = 0;
        xSql = "select count(f.feedback_id) as numberOfFeedbacks\n"
                + "from product p join feedback f on p.product_id = f.product_id\n"
                + "where p.product_id = " + id + "\n"
                + "group by p.product_id;";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            if (rs.next()) {
                count = rs.getInt("numberOfFeedbacks");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }

    public LinkedHashMap<Feedback, String> getFeedbacksOfProductById(int id) {
        LinkedHashMap<Feedback, String> feedbacks = new LinkedHashMap<>();
        xSql = "select f.*, u.full_name\n"
                + "from feedback f join user u on f.user_id = u.user_id\n"
                + "where f.product_id = " + id;
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Feedback feedback = new Feedback(
                        rs.getInt("feedback_id"), 
                        rs.getInt("user_id"), 
                        rs.getInt("product_id"), 
                        rs.getString("content"), 
                        rs.getInt("rated_star"), 
                        rs.getDate("updated_date")
                );
                feedbacks.put(feedback, rs.getString("full_name"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return feedbacks;
    }
    
    public Feedback createFeedback(Feedback feedback) {
        String query = "INSERT INTO feedback (user_id, product_id, content, rated_star, updated_date, status) "
                + "VALUES (?, ?, ?, ?, NOW(), ?)";
        try {
            ps = con.prepareStatement(query, ps.RETURN_GENERATED_KEYS);
            ps.setInt(1, feedback.getUserId());
            ps.setInt(2, feedback.getProductId());
            ps.setString(3, feedback.getContent());
            ps.setInt(4, feedback.getRatedStar());
            ps.setBoolean(5, feedback.isStatus());

            int rowsAffected = ps.executeUpdate();
            if (rowsAffected > 0) {
                rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    feedback.setFeedbackId(rs.getInt(1));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return feedback;
    }

    public Feedback getByUserIdAndOrderId(int userId, int orderId) {
        Feedback feedback = null;
        String query = "SELECT * FROM feedback WHERE user_id = ? AND product_id = ?";
        try {
            ps = con.prepareStatement(query);
            ps.setInt(1, userId);
            ps.setInt(2, orderId);
            rs = ps.executeQuery();
            if (rs.next()) {
                feedback = new Feedback(
                        rs.getInt("feedback_id"),
                        rs.getInt("user_id"),
                        rs.getInt("product_id"),
                        rs.getString("content"),
                        rs.getInt("rated_star"),
                        rs.getDate("updated_date"),
                        rs.getBoolean("status")
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return feedback;
    }

    public LinkedHashMap<Feedback, String> getActiveFeedbacksOfProductById(int id) {
        LinkedHashMap<Feedback, String> feedbacks = new LinkedHashMap<>();
        xSql = "select f.*, u.full_name\n"
                + "from feedback f join user u on f.user_id = u.user_id\n"
                + "where f.product_id = " + id + " and f.status = 1";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Feedback feedback = new Feedback(
                        rs.getInt("feedback_id"), 
                        rs.getInt("user_id"), 
                        rs.getInt("product_id"), 
                        rs.getString("content"), 
                        rs.getInt("rated_star"), 
                        rs.getDate("updated_date")
                );
                feedbacks.put(feedback, rs.getString("full_name"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return feedbacks;
    }
    
    public List<Feedback> getAllFeedbacks() {
        List<Feedback> feedbacks = new ArrayList<>();
        xSql = "select * from online_shop.feedback order by updated_date desc";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while(rs.next()) {
                Feedback feedback = new Feedback(
                        rs.getInt("feedback_id"), 
                        rs.getInt("user_id"), 
                        rs.getInt("product_id"), 
                        rs.getString("content"), 
                        rs.getInt("rated_star"), 
                        rs.getDate("updated_date"),
                        rs.getInt("status") == 1
                );
                feedbacks.add(feedback);
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return feedbacks;
    }
    
    public void changeStatusOfFeedbackById(int id, boolean status) {
        int newStatus = status ? 0 : 1;
        xSql = "update feedback set status = ? where feedback_id = ?";
        try {
            ps = con.prepareStatement(xSql);
            ps.setInt(1, newStatus);
            ps.setInt(2, id);
            ps.executeUpdate();
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }


    public static void main(String[] args) {
        FeedbackDAO dao = new FeedbackDAO();
        Date toDate = new Date(System.currentTimeMillis());
        Date fromDate = new Date(toDate.getTime() - 7 * 24 * 60 * 60 * 1000);
        LinkedHashMap<Feedback, String[]> fb = dao.getNewlyFeedBacks(fromDate, toDate);
        System.out.println(fb.size());
    }
}
