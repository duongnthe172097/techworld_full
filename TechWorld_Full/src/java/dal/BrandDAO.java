/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Brand;

/**
 *
 * @author admin
 */
public class BrandDAO extends DAO{
    public List<Brand> getAllBrands() {
        List<Brand> brands = new ArrayList<>();
        xSql = "select * from brand";
        try{
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            int brand_id;
            String brand_name,image;
            boolean isActive = true;
            Brand brand;
            while(rs.next()) {
                brand_id = rs.getInt("brand_id");
                brand_name = rs.getString("brand_name");
                image = rs.getString("image");
                isActive = rs.getInt("is_active") == 1;
                brand = new Brand(brand_id, brand_name,image, isActive);
                brands.add(brand);
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return brands;
    }
    
    public List<Brand> getAllActiveBrands() {
        List<Brand> brands = new ArrayList<>();
        xSql = "select * from brand  where is_active = 1";
        try{
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            int brand_id;
            String brand_name,image;
            boolean isActive = true;
            Brand brand;
            while(rs.next()) {
                brand_id = rs.getInt("brand_id");
                brand_name = rs.getString("brand_name");
                image = rs.getString("image");
                isActive = rs.getInt("is_active") == 1;
                brand = new Brand(brand_id, brand_name,image, isActive);
                brands.add(brand);
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return brands;
    }
    
    public String getBrandNameById(int id) {
        xSql = "select brand_name from brand where brand_id = " + id;
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            if(rs.next()) {
                return rs.getString("brand_name");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public int getBrandIdbyBrandName (String name) {
        xSql = "Select brand_id from brand where brand_name like ?";
        try {
            ps = con.prepareStatement(xSql);
     
            ps.setString(1, name);
            rs = ps.executeQuery();
            if(rs.next()) {
                return rs.getInt("brand_id");}
        }catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
    public int updateBrandStatusById(int bid, boolean status) {
        String xSql = "UPDATE brand SET is_active = ? WHERE brand_id = ?";
        int n = 0;
        try {
            ps = con.prepareStatement(xSql);
            ps.setBoolean(1, status);
            ps.setInt(2, bid);
            n = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return n;
    }
    
    public boolean checkExistedBrand(String brandName) {
        xSql = "select brand_name from brand where brand_name = '" + brandName + "'";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            if (rs.next()) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public int getLastBrandId() {
        xSql = "select brand_id from brand order by brand_id desc limit 1";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("brand_id");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 1;
    }

    public int insertBrandProduct(Brand brand) {
        xSql = "INSERT INTO `online_shop`.`brand`\n"
                + "(`brand_name`,\n"
                + "`is_active`)\n"
                + "VALUES\n"
                + "(?, ?)";
        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, brand.getBrandName());
            ps.setInt(2, brand.isIsActive() ? 1 : 0);
            return ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
}
