/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.OrderDetail;

public class OrderDetailDAO extends DBContext {

    public boolean addOrderDetail(OrderDetail orderDetail) {
        try {
            String query = "INSERT INTO order_detail (order_id, product_id, quantity) VALUES (?, ?, ?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);

            preparedStatement.setInt(1, orderDetail.getOrderId());
            preparedStatement.setInt(2, orderDetail.getProductId());
            preparedStatement.setInt(3, orderDetail.getQuantity());

            int rowsAffected = preparedStatement.executeUpdate();
            return rowsAffected > 0;

        } catch (SQLException e) {
            System.out.println("addOrderDetail: " + e.getMessage());
            return false;
        }
    }

    public List<OrderDetail> getOrderDetailsByOrderId(int orderId) {
        List<OrderDetail> orderDetailList = new ArrayList<>();

        try {
            String query = "SELECT * FROM order_detail WHERE order_id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, orderId);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                OrderDetail orderDetail = new OrderDetail();
                orderDetail.setOrderId(resultSet.getInt("order_id"));
                orderDetail.setProductId(resultSet.getInt("product_id"));
                orderDetail.setQuantity(resultSet.getInt("quantity"));
                orderDetailList.add(orderDetail);
            }

        } catch (SQLException e) {
            System.out.println("getOrderDetailsByOrderId: " + e.getMessage());
        }

        return orderDetailList;
    }

    public boolean deleteOrderDetailsByOrderId(int orderId) {
        try {
            String query = "DELETE FROM order_detail WHERE order_id=?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);

            preparedStatement.setInt(1, orderId);

            int rowsAffected = preparedStatement.executeUpdate();
            return rowsAffected > 0;

        } catch (SQLException e) {
            System.out.println("deleteOrderDetailsByOrderId: " + e.getMessage());
            return false;
        }
    }
}

