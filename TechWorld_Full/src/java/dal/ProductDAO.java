/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import model.Product;

/**
 *
 * @author admin
 */
public class ProductDAO extends DAO {

    public List<Product> getAllProducts() {
        List<Product> products = new ArrayList<>();
        xSql = "select * from product order by category_id, brand_id, updated_date desc";
        System.out.println(xSql);
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            int productId;
            String productName;
            int categoryId;
            int brandId;
            int quantityInStock;
            double price;
            double discount;
            String description;
            boolean status;
            Date updatedDate;
            Product product;
            while (rs.next()) {
                productId = rs.getInt("product_id");
                productName = rs.getString("product_name");
                categoryId = rs.getInt("category_id");
                brandId = rs.getInt("brand_id");
                quantityInStock = rs.getInt("quantity_in_stock");
                price = rs.getDouble("price");
                discount = rs.getInt("discount");
                description = rs.getString("description");
                status = rs.getInt("status") == 1;
                updatedDate = rs.getDate("updated_date");
                product = new Product(productId, productName, categoryId, brandId,
                        quantityInStock, price, discount, description, status, updatedDate);
                products.add(product);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return products;
    }

    public List<Product> getAllActiveProducts() {
        List<Product> products = new ArrayList<>();
        xSql = "SELECT p.product_id, p.product_name, p.category_id, p.brand_id, p.quantity_in_stock, p.price, p.discount, p.description, p.status, p.updated_date\n"
                + "FROM product p\n"
                + "JOIN category c ON p.category_id = c.category_id\n"
                + "JOIN brand b ON p.brand_id = b.brand_id\n"
                + "WHERE p.status = 1 AND c.is_active = 1 AND b.is_active = 1;";
        System.out.println(xSql);
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            int productId;
            String productName;
            int categoryId;
            int brandId;
            int quantityInStock;
            double price;
            double discount;
            String description;
            boolean status;
            Date updatedDate;
            Product product;
            while (rs.next()) {
                productId = rs.getInt("product_id");
                productName = rs.getString("product_name");
                categoryId = rs.getInt("category_id");
                brandId = rs.getInt("brand_id");
                quantityInStock = rs.getInt("quantity_in_stock");
                price = rs.getDouble("price");
                discount = rs.getInt("discount");
                description = rs.getString("description");
                status = rs.getInt("status") == 1;
                updatedDate = rs.getDate("updated_date");
                product = new Product(productId, productName, categoryId, brandId,
                        quantityInStock, price, discount, description, status, updatedDate);
                products.add(product);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return products;
    }

    public List<Product> getPaginatedProducts(List<Product> list, int start, int end) {
        List<Product> products = new ArrayList<>();
        for (int i = start; i < end; i++) {
            products.add(list.get(i));
        }
        return products;
    }

    public List<Product> filterProducts(int[] categoryIds, int[] brandIds) {
        List<Product> products = new ArrayList<>();
        xSql = "select p.*, c.parent_id \n"
                + "from product p join category c on p.category_id = c.category_id"
                + " where true";
        if (categoryIds != null) {
            xSql += " and (p.category_id in(";
            for (int i = 0; i < categoryIds.length; i++) {
                xSql += categoryIds[i] + ",";
            }
            if (xSql.endsWith(",")) {
                xSql = xSql.substring(0, xSql.length() - 1);
            }
            xSql += ")";

            xSql += " or parent_id in(";
            for (int i = 0; i < categoryIds.length; i++) {
                xSql += categoryIds[i] + ",";
            }
            if (xSql.endsWith(",")) {
                xSql = xSql.substring(0, xSql.length() - 1);
            }
            xSql += "))";
        }
        if (brandIds != null) {
            xSql += " and brand_id in(";
            for (int i = 0; i < brandIds.length; i++) {
                xSql += brandIds[i] + ",";
            }
            if (xSql.endsWith(",")) {
                xSql = xSql.substring(0, xSql.length() - 1);
            }
            xSql += ")\n"
                    + "order by category_id, brand_id, updated_date desc";
        }
        System.out.println(xSql);
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            int productId;
            String productName;
            int categoryId;
            int brandId;
            int quantityInStock;
            double price;
            double discount;
            String description;
            boolean status;
            Date updatedDate;
            Product product;
            while (rs.next()) {
                productId = rs.getInt("product_id");
                productName = rs.getString("product_name");
                categoryId = rs.getInt("category_id");
                brandId = rs.getInt("brand_id");
                quantityInStock = rs.getInt("quantity_in_stock");
                price = rs.getDouble("price");
                discount = rs.getInt("discount");
                description = rs.getString("description");
                status = rs.getInt("status") == 1;
                updatedDate = rs.getDate("updated_date");
                product = new Product(productId, productName, categoryId, brandId,
                        quantityInStock, price, discount, description, status, updatedDate);
                products.add(product);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return products;
    }

    public List<Product> filterActiveProducts(int[] categoryIds, int[] brandIds) {
        List<Product> products = new ArrayList<>();
        xSql = "SELECT p.*, c.parent_id\n"
                + "FROM product p\n"
                + "JOIN category c ON p.category_id = c.category_id\n"
                + "JOIN brand b ON p.brand_id = b.brand_id\n"
                + "WHERE p.status = 1 AND c.is_active = 1 AND b.is_active = 1";
        if (categoryIds != null) {
            xSql += " and (p.category_id in(";
            for (int i = 0; i < categoryIds.length; i++) {
                xSql += categoryIds[i] + ",";
            }
            if (xSql.endsWith(",")) {
                xSql = xSql.substring(0, xSql.length() - 1);
            }
            xSql += ")";

            xSql += " or parent_id in(";
            for (int i = 0; i < categoryIds.length; i++) {
                xSql += categoryIds[i] + ",";
            }
            if (xSql.endsWith(",")) {
                xSql = xSql.substring(0, xSql.length() - 1);
            }
            xSql += "))";
        }
        if (brandIds != null) {
            xSql += " and p.brand_id in(";
            for (int i = 0; i < brandIds.length; i++) {
                xSql += brandIds[i] + ",";
            }
            if (xSql.endsWith(",")) {
                xSql = xSql.substring(0, xSql.length() - 1);
            }
            xSql += ")\n"
                    + "order by category_id, brand_id, updated_date desc";
        }
        System.out.println(xSql);
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            int productId;
            String productName;
            int categoryId;
            int brandId;
            int quantityInStock;
            double price;
            double discount;
            String description;
            boolean status;
            Date updatedDate;
            Product product;
            while (rs.next()) {
                productId = rs.getInt("product_id");
                productName = rs.getString("product_name");
                categoryId = rs.getInt("category_id");
                brandId = rs.getInt("brand_id");
                quantityInStock = rs.getInt("quantity_in_stock");
                price = rs.getDouble("price");
                discount = rs.getInt("discount");
                description = rs.getString("description");
                status = rs.getInt("status") == 1;
                updatedDate = rs.getDate("updated_date");
                product = new Product(productId, productName, categoryId, brandId,
                        quantityInStock, price, discount, description, status, updatedDate);
                products.add(product);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return products;
    }

    public List<Product> searchActiveProducts(String searchKey) {
        List<Product> products = new ArrayList<>();
        xSql = "SELECT DISTINCT p.*\n"
                + "FROM product p\n"
                + "JOIN category c ON p.category_id = c.category_id\n"
                + "JOIN brand b ON p.brand_id = b.brand_id\n"
                + "WHERE (p.product_name LIKE '%" + searchKey + "%'\n"
                + "    OR c.category_name LIKE '%" + searchKey + "%'\n"
                + "    OR c.parent_id IN (SELECT category_id FROM category WHERE category_name LIKE '%" + searchKey + "%')\n"
                + "    OR b.brand_name LIKE '%" + searchKey + "%')\n"
                + "AND p.status = 1\n"
                + "AND c.is_active = 1\n"
                + "AND b.is_active = 1\n"
                + "ORDER BY category_id, brand_id, updated_date DESC;";
        System.out.println(xSql);
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            int productId;
            String productName;
            int categoryId;
            int brandId;
            int quantityInStock;
            double price;
            double discount;
            String description;
            boolean status;
            Date updatedDate;
            Product product;
            while (rs.next()) {
                productId = rs.getInt("product_id");
                productName = rs.getString("product_name");
                categoryId = rs.getInt("category_id");
                brandId = rs.getInt("brand_id");
                quantityInStock = rs.getInt("quantity_in_stock");
                price = rs.getDouble("price");
                discount = rs.getInt("discount");
                description = rs.getString("description");
                status = rs.getInt("status") == 1;
                updatedDate = rs.getDate("updated_date");
                product = new Product(productId, productName, categoryId, brandId,
                        quantityInStock, price, discount, description, status, updatedDate);
                products.add(product);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return products;
    }

    public Product getProductById(int id) {
        Product product = new Product();
        xSql = "select * from product where product_id = " + id;
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            int productId;
            String productName;
            int categoryId;
            int brandId;
            int quantityInStock;
            double price;
            double discount;
            String description;
            boolean status;
            Date updatedDate;
            if (rs.next()) {
                productId = rs.getInt("product_id");
                productName = rs.getString("product_name");
                categoryId = rs.getInt("category_id");
                brandId = rs.getInt("brand_id");
                quantityInStock = rs.getInt("quantity_in_stock");
                price = rs.getDouble("price");
                discount = rs.getInt("discount");
                description = rs.getString("description");
                status = rs.getInt("status") == 1;
                updatedDate = rs.getDate("updated_date");
                product = new Product(productId, productName, categoryId, brandId,
                        quantityInStock, price, discount, description, status, updatedDate);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return product;
    }

    public Product getProductByOrderId(int id) {
        Product product = new Product();
        xSql = "select `product`.* from `product` join `order_detail` on `order_detail`.`product_id`=`product`.`product_id` and `order_detail`.`order_id` = " + id;
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            int productId;
            String productName;
            int categoryId;
            int brandId;
            int quantityInStock;
            double price;
            double discount;
            String description;
            boolean status;
            Date updatedDate;
            if (rs.next()) {
                productId = rs.getInt("product_id");
                productName = rs.getString("product_name");
                categoryId = rs.getInt("category_id");
                brandId = rs.getInt("brand_id");
                quantityInStock = rs.getInt("quantity_in_stock");
                price = rs.getDouble("price");
                discount = rs.getInt("discount");
                description = rs.getString("description");
                status = rs.getInt("status") == 1;
                updatedDate = rs.getDate("updated_date");
                product = new Product(productId, productName, categoryId, brandId,
                        quantityInStock, price, discount, description, status, updatedDate);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return product;
    }

    public Map<String, Object> getProductWithImageByProductId(int pid) {
        Map<String, Object> productDetails = new LinkedHashMap<>();
        String xSql = "SELECT \n"
                + "    p.product_id,\n"
                + "    p.product_name,\n"
                + "    p.category_id,\n"
                + "    c.category_name,\n"
                + "    p.brand_id,\n"
                + "    b.brand_name,\n"
                + "    p.quantity_in_stock,\n"
                + "    p.price,\n"
                + "    p.discount,\n"
                + "    p.description,\n"
                + "    p.status,\n"
                + "    p.updated_date,\n"
                + "    pi.image_id,\n"
                + "    pi.image_url\n"
                + "FROM \n"
                + "    product p\n"
                + "LEFT JOIN\n"
                + "    product_image pi ON p.product_id = pi.product_id\n"
                + "LEFT JOIN\n"
                + "    brand b ON p.brand_id = b.brand_id\n"
                + "INNER JOIN \n"
                + "    category c ON p.category_id = c.category_id\n"
                + "where p.product_id = ?";

        try {
            ps = con.prepareStatement(xSql);
            ps.setInt(1, pid);
            rs = ps.executeQuery();
            if (rs.next()) {
                Product product = new Product(rs.getInt("product_id"), rs.getString("product_name"), rs.getInt("category_id"), rs.getInt("brand_id"), rs.getInt("quantity_in_stock"), rs.getDouble("price"),
                        rs.getDouble("discount"), rs.getString("description"), rs.getBoolean("status"), rs.getDate("updated_date"));
                productDetails.put("product", product);
                productDetails.put("categoryName", rs.getString("category_name"));
                productDetails.put("brandName", rs.getString("brand_name"));

                List<String> imageUrls = new ArrayList<>(); // Danh sách các URL hình ảnh
                do {
                    imageUrls.add(rs.getString("image_url")); // Thêm URL hình ảnh vào danh sách
                } while (rs.next());

                productDetails.put("productImage", imageUrls); // Lưu trữ danh sách URL hình ảnh vào Map
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return productDetails;
    }

    public int updateProduct(Product product) {
        String xSql = "UPDATE product SET product_name = ?, category_id = ?, brand_id = ?, description = ?, quantity_in_stock = ?, price = ?, discount = ?, status = ? WHERE product_id = ?";
        int n = 0;
        try {
            ps = con.prepareStatement(xSql);

            ps.setString(1, product.getProductName());
            ps.setInt(2, product.getCategoryId());
            ps.setInt(3, product.getBrandId());
            ps.setString(4, product.getDescription());
            ps.setInt(5, product.getQuantityInStock());
            ps.setDouble(6, product.getPrice());
            ps.setDouble(7, product.getDiscount());
            ps.setBoolean(8, product.isStatus());
            ps.setInt(9, product.getProductId());

            n = ps.executeUpdate();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return n;
    }
    
    public List<Product> getAllProductsInTime(Date fromDate, Date toDate) {
        List<Product> products = new ArrayList<>();
        xSql = "select * from product where updated_date between '" + fromDate + "' and '" + toDate + "'"
                + " order by category_id, brand_id, updated_date desc";
        System.out.println(xSql);
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            int productId;
            String productName;
            int categoryId;
            int brandId;
            int quantityInStock;
            double price;
            double discount;
            String description;
            boolean status;
            Date updatedDate;
            Product product;
            System.out.println(xSql);
            while (rs.next()) {
                productId = rs.getInt("product_id");
                productName = rs.getString("product_name");
                categoryId = rs.getInt("category_id");
                brandId = rs.getInt("brand_id");
                quantityInStock = rs.getInt("quantity_in_stock");
                price = rs.getDouble("price");
                discount = rs.getInt("discount");
                description = rs.getString("description");
                status = rs.getInt("status") == 1;
                updatedDate = rs.getDate("updated_date");
                product = new Product(productId, productName, categoryId, brandId,
                        quantityInStock, price, discount, description, status, updatedDate);
                products.add(product);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return products;
    }

    public void changeStatusOfProductById(int id, boolean status) {
        int newStatus = status ? 0 : 1;
        xSql = "update product set status = ? where product_id = ?";
        try {
            ps = con.prepareStatement(xSql);
            ps.setInt(1, newStatus);
            ps.setInt(2, id);
            ps.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public int insertProduct(Product product) {
        xSql = "INSERT INTO `online_shop`.`product`\n"
                + "(`product_name`,\n"
                + "`category_id`,\n"
                + "`brand_id`,\n"
                + "`quantity_in_stock`,\n"
                + "`price`,\n"
                + "`discount`,\n"
                + "`description`,\n"
                + "`status`,\n"
                + "`updated_date`)\n"
                + "VALUES\n"
                + "(?,?,?,?,?,?,?,?,?);";
        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, product.getProductName());
            ps.setInt(2, product.getCategoryId());
            ps.setInt(3, product.getBrandId());
            ps.setInt(4, product.getQuantityInStock());
            ps.setDouble(5, product.getPrice());
            ps.setDouble(6, product.getDiscount());
            ps.setString(7, product.getDescription());
            ps.setInt(8, product.isStatus() ? 1 : 0);
            ps.setDate(9, product.getUpdatedDate());
            return ps.executeUpdate();
        } catch(Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
    
    public int getLastProductId() {
        xSql = "SELECT * FROM product order by product_id desc LIMIT 1";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            if(rs.next()) {
                return rs.getInt("product_id");
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
    public double getPriceByProductId(int id) {
        double price=0;
    
        xSql = "Select price from product where product_id = ? ";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            ps.setInt(1, id);
            
             if (rs.next()) {
                price = rs.getDouble("price");
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return price;
    }
    
    public boolean checkExistedProduct(String productName) {
        xSql = "select product_name from product where product_name = '" + productName + "'";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            if (rs.next()) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

}
