/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import model.Setting;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ns
 */
public class SettingDAO extends DBContext {

    private final Connection cn = super.getConnection();

    public boolean isIdExisted(int userId) {
        String sql = "select id from setting where id = ?";
        try ( PreparedStatement ps = cn.prepareStatement(sql);) {
            ps.setInt(1, userId);
            try ( ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return true;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(SettingDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public Setting addSetting(Setting setting) {
        String sql = "insert into setting(`id`, `description`, `name`, `value`, `type`, `isActive` ) values (?, ?, ?, ?, ?, ?)";
        try ( PreparedStatement ps = cn.prepareStatement(sql);) {
            ps.setInt(1, setting.getId());
            ps.setString(2, setting.getDescription());
            ps.setString(3, setting.getName());
            ps.setString(4, setting.getValue());
            ps.setString(5, setting.getType());
            ps.setBoolean(6, setting.isIsActive());
            int re = ps.executeUpdate();
            if (re > 0) {
                return setting;
            }
        } catch (SQLException ex) {
            Logger.getLogger(SettingDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Setting getSettingById(int userId) {
        String sql = "select * from setting where id = ?";
        try ( PreparedStatement ps = cn.prepareStatement(sql);) {
            ps.setInt(1, userId);
            try ( ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    String name = rs.getString("name");
                    String description = rs.getString("description");
                    String value = rs.getString("value");
                    String type = rs.getString("type");
                    Boolean isActive = rs.getBoolean("isActive");
                    
                    
                    Setting setting = new Setting(userId, name,description, value, type, isActive);
                    return setting;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(SettingDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int updateSetting(int oldId, Setting setting) {
       String sql = "UPDATE setting SET `name` = ?, `description` = ?, `value` = ?, `type` = ?, `isActive` = ? WHERE `id` = ?";
        try ( PreparedStatement ps = cn.prepareStatement(sql);) {
          ;
            ps.setString(1, setting.getName());
            ps.setString(2, setting.getDescription());
             ps.setString(3, setting.getValue());
            ps.setString(4, setting.getType());
            ps.setBoolean(5, setting.isIsActive());
           
            ps.setInt(6, oldId);
            int re = ps.executeUpdate();
            if (re > 0) {
                return re;
            }
        } catch (SQLException ex) {
            Logger.getLogger(SettingDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public List<Setting> getAllSettings() {
        String sql = "select * from setting order by id";
        try ( PreparedStatement ps = cn.prepareStatement(sql);) {
            try ( ResultSet rs = ps.executeQuery()) {
                List<Setting> settings = new ArrayList<>();
                while (rs.next()) {
                    int userId = rs.getInt("id");
                    String name = rs.getString("name");
                    String description = rs.getString("description");
                    String value = rs.getString("value");
                    String type = rs.getString("type");
                    boolean isActive = rs.getBoolean("isActive");
                    Setting setting = new Setting(userId, name, description, value, type , isActive);
                    settings.add(setting);
                }
                return settings;
            }
        } catch (SQLException ex) {
            Logger.getLogger(SettingDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Setting deleteSetting(Setting setting) {
        String sql = "delete from setting where id = ?";
        try ( PreparedStatement ps = cn.prepareStatement(sql);) {
            ps.setInt(1, setting.getId());
            int re = ps.executeUpdate();
            if (re > 0) {
                return setting;
            }
        } catch (SQLException ex) {
            Logger.getLogger(SettingDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public int updateSettingStatus(int sid, boolean status) {
        String xSql = "UPDATE setting SET isActive = ? WHERE id = ?";
        int n = 0;
        try {
            PreparedStatement ps = cn.prepareStatement(xSql);
            ps.setBoolean(1, status);
            ps.setInt(2, sid);
            n = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return n;
    }
    
    public Setting insertSetting(Setting setting) {
        String sql = "insert into setting(`name`, `value`, `type`, `isActive` ) values (?, ?, ?, ?)";
        try ( PreparedStatement ps = cn.prepareStatement(sql);) {
            ps.setString(1, setting.getName());
            ps.setString(2, setting.getValue());
            ps.setString(3, setting.getType());
            ps.setBoolean(4, setting.isIsActive());
            int re = ps.executeUpdate();
            if (re > 0) {
                return setting;
            }
        } catch (SQLException ex) {
            Logger.getLogger(SettingDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
}
        
    

