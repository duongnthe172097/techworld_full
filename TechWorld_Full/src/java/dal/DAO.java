/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DAO {

    protected Connection con = null;
    protected Connection connection = null;
    protected PreparedStatement ps = null;
    protected ResultSet rs = null;
    protected String xSql = null;

    public DAO() {
        // Initialize the connection using the shared connection from DBContext
        con = DBContext.getConnection();
        connection = con;
    }

    // Close resources in the finalize method
    @Override
    protected void finalize() {
        try {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
