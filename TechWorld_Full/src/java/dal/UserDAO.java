/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import model.Role;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.User;

/**
 *
 * @author admin
 */
public class UserDAO extends DAO {
    
    public int updateImage(int userId, String image) {
        xSql = "update `user` set image = ? where user_id = ?";
        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, image);
            ps.setInt(2, userId);
            int rowsAffected = ps.executeUpdate();
            return rowsAffected;
        }catch(Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
    
    public boolean changePassWord(String user_name, String newPassword) {
        try {
            String query = "UPDATE `user`\n"
                    + "SET password = ?\n"
                    + "WHERE user_name = ?";
            PreparedStatement statement = con.prepareStatement(query);

            statement.setString(1, newPassword);
            statement.setString(2, user_name);
            

            int rowsAffected = statement.executeUpdate();

            statement.close();

            // Check if the update was successful (1 row affected means success)
            return rowsAffected == 1;
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false; // Handle the error as needed
        }
    }
    
    public void editProfile(String full_name, String mobile, String address, String user_name) {
        xSql = "UPDATE `user` SET full_name = ?, mobile = ?, address = ?"
                + "WHERE user_name = ?";
        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, full_name);
            ps.setString(2, mobile);
            ps.setString(3, address);
            ps.setString(4, user_name);       
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public User login(String user_name, String password) {

        xSql = "select * from `user`\n"
                + "where user_name = ?\n"
                + "and password = ?";
        try {
            con = new DBContext().getConnection();//mo ket noi voi sql
            ps = con.prepareStatement(xSql);
            ps.setString(1, user_name);
            ps.setString(2, password);
            rs = ps.executeQuery();
            while (rs.next()) {
                User user = new User();
                user.setUserName(rs.getString("user_name"));
                user.setPassword(rs.getString("password"));
                user.setAddress(rs.getString("address"));
                user.setEmail(rs.getString("email"));
                user.setFullName(rs.getString("full_name"));
                user.setImage(rs.getString("image"));
                user.setMobile(rs.getString("mobile"));
                user.setGender(rs.getInt("gender") == 1);
                user.setRoleId(rs.getInt("role_id"));
                user.setStatus(rs.getInt("status") == 1);
                user.setUpdatedDate(rs.getDate("updated_date"));
                user.setUserId(rs.getInt("user_id"));
                return user;
            }
        } catch (Exception e) {
        }
        return null;
    }

    public String getPassWord(String user_name) {
        xSql = "select * from `user` "
                + "where user_name = ?";
        try {
            con = new DBContext().getConnection();
            ps = con.prepareStatement(xSql);
            ps.setString(1, user_name);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getString("password");
            }

        } catch (Exception e) {
        }
        return null;
    }
    
    public User getUserByUsername(String user_name) {
        xSql = "SELECT * FROM `user` "
                + "WHERE user_name = ? ";
       
        try {
            con = new DBContext().getConnection();//mo ket noi voi sql
            ps = con.prepareStatement(xSql);
            ps.setString(1, user_name);
            rs = ps.executeQuery();
            while (rs.next()) { 
               return new User (rs.getInt("role_id"),
                        rs.getString("user_name"),
                        rs.getString("password"), 
                        rs.getBoolean("status"),
                        rs.getString("email"),
                        rs.getString("full_name"),
                        rs.getBoolean("gender"),
                        rs.getString("mobile"),
                        rs.getString("address"),
                        rs.getString("image"),
                        rs.getDate("updated_date")
               );        
            }
        } catch (Exception e) {   
        }
        return null;
    }


    public void insertNewAccount(User user) {
        xSql = "insert into `user` (role_id, user_name, `password`, email, full_name, gender, mobile, address, image, updated_date, `code`)\n"
                + "values(1, ?, ?, ?, ?, ?, ?, ?, null, ?, ?)";
        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, user.getUserName());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getEmail());
            ps.setString(4, user.getFullName());
            ps.setBoolean(5, user.isGender());
            ps.setString(6, user.getMobile());
            ps.setString(7, user.getAddress());
            ps.setDate(8, user.getUpdatedDate());
            ps.setString(9, user.getCode());
            ps.executeUpdate();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean checkExistUsername(String user_name) {
        xSql = "select * from user where user_name = ?";
        String user = null;
        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, user_name);
            rs = ps.executeQuery();
            while (rs.next()) {
                user = rs.getString("user_name");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (user != null) {
            return true;
        } else {
            return false;
        }
    }

    public boolean checkExistEmail(String email) {
        xSql = "select * from user where email = ?";
        String mail = null;
        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, email);
            rs = ps.executeQuery();
            while (rs.next()) {
                mail = rs.getString("email");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (mail != null) {
            return true;
        } else {
            return false;
        }
    }

    public User searchAccountByEmail(String email) {
        User user = null;

        try {
            String query = "SELECT * FROM `user` WHERE email = ?";
            PreparedStatement statement = con.prepareStatement(query);

            statement.setString(1, email);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                user = new User();
                user.setUserName(resultSet.getString("Username"));
                user.setEmail(resultSet.getString("Email"));
                user.setPassword(resultSet.getString("Password"));
                user.setEmail(resultSet.getString("Email"));
            }

            resultSet.close();
            statement.close();
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return user;
    }

    public boolean updatePassword(String email, String newPassword) {
        try {
            String query = "UPDATE `user`\n"
                    + "SET `password` = ?\n"
                    + "WHERE `email` = ?";
            PreparedStatement statement = con.prepareStatement(query);

            statement.setString(1, newPassword);
            statement.setString(2, email);

            int rowsAffected = statement.executeUpdate();

            statement.close();

            // Check if the update was successful (1 row affected means success)
            return rowsAffected == 1;
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false; // Handle the error as needed
        }
    }

    public List<User> getNewlyRegisteredCustomers(Date fromDate, Date toDate) {
        List<User> customers = new ArrayList<>();
        xSql = "select * \n"
                + "from user \n"
                + "where role_id = 1\n"
                + "    and updated_date between '" + fromDate + "' and '" + toDate + "' \n"
                + "order by updated_date desc";
        System.out.println(xSql);
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                customers.add(new User(rs.getInt("user_id"),
                        rs.getInt("role_id"),
                        rs.getString("user_name"),
                        rs.getString("password"),
                        rs.getInt("status") == 1,
                        rs.getString("email"),
                        rs.getString("full_name"),
                        rs.getInt("gender") == 1,
                        rs.getString("mobile"),
                        rs.getString("address"),
                        rs.getString("image"),
                        rs.getDate("updated_date")
                )
                );
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return customers;
    }

    public LinkedHashMap<User, String[]> getNewlyBoughtCustomers(Date fromDate, Date toDate) {
        LinkedHashMap<User, String[]> customers = new LinkedHashMap<>();
        xSql = "select u.user_id, u.full_name, u.mobile, o.order_date, o.state,\n"
                + "	count(p.product_id) as total, \n"
                + "	sum(p.price*(1 - p.discount/100)*od.quantity) as total_amount\n"
                + "from `user` u join `order` o on u.user_id = o.user_id\n"
                + "	join `order_detail` od on od.order_id = o.order_id\n"
                + "    join `product` p on od.product_id = p.product_id\n"
                + "where o.order_date between '" + fromDate + "' and '" + toDate + "'\n"
                + "group by u.user_id, u.full_name, u.mobile, o.order_date, o.state\n"
                + "order by o.order_date desc";
        System.out.println(xSql);
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                User user = new User();
                user.setUserId(rs.getInt("user_id"));
                user.setFullName(rs.getString("full_name"));
                user.setMobile(rs.getString("mobile"));

                String[] orderInfo = new String[4];
                orderInfo[0] = rs.getDate("order_date").toString();
                orderInfo[1] = rs.getString("state");
                orderInfo[2] = rs.getString("total");
                orderInfo[3] = rs.getString("total_amount");

                customers.put(user, orderInfo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return customers;
    }

    public List<User> getAllUsers() {
        List<User> users = new ArrayList();
        xSql = "select * from user";
        System.out.println(xSql);
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                User user = new User(
                        rs.getInt("user_id"),
                        rs.getInt("role_id"),
                        rs.getString("user_name"),
                        rs.getString("password"),
                        rs.getInt("status") == 1,
                        rs.getString("email"),
                        rs.getString("full_name"),
                        rs.getInt("gender") == 1,
                        rs.getString("mobile"),
                        rs.getString("address"),
                        rs.getString("image"),
                        rs.getDate("updated_date")
                );
                users.add(user);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return users;
    }

    public List<User> getPaginatedUsers(List<User> list, int start, int end) {
        List<User> users = new ArrayList<>();
        for (int i = start; i < end; i++) {
            users.add(list.get(i));
        }
        return users;
    }

    public List<User> searchUsers(String keyword) {
        List<User> users = new ArrayList();
        xSql = "select * \n"
                + "from user \n"
                + "where full_name like '%" + keyword + "%'\n"
                + "	or email like '%" + keyword + "%'\n"
                + "    or mobile like '%" + keyword + "%'";
        System.out.println(xSql);
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                User user = new User(
                        rs.getInt("user_id"),
                        rs.getInt("role_id"),
                        rs.getString("user_name"),
                        rs.getString("password"),
                        rs.getInt("status") == 1,
                        rs.getString("email"),
                        rs.getString("full_name"),
                        rs.getInt("gender") == 1,
                        rs.getString("mobile"),
                        rs.getString("address"),
                        rs.getString("image"),
                        rs.getDate("updated_date")
                );
                users.add(user);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return users;
    }

    public List<User> filterUsers(String gender, String status, int[] roleId) {
        List<User> users = new ArrayList<>();
        xSql = "select * \n"
                + "from user \n"
                + "where true";
        if (gender != null) {
            xSql += " and gender = " + gender;
        }
        if (status != null) {
            xSql += " and status = " + status;
        }
        if (roleId != null) {
            xSql += " and role_id in(";
            for (int i = 0; i < roleId.length; i++) {
                xSql += roleId[i] + ", ";
            }
            if (xSql.endsWith(", ")) {
                xSql = xSql.substring(0, xSql.length() - 2);
            }
            xSql += ")";
        }
        System.out.println(xSql);
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                User user = new User(
                        rs.getInt("user_id"),
                        rs.getInt("role_id"),
                        rs.getString("user_name"),
                        rs.getString("password"),
                        rs.getInt("status") == 1,
                        rs.getString("email"),
                        rs.getString("full_name"),
                        rs.getInt("gender") == 1,
                        rs.getString("mobile"),
                        rs.getString("address"),
                        rs.getString("image"),
                        rs.getDate("updated_date")
                );
                users.add(user);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return users;
    }

    public boolean isValidUser(HttpServletRequest request, String username, String password) {
        try ( Connection connection = new DBContext().getConnection();  PreparedStatement preparedStatement = connection.prepareStatement("Select * from account where username = ? and password = ?")) {

            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);
            System.err.println(username + "|" + password);
            try ( ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    int userId = resultSet.getInt("user_id");

                    System.err.println(userId);
                    //Get role for the user 

                    RoleDAO roleDAO = new RoleDAO();
                    List<Role> roles = roleDAO.getRolesByUserId(userId);

                    HttpSession session = request.getSession();
                    session.setAttribute("username", username);
                    session.setAttribute("roles", roles);
                    return true;
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return false;

    }

    public User addNewUser(User user) {
        xSql = "INSERT INTO `online_shop`.`user` (`role_id`, `user_name`, `password`, "
                + "`status`, `email`, `full_name`, `gender`, `mobile`, `address`, `updated_date`, `code`) \n"
                + "VALUES (?,?,?,?,?,?,?,?,?,?,?);";

        System.out.println(xSql);
        try ( PreparedStatement ps = con.prepareStatement(xSql)) {
            ps.setInt(1, user.getRoleId());
            ps.setString(2, user.getUserName());
            ps.setString(3, user.getPassword());
            ps.setInt(4, user.isStatus() ? 1 : 0);
            ps.setString(5, user.getEmail());
            ps.setString(6, user.getFullName());
            ps.setInt(7, user.isGender() ? 1 : 0);
            ps.setString(8, user.getMobile());
            ps.setString(9, user.getAddress());
            ps.setDate(10, user.getUpdatedDate());
            ps.setString(11, user.getCode());

            int rowsAffected = ps.executeUpdate();

            if (rowsAffected > 0) {
                return user;
            }

        } catch (SQLException ex) {
            ex.printStackTrace(); // hoặc ghi nhật ký cho ngoại lệ
            // Xử lý ngoại lệ hoặc ném lại nếu cần
        }

        return null;
    }

    public User updateRoleAndStatus(User user) {
        String sql = "UPDATE online_shop.user set role_id = ?, status = ?   where user_id = ?;";
        try ( PreparedStatement ps = con.prepareStatement(sql);) {

            ps.setInt(1, user.getRoleId());
            ps.setInt(2, user.isStatus() ? 1 : 0);
            ps.setInt(3, user.getUserId());
            int re = ps.executeUpdate();
            if (re > 0) {
                return user;
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return null;
    }

    public User getUserByID(int userId) {
        User u = new User();
        xSql = "select *\n"
                + "from `user` where user_id = ?";

        try {
            ps = con.prepareStatement(xSql);
            ps.setInt(1, userId);
            rs = ps.executeQuery();
            while (rs.next()) {
                u.setUserId(rs.getInt("user_id"));
                u.setRoleId(rs.getInt("role_id"));
                u.setUserName(rs.getString("user_name"));
                u.setPassword(rs.getString("password"));
                u.setStatus(rs.getBoolean("status"));
                u.setEmail(rs.getString("email"));
                u.setFullName(rs.getString("full_name"));
                u.setGender(rs.getBoolean("gender"));
                u.setMobile(rs.getString("mobile"));
                u.setAddress(rs.getString("address"));
                u.setImage(rs.getString("image"));
                u.setUpdatedDate(rs.getDate("updated_date"));
                u.setCode(rs.getString("code"));
            }
        } catch (Exception e) {
            System.out.println(e);

        }
        return u;
    }
    
    public List<User> getAllCustomers() {
        List<User> users = new ArrayList();
        xSql = "SELECT * FROM online_shop.user where role_id=1;";

        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                User user = new User(
                        rs.getInt("user_id"),
                        rs.getInt("role_id"),
                        rs.getString("user_name"),
                        rs.getString("password"),
                        rs.getInt("status") == 1,
                        rs.getString("email"),
                        rs.getString("full_name"),
                        rs.getInt("gender") == 1,
                        rs.getString("mobile"),
                        rs.getString("address"),
                        rs.getString("image"),
                        rs.getDate("updated_date")
                );
                users.add(user);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return users;
    }
    
    public User updateCustomer(User user) {
        String sql = "UPDATE `online_shop`.`user` SET `role_id` = 1, `status` = ?, `email` = ?, `full_name` = ?,"
                + "           `gender` = ?, `mobile` = ?, `address` = ? WHERE (`user_id` = ?);";
        try ( PreparedStatement ps = con.prepareStatement(sql);) {

            ps.setInt(1, user.isStatus() ? 1 : 0);
            ps.setString(2, user.getEmail());
            ps.setString(3, user.getFullName());
            ps.setInt(4, user.isGender() ? 1 : 0);
            ps.setString(5, user.getMobile());
            ps.setString(6, user.getAddress());
            ps.setInt(7, user.getUserId());
            int re = ps.executeUpdate();
            if (re > 0) {
                return user;
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return null;
    }
    
    public boolean checkExistMobile(String mobile) {
        xSql = "select * from user where mobile = ?";
        String mail = null;
        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, mobile);
            rs = ps.executeQuery();
            while (rs.next()) {
                mail = rs.getString("email");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (mail != null) {
            return true;
        } else {
            return false;
        }
    }
    
    public User addNewCustmer(User user) {
        xSql = "INSERT INTO `online_shop`.`user` (`role_id`, `user_name`, `password`, "
                + "`status`, `email`, `full_name`, `gender`, `mobile`, `address`, `updated_date`, `code`) \n"
                + "VALUES (1,?,?,?,?,?,?,?,?,?,?);";

        System.out.println(xSql);
        try ( PreparedStatement ps = con.prepareStatement(xSql)) {

            ps.setString(1, user.getUserName());
            ps.setString(2, user.getPassword());
            ps.setInt(3, user.isStatus() ? 1 : 0);
            ps.setString(4, user.getEmail());
            ps.setString(5, user.getFullName());
            ps.setInt(6, user.isGender() ? 1 : 0);
            ps.setString(7, user.getMobile());
            ps.setString(8, user.getAddress());
            ps.setDate(9, user.getUpdatedDate());
            ps.setString(10, user.getCode());

            int rowsAffected = ps.executeUpdate();

            if (rowsAffected > 0) {
                return user;
            }

        } catch (SQLException ex) {
            ex.printStackTrace(); // hoặc ghi nhật ký cho ngoại lệ
            // Xử lý ngoại lệ hoặc ném lại nếu cần
        }

        return null;
    }
    
    public List<User> getUsersByRoleId(int id) {
        List<User> users = new ArrayList<>();
        xSql = "select * from user where role_id = " + id;
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while(rs.next()) {
                User user = new User(
                        rs.getInt("user_id"),
                        rs.getInt("role_id"),
                        rs.getString("user_name"),
                        rs.getString("password"),
                        rs.getInt("status") == 1,
                        rs.getString("email"),
                        rs.getString("full_name"),
                        rs.getInt("gender") == 1,
                        rs.getString("mobile"),
                        rs.getString("address"),
                        rs.getString("image"),
                        rs.getDate("updated_date")
                );
                users.add(user);
            }
        } catch (Exception e) {
            e.printStackTrace(); 
        }
        return users;
    }


    public Boolean checkStatus(String user_name) {
        try {
            xSql = "SELECT `status` FROM `user` WHERE `user_name` = ?";
            ps = con.prepareStatement(xSql);
            ps.setString(1, user_name);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("status") == 1;
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null; // or handle the absence of the user based on your application's logic
    }
    
    public boolean update(User user) {
        String query = "UPDATE user SET role_id=1, full_name = ?, status = ?, gender = ?,mobile = ?, image= ? WHERE user_id = ?";
        try {
            ps = con.prepareStatement(query);
            ps.setString(1, user.getFullName());
            ps.setInt(2, user.isStatus() ? 1 : 0);
            ps.setInt(3, user.isGender() ? 1 : 0);
            //  ps.setInt(4, slider.getSliderId());
            ps.setString(4, user.getMobile());
            ps.setString(5,user.getImage());
            ps.setInt(6, user.getUserId());
            int rowsAffected = ps.executeUpdate();
            return rowsAffected > 0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}
