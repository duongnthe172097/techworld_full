package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import model.CartItem;

public class CartItemDAO extends DBContext {

    public List<CartItem> getAllCartItems() {
        List<CartItem> cartItemList = new ArrayList<>();

        try {
            String query = "SELECT * FROM cart_item";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                CartItem cartItem = new CartItem();
                cartItem.setCartId(resultSet.getInt("cart_id"));
                cartItem.setProductId(resultSet.getInt("product_id"));
                cartItem.setQuantity(resultSet.getInt("quantity"));
                cartItemList.add(cartItem);
            }

        } catch (SQLException e) {
            System.out.println("getAllCartItems: " + e.getMessage());
        }

        return cartItemList;
    }

    public CartItem getCartItemById(int cartId) {
        CartItem cartItem = null;

        try {
            String query = "SELECT * FROM cart_item WHERE cart_id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, cartId);

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                cartItem = new CartItem();
                cartItem.setCartId(resultSet.getInt("cart_id"));
                cartItem.setProductId(resultSet.getInt("product_id"));
                cartItem.setQuantity(resultSet.getInt("quantity"));
            }

        } catch (SQLException e) {
            System.out.println("getCartItemById: " + e.getMessage());
        }

        return cartItem;
    }

    public boolean addCartItem(CartItem cartItem) {
        try {
            String query = "INSERT INTO cart_item (cart_id, product_id, quantity) VALUES (?, ?, ?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);

            preparedStatement.setInt(1, cartItem.getCartId());
            preparedStatement.setInt(2, cartItem.getProductId());
            preparedStatement.setInt(3, cartItem.getQuantity());

            int rowsAffected = preparedStatement.executeUpdate();
            return rowsAffected > 0;

        } catch (SQLException e) {
            System.out.println("addCartItem: " + e.getMessage());
            return false;
        }
    }

    public boolean updateCartItem(CartItem cartItem) {
        try {
            String query = "UPDATE cart_item SET quantity=? WHERE product_id=? AND cart_id=?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);

            preparedStatement.setInt(1, cartItem.getQuantity());
            preparedStatement.setInt(2, cartItem.getProductId());
            preparedStatement.setInt(3, cartItem.getCartId());

            int rowsAffected = preparedStatement.executeUpdate();
            return rowsAffected > 0;

        } catch (SQLException e) {
            System.out.println("updateCartItem: " + e.getMessage());
            return false;
        }
    }

    public boolean deleteCartItem(int cartId, int productId) {
        try {
            String query = "DELETE FROM cart_item WHERE cart_id=? AND product_id=?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);

            preparedStatement.setInt(1, cartId);
            preparedStatement.setInt(2, productId);

            int rowsAffected = preparedStatement.executeUpdate();
            return rowsAffected > 0;

        } catch (SQLException e) {
            System.out.println("deleteCartItem: " + e.getMessage());
            return false;
        }
    }

    public List<CartItem> getCartItemsByCartId(int cartId) {
        List<CartItem> cartItemList = new ArrayList<>();

        try {
            String query = "SELECT * FROM cart_item WHERE cart_id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, cartId);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                CartItem cartItem = new CartItem();
                cartItem.setCartId(resultSet.getInt("cart_id"));
                cartItem.setProductId(resultSet.getInt("product_id"));
                cartItem.setQuantity(resultSet.getInt("quantity"));
                cartItemList.add(cartItem);
            }

        } catch (SQLException e) {
            System.out.println("getCartItemsByCartId: " + e.getMessage());
        }

        return cartItemList;
    }

    public boolean deleteCartItemsByCartId(int cartId) {
        try {
            String query = "DELETE FROM cart_item WHERE cart_id=?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);

            preparedStatement.setInt(1, cartId);

            int rowsAffected = preparedStatement.executeUpdate();
            return rowsAffected > 0;

        } catch (SQLException e) {
            System.out.println("deleteCartItemsByCartId: " + e.getMessage());
            return false;
        }
    }

    public CartItem getCartItemByCartIdAndProductId(int cartId, int productId) {
        CartItem cartItem = null;

        try {
            String query = "SELECT * FROM cart_item WHERE cart_id = ? AND product_id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, cartId);
            preparedStatement.setInt(2, productId);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                cartItem = new CartItem();
                cartItem.setCartId(resultSet.getInt("cart_id"));
                cartItem.setProductId(resultSet.getInt("product_id"));
                cartItem.setQuantity(resultSet.getInt("quantity"));
            }

        } catch (SQLException e) {
            System.out.println("getCartItemByCartIdAndProductId: " + e.getMessage());
        }

        return cartItem;
    }

    public List<CartItem> getCartItemsByUserId(int userId) {
        List<CartItem> cartItems = new ArrayList<>();
        try {
          String sql = "SELECT product_id, quantity FROM cart_item WHERE cart_id = (SELECT cart_id FROM cart WHERE user_id = ?)";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, userId);
            ResultSet rs = preparedStatement.executeQuery();
             while (rs.next()) {
                int productId = rs.getInt("product_id");
                int quantity = rs.getInt("quantity");
                cartItems.add(new CartItem(productId, quantity));
            }
    } catch (SQLException e) {
        e.printStackTrace();
         }return cartItems;
    }
    public static void main(String[] args) {
       CartItemDAO cartItemDAO = new CartItemDAO();
        int userId = 1; // Thay thế bằng user_id của người dùng bạn muốn lấy giỏ hàng của họ
        List<CartItem> cartItems = cartItemDAO.getCartItemsByUserId(31);
        for (CartItem item : cartItems) {
            System.out.println("Product ID: " + item.getProductId() + ", Quantity: " + item.getQuantity());
        }
    }
    }

