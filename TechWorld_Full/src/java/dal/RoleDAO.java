/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Role;

/**
 *
 * @author admin
 */
public class RoleDAO extends DAO{
    
    public List<Role> getAllRoles() {
        List<Role> roles = new ArrayList<>();
        xSql = "select * from role";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while(rs.next()) {
                Role role = new Role(rs.getInt("role_id"), rs.getString("role_name"));
                roles.add(role);
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return roles;
    }
    
    public String getRoleNameById(int id) {
        String role = "";
        xSql = "select role_name from role where role_id = " + id;
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            if(rs.next()) {
                role = rs.getString("role_name");
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return role;
    }
    
    public Role getRoleById(int id) {
        Role role = new Role();
        xSql = "select role_name from role where role_id = " + id;
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            if(rs.next()) {
                role = new Role(rs.getInt("role_id"), rs.getString("role_name"));
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return role;
    }
    
    public List<Role> getRolesByUserId(int userId) {
        List<Role> roles = new ArrayList<>();

        try (Connection connection = new DBContext().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("Select r.role_id, r.role_name from `account` a join `user` u on a.user_id = u.user_id " +
                     "join `role` r on r.role_id = u.role_id where a.user_id = ?");
            ) {

            preparedStatement.setInt(1, userId);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    Role role = new Role();
                    role.setRoleId(resultSet.getInt("role_id"));
                    role.setRoleName(resultSet.getString("role_name"));
                    roles.add(role);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return roles;
    }
}
