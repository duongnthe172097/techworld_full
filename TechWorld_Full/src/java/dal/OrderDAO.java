/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Order;
import model.OrderDetail;
import model.OrderView;
import model.ProductImageAndName;

/**
 *
 * @author admin
 */
public class OrderDAO extends DAO {

    public int[] getNumberOfNewlyOrdersInTime(Date fromDate, Date toDate) {
        int[] orderNumber = new int[3];
        xSql = "SELECT\n"
                + "  (SELECT COUNT(*) FROM `order` WHERE state = 1 AND order_date BETWEEN '" + fromDate + "' AND '" + toDate + "') AS success,\n"
                + "  (SELECT COUNT(*) FROM `order` WHERE state = 2 AND order_date BETWEEN '" + fromDate + "' AND '" + toDate + "') AS cancelled,\n"
                + "  (SELECT COUNT(*) FROM `order` WHERE state = 3 AND order_date BETWEEN '" + fromDate + "' AND '" + toDate + "') AS submitted";
        System.out.println(xSql);
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            if (rs.next()) {
                orderNumber[0] = rs.getInt("success");
                orderNumber[1] = rs.getInt("cancelled");
                orderNumber[2] = rs.getInt("submitted");
            }
        } catch (Exception e) {
        }
        return orderNumber;
    }

    public List<OrderView> orderView(int userId, int index) {
        List<OrderView> list = new ArrayList<>();
        ProductImageAndNameDAO pDAO = new ProductImageAndNameDAO();
        int i = (index - 1) * 3;
        xSql = " select `order`.`order_id`,`order`.`order_date`, count(*) as `Number of Product`,sum(`product`.`price`*(100-`product`.`discount`)/100*`order_detail`.`quantity`) as `Total`,`order`.`state`\n"
                + " from `order` join `order_detail` on `order`.`order_id`= `order_detail`.`order_id` and `order`.`user_id`=?\n"
                + " join `product` on `product`.`product_id`=`order_detail`.`product_id`\n"
                + " group by `order`.`order_id`,`order`.`order_date`,`order`.`state`\n"
                + " order by `order`.`order_id` desc\n"
                + "limit 3 offset ?";

        try {
            ps = con.prepareStatement(xSql);
            ps.setInt(1, userId);
            ps.setInt(2, i);
            rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("order_id");
                Date date = rs.getDate("order_date");
                int number = rs.getInt("Number of Product");
                int total = rs.getInt("Total");
                int state = rs.getInt("state");
                ProductImageAndName p = pDAO.getProductImageByOrderId(id);
                list.add(new OrderView(id, date, number, total, state, p));
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public int orderTotal(int userId) {
        xSql = "select count(*) as 'Total' from order where user_id = ?";

        try {
            ps = con.prepareStatement(xSql);
            ps.setInt(1, userId);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt("Total");
            }

            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public List<Order> getAllOrders() {
        List<Order> orderList = new ArrayList<>();

        try {
            String query = "SELECT * FROM `order`";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Order order = new Order();
                order.setOrderId(resultSet.getInt("order_id"));
                order.setOrderDate(resultSet.getDate("order_date"));
                order.setDeliveryId(resultSet.getInt("delivery_id"));
                order.setUserId(resultSet.getInt("user_id"));
                order.setState(resultSet.getInt("state"));
                orderList.add(order);
            }

        } catch (SQLException e) {
            System.out.println("getAllOrders: " + e.getMessage());
        }

        return orderList;
    }

    public List<Order> getAllOrders(int pageNumber, int pageSize) {
        List<Order> orderList = new ArrayList<>();

        try {
            // Calculate the offset based on page number and page size
            int offset = (pageNumber - 1) * pageSize;

            String query = "SELECT * FROM `order` LIMIT ? OFFSET ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);

            // Set parameters for limit and offset
            preparedStatement.setInt(1, pageSize);
            preparedStatement.setInt(2, offset);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Order order = new Order();
                order.setOrderId(resultSet.getInt("order_id"));
                order.setOrderDate(resultSet.getDate("order_date"));
                order.setDeliveryId(resultSet.getInt("delivery_id"));
                order.setUserId(resultSet.getInt("user_id"));
                order.setState(resultSet.getInt("state"));
                orderList.add(order);
            }

        } catch (SQLException e) {
            System.out.println("getAllOrders: " + e.getMessage());
        }

        return orderList;
    }

    public Order getOrderById(int orderId) {
        Order order = null;

        try {
            String query = "SELECT * FROM `order` WHERE order_id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, orderId);

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                order = new Order();
                order.setOrderId(resultSet.getInt("order_id"));
                order.setOrderDate(resultSet.getDate("order_date"));
                order.setDeliveryId(resultSet.getInt("delivery_id"));
                order.setUserId(resultSet.getInt("user_id"));
                order.setState(resultSet.getInt("state"));
            }

        } catch (SQLException e) {
            System.out.println("getOrderById: " + e.getMessage());
        }

        return order;
    }

    public List<Order> getOrdersByUserId(int userId) {
        List<Order> orderList = new ArrayList<>();

        try {
            String query = "SELECT * FROM `order` WHERE user_id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, userId);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Order order = new Order();
                order.setOrderId(resultSet.getInt("order_id"));
                order.setOrderDate(resultSet.getDate("order_date"));
                order.setDeliveryId(resultSet.getInt("delivery_id"));
                order.setUserId(resultSet.getInt("user_id"));
                order.setState(resultSet.getInt("state"));
                orderList.add(order);
            }

        } catch (SQLException e) {
            System.out.println("getOrdersByUserId: " + e.getMessage());
        }

        return orderList;
    }

    public Order addOrder(Order order) {
        try {
            String query = "INSERT INTO `order` (order_date, delivery_id, user_id, state) "
                    + "VALUES (?, ?, ?, ?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);

            preparedStatement.setDate(1, new java.sql.Date(order.getOrderDate().getTime()));
            preparedStatement.setInt(2, order.getDeliveryId());
            preparedStatement.setInt(3, order.getUserId());
            preparedStatement.setInt(4, order.getState());

            int rowsAffected = preparedStatement.executeUpdate();
            if (rowsAffected > 0) {
                ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
                if (generatedKeys.next()) {
                    order.setOrderId(generatedKeys.getInt(1));
                }
            }

        } catch (SQLException e) {
            System.out.println("addOrder: " + e.getMessage());
        }

        return order;
    }
      public OrderDetail addOrderDetail(OrderDetail orderDetail) {
        String sql = "insert into orderdetails(`order_id`, `product_id`, `quantity`)\n"
                + "values  (?, ?, ?, ?)";
        try ( PreparedStatement ps = con.prepareStatement(sql)) {
            ps.setInt(1, orderDetail.getOrderId());
            ps.setInt(2, orderDetail.getProductId());
            ps.setDouble(3, orderDetail.getQuantity());
         
            int re = ps.executeUpdate();
            if (re > 0) {
                return orderDetail;
            }
        } catch (SQLException ex) {
            Logger.getLogger(OrderDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public boolean updateOrder(Order order) {
        try {
            String query = "UPDATE `order` SET order_date=?, delivery_id=?, user_id=?, state=? "
                    + "WHERE order_id=?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);

            preparedStatement.setDate(1, new java.sql.Date(order.getOrderDate().getTime()));
            preparedStatement.setInt(2, order.getDeliveryId());
            preparedStatement.setInt(3, order.getUserId());
            preparedStatement.setInt(4, order.getState());
            preparedStatement.setInt(5, order.getOrderId());

            int rowsAffected = preparedStatement.executeUpdate();
            return rowsAffected > 0;

        } catch (SQLException e) {
            System.out.println("updateOrder: " + e.getMessage());
            return false;
        }
    }

    public boolean deleteOrder(int orderId) {
        try {
            String query = "DELETE FROM `order` WHERE order_id=?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);

            preparedStatement.setInt(1, orderId);

            int rowsAffected = preparedStatement.executeUpdate();
            return rowsAffected > 0;

        } catch (SQLException e) {
            System.out.println("deleteOrder: " + e.getMessage());
            return false;
        }
    }

    public List<Order> getOrdersByUserId(int userId, int pageSize, int pageNumber) {
        List<Order> orderList = new ArrayList<>();

        try {
            String query = "SELECT * FROM `order` WHERE user_id = ? LIMIT ? OFFSET ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, userId);
            preparedStatement.setInt(2, pageSize);
            preparedStatement.setInt(3, (pageNumber - 1) * pageSize); // Adjusted offset calculation
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Order order = new Order();
                order.setOrderId(resultSet.getInt("order_id"));
                order.setOrderDate(resultSet.getDate("order_date"));
                order.setDeliveryId(resultSet.getInt("delivery_id"));
                order.setUserId(resultSet.getInt("user_id"));
                order.setState(resultSet.getInt("state"));
                orderList.add(order);
            }

        } catch (SQLException e) {
            System.out.println("getOrdersByUserId: " + e.getMessage());
        }

        return orderList;
    }


    public int[] getTotalOfOrderById(int id) {
        int[] total = new int[2];
        xSql = "select count(od.product_id) as totalNumber, sum(p.price * (1 - (p.discount/100))) as totalAmount\n"
                + "from  `order` o join `order_detail` od on o.order_id = od.order_id\n"
                + "	join product p on od.product_id = p.product_id\n"
                + "where o.order_id = ?\n"
                + "group by o.order_id";
        try {
            ps = con.prepareStatement(xSql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            if (rs.next()) {
                total[0] = rs.getInt("totalNumber");
                total[1] = rs.getInt("totalAmount");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return total;
    }

    public List<Order> getOrdersInTime(Date fromDate, Date toDate) {
        List<Order> orderList = new ArrayList<>();

        try {
            String query = "SELECT * FROM `order` where order_date between '" + fromDate + "' and '" + toDate + "'";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Order order = new Order();
                order.setOrderId(resultSet.getInt("order_id"));
                order.setOrderDate(resultSet.getDate("order_date"));
                order.setDeliveryId(resultSet.getInt("delivery_id"));
                order.setUserId(resultSet.getInt("user_id"));
                order.setState(resultSet.getInt("state"));
                order.setSaleId(resultSet.getInt("sale_id"));
                order.setSaleNote(resultSet.getString("sale_note"));
                orderList.add(order);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return orderList;
    }

    public List<Order> getOrdersInTimeBySaleId(Date fromDate, Date toDate, int saleId) {
        List<Order> orderList = new ArrayList<>();

        try {
            String query = "SELECT * FROM `order` where sale_id =" + saleId + " and order_date between '" + fromDate + "' and '" + toDate + "'";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Order order = new Order();
                order.setOrderId(resultSet.getInt("order_id"));
                order.setOrderDate(resultSet.getDate("order_date"));
                order.setDeliveryId(resultSet.getInt("delivery_id"));
                order.setUserId(resultSet.getInt("user_id"));
                order.setState(resultSet.getInt("state"));
                order.setSaleId(resultSet.getInt("sale_id"));
                order.setSaleNote(resultSet.getString("sale_note"));
                orderList.add(order);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return orderList;
    }

    public List<Double[]> getStatisticRevenuesOfSalesInTime(Date fromDate, Date toDate) {
        List<Double[]> results = new ArrayList<>();
        xSql = "SELECT\n"
                + "    o.sale_id,\n"
                + "    COUNT(DISTINCT o.order_id) AS numberOfOrder,\n"
                + "    COUNT(od.product_id) AS numberOfProduct,\n"
                + "    SUM(p.price * od.quantity * (1 - p.discount/100)) AS totalAmount\n"
                + "FROM\n"
                + "    `order` o\n"
                + "JOIN\n"
                + "    `order_detail` od ON o.order_id = od.order_id\n"
                + "JOIN\n"
                + "    product p ON od.product_id = p.product_id\n"
                + "WHERE\n"
                + "	o.order_date BETWEEN '" + fromDate + "' AND '" + toDate + "'\n"
                + "     AND o.state = 1\n"
                + "GROUP BY\n"
                + "    o.sale_id;";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Double[] result = new Double[4];
                result[0] = rs.getDouble("sale_id");
                result[1] = rs.getDouble("numberOfOrder");
                result[2] = rs.getDouble("numberOfProduct");
                result[3] = rs.getDouble("totalAmount");
                results.add(result);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return results;
    }

    public Order getOrderForSaleManagerByOrderId(int id) {
        Order order = null;

        try {
            String query = "SELECT * FROM `order` WHERE order_id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, id);

            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                order = new Order();
                order.setOrderId(resultSet.getInt("order_id"));
                order.setOrderDate(resultSet.getDate("order_date"));
                order.setDeliveryId(resultSet.getInt("delivery_id"));
                order.setUserId(resultSet.getInt("user_id"));
                order.setState(resultSet.getInt("state"));
                order.setSaleId(resultSet.getInt("sale_id"));
                order.setSaleNote(resultSet.getString("sale_note"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return order;
    }

    public List<OrderDetail> getOrderDetailsByOrderId(int id) {
        List<OrderDetail> orderDetails = new ArrayList<>();
        xSql = "select * from `order_detail` where order_id = " + id;
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                OrderDetail orderDetail = new OrderDetail();
                orderDetail.setOrderId(rs.getInt("order_id"));
                orderDetail.setProductId(rs.getInt("product_id"));
                orderDetail.setQuantity(rs.getInt("quantity"));
                orderDetails.add(orderDetail);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return orderDetails;
    }

    public int updateStateAndSaleIdAndSaleNote(int state, int saleId, int orderId, String saleNote) {
        xSql = "update `order`\n"
                + "set state = ?, sale_id = ?, sale_note = ?\n"
                + "where order_id = ?";
        try {
            ps = con.prepareStatement(xSql);
            ps.setInt(1, state);
            ps.setInt(2, saleId);
            ps.setString(3, saleNote);
            ps.setInt(4, orderId);
            return ps.executeUpdate();
        } catch(Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
    
    public int updateStateAndSaleNote(int state, int orderId, String saleNote) {
        xSql = "update `order`\n"
                + "set state = ?, sale_note = ?\n"
                + "where order_id = ?";
        try {
            ps = con.prepareStatement(xSql);
            ps.setInt(1, state);
            ps.setString(2, saleNote);
            ps.setInt(3, orderId);
            return ps.executeUpdate();
        } catch(Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

}

