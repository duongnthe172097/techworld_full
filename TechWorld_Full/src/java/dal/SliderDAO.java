/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Setting;
import model.Slider;

/**
 *
 * @author admin
 */
public class SliderDAO extends DAO {

    public Slider getSliderByID(int sliderId) {
        Slider slider = new Slider();
        xSql = "SELECT * FROM online_shop.slider where slider_id = ?";

        try {
            ps = con.prepareStatement(xSql);
            ps.setInt(1, sliderId);
            rs = ps.executeQuery();
            while (rs.next()) {
                slider.setSliderId(rs.getInt("slider_id"));
                slider.setTitle(rs.getString("title"));
                slider.setImage(rs.getString("image"));
                slider.setStatus(rs.getBoolean("status"));
                slider.setNote(rs.getString("notes"));
                slider.setUserId(rs.getInt("user_id"));
                slider.setProductId(rs.getInt("product_id"));
            }
        } catch (Exception e) {
            System.out.println(e);

        }

        return slider;
    }

    public void changeStatusOfSliderById(int id, boolean status) {
        int newStatus = status ? 0 : 1;
        xSql = "update slider set status = ? where slider_id = ?";
        try {
            ps = con.prepareStatement(xSql);
            ps.setInt(1, newStatus);
            ps.setInt(2, id);
            ps.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public boolean update(Slider slider) {
        String query = "UPDATE slider SET title = ?, notes = ?, status = ?, image= ? WHERE slider_id = ?";
        try {
            ps = con.prepareStatement(query);
            ps.setString(1, slider.getTitle());
            ps.setString(2, slider.getNote());
            ps.setInt(3, slider.isStatus() ? 1 : 0);
            //  ps.setInt(4, slider.getSliderId());
            ps.setString(4, slider.getImage());
            ps.setInt(5, slider.getSliderId());
            int rowsAffected = ps.executeUpdate();
            return rowsAffected > 0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean isIdExisted(int sliderId) {
        String sql = "select slider_id from slider where slider_id = ?";
        try ( PreparedStatement ps = con.prepareStatement(sql);) {
            ps.setInt(1, sliderId);
            try ( ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return true;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(SliderDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public Slider updateSliders(int oldId, Slider slider) {
        String sql = "update slider set `title` = ?, `status` = ?, `notes` = ? where `slider_id` = ?;";
        try ( PreparedStatement ps = con.prepareStatement(sql);) {
            ps.setString(1, slider.getTitle());

            ps.setBoolean(2, slider.isStatus());
            ps.setString(3, slider.getNote());
            ps.setInt(4, oldId);
            int re = ps.executeUpdate();
            if (re > 0) {
                return slider;
            }
        } catch (SQLException ex) {
            Logger.getLogger(SliderDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    // Phương thức này cập nhật thông tin slider và trả về slider đã được cập nhật
    public Slider updateSlider(Slider slider) {
        xSql = "UPDATE online_shop.slider SET image=?, title=?, status=?, notes=? WHERE slider_id=?";
        try {
            PreparedStatement ps = con.prepareStatement(xSql);
            ps.setString(1, slider.getImage());
            ps.setString(2, slider.getTitle());
            ps.setInt(3, slider.isStatus() ? 1 : 0);
            ps.setString(4, slider.getNote());
            ps.setInt(5, slider.getSliderId());

            int rowsAffected = ps.executeUpdate();
            if (rowsAffected > 0) {
                System.out.println("Slider updated successfully!");
                return slider;
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return slider;
    }

    public List<Slider> getAllSliders() {
        List<Slider> sliders = new ArrayList<>();
        xSql = "select * from slider";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            int sliderId;
            String title, image;
            boolean status;
            String note;
            int userId, productId;
            Slider slider;
            while (rs.next()) {
                sliderId = rs.getInt("slider_id");
                title = rs.getString("title");
                image = rs.getString("image");
                status = rs.getInt("status") == 1;
                note = rs.getString("notes");
                userId = rs.getInt("user_id");
                productId = rs.getInt("product_id");
                slider = new Slider(sliderId, title, image, status, note, userId, productId);
                sliders.add(slider);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sliders;
    }

    public int updateImage(int userId, String image) {
        xSql = "update `user` set image = ? where user_id = ?";
        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, image);
            ps.setInt(2, userId);
            int rowsAffected = ps.executeUpdate();
            return rowsAffected;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public List<Slider> getActiveSliders() {
        List<Slider> sliders = new ArrayList<>();
        xSql = "select * from slider where status = 1";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            int sliderId;
            String title, image;
            boolean status;
            String note;
            int userId, productId;
            Slider slider;
            while (rs.next()) {
                sliderId = rs.getInt("slider_id");
                title = rs.getString("title");
                image = rs.getString("image");
                status = rs.getInt("status") == 1;
                note = rs.getString("notes");
                userId = rs.getInt("user_id");
                productId = rs.getInt("product_id");
                slider = new Slider(sliderId, title, image, status, note, userId, productId);
                sliders.add(slider);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sliders;
    }

    public List<Slider> getInActiveSliders() {
        List<Slider> sliders = new ArrayList<>();
        xSql = "select * from slider where status = 0";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            int sliderId;
            String title, image;
            boolean status;
            String note;
            int userId, productId;
            Slider slider;
            while (rs.next()) {
                sliderId = rs.getInt("slider_id");
                title = rs.getString("title");
                image = rs.getString("image");
                status = rs.getInt("status") == 1;
                note = rs.getString("notes");
                userId = rs.getInt("user_id");
                productId = rs.getInt("product_id");
                slider = new Slider(sliderId, title, image, status, note, userId, productId);
                sliders.add(slider);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sliders;
    }

    public List<Slider> filterSliders(boolean[] statusArray, int userId, int productId) {
        List<Slider> sliders = new ArrayList<>();
        xSql = "SELECT * FROM slider WHERE true";

        if (statusArray != null) {
            xSql += " AND status IN (";
            for (int i = 0; i < statusArray.length; i++) {
                xSql += statusArray[i] + ",";
            }
            if (xSql.endsWith(",")) {
                xSql = xSql.substring(0, xSql.length() - 1);
            }
            xSql += ")";
        }

        if (userId > 0) {
            xSql += " AND user_id = " + userId;
        }

        if (productId > 0) {
            xSql += " AND product_id = " + productId;
        }

        xSql += " ORDER BY user_id, product_id, slider_id";

        System.out.println(xSql);

        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();

            while (rs.next()) {
                int sliderId = rs.getInt("slider_id");
                String title = rs.getString("title");
                String image = rs.getString("image");
                boolean sliderStatus = rs.getBoolean("status");
                String notes = rs.getString("notes");
                int sliderUserId = rs.getInt("user_id");
                int sliderProductId = rs.getInt("product_id");

                Slider slider = new Slider(sliderId, title, image, sliderStatus, notes, sliderUserId, sliderProductId);
                sliders.add(slider);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return sliders;
    }

    public List<Slider> searchSliders(String keyword) {
        List<Slider> sliders = new ArrayList<>();
        String xSql = "SELECT * FROM online_shop.slider WHERE title LIKE '%" + keyword
                + "%' ";

        try {
            PreparedStatement ps = con.prepareStatement(xSql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Slider slider = new Slider(
                        rs.getInt("slider_id"),
                        rs.getString("title"),
                        rs.getString("image"),
                        rs.getInt("status") == 1,
                        rs.getString("notes"),
                        rs.getInt("user_id"),
                        rs.getInt("product_id")
                );
                sliders.add(slider);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sliders;
    }

    public List<Slider> getPaginatedSlider(List<Slider> list, int start, int end) {
        List<Slider> sliders = new ArrayList<>();
        for (int i = start; i < end; i++) {
            sliders.add(list.get(i));
        }

        return sliders;
    }

}
