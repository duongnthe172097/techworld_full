/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Date;
import model.ProductImageAndName;

/**
 *
 * @author Admin
 */
public class ProductImageAndNameDAO extends DAO {

    public ProductImageAndName getProductImageByOrderId(int id) {
        ProductImageAndName product = new ProductImageAndName();
        xSql = "select product.product_id,product.product_name,product_image.image_url \n"
                + "from product join product_image on product.product_id = product_image.product_id\n"
                + "join order_detail on product.product_id=order_detail.product_id and order_detail.order_id=?\n"
                + "limit 1";
        try {
            ps = con.prepareStatement(xSql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            int productId;
            String img;
            String name;
            if (rs.next()) {
                productId = rs.getInt("product_id");
                name = rs.getString("product_name");
                img = rs.getString("image_url");
                product = new ProductImageAndName(productId, name, img);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return product;
    }
    public static void main(String[] args) {
        ProductImageAndNameDAO dao = new ProductImageAndNameDAO();
        System.out.println(dao.getProductImageByOrderId(1));
    }
}
