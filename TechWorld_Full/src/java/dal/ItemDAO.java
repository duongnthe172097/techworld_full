/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.util.ArrayList;
import java.util.List;
import model.Item;

/**
 *
 * @author HP
 */
public class ItemDAO extends DAO {

    public List<Item> getAllItems(int userId) {
    List<Item> items = new ArrayList<>();
    String sql = "SELECT product.product_id, product_image.image_url, product.price, cart_item.quantity " +
                 "FROM cart " +
                 "JOIN cart_item ON cart.cart_id = cart_item.cart_id AND cart.user_id = ? " +
                 "JOIN product ON cart_item.product_id = product.product_id " +
                 "JOIN product_image ON product.product_id = product_image.product_id";
    try {
        ps = con.prepareStatement(sql);
        ps.setInt(1, userId);
        rs = ps.executeQuery();
        while (rs.next()) {
            int productId = rs.getInt("product_id");
            String imageUrl = rs.getString("image_url");
            double price = rs.getDouble("price");
            int quantity = rs.getInt("quantity");
            Item item = new Item(productId, imageUrl, price, quantity);
            items.add(item);
        }
    } catch (Exception e) {
        e.printStackTrace();
    }
    return items;
}

}
