/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import model.Post;

/**
 *
 * @author admin
 */
public class PostDAO extends DAO {

    public List<Post> getAllPosts() {
        List<Post> posts = new ArrayList<>();
        xSql = "select * from post";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            int postId;
            String title;
            String briefInfo;
            String content;
            Date updatedDate;
            boolean status;
            int userId;
            Post post;
            while (rs.next()) {
                postId = rs.getInt("post_id");
                title = rs.getString("title");
                briefInfo = rs.getString("brief_info");
                content = rs.getString("content");
                updatedDate = rs.getDate("updated_date");
                status = rs.getInt("status") == 1;
                userId = rs.getInt("user_id");
                post = new Post(postId, title, briefInfo, content, updatedDate, status, userId);
                posts.add(post);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Collections.sort(posts, new Comparator<Post>() {
            @Override
            public int compare(Post o1, Post o2) {
                return o2.getUpdatedDate().compareTo(o1.getUpdatedDate());
            }
        });
        return posts;
    }

    public List<Post> getAllFullPosts() {
        List<Post> posts = new ArrayList<>();
        xSql = "select * from post";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            int postId;
            String title;
            String briefInfo;
            String content;
            Date updatedDate;
            boolean status;
            int userId;
            int productId;
            Post post;
            while (rs.next()) {
                postId = rs.getInt("post_id");
                title = rs.getString("title");
                briefInfo = rs.getString("brief_info");
                content = rs.getString("content");
                updatedDate = rs.getDate("updated_date");
                status = rs.getInt("status") == 1;
                userId = rs.getInt("user_id");
                productId = rs.getInt("product_id");
                post = new Post(postId, title, briefInfo, content, updatedDate, status, userId, productId);
                posts.add(post);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Collections.sort(posts, new Comparator<Post>() {
            @Override
            public int compare(Post o1, Post o2) {
                return o2.getUpdatedDate().compareTo(o1.getUpdatedDate());
            }
        });
        return posts;
    }

    public int getLastPostId() {
        xSql = "SELECT * FROM post order by post_id desc LIMIT 1";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("post_id");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int insertPost(Post post) {
        xSql = "INSERT INTO `online_shop`.`post` ( `title`, `brief_info`, "
                + "`content`, `updated_date`, `status`, `user_id`, `product_id`) VALUES\n"
                + "      ( ?, ? ,? ,?,?,?,?);";
        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, post.getTitle());
            ps.setString(2, post.getBriefInfo());
            ps.setString(3, post.getContent());
            ps.setDate(4, post.getUpdatedDate());
            ps.setBoolean(5, post.isStatus());
            ps.setInt(6, post.getUserId());
            ps.setInt(7, post.getProductId());

            return ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void changeStatusOfPostById(int id, boolean status) {
        int newStatus = status ? 0 : 1;
        xSql = "update post set status = ? where post_id = ?";
        try {
            ps = con.prepareStatement(xSql);
            ps.setInt(1, newStatus);
            ps.setInt(2, id);
            ps.executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public List<Post> getAllActivePosts() {
        List<Post> posts = new ArrayList<>();
        xSql = "select * from post where status = 1";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            int postId;
            String title;
            String briefInfo;
            String content;
            Date updatedDate;
            boolean status;
            int userId;
            Post post;
            while (rs.next()) {
                postId = rs.getInt("post_id");
                title = rs.getString("title");
                briefInfo = rs.getString("brief_info");
                content = rs.getString("content");
                updatedDate = rs.getDate("updated_date");
                status = rs.getInt("status") == 1;
                userId = rs.getInt("user_id");
                post = new Post(postId, title, briefInfo, content, updatedDate, status, userId);
                posts.add(post);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Collections.sort(posts, new Comparator<Post>() {
            @Override
            public int compare(Post o1, Post o2) {
                return o2.getUpdatedDate().compareTo(o1.getUpdatedDate());
            }
        });
        return posts;
    }

    public Post getById(int postId) {
        Post post = null;
        String query = "SELECT * FROM post WHERE post_id = ?";
        try {
            ps = con.prepareStatement(query);
            ps.setInt(1, postId);
            rs = ps.executeQuery();
            if (rs.next()) {
                post = new Post(
                        rs.getInt("post_id"),
                        rs.getString("title"),
                        rs.getString("brief_info"),
                        rs.getString("content"),
                        rs.getDate("updated_date"),
                        rs.getInt("status") == 1,
                        rs.getInt("user_id")
                );
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return post;
    }

    public boolean update(Post post) {
        String query = "UPDATE post SET title = ?, brief_info = ?, content = ?, updated_date = ?, status = ? WHERE post_id = ?";
        try {
            ps = con.prepareStatement(query);
            ps.setString(1, post.getTitle());
            ps.setString(2, post.getBriefInfo());
            ps.setString(3, post.getContent());
            ps.setDate(4, post.getUpdatedDate());
            ps.setInt(5, post.isStatus() ? 1 : 0);
            ps.setInt(6, post.getPostId());

            int rowsAffected = ps.executeUpdate();
            return rowsAffected > 0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void main(String[] args) {
        PostDAO postDAO = new PostDAO();

        // Test getById
        int postId = 1; // Replace with the ID of the post you want to retrieve
        Post post = postDAO.getById(postId);
        if (post != null) {
            System.out.println("Post retrieved successfully:");
            System.out.println("Title: " + post.getTitle());
            System.out.println("Brief Info: " + post.getBriefInfo());
            System.out.println("Content: " + post.getContent());
            System.out.println("Updated Date: " + post.getUpdatedDate());
            System.out.println("Status: " + post.isStatus());
            System.out.println("User ID: " + post.getUserId());
        } else {
            System.out.println("Post with ID " + postId + " not found.");
        }

        // Test update
        if (post != null) {
            post.setTitle("Updated Title");
            post.setBriefInfo("Updated Brief Info");
            post.setContent("Updated Content");
            post.setUpdatedDate(new Date(System.currentTimeMillis()));
            post.setStatus(false);

            boolean updateResult = postDAO.update(post);
            if (updateResult) {
                System.out.println("Post updated successfully.");
            } else {
                System.out.println("Failed to update post.");
            }
        } else {
            System.out.println("Cannot update post because it was not retrieved successfully.");
        }
    }

    public Post getPostById(int id) {
        xSql = "select * from post where post_id =" + id;
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            int postId;
            String title;
            String briefInfo;
            String content;
            Date updatedDate;
            boolean status;
            int userId;
            int productId;
            if (rs.next()) {
                postId = rs.getInt("post_id");
                title = rs.getString("title");
                briefInfo = rs.getString("brief_info");
                content = rs.getString("content");
                updatedDate = rs.getDate("updated_date");
                status = rs.getInt("status") == 1;
                userId = rs.getInt("user_id");
                productId = rs.getInt("product_id");
                return new Post(postId, title, briefInfo, content, updatedDate, status, userId, productId);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Post> getPaginatedPosts(List<Post> list, int start, int end) {
        List<Post> posts = new ArrayList<>();
        for (int i = start; i < end; i++) {
            posts.add(list.get(i));
        }
        return posts;
    }

    public List<Post> searchActivePosts(String searchKey) {
        List<Post> posts = new ArrayList<>();
        xSql = "SELECT DISTINCT po.*\n"
                + "FROM post po\n"
                + "JOIN product p ON po.product_id = p.product_id\n"
                + "JOIN category c ON p.category_id = c.category_id\n"
                + "JOIN brand b ON p.brand_id = b.brand_id\n"
                + "WHERE (\n"
                + "    p.product_name LIKE '%" + searchKey + "%'\n"
                + "    OR c.category_name LIKE '%" + searchKey + "%'\n"
                + "    OR c.parent_id IN (SELECT category_id FROM category WHERE category_name LIKE '%" + searchKey + "%')\n"
                + "    OR b.brand_name LIKE '%" + searchKey + "%'\n"
                + ")\n"
                + "AND p.status = 1\n"
                + "ORDER BY po.updated_date DESC;";
        System.out.println(xSql);
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            int postId;
            String title;
            String briefInfo;
            String content;
            Date updatedDate;
            boolean status;
            int userId;
            int productId;
            Post post;
            while (rs.next()) {
                postId = rs.getInt("post_id");
                title = rs.getString("title");
                briefInfo = rs.getString("brief_info");
                content = rs.getString("content");
                updatedDate = rs.getDate("updated_date");
                status = rs.getInt("status") == 1;
                userId = rs.getInt("user_id");
                productId = rs.getInt("product_id");
                post = new Post(postId, title, briefInfo, content, updatedDate, status, userId, productId);
                posts.add(post);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return posts;
    }

    public List<Post> filterActivePosts(int[] categoryIds, int[] brandIds) {
        List<Post> posts = new ArrayList<>();
        xSql = "select po.*, c.parent_id \n"
                + "from post po join product p on po.product_id = p.product_id\n"
                + "	join category c on p.category_id = c.category_id \n"
                + "where p.status = 1";
        if (categoryIds != null) {
            xSql += " and (p.category_id in(";
            for (int i = 0; i < categoryIds.length; i++) {
                xSql += categoryIds[i] + ",";
            }
            if (xSql.endsWith(",")) {
                xSql = xSql.substring(0, xSql.length() - 1);
            }
            xSql += ")";

            xSql += " or parent_id in(";
            for (int i = 0; i < categoryIds.length; i++) {
                xSql += categoryIds[i] + ",";
            }
            if (xSql.endsWith(",")) {
                xSql = xSql.substring(0, xSql.length() - 1);
            }
            xSql += "))";
        }
        if (brandIds != null) {
            xSql += " and p.brand_id in(";
            for (int i = 0; i < brandIds.length; i++) {
                xSql += brandIds[i] + ",";
            }
            if (xSql.endsWith(",")) {
                xSql = xSql.substring(0, xSql.length() - 1);
            }
            xSql += ")\n"
                    + "order by updated_date desc";
        }
        System.out.println(xSql);
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            int postId;
            String title;
            String briefInfo;
            String content;
            Date updatedDate;
            boolean status;
            int userId;
            int productId;
            Post post;
            while (rs.next()) {
                postId = rs.getInt("post_id");
                title = rs.getString("title");
                briefInfo = rs.getString("brief_info");
                content = rs.getString("content");
                updatedDate = rs.getDate("updated_date");
                status = rs.getInt("status") == 1;
                userId = rs.getInt("user_id");
                productId = rs.getInt("product_id");
                post = new Post(postId, title, briefInfo, content, updatedDate, status, userId, productId);
                posts.add(post);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return posts;
    }

    public List<Post> getAllPostsInTime(Date fromDate, Date toDate) {
        List<Post> posts = new ArrayList<>();
        xSql = "select * from post where updated_date between '" + fromDate + "' and '" + toDate + "'";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            int postId;
            String title;
            String briefInfo;
            String content;
            Date updatedDate;
            boolean status;
            int userId;
            Post post;
            while (rs.next()) {
                postId = rs.getInt("post_id");
                title = rs.getString("title");
                briefInfo = rs.getString("brief_info");
                content = rs.getString("content");
                updatedDate = rs.getDate("updated_date");
                status = rs.getInt("status") == 1;
                userId = rs.getInt("user_id");
                post = new Post(postId, title, briefInfo, content, updatedDate, status, userId);
                posts.add(post);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Collections.sort(posts, new Comparator<Post>() {
            @Override
            public int compare(Post o1, Post o2) {
                return o2.getUpdatedDate().compareTo(o1.getUpdatedDate());
            }
        });
        return posts;
    }

}
