/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.ProductImage;

/**
 *
 * @author admin
 */
public class ProductImageDAO extends DAO {

    public List<ProductImage> getAllImageOfProductById(int id) {
        List<ProductImage> images = new ArrayList<>();
        xSql = "select * from product_image where product_id = " + id;
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            int imageId;
            String imageUrl;
            int productId;
            ProductImage image;
            while (rs.next()) {
                imageId = rs.getInt("image_id");
                imageUrl = rs.getString("image_url");
                productId = rs.getInt("product_id");
                image = new ProductImage(imageId, imageUrl, productId);
                images.add(image);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return images;
    }

    public boolean updateProductImage(int id, String imageUrl) {
        String xSql = "UPDATE product_image SET image_url = ? where product_id = ?";
        try {
            ps = con.prepareStatement(xSql);

            ps.setString(1, imageUrl);
            ps.setInt(2, id);

            int n = ps.executeUpdate();
            return n > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public int insertProductImage(int id, String imageUrl) {
        xSql = "INSERT INTO `product_image`\n"
                + "(`image_url`,\n"
                + "`product_id`)\n"
                + "VALUES\n"
                + "(?,?)";
        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, imageUrl);
            ps.setInt(2, id);
            return ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public ProductImage getImageById(int imageId) {
        xSql = "SELECT * FROM product_image WHERE image_id = ?";
        try {
            ps = con.prepareStatement(xSql);
            ps.setInt(1, imageId);
            rs = ps.executeQuery();
            if (rs.next()) {
                int productId = rs.getInt("product_id");
                String imageUrl = rs.getString("image_url");
                return new ProductImage(imageId, imageUrl, productId);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean updateProductImage(ProductImage image) {
        xSql = "UPDATE product_image SET image_url = ? WHERE image_id = ?";
        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, image.getImageUrl());
            ps.setInt(2, image.getImageId());
            int rowsAffected = ps.executeUpdate();
            return rowsAffected > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public String toString() {
        return "ProductImageDAO{" + '}';
    }

    public static void main(String[] args) {
        ProductImageDAO dao = new ProductImageDAO();
        dao.insertProductImage(1, "hdbajda");
    }

}

//    public boolean addProductImage(int productId, String imageUrl) {
//    boolean success = false;
//    xSql = "INSERT INTO product_image (image_url, product_id) VALUES (?, ?)";
//    try {
//        ps = con.prepareStatement(xSql);
//        ps.setString(1, imageUrl);
//        ps.setInt(2, productId);
//        int rowsAffected = ps.executeUpdate();
//        if (rowsAffected > 0) {
//            success = true;
//        }
//    } catch (Exception e) {
//        e.printStackTrace();
//    }
//    return success;
//}
