package dal;

import model.Delivery;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DeliveryDAO extends DBContext {

    public List<Delivery> getAllDeliveries() {
        List<Delivery> deliveryList = new ArrayList<>();

        try {
            String query = "SELECT * FROM delivery";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Delivery delivery = new Delivery();
                delivery.setDelivery(resultSet.getInt("delivery_id"));
                delivery.setCompanyName(resultSet.getString("company_name"));
                deliveryList.add(delivery);
            }
        } catch (SQLException e) {
            System.out.println("getAllDeliveries: " + e.getMessage());
        }

        return deliveryList;
    }

    public Delivery getDeliveryById(int deliveryId) {
        Delivery delivery = null;

        try {
            String query = "SELECT * FROM delivery WHERE delivery_id=?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, deliveryId);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                delivery = new Delivery();
                delivery.setDelivery(resultSet.getInt("delivery_id"));
                delivery.setCompanyName(resultSet.getString("company_name"));
            }
        } catch (SQLException e) {
            System.out.println("getDeliveryById: " + e.getMessage());
        }

        return delivery;
    }

    public boolean addDelivery(Delivery delivery) {
        try {
            String query = "INSERT INTO delivery (company_name) VALUES (?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, delivery.getCompanyName());
            int rowsAffected = preparedStatement.executeUpdate();
            return rowsAffected > 0;
        } catch (SQLException e) {
            System.out.println("addDelivery: " + e.getMessage());
            return false;
        }
    }

    public boolean updateDelivery(Delivery delivery) {
        try {
            String query = "UPDATE delivery SET company_name=? WHERE delivery_id=?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, delivery.getCompanyName());
            preparedStatement.setInt(2, delivery.getDelivery());
            int rowsAffected = preparedStatement.executeUpdate();
            return rowsAffected > 0;
        } catch (SQLException e) {
            System.out.println("updateDelivery: " + e.getMessage());
            return false;
        }
    }

    public boolean deleteDelivery(int deliveryId) {
        try {
            String query = "DELETE FROM delivery WHERE delivery_id=?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, deliveryId);
            int rowsAffected = preparedStatement.executeUpdate();
            return rowsAffected > 0;
        } catch (SQLException e) {
            System.out.println("deleteDelivery: " + e.getMessage());
            return false;
        }
    }
}
