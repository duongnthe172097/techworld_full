/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import model.Category;

/**
 *
 * @author admin
 */
public class CategoryDAO extends DAO {

    public List<Category> getAllCategories() {
        List<Category> categories = new ArrayList<>();
        xSql = "select * from category";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            int categoryId;
            String categoryName, description, image;
            int parentId = 0;
            boolean isActive = true;
            Category category;
            while (rs.next()) {
                categoryId = rs.getInt("category_id");
                categoryName = rs.getString("category_name");
                description = rs.getString("description");
                image = rs.getString("image");
                parentId = rs.getInt("parent_id");
                isActive = rs.getInt("is_active") == 1;
                category = new Category(categoryId, categoryName, description, image, parentId, isActive);
                categories.add(category);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return categories;
    }

    public List<Category> getAllActiveCategories() {
        List<Category> categories = new ArrayList<>();
        xSql = "select * from category where is_active = 1";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            int categoryId;
            String categoryName, description, image;
            int parentId = 0;
            boolean isActive = true;
            Category category;
            while (rs.next()) {
                categoryId = rs.getInt("category_id");
                categoryName = rs.getString("category_name");
                description = rs.getString("description");
                image = rs.getString("image");
                parentId = rs.getInt("parent_id");
                isActive = rs.getInt("is_active") == 1;
                category = new Category(categoryId, categoryName, description, image, parentId, isActive);
                categories.add(category);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return categories;
    }

    public boolean hasSubCategory(int id) {
        boolean check = false;
        xSql = "SELECT\n"
                + "    category_id,\n"
                + "    category_name\n"
                + "FROM\n"
                + "    category\n"
                + "WHERE\n"
                + "    parent_id = " + id;
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            if (rs.next()) {
                check = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return check;
    }

    public double getTotalRevenueOfRootCategoryById(int id, Date fromDate, Date toDate) {
        double revenue = 0;
        xSql = "SELECT\n"
                + "    c_parent.category_id AS parent_category_id,\n"
                + "    c_parent.category_name AS parent_category_name,\n"
                + "    SUM(p.price * (1 - p.discount / 100) * od.quantity) AS total_revenue\n"
                + "FROM\n"
                + "    category c_parent\n"
                + "LEFT JOIN\n"
                + "    category c_child ON c_parent.category_id = c_child.parent_id\n"
                + "LEFT JOIN\n"
                + "    product p ON c_child.category_id = p.category_id\n"
                + "LEFT JOIN\n"
                + "    order_detail od ON p.product_id = od.product_id\n"
                + "LEFT JOIN\n"
                + "    `order` o ON od.order_id = o.order_id\n"
                + "WHERE\n"
                + "    o.order_date >= '" + fromDate + "' AND o.order_date <= '" + toDate + "'\n"
                + "    and c_parent.category_id = " + id + "\n"
                + " and o.state = 1\n"
                + "GROUP BY\n"
                + "    c_parent.category_id, c_parent.category_name;";
        System.out.println(xSql);
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            if (rs.next()) {
                revenue = rs.getDouble("total_revenue");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return revenue;
    }

    //only get revenue for category that have orders
    public LinkedHashMap<Category, Double> getRevenuesOfCategories(Date fromDate, Date toDate) {
        LinkedHashMap<Category, Double> revenues = new LinkedHashMap<>();
        xSql = "SELECT\n"
                + "    cs.category_id,\n"
                + "    cs.category_name,\n"
                + "    SUM(p.price * (1 - p.discount / 100) * od.quantity) AS revenue\n"
                + "FROM\n"
                + "    category cs\n"
                + "LEFT JOIN\n"
                + "    product p ON cs.category_id = p.category_id\n"
                + "LEFT JOIN\n"
                + "    order_detail od ON p.product_id = od.product_id\n"
                + "LEFT JOIN\n"
                + "    `order` o ON od.order_id = o.order_id\n"
                + "WHERE\n"
                + "    o.order_date >= '" + fromDate + "' AND o.order_date <= '" + toDate + "'\n"
                + "GROUP BY\n"
                + "    cs.category_id, cs.category_name\n"
                + "UNION ALL\n"
                + "SELECT\n"
                + "    c_parent.category_id AS parent_category_id,\n"
                + "    c_parent.category_name AS parent_category_name,\n"
                + "    SUM(p.price * (1 - p.discount / 100) * od.quantity) AS total_revenue\n"
                + "FROM\n"
                + "    category c_parent\n"
                + "LEFT JOIN\n"
                + "    category c_child ON c_parent.category_id = c_child.parent_id\n"
                + "LEFT JOIN\n"
                + "    product p ON c_child.category_id = p.category_id\n"
                + "LEFT JOIN\n"
                + "    order_detail od ON p.product_id = od.product_id\n"
                + "LEFT JOIN\n"
                + "    `order` o ON od.order_id = o.order_id\n"
                + "WHERE\n"
                + "    o.order_date >= '" + fromDate + "' AND o.order_date <= '" + toDate + "'\n"
                + "and o.state = 1\n"
                + "GROUP BY\n"
                + "    c_parent.category_id, c_parent.category_name;";
        System.out.println(xSql);
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Category category = new Category();
                category.setCategoryId(rs.getInt("category_id"));
                category.setCategoryName(rs.getString("category_name"));

                Double revenue = rs.getDouble("revenue");

                revenues.put(category, revenue);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return revenues;
    }

    //get revenues of all categories even if they do not have orders
    public LinkedHashMap<Category, Double> getRevenuesOfAllCategories(Date fromDate, Date toDate) {
        LinkedHashMap<Category, Double> fullRevenues = new LinkedHashMap<>();
        xSql = "select category_id, category_name from category";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Category category = new Category();
                category.setCategoryId(rs.getInt("category_id"));
                category.setCategoryName(rs.getString("category_name"));
                fullRevenues.put(category, 0.0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        LinkedHashMap<Category, Double> revenues = getRevenuesOfCategories(fromDate, toDate);
        revenues.keySet().forEach(category -> {
            int categoryId = category.getCategoryId();
            fullRevenues.entrySet().removeIf(entry -> entry.getKey().getCategoryId() == categoryId);
        });

        fullRevenues.putAll(revenues);
        return fullRevenues;
    }

    //get avg feedback of categories that have feedback
    public LinkedHashMap<Category, Double> getRatedStarFeedbacksOfAllCategories(Date fromDate, Date toDate) {
        LinkedHashMap<Category, Double> stars = new LinkedHashMap<>();
        xSql = "SELECT\n"
                + "    c.category_id,\n"
                + "    c.category_name,\n"
                + "    AVG(f.rated_star) AS avg_star\n"
                + "FROM\n"
                + "    feedback f\n"
                + "JOIN\n"
                + "    product p ON f.product_id = p.product_id\n"
                + "JOIN\n"
                + "    category c ON c.category_id = p.category_id\n"
                + "WHERE\n"
                + "    f.updated_date BETWEEN '" + fromDate + "' AND '" + toDate + "'\n"
                + "GROUP BY\n"
                + "    c.category_id, c.category_name\n"
                + "\n"
                + "UNION ALL\n"
                + "\n"
                + "-- Feedback cho category lớn\n"
                + "SELECT\n"
                + "    c_parent.category_id,\n"
                + "    c_parent.category_name,\n"
                + "    AVG(f.rated_star) AS avg_star\n"
                + "FROM\n"
                + "    feedback f\n"
                + "JOIN\n"
                + "    product p ON f.product_id = p.product_id\n"
                + "JOIN\n"
                + "    category c_child ON p.category_id = c_child.category_id\n"
                + "JOIN\n"
                + "    category c_parent ON c_child.parent_id = c_parent.category_id\n"
                + "WHERE\n"
                + "    f.updated_date BETWEEN '" + fromDate + "' AND '" + toDate + "'\n"
                + "GROUP BY\n"
                + "    c_parent.category_id, c_parent.category_name";
        System.out.println(xSql);
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Category category = new Category();
                category.setCategoryId(rs.getInt("category_id"));
                category.setCategoryName(rs.getString("category_name"));

                Double revenue = rs.getDouble("avg_star");

                stars.put(category, revenue);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stars;
    }

    public String getCategoryNameById(int id) {
        xSql = "select category_name from category where category_id = " + id;
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getString("category_name");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public int getCategoryIdbyCategoryName(String name) {
        xSql = "Select category_id from category where category_name like ?";
        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, name);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("category_id");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int updateCategoryStatusById(int bid, boolean status) {
        String xSql = "UPDATE category SET is_active = ? WHERE category_id = ?";
        int n = 0;
        try {
            ps = con.prepareStatement(xSql);
            ps.setBoolean(1, status);
            ps.setInt(2, bid);
            n = ps.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return n;
    }

    public boolean checkExistedCategory(String categoryName) {
        xSql = "select category_name from category where category_name = '" + categoryName + "'";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            if (rs.next()) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public int getLastCategoryId() {
        xSql = "select category_id from category order by category_id desc limit 1";
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("category_id");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 1;
    }

    public int insertCategoryProduct(Category category) {
        xSql = "INSERT INTO `online_shop`.`category`\n"
                + "(`category_name`,\n"
                + "`is_active`)\n"
                + "VALUES\n"
                + "(?, ?)";
        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, category.getCategoryName());
            ps.setInt(2, category.isIsActive() ? 1 : 0);
            return ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
}
