/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.PostImage;

/**
 *
 * @author admin
 */
public class PostImageDAO extends DAO {

    public List<PostImage> getAllImageOfPostById(int id) {
        List<PostImage> images = new ArrayList<>();
        xSql = "select * from post_image where post_id = " + id;
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            int imageId;
            String imageUrl;
            int postId;
            PostImage image;
            while (rs.next()) {
                imageId = rs.getInt("image_id");
                imageUrl = rs.getString("image_url");
                postId = rs.getInt("post_id");
                image = new PostImage(imageId, imageUrl, postId);
                images.add(image);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return images;
    }

    public int insertPostImage(int id, String imageUrl) {
        xSql = "INSERT INTO `online_shop`.`post_image`\n"
                + "(`image_url`,\n"
                + "`post_id`)\n"
                + "VALUES\n"
                + "(?,?)";
        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, imageUrl);
            ps.setInt(2, id);
            return ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public boolean createPostImage(PostImage image) {
        xSql = "INSERT INTO post_image (image_url, post_id) VALUES (?, ?)";
        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, image.getImageUrl());
            ps.setInt(2, image.getPostId());
            int rowsAffected = ps.executeUpdate();
            return rowsAffected > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean deletePostImageById(int imageId) {
        xSql = "DELETE FROM post_image WHERE image_id = ?";
        try {
            ps = con.prepareStatement(xSql);
            ps.setInt(1, imageId);
            int rowsAffected = ps.executeUpdate();
            return rowsAffected > 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

}
