/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author admin
 */
public class Slider {
   private int sliderId;
   private String title;
   private String image;
   private boolean status;
   private String note;
   private int userId;
   private int productId;

    public Slider() {
    }

    public Slider(int sliderId, String title, String image, boolean status, String note, int userId, int productId) {
        this.sliderId = sliderId;
        this.title = title;
        this.image = image;
        this.status = status;
        this.note = note;
        this.userId = userId;
        this.productId = productId;
    }

    public Slider(String title, String image, boolean status, String note, int userId, int productId) {
        this.title = title;
        this.image = image;
        this.status = status;
        this.note = note;
        this.userId = userId;
        this.productId = productId;
    }

    public int getSliderId() {
        return sliderId;
    }

    public void setSliderId(int sliderId) {
        this.sliderId = sliderId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    @Override
    public String toString() {
        return "Slider{" + "sliderId=" + sliderId + ", title=" + title + ", image=" + image + ", status=" + status + ", note=" + note + ", userId=" + userId + ", productId=" + productId + '}';
    }

    
   
}
