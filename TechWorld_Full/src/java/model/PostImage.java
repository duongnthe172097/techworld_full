/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author admin
 */
public class PostImage {
    private int imageId;
    private String imageUrl;
    private int postId;

    public PostImage() {
    }

    public PostImage(int imageId, String imageUrl, int postId) {
        this.imageId = imageId;
        this.imageUrl = imageUrl;
        this.postId = postId;
    }

    public PostImage(String imageUrl, int postId) {
        this.imageUrl = imageUrl;
        this.postId = postId;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    @Override
    public String toString() {
        return "PostImage{" + "imageId=" + imageId + ", imageUrl=" + imageUrl + ", postId=" + postId + '}';
    }
    
    
}
