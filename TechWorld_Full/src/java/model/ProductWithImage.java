    /*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author izayo
 */
public class ProductWithImage {
    private int productId;
    private String imageUrl;
    private String productName;
    private int categoryId;
    private String description;
    private int quantityInStock;
    private double price;
    private double discount;
    private boolean status;
    public ProductWithImage() {
    }

    public ProductWithImage(int productId, String imageUrl, String productName, int categoryId, String description, int quantityInStock, double price, double discount, boolean status) {
        this.productId = productId;
        this.imageUrl = imageUrl;
        this.productName = productName;
        this.categoryId = categoryId;
        this.description = description;
        this.quantityInStock = quantityInStock;
        this.price = price;
        this.discount = discount;
        this.status = status;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getDescription() {
        return description;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getQuantityInStock() {
        return quantityInStock;
    }

    public void setQuantityInStock(int quantityInStock) {
        this.quantityInStock = quantityInStock;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }
    
            
}
