/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;

/**
 *
 * @author Admin
 */
public class OrderView {
    private int orderId;
    private Date orderDate;
    private int numberOfproduct;
    private  int total;
    private int state;
    private ProductImageAndName p;
    public OrderView() {
    }

    public OrderView(int orderId, Date orderDate, int numberOfproduct, int total, int state, ProductImageAndName p) {
        this.orderId = orderId;
        this.orderDate = orderDate;
        this.numberOfproduct = numberOfproduct;
        this.total = total;
        this.state = state;
        this.p = p;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public int getNumberOfproduct() {
        return numberOfproduct;
    }

    public void setNumberOfproduct(int numberOfproduct) {
        this.numberOfproduct = numberOfproduct;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public ProductImageAndName getP() {
        return p;
    }

    public void setP(ProductImageAndName p) {
        this.p = p;
    }

    public String getStateString() {
        if (state == 1) return "Pending";
        if (state == 2) return "Shiping";
        if (state == 3) return "Delivered";
        return "Canceled";
    }
    public  String getPrice(){
        String s_total = String.valueOf(total);
        int remain  = s_total.length()%3;
        String result = s_total.substring(0,remain);
        for(int i =remain;i<s_total.length();i=i+3){
            result+="."+s_total.substring(i,i+3);
        }
        return result;
    }
    @Override
    public String toString() {
        return "OrderView{" + "orderId=" + orderId + ", orderDate=" + orderDate + ", numberOfproduct=" + numberOfproduct + ", total=" + total + ", state=" + state + ", p=" + p + '}';
    }
    

    
    
}
