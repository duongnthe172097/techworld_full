/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;

/**
 *
 * @author admin
 */
public class Comment {
    private int commentId;
    private String content;
    private int postId;
    private int userId;
    private Date updatedDate;
    private int replyOf;

    public Comment() {
    }

    public Comment(int commentId, String content, int postId, int userId, Date updatedDate, int replyOf) {
        this.commentId = commentId;
        this.content = content;
        this.postId = postId;
        this.userId = userId;
        this.updatedDate = updatedDate;
        this.replyOf = replyOf;
    }

    public Comment(String content, int postId, int userId, Date updatedDate, int replyOf) {
        this.content = content;
        this.postId = postId;
        this.userId = userId;
        this.updatedDate = updatedDate;
        this.replyOf = replyOf;
    }

    public Comment(int commentId, String content, int postId, int userId, Date updatedDate) {
        this.commentId = commentId;
        this.content = content;
        this.postId = postId;
        this.userId = userId;
        this.updatedDate = updatedDate;
    }

    public Comment(String content, int postId, int userId, Date updatedDate) {
        this.content = content;
        this.postId = postId;
        this.userId = userId;
        this.updatedDate = updatedDate;
    }

    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public int getReplyOf() {
        return replyOf;
    }

    public void setReplyOf(int replyOf) {
        this.replyOf = replyOf;
    }

    @Override
    public String toString() {
        return "Comment{" + "commentId=" + commentId + ", content=" + content + ", postId=" + postId + ", userId=" + userId + ", updatedDate=" + updatedDate + ", replyOf=" + replyOf + '}';
    }

}
