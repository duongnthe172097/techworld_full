/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Admin
 */
public class ProductImageAndName {

    private int productId;
    private String productName;
    private String imageUrl;

    public ProductImageAndName() {
    }

    public ProductImageAndName(int productId, String productName, String imageUrl) {
        this.productId = productId;
        this.productName = productName;
        this.imageUrl = imageUrl;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return "ProductImageAndName{" + "productId=" + productId + ", productName=" + productName + ", imageUrl=" + imageUrl + '}';
    }
    
}
