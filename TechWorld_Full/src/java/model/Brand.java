/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author admin
 */
public class Brand {
    private int brandId;
    private String brandName;
    private String image;
    private boolean isActive;

    public Brand() {
    }

    public Brand(int brandId, String brandName, String image, boolean isActive) {
        this.brandId = brandId;
        this.brandName = brandName;
        this.image = image;
        this.isActive = isActive;
    }

    public Brand(String brandName, String image, boolean isActive) {
        this.brandName = brandName;
        this.image = image;
        this.isActive = isActive;
    }


    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    @Override
    public String toString() {
        return "Brand{" + "brandId=" + brandId + ", brandName=" + brandName + ", image=" + image + ", isActive=" + isActive + '}';
    }
    
    
    
}
