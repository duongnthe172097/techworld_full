/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dal.FeedbackDAO;
import dal.ProductDAO;

/**
 *
 * @author admin
 */
public class OrderDetail {
    private int orderId;
    private int productId;
    private int quantity;
    

    public OrderDetail() {
    }

    public OrderDetail(int orderId, int productId, int quatity) {
        this.orderId = orderId;
        this.productId = productId;
        this.quantity = quatity;
      
    }

    public OrderDetail(int productId, int quatity) {
        this.productId = productId;
        this.quantity = quantity;
    
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

  
    
    
    public Product getProduct() {
        return new ProductDAO().getProductById(productId);
    }
    
    public double getTotalCost() {
        return getProduct().getPrice() * quantity;
    }
    
    public boolean getIsFeedback(User user) {
        if (user == null) return false;
        
        return new FeedbackDAO().getByUserIdAndOrderId(user.getUserId(), productId) != null;
    }

    @Override
    public String toString() {
        return "OrderDetail{" + "orderId=" + orderId + ", productId=" + productId + ", quatity=" + quantity + '}';
    }

}
