/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dal.PostImageDAO;
import java.sql.Date;
import java.util.List;

/**
 *
 * @author admin
 */
public class Post {
    private int postId;
    private String title;
    private String briefInfo;
    private String content;
    private Date updatedDate;
    private boolean status;
    private int userId;
    private int productId;
    

    public Post() {
    }

    public Post(int postId, String title, String briefInfo, String content, Date updatedDate, boolean status, int userId) {
        this.postId = postId;
        this.title = title;
        this.briefInfo = briefInfo;
        this.content = content;
        this.updatedDate = updatedDate;
        this.status = status;
        this.userId = userId;
    }

    public Post(String title, String briefInfo, String content, Date updatedDate, boolean status, int userId) {
        this.title = title;
        this.briefInfo = briefInfo;
        this.content = content;
        this.updatedDate = updatedDate;
        this.status = status;
        this.userId = userId;
    }

    public Post(int postId, String title, String briefInfo, String content, Date updatedDate, boolean status, int userId, int productId) {
        this.postId = postId;
        this.title = title;
        this.briefInfo = briefInfo;
        this.content = content;
        this.updatedDate = updatedDate;
        this.status = status;
        this.userId = userId;
        this.productId = productId;
    }

    public Post(String title, String briefInfo, String content, Date updatedDate, boolean status, int userId, int productId) {
        this.title = title;
        this.briefInfo = briefInfo;
        this.content = content;
        this.updatedDate = updatedDate;
        this.status = status;
        this.userId = userId;
        this.productId = productId;
    }
    
    

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBriefInfo() {
        return briefInfo;
    }

    public void setBriefInfo(String briefInfo) {
        this.briefInfo = briefInfo;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }
    
    public List<PostImage> getPostImages() {
        return new PostImageDAO().getAllImageOfPostById(postId);
    }

    
    @Override
    public String toString() {
        return "Post{" + "postId=" + postId + ", title=" + title + ", briefInfo=" + briefInfo + ", content=" + content + ", updatedDate=" + updatedDate + ", status=" + status + ", userId=" + userId + '}';
    }

    
}
