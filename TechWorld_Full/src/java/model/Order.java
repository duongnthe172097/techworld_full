/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dal.OrderDetailDAO;
import dal.UserDAO;
import java.sql.Date;
import java.util.List;

/**
 *
 * @author admin
 */
public class Order {

    private int orderId;
    private Date orderDate;
    private int deliveryId;
    private int userId;
    private int state;
    private int saleId;
    private String saleNote;
    private int productId;
    private int quantity;
    

    public Order(int orderId, Date orderDate, int deliveryId, int userId, int state, int saleId, String saleNote) {
        this.orderId = orderId;
        this.orderDate = orderDate;
        this.deliveryId = deliveryId;
        this.userId = userId;
        this.state = state;
        this.saleId = saleId;
        this.saleNote = saleNote;
      
       
    }

    public Order(Date orderDate, int deliveryId, int userId, int state, int saleId, String saleNote) {
        this.orderDate = orderDate;
        this.deliveryId = deliveryId;
        this.userId = userId;
        this.state = state;
        this.saleId = saleId;
        this.saleNote = saleNote;     
    }

    public Order() {
    }

    public Order(int orderId, Date orderDate, int deliveryId, int userId, int state) {
        this.orderId = orderId;
        this.orderDate = orderDate;
        this.deliveryId = deliveryId;
        this.userId = userId;
        this.state = state;
    }

    public Order(Date orderDate, int deliveryId, int userId, int state) {
        this.orderDate = orderDate;
        this.deliveryId = deliveryId;
        this.userId = userId;
        this.state = state;
        
        
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public int getDeliveryId() {
        return deliveryId;
    }

    public void setDeliveryId(int deliveryId) {
        this.deliveryId = deliveryId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getSaleId() {
        return saleId;
    }

    public void setSaleId(int saleId) {
        this.saleId = saleId;
    }

    public String getSaleNote() {
        return saleNote;
    }

    public void setSaleNote(String saleNote) {
        this.saleNote = saleNote;
    }


    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    
    public String getStateString() {
        if (state == 1) {
            return "Đã nhận";
        }
        if (state == 2) {
            return "Đã hủy";
        }
        if (state == 3) {
            return "Đã đặt";
        }
        return "Đã hủy";
    }

    public List<OrderDetail> getOrderDetailList() {
        return new OrderDetailDAO().getOrderDetailsByOrderId(orderId);
    }

    public User getUser() {
        return new UserDAO().getUserByID(userId);
    }

    public double getTotalCost() {

        double totalCost = 0;
        List<OrderDetail> orderDetails = getOrderDetailList();

        for (OrderDetail orderDetail : orderDetails) {
            totalCost += orderDetail.getQuantity() * orderDetail.getProduct().getPrice();
        }

        return totalCost;
    }

    @Override
    public String toString() {
        return "Order{" + "orderId=" + orderId + ", orderDate=" + orderDate + ", deliveryId=" + deliveryId + ", userId=" + userId + ", state=" + state + '}';
    }

}
