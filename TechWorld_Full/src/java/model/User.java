/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;

/**
 *
 * @author admin
 */
public class User {
    private int userId;
    private int roleId;
    private String userName;
    private String password;
    private boolean status;
    private String email;
    private String fullName;
    private boolean gender;
    private String mobile;
    private String address;
    private String image;
    private Date updatedDate;
    private String code;

    public User() {
    }

    public User(int roleId, String userName, String password, boolean status, String email, String fullName, boolean gender, String mobile, String address, String image, Date updatedDate) {
        this.roleId = roleId;
        this.userName = userName;
        this.password = password;
        this.status = status;
        this.email = email;
        this.fullName = fullName;
        this.gender = gender;
        this.mobile = mobile;
        this.address = address;
        this.image = image;
        this.updatedDate = updatedDate;
    }
    
    

    public User(int userId, int roleId, String userName, String password, boolean status, String email, String fullName, boolean gender, String mobile, String address, String image, Date updatedDate) {
        this.userId = userId;
        this.roleId = roleId;
        this.userName = userName;
        this.password = password;
        this.status = status;
        this.email = email;
        this.fullName = fullName;
        this.gender = gender;
        this.mobile = mobile;
        this.address = address;
        this.image = image;
        this.updatedDate = updatedDate;
    }

    public User(int userId, int roleId, String userName, String password, boolean status, String email, String fullName, boolean gender, String mobile, String address, String image, Date updatedDate, String code) {
        this.userId = userId;
        this.roleId = roleId;
        this.userName = userName;
        this.password = password;
        this.status = status;
        this.email = email;
        this.fullName = fullName;
        this.gender = gender;
        this.mobile = mobile;
        this.address = address;
        this.image = image;
        this.updatedDate = updatedDate;
        this.code = code;
    }

    public User(int roleId, String userName, String password, boolean status, String email, String fullName, boolean gender, String mobile, String address, String image, Date updatedDate, String code) {
        this.roleId = roleId;
        this.userName = userName;
        this.password = password;
        this.status = status;
        this.email = email;
        this.fullName = fullName;
        this.gender = gender;
        this.mobile = mobile;
        this.address = address;
        this.image = image;
        this.updatedDate = updatedDate;
        this.code = code;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public boolean isGender() {
        return gender;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "User{" + "userId=" + userId + ", roleId=" + roleId + ", userName=" + userName + ", password=" + password + ", status=" + status + ", email=" + email + ", fullName=" + fullName + ", gender=" + gender + ", mobile=" + mobile + ", address=" + address + ", image=" + image + ", updatedDate=" + updatedDate + ", code=" + code + '}';
    }

    public User(int roleId, String userName, String password, boolean status, String email, String fullName, boolean gender, String mobile, String address, Date updatedDate, String code) {
        this.roleId = roleId;
        this.userName = userName;
        this.password = password;
        this.status = status;
        this.email = email;
        this.fullName = fullName;
        this.gender = gender;
        this.mobile = mobile;
        this.address = address;
        this.updatedDate = updatedDate;
        this.code = code;
    }


}
