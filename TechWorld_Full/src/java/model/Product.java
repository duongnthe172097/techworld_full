/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dal.ProductImageDAO;
import java.sql.Date;
import java.util.List;

/**
 *
 * @author admin
 */
public class Product {
    private int productId;
    private String productName;
    private int categoryId;
    private int brandId;
    private int quantityInStock;
    private double price;
    private double discount;
    private String description;
    private boolean status;
    private Date updatedDate;

    public Product() {
    }

    public Product(int productId, String productName, int categoryId, int brandId, int quantityInStock, double price, double discount, String description, boolean status, Date updatedDate) {
        this.productId = productId;
        this.productName = productName;
        this.categoryId = categoryId;
        this.brandId = brandId;
        this.quantityInStock = quantityInStock;
        this.price = price;
        this.discount = discount;
        this.description = description;
        this.status = status;
        this.updatedDate = updatedDate;
    }

    public Product(String productName, int categoryId, int brandId, int quantityInStock, double price, double discount, String description, boolean status, Date updatedDate) {
        this.productName = productName;
        this.categoryId = categoryId;
        this.brandId = brandId;
        this.quantityInStock = quantityInStock;
        this.price = price;
        this.discount = discount;
        this.description = description;
        this.status = status;
        this.updatedDate = updatedDate;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public int getQuantityInStock() {
        return quantityInStock;
    }

    public void setQuantityInStock(int quantityInStock) {
        this.quantityInStock = quantityInStock;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
    
    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Override
    public String toString() {
        return "Product{" + "productId=" + productId + ", productName=" + productName + ", categoryId=" + categoryId + ", brandId=" + brandId + ", quantityInStock=" + quantityInStock + ", price=" + price + ", discount=" + discount + ", description=" + description + ", status=" + status + ", updatedDate=" + updatedDate + '}';
    }
    
    public List<ProductImage> getProductImage() {
        return new ProductImageDAO().getAllImageOfProductById(productId);
    }


}
