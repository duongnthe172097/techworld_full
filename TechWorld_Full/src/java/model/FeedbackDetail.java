/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author HP
 */
public class FeedbackDetail {

    private int feedbackId;
    private String imageUrl;
    private String image;
    private String fullName;
    private String email;
    private String mobile;
    private int ratedStar;
    private String content;
    private boolean status;

    public FeedbackDetail() {
    }

    public FeedbackDetail(int feedbackId, String imageUrl, String image, String fullName, String email, String mobile, int ratedStar, String content, boolean status) {
        this.feedbackId = feedbackId;
        this.imageUrl = imageUrl;
        this.image = image;
        this.fullName = fullName;
        this.email = email;
        this.mobile = mobile;
        this.ratedStar = ratedStar;
        this.content = content;
        this.status = status;
    }

    public int getFeedbackId() {
        return feedbackId;
    }

    public void setFeedbackId(int feedbackId) {
        this.feedbackId = feedbackId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getRatedStar() {
        return ratedStar;
    }

    public void setRatedStar(int ratedStar) {
        this.ratedStar = ratedStar;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "FeedbackDetail{" + "feedbackId=" + feedbackId + ", imageUrl=" + imageUrl + ", image=" + image + ", fullName=" + fullName + ", email=" + email + ", mobile=" + mobile + ", ratedStar=" + ratedStar + ", content=" + content + ", status=" + status + '}';
    }

}
