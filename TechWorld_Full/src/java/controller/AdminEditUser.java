/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller;

import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.User;

/**
 *
 * @author Admin
 */
@WebServlet(name="AdminEditUser", urlPatterns={"/admin/admin-edituser"})
public class AdminEditUser extends HttpServlet {
   
    
    private final UserDAO userDAO = new UserDAO();
    
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AdminEditUser</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AdminEditUser at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
       request.setAttribute("isEdit", true);
        
        int id = Integer.parseInt(request.getParameter("id").trim());
        User user = userDAO.getUserByID(id);
        
        if (user == null) {
            response.sendRedirect(request.getContextPath() + "/user");
            return;
        }
        request.setAttribute("oldId", id);
        request.setAttribute("user", user);
        request.getRequestDispatcher("/view/add-userform.jsp").forward(request, response);
        
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
       request.setAttribute("isEdit", true);
       UserDAO userDAO = new UserDAO();
       int user_id = Integer.parseInt(request.getParameter("user_id").trim());
       int role_id = Integer.parseInt(request.getParameter("role_id").trim());
       boolean status = request.getParameter("status").equals("1");
       User user = new User();
       user = userDAO.getUserByID(user_id);
       user.setRoleId(role_id);
       user.setStatus(status);
       user = userDAO.updateRoleAndStatus(user);
       request.setAttribute("msg", "Cập nhật thông tin người dùng thành công!");
       request.setAttribute("userdetail", user);
       request.getRequestDispatcher("/view/user-details.jsp").forward(request, response);
    }

 
    
    
    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
