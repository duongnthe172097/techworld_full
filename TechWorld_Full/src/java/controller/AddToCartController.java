/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.CartDAO;
import dal.CartItemDAO;
import dal.ProductDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.Cart;
import model.CartItem;
import model.Product;
import model.User;

/**
 *
 * @author HP
 */
@WebServlet(name = "AddToCartController", urlPatterns = {"/addToCart"})
public class AddToCartController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //Call out important function for use
        HttpSession session = request.getSession();
        CartDAO cartDao = new CartDAO();
        ProductDAO productDao = new ProductDAO();
        int quantity = Integer.parseInt(request.getParameter("quantity"));

        //Get product id from front end
        int productId = Integer.parseInt(request.getParameter("productId"));

        //Get user
        User user = (User) session.getAttribute("user");
        
        if (user == null) {
            response.sendRedirect("login");
            return;
        }
        
        int userId = user.getUserId();

        Cart cart = cartDao.getFirstCartByUserId(userId);
        
        // check exist cart
        if (cart == null) {
            
            Cart newCart = new Cart();
            newCart.setUserId(userId);
            
            cartDao.addCart(newCart);
            
            cart = cartDao.getFirstCartByUserId(userId);
        }
        
        //Product on the page
        Product product = productDao.getProductById(productId);
        
        // if remove action
        if (request.getParameter("action")!=null && request.getParameter("action").equals("remove")) {
            
            new CartItemDAO().deleteCartItem(cart.getCartId(), productId);
            response.sendRedirect("cartController?removed");
            return;
            
        }
        
        // check stock
        if (product.getQuantityInStock() - quantity < 0) {
            
            response.sendRedirect("cartController?out_of_stock");
            return;
            
        }
        
        CartItem cartItem = new CartItemDAO().getCartItemByCartIdAndProductId(cart.getCartId(), productId);
        
        // check exist cart item
        if (cartItem == null) {
            
            CartItem newCartItem = new CartItem();
            
            newCartItem.setCartId(cart.getCartId());
            newCartItem.setProductId(productId);
            newCartItem.setQuantity(0);
            
            new CartItemDAO().addCartItem(newCartItem);
            
            cartItem = new CartItemDAO().getCartItemByCartIdAndProductId(cart.getCartId(), productId);
            
        }
        
        // check quantity
        if (cartItem.getQuantity() + quantity <= 0) {
            response.sendRedirect("cartController?error");
            return;
        }
        
        // update product stock
        product.setQuantityInStock(product.getQuantityInStock() - quantity);
        
        cartItem.setQuantity(cartItem.getQuantity() + quantity);
        
        new CartItemDAO().updateCartItem(cartItem);
        
        response.sendRedirect("cartController?success");

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

}
