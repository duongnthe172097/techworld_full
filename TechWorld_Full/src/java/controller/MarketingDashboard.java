/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.CategoryDAO;
import dal.FeedbackDAO;
import dal.OrderDAO;
import dal.PostDAO;
import dal.ProductDAO;
import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.Date;
import java.util.LinkedHashMap;
import java.util.List;
import model.Category;
import model.Feedback;
import model.Post;
import model.Product;
import model.User;

/**
 *
 * @author admin
 */
@WebServlet(name = "MarketingDashboard", urlPatterns = {"/marketing/marketing-dashboard"})
public class MarketingDashboard extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MarketingDashboard</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MarketingDashboard at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //The range of time to view data
        String toDate_raw = request.getParameter("toDate");
        String fromDate_raw = request.getParameter("fromDate");
        Date toDate;
        Date fromDate;
        if (toDate_raw == null || fromDate_raw == null) {
            toDate = new Date(System.currentTimeMillis());
            fromDate = new Date(toDate.getTime() - 7 * 24 * 60 * 60 * 1000);
        } else {
            toDate = Date.valueOf(toDate_raw);
            fromDate = Date.valueOf(fromDate_raw);
        }
        request.setAttribute("fromDate", fromDate.toString());
        request.setAttribute("toDate", toDate.toString());

        String service = request.getParameter("view");
        if (service == null || service.equals("")) {
            service = "statistic";
        }

        //view statistic
        if (service.equals("statistic")) {
            //statics order
            OrderDAO orderDAO = new OrderDAO();
            int[] staticOrder = orderDAO.getNumberOfNewlyOrdersInTime(fromDate, toDate);
            request.setAttribute("staticOrder", staticOrder);

            //statics customer
            UserDAO userDAO = new UserDAO();
            String customerType = request.getParameter("customerType");
            if (customerType == null || customerType.equals("")) {
                customerType = "registered";
            }
            request.setAttribute("customerType", customerType);
            //newly bought customers
            List<User> newlyRegisteredCustomers = userDAO.getNewlyRegisteredCustomers(fromDate, toDate);
            request.setAttribute("customerType", customerType);
            request.setAttribute("newlyRegisteredCustomers", newlyRegisteredCustomers);
            //newly registered customers
            LinkedHashMap<User, String[]> newlyBoughtCustomers = userDAO.getNewlyBoughtCustomers(fromDate, toDate);
            request.setAttribute("customerType", customerType);
            request.setAttribute("newlyBoughtCustomers", newlyBoughtCustomers);

            //static feedback
            FeedbackDAO feedbackDAO = new FeedbackDAO();
            LinkedHashMap<Feedback, String[]> newlyFeedbacks = feedbackDAO.getNewlyFeedBacks(fromDate, toDate);
            request.setAttribute("newlyFeedbacks", newlyFeedbacks);
            //navigate to mkt dashboard
            request.getRequestDispatcher("/view/mkt-statistic.jsp").forward(request, response);

        } //view product list
        else if (service.equals("products")) {
            //product list
            ProductDAO productDAO = new ProductDAO();
            List<Product> products = productDAO.getAllProductsInTime(fromDate, toDate);
            request.setAttribute("products", products);
            //navigate to mkt dashboard
            request.getRequestDispatcher("/view/mkt-products.jsp").forward(request, response);
            
        } else if (service.equals("posts")) {
            //Post list
            PostDAO postDAO = new PostDAO();
            List<Post> posts = postDAO.getAllPostsInTime(fromDate, toDate);
            request.setAttribute("posts", posts);
            //navigate to mkt dashboard
            request.getRequestDispatcher("/view/mkt-posts.jsp").forward(request, response);
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
