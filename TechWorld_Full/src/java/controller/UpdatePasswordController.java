/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import controller.SendEmail.EmailConfig;
import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.User;

/**
 *
 * @author HP
 */
@WebServlet(name = "UpdatePasswordController", urlPatterns = {"/updatepassword"})
public class UpdatePasswordController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UpdatePasswordController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UpdatePasswordController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String code = request.getParameter("code");
        String newpassword = request.getParameter("newPassword");
        String repass = request.getParameter("confirmPassword");

        String cd = (String) request.getSession().getAttribute("code");
        if (cd != null && cd.equals(code)) {

            if (newpassword.equals(repass)) {

                User user = (User) request.getSession().getAttribute("gui");
                String email = user.getEmail();

                UserDAO dao = new UserDAO();
                boolean updateSuccess = dao.updatePassword(email, newpassword);

                if (updateSuccess) {
                    request.setAttribute("mess", "Thay đổi mật khẩu thành công");
                } else {
                    request.setAttribute("error", "Lỗi cập nhật mật khẩu");
                }

                request.getSession().removeAttribute("code");
                request.getSession().removeAttribute("gui");

                request.getRequestDispatcher("view/checkPassword.jsp").forward(request, response);
            } else {
                if (newpassword.length() < 8) {
                    request.setAttribute("error", "Mật khẩu phải dài tối thiểu 8 kí tự");
                    request.getRequestDispatcher("view/checkPassword.jsp").forward(request, response);
                } else {
                    request.setAttribute("error", "Mật khẩu mới và mật khẩu xác nhận không khớp");
                    request.getRequestDispatcher("view/checkPassword.jsp").forward(request, response);
                }
            }
        } else {
            request.setAttribute("error", "Mã OTP không hợp lệ");
            request.getRequestDispatcher("view/checkPassword.jsp").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
