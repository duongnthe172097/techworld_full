/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.SettingDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Setting;

/**
 *
 * @author ns
 */
@WebServlet(name = "AdminAddSetting", urlPatterns = {"/admin/settings/add"})
public class AdminAddSetting extends HttpServlet {

    private static final String FORM_PATH = "/view/setting-form.jsp";

    private final SettingDAO settingDAO = new SettingDAO();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher(FORM_PATH).forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id").trim());
        String name = request.getParameter("name").trim();
        String description = request.getParameter("description").trim();
        String value = request.getParameter("value");
        String type = request.getParameter("type");
        Boolean iActive = Boolean.valueOf(request.getParameter("isActive").trim());
      
        Setting setting = new Setting(id, name, description, value, type, iActive);
        request.setAttribute("setting", setting);
        if (settingDAO.isIdExisted(id)) {
            request.setAttribute("error", "Id is already existed. Please choose another id!");
            request.getRequestDispatcher(FORM_PATH).forward(request, response);
            return;
        }
        Setting addedSetting = settingDAO.addSetting(setting);
        if (addedSetting == null) {
            request.setAttribute("error", "Adding failed. Please check if user with id " + id + " existed!");
            request.getRequestDispatcher(FORM_PATH).forward(request, response);
            return;
        }
        request.setAttribute("success", "Added setting successfully!");
        request.getRequestDispatcher(FORM_PATH).forward(request, response);
    }

}
