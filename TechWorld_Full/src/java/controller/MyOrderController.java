/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.OrderDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.Order;
import model.OrderView;
import model.User;

/**
 *
 * @author Admin
 */
@WebServlet(name = "MyOrder", urlPatterns = {"/customer/my-order"})
public class MyOrderController extends HttpServlet {

    private static int PAGE_SIZE = 5;
    
    private OrderDAO orderDAO;

    @Override
    public void init() throws ServletException {
        super.init();
        orderDAO = new OrderDAO();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        User user = (User) request.getSession().getAttribute("user");

        if (user == null) {
            response.sendRedirect("login");
            return;
        }

        String index = request.getParameter("index");
        
        int page = index==null ? 1 : Integer.parseInt(index);
        
        int total = (orderDAO.getOrdersByUserId(user.getUserId()).size() + PAGE_SIZE - 1) / PAGE_SIZE;
        List<Order> listOrder = orderDAO.getOrdersByUserId(user.getUserId(), PAGE_SIZE, page);
        
        if (listOrder.isEmpty() && page != 1) {
            response.sendRedirect("my-order");
            return;
        }
        System.out.println(listOrder);
        request.setAttribute("page", page);
        request.setAttribute("listOrder", listOrder);
        request.setAttribute("total", total);
        request.getRequestDispatcher("/view/myorder.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Order order = (Order) session.getAttribute("order");

        request.getRequestDispatcher("/view/order.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
