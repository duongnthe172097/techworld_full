/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import controller.SendEmail.EmailConfig;
import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import model.User;

/**
 *
 * @author Admin
 */
@WebServlet(name = "AdminAddNewUser", urlPatterns = {"/admin/admin-addnewuser"})
public class AdminAddNewUser extends HttpServlet {

    private final UserDAO userDAO = new UserDAO();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AdminAddNewUser</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AdminAddNewUser at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    private boolean validatePhone(String phone) {
        if(phone.length() != 10 || !phone.startsWith("0")) 
            return false;
        for(int i = 0; i < phone.length(); i++)
            if(!Character.isDigit(phone.charAt(i)))
                return false;
        return true;
    }
    
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int role_id = Integer.parseInt(request.getParameter("role_id").trim());
        String user_name = request.getParameter("user_name");
        String password = request.getParameter("password");
        String statusParam = request.getParameter("status");
        boolean status = (statusParam != null && statusParam.equals("1"));
        String email = request.getParameter("email").trim();
        String full_name = request.getParameter("full_name").trim();
        String genderParam = request.getParameter("gender");
        boolean gender = (genderParam != null && genderParam.equals("1"));
        String mobile = request.getParameter("mobile");
        String address = request.getParameter("address");
        Date updated_date = new Date(System.currentTimeMillis());
        String code = String.valueOf(new Random().nextInt(900000) + 100000);

        User user = new User(role_id, user_name,
                password, status, email, full_name,
                gender, mobile, address,updated_date, code);
        request.setAttribute("user", user);
        
        if (!validatePhone(mobile)) {
            request.setAttribute("message", "Số điện thoại chỉ bao gồm 10 chữ số và bắt đầu bằng 0");
            request.getRequestDispatcher("/view/add-userform.jsp").forward(request, response);
            return;
        } else if (password.length() < 8) {
            request.setAttribute("message", "Mật khẩu của bạn phải tối thiểu 8 ký tự!");
            request.getRequestDispatcher("/view/add-userform.jsp").forward(request, response);
            return;
        } else if (userDAO.checkExistUsername(user_name)) {
            request.setAttribute("message", "Tên người dùng đã tồn tại!");
            request.getRequestDispatcher("/view/add-userform.jsp").forward(request, response);
            return;
        } else if (userDAO.checkExistEmail(email)) {
            request.setAttribute("message", "Email đã tồn tại!");
            request.getRequestDispatcher("/view/add-userform.jsp").forward(request, response);
            return;
        } else if (userDAO.checkExistMobile(mobile)) {
            request.setAttribute("message", "Số điện thoại này đã tồn tại!");
            request.getRequestDispatcher("/view/add-userform.jsp").forward(request, response);
            return;
        }
        
        User addNewUser = userDAO.addNewUser(user);
        if (addNewUser == null) {
            request.setAttribute("error", "Thêm người dùng thất bại!");
            request.getRequestDispatcher("/view/add-userform.jsp").forward(request, response);
        } else {
            try {
                EmailConfig ec = new EmailConfig();
                ec.SendEmail(email, "Bạn vừa được tạo một tài khoản mới tại TechWorld!", "user_name: " + user_name + "\n" + "password:" + password);
                request.setAttribute("success", "Thêm người dùng thành công!");
                request.getRequestDispatcher("/view/add-userform.jsp").forward(request, response);
            } catch (MessagingException ex) {
                Logger.getLogger(AdminAddNewUser.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
