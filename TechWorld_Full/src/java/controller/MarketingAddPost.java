/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.PostDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;
import model.Post;
import model.User;
import utils.ImageUploader;

/**
 *
 * @author admin
 */
@MultipartConfig(maxFileSize = 16177215)
@WebServlet(name = "MarketingAddPost", urlPatterns = {"/marketing/post/add"})
public class MarketingAddPost extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MarketingAddPost</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MarketingAddPost at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("/view/mkt-addPost.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String title = request.getParameter("title");
        String brief_info = request.getParameter("brief_info");
        String content = request.getParameter("content");
        Date updatedDate = new Date(System.currentTimeMillis());
        String statusParam = request.getParameter("status");
        boolean status = (statusParam != null && statusParam.equals("1"));
        int product_id = Integer.parseInt(request.getParameter("product_id"));

        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        int user_id = user.getUserId();
        //   int product_id = Integer.parseInt(request.getParameter("product_id"));
        Post post = new Post(title, brief_info, content, updatedDate, status, user_id, product_id);
        PostDAO postDAO = new PostDAO();
        postDAO.insertPost(post);
        //insert image
        Collection<Part> parts = request.getParts();
        Collection<Part> listParts = new ArrayList<>();
        for (Part part : parts) {
            if (part.getName().equals("postImage")) {
                listParts.add(part);
            }
        }
        ImageUploader imageUploader = new ImageUploader();
        imageUploader.uploadImagePosts(request, listParts, postDAO.getLastPostId());
        request.setAttribute("msg", "Thêm bài đăng thành công");
        request.getRequestDispatcher("/view/mkt-addPost.jsp").forward(request, response);
    }

    
    
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
