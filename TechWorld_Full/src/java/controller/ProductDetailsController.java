/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller;

import dal.FeedbackDAO;
import dal.ProductDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.LinkedHashMap;
import java.util.List;
import model.Feedback;
import model.Product;

/**
 *
 * @author admin
 */
@WebServlet(name="ProductDetailsController", urlPatterns={"/product"})
public class ProductDetailsController extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ProductDetailsController</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ProductDetailsController at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        ProductDAO productDAO = new ProductDAO();
        FeedbackDAO feedbackDAO = new FeedbackDAO();
        String id = request.getParameter("id");
        int productId = Integer.parseInt(id);
        Product product = productDAO.getProductById(productId);
        
        String imgIndex = request.getParameter("img");
        if(imgIndex == null) {
            imgIndex = "0";
        }
        
        List<Product> lastestProducts = productDAO.getAllActiveProducts().subList(0, 3);
        double avgRating = feedbackDAO.getAvgRatedStarOfProductById(productId);
        int numberOfFeedbacks = feedbackDAO.getCountFeedbackOfProductById(productId);
        LinkedHashMap<Feedback, String> feedbacksOfProduct = feedbackDAO.getFeedbacksOfProductById(productId);
        
        request.setAttribute("feedbacksOfProduct", feedbacksOfProduct);
        request.setAttribute("numberOfFeedbacks", numberOfFeedbacks);
        request.setAttribute("avgRating", avgRating);
        request.setAttribute("lastestProducts", lastestProducts);
        request.setAttribute("imgIndex", Integer.parseInt(imgIndex));
        request.setAttribute("productDetails", product);
        request.getRequestDispatcher("view/product-details.jsp").forward(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
