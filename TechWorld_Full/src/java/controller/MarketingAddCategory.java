/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller;

import dal.CategoryDAO;
import dal.SettingDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Category;
import model.Setting;

/**
 *
 * @author admin
 */
@WebServlet(name="MarketingAddCategory", urlPatterns={"/marketing/addCategory"})
public class MarketingAddCategory extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MarketingAddCategory</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MarketingAddCategory at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.sendRedirect(request.getContextPath() + "/marketing/product/add");
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String categoryName = request.getParameter("categoryName");
        CategoryDAO categoryDAO = new CategoryDAO();
        if(categoryDAO.checkExistedCategory(categoryName)) {
            request.setAttribute("error", "Danh mục " + categoryName + " đã tồn tại, vui lòng kiểm tra lại");
            request.getRequestDispatcher("/view/mkt-addProduct.jsp").forward(request, response);
        }
        else {
            boolean status = request.getParameter("status").equals("1");
            Category category = new Category();
            category.setCategoryName(categoryName);
            category.setIsActive(status);
            categoryDAO.insertCategoryProduct(category);
            
            int lastId = categoryDAO.getLastCategoryId();
            Setting setting = new Setting();
            setting.setName(categoryName);
            setting.setValue("" + lastId);
            setting.setIsActive(category.isIsActive());
            setting.setType("Category");
            
            SettingDAO settingDAO = new SettingDAO();
            settingDAO.insertSetting(setting);
            request.setAttribute("msg", "Danh mục " + category.getCategoryName() + " đã được thêm thành công");
            request.getRequestDispatcher("/view/mkt-addProduct.jsp").forward(request, response);
        }
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
