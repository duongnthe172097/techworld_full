/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.DBContext;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import static java.lang.System.out;

@WebServlet(name = "editSettingServlet", urlPatterns = {"/controller/editsetting"})
public class editSettingServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            DBContext dbContext = new DBContext();
            connection = dbContext.getConnection();
            int userIdToEdit = Integer.parseInt(request.getParameter("user_id"));

            String sql = "SELECT * FROM settings WHERE user_id=?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, userIdToEdit);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                int userId = resultSet.getInt("user_id");
                String description = resultSet.getString("description");
                String name = resultSet.getString("name");

                // Set attributes in request to be used in JSP
                request.setAttribute("userId", userId);
                request.setAttribute("description", description);
                request.setAttribute("name", name);

                // Forward the request to the updateSetting.jsp
                RequestDispatcher dispatcher = request.getRequestDispatcher("/updateSetting.jsp");
                dispatcher.forward(request, response);
            } else {
                out.println("<p>Setting with ID " + userIdToEdit + " not found.</p>");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
   
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            DBContext dbContext = new DBContext();
            connection = dbContext.getConnection();
            int userIdToUpdate = Integer.parseInt(request.getParameter("user_id"));
            String description = request.getParameter("description");
            String name = request.getParameter("name");

            String sql = "UPDATE settings SET description=?, name=? WHERE user_id=?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, description);
            preparedStatement.setString(2, name);
            preparedStatement.setInt(3, userIdToUpdate);

            int rowUpdated = preparedStatement.executeUpdate();

            if (rowUpdated > 0) {
                out.println("<p>Setting updated successfully.</p>");
            } else {
                out.println("<p>Setting update failed.</p>");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (preparedStatement != null) preparedStatement.close();
                if (connection != null) connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
