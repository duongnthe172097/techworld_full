/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller;

import dal.OrderDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 *
 * @author lvhn1
 */
@WebServlet(name="OrderListController", urlPatterns={"/customer/order-list"})
public class OrderListController extends HttpServlet {
    
    private static int PAGE_SIZE = 5;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        
        String pageInput = request.getParameter("page");
        int page = pageInput!=null ? Integer.parseInt(pageInput) : 1;
        
        request.setAttribute("orderList", new OrderDAO().getAllOrders(page, PAGE_SIZE));
        request.setAttribute("page", page);
        request.setAttribute("totalPage", new OrderDAO().getAllOrders().size() / PAGE_SIZE);
        
        request.getRequestDispatcher("/view/orderlist.jsp").forward(request, response);
        
    } 

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
    }

}
