/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller;

import dal.BrandDAO;
import dal.CategoryDAO;
import dal.ProductDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Brand;
import model.Category;
import model.Product;

/**
 *
 * @author admin
 */
@WebServlet(name="ProductListController", urlPatterns={"/products"})
public class ProductListController extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ProductListController</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ProductListController at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 
    
    private boolean isChecked(int id, int[] ids) {
        if (ids == null) {
            return false;
        } else {
            for (int i = 0; i < ids.length; i++) {
                if (ids[i] == id) {
                    return true;
                }
            }
            return false;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        ProductDAO productDAO = new ProductDAO();
        String searchKey = request.getParameter("search");
        List<Product> allProducts;

        //filter product
        if (searchKey == null || searchKey.equals("")) {
            //filter product by category
            String[] categoryIdRaw = request.getParameterValues("categoryId");
            int[] categoryIds = null;
            if (categoryIdRaw != null) {
                categoryIds = new int[categoryIdRaw.length];
                for (int i = 0; i < categoryIdRaw.length; i++) {
                    categoryIds[i] = Integer.parseInt(categoryIdRaw[i]);
                }
            }
            //get checked category list
            CategoryDAO categoryDAO = new CategoryDAO();
            List<Category> activeCategories = categoryDAO.getAllActiveCategories();
            boolean[] checkedCategories = new boolean[activeCategories.size()];
            for (int i = 0; i < checkedCategories.length; i++) {
                if (isChecked(activeCategories.get(i).getCategoryId(), categoryIds)) {
                    checkedCategories[i] = true;
                } else {
                    checkedCategories[i] = false;
                }
            }
            request.setAttribute("checkedCategories", checkedCategories);

            //filter product by brand
            String[] brandIdRaw = request.getParameterValues("brandId");
            int[] brandIds = null;
            if (brandIdRaw != null) {
                brandIds = new int[brandIdRaw.length];
                for (int i = 0; i < brandIdRaw.length; i++) {
                    brandIds[i] = Integer.parseInt(brandIdRaw[i]);
                }
            }
            //get checked brand
            BrandDAO brandDAO = new BrandDAO();
            List<Brand> activeBrands = brandDAO.getAllActiveBrands();
            boolean[] checkedBrands = new boolean[activeBrands.size()];
            for (int i = 0; i < checkedBrands.length; i++) {
                if (isChecked(activeBrands.get(i).getBrandId(), brandIds)) {
                    checkedBrands[i] = true;
                } else {
                    checkedBrands[i] = false;
                }
            }
            request.setAttribute("checkedBrands", checkedBrands);
            allProducts = productDAO.filterActiveProducts(categoryIds, brandIds);
        }
        
        //search product
        else {
            allProducts = productDAO.searchActiveProducts(searchKey);
            request.setAttribute("searchKey", searchKey);
        }

        //paginate product
        int size = allProducts.size();
        int page, numberPerPage = 12;
        int numberOfPages = (size % numberPerPage == 0) ? (size / numberPerPage) : (size / numberPerPage + 1);
        String pageNo = request.getParameter("page");
        if (pageNo == null) {
            page = 1;
        } else {
            page = Integer.parseInt(pageNo);
        }
        int start, end;
        start = (page - 1) * numberPerPage;
        end = Math.min(page * numberPerPage, size);
        //get list product after paginating
        List<Product> paginatedProduct = productDAO.getPaginatedProducts(allProducts, start, end);
        request.setAttribute("allProducts", paginatedProduct);
        request.setAttribute("page", page);
        request.setAttribute("numberOfPages", numberOfPages);

        //send productImageDAO
        request.getRequestDispatcher("/view/shop.jsp").forward(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
