/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import java.sql.Date;
import model.User;
import utils.ImageUploader;

/**
 *
 * @author Admin
 */
@MultipartConfig()
@WebServlet(name = "EditProfile", urlPatterns = {"/editprofile"})
public class EditProfile extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet EditProfile</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet EditProfile at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    private boolean validatePhone(String mobile) {
        if(mobile.length() != 10 || !mobile.startsWith("0")) 
            return false;
        for(int i = 0; i < mobile.length(); i++)
            if(!Character.isDigit(mobile.charAt(i)))
                return false;
        return true;
    }
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
* @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        HttpSession ses = request.getSession();
        User u = (User) ses.getAttribute("user");
        
        String service = request.getParameter("service");
        if (service.equals("update-info")) {
            String fullname = request.getParameter("fullname");
            String mobile = request.getParameter("mobile");
            String address = request.getParameter("address");
            UserDAO userDao = new UserDAO();

            if (fullname != null && mobile != null && address != null) {
                // Thay đổi thông tin hồ sơ người dùng
                userDao.editProfile(fullname, mobile, address, u.getUserName());

                // Cập nhật thông tin mới cho người dùng trong session
                u.setFullName(fullname);
                u.setMobile(mobile);
                u.setAddress(address);

                ses.setAttribute("user", u);
                request.setAttribute("user", u);

            }
            request.setAttribute("message", "Cập nhật profile thành công!");
            request.getRequestDispatcher("/view/editprofile.jsp").forward(request, response);
        } else if (service.equals("update-image")) {
            ImageUploader imageUploader = new ImageUploader();

            Part part = request.getPart("imagePath");

            String newImage = imageUploader.uploadUserImage(request, part, u.getUserId());
            if(newImage != null) {
                u.setImage(newImage);
                ses.removeAttribute("user");
                ses.setAttribute("user", u);
            }
            request.setAttribute("message", "Cập nhật ảnh thành công");
            request.getRequestDispatcher("view/profile.jsp").forward(request, response);
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}