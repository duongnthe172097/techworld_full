/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import controller.SendEmail.EmailConfig;
import dal.CartDAO;
import dal.CartItemDAO;
import dal.DeliveryDAO;
import dal.ItemDAO;
import dal.OrderDAO;
import dal.OrderDetailDAO;
import dal.ProductDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.Date;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import model.Cart;
import model.CartItem;
import model.Item;
import model.Order;
import model.OrderDetail;
import model.Product;
import model.User;

/**
 *
 * @author HP
 */
@WebServlet(name = "CartController", urlPatterns = {"/cartController"})
public class CartController extends HttpServlet {

    User user = new User();
    CartDAO cartDAO = new CartDAO();
    OrderDAO orderDAO = new OrderDAO();
    CartItemDAO cartItemDAO = new CartItemDAO();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CartController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CartController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        user = (User) request.getSession().getAttribute("user");

        if (user == null) {
            response.sendRedirect("login");
            return;
        }

        Cart cart = new CartDAO().getFirstCartByUserId(user.getUserId());

        // check exist cart
        if (cart == null) {

            Cart newCart = new Cart();
            newCart.setUserId(user.getUserId());

            new CartDAO().addCart(newCart);

            cart = new CartDAO().getFirstCartByUserId(user.getUserId());
        }

        List<CartItem> cartItemList = cart == null ? new ArrayList<>() : new CartItemDAO().getCartItemsByCartId(cart.getCartId());

        // count total cost
        double totalCost = 0;
        if (!cartItemList.isEmpty()) {
            for (CartItem cartItem : cartItemList) {
                totalCost += cartItem.getQuantity() * cartItem.getProduct().getPrice();
            }
        }

        request.setAttribute("listItem", cartItemList);
        request.setAttribute("cartId", cart.getCartId());
        request.setAttribute("userId", user.getUserId());
        request.setAttribute("user", user);
        request.setAttribute("totalCost", totalCost);
        request.setAttribute("listDelivery", new DeliveryDAO().getAllDeliveries());

        request.getRequestDispatcher("view/cart.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if (session.getAttribute("user") == null) {
            response.sendRedirect(request.getContextPath() + "/home");
            return;
        }
        User user = new User();
        int userId = user.getUserId();

        String email = request.getParameter("userEmail");
        String userFullName = request.getParameter("userFullName");
        String userMobile = request.getParameter("userMobile");
        String userAddress = request.getParameter("userAddress");
        int cartId = Integer.parseInt(request.getParameter("cartId"));
        int userIdParam = Integer.parseInt(request.getParameter("userId"));
        int deliveryId = Integer.parseInt(request.getParameter("deliveryId"));

        // create order
        Order order = new Order();

        order.setDeliveryId(deliveryId);
        order.setUserId(userIdParam);
        order.setOrderDate(new Date(new java.util.Date().getTime()));
        order.setState(3);

        // get item in cart
        List<CartItem> listCartItem = new CartItemDAO().getCartItemsByCartId(cartId);

        // check cart empty
        if (listCartItem.isEmpty()) {
            response.sendRedirect("cartController?error");
            return;
        }

        // check quantity
        for (CartItem item : listCartItem) {
            Product product = item.getProduct();
            System.out.println(item.getQuantity() + "adjbwdkja");
            if (product.getQuantityInStock() - item.getQuantity() < 0) {
                response.sendRedirect("cartController?error");
                return;
            }
        }

        // update stock
        for (CartItem item : listCartItem) {
            Product product = item.getProduct();
            product.setQuantityInStock(product.getQuantityInStock() - item.getQuantity());
            new ProductDAO().updateProduct(product);
        }

        OrderDAO orderDAO = new OrderDAO();

        // add order to db
        order = orderDAO.addOrder(order);

        // add order detail
        for (CartItem cartItem : listCartItem) {

            OrderDetail orderDetail = new OrderDetail();

            orderDetail.setProductId(cartItem.getProductId());
            orderDetail.setQuantity(cartItem.getQuantity());
            orderDetail.setOrderId(order.getOrderId());

            new OrderDetailDAO().addOrderDetail(orderDetail);
        }

        // clear cart
        new CartItemDAO().deleteCartItemsByCartId(cartId);
        new CartDAO().deleteCart(cartId);

        double total = 0;

        for (CartItem item : listCartItem) {
            total += item.getTotalCost();
        }
        String subject = "Confirm Your Order - Techworld.";

        String content = "<!DOCTYPE html>\n"
                + "<html>\n"
                + "\n"
                + "<head>\n"
                + "    <meta charset=\"utf-8\" />\n"
                + "    <meta http-equiv=\"x-ua-compatible\" content=\"ie=edge\" />\n"
                + "    <title>Email Receipt</title>\n"
                + "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" />\n"
                + "\n"
                + "    <style type=\"text/css\">\n"
                + "        /**\n"
                + "     * Google webfonts. Recommended to include the .woff version for cross-client compatibility.\n"
                + "     */\n"
                + "        @media screen {\n"
                + "            @font-face {\n"
                + "                font-family: \"Source Sans Pro\";\n"
                + "                font-style: normal;\n"
                + "                font-weight: 400;\n"
                + "                src: local(\"Source Sans Pro Regular\"), local(\"SourceSansPro-Regular\"),\n"
                + "                    url(https://fonts.gstatic.com/s/sourcesanspro/v10/ODelI1aHBYDBqgeIAH2zlBM0YzuT7MdOe03otPbuUS0.woff) format(\"woff\");\n"
                + "            }\n"
                + "\n"
                + "            @font-face {\n"
                + "                font-family: \"Source Sans Pro\";\n"
                + "                font-style: normal;\n"
                + "                font-weight: 700;\n"
                + "                src: local(\"Source Sans Pro Bold\"), local(\"SourceSansPro-Bold\"),\n"
                + "                    url(https://fonts.gstatic.com/s/sourcesanspro/v10/toadOcfmlt9b38dHJxOBGFkQc6VGVFSmCnC_l7QZG60.woff) format(\"woff\");\n"
                + "            }\n"
                + "        }\n"
                + "\n"
                + "        /**\n"
                + "     * Avoid browser level font resizing.\n"
                + "     * 1. Windows Mobile\n"
                + "     * 2. iOS / OSX\n"
                + "     */\n"
                + "        body,\n"
                + "        table,\n"
                + "        td,\n"
                + "        a {\n"
                + "            -ms-text-size-adjust: 100%;\n"
                + "            /* 1 */\n"
                + "            -webkit-text-size-adjust: 100%;\n"
                + "            /* 2 */\n"
                + "        }\n"
                + "\n"
                + "        /**\n"
                + "     * Remove extra space added to tables and cells in Outlook.\n"
                + "     */\n"
                + "        table,\n"
                + "        td {\n"
                + "            mso-table-rspace: 0pt;\n"
                + "            mso-table-lspace: 0pt;\n"
                + "        }\n"
                + "\n"
                + "        /**\n"
                + "     * Better fluid images in Internet Explorer.\n"
                + "     */\n"
                + "        img {\n"
                + "            -ms-interpolation-mode: bicubic;\n"
                + "        }\n"
                + "\n"
                + "        /**\n"
                + "     * Remove blue links for iOS devices.\n"
                + "     */\n"
                + "        a[x-apple-data-detectors] {\n"
                + "            font-family: inherit !important;\n"
                + "            font-size: inherit !important;\n"
                + "            font-weight: inherit !important;\n"
                + "            line-height: inherit !important;\n"
                + "            color: inherit !important;\n"
                + "            text-decoration: none !important;\n"
                + "        }\n"
                + "\n"
                + "        /**\n"
                + "     * Fix centering issues in Android 4.4.\n"
                + "     */\n"
                + "        div[style*=\"margin: 16px 0;\"] {\n"
                + "            margin: 0 !important;\n"
                + "        }\n"
                + "\n"
                + "        body {\n"
                + "            width: 100% !important;\n"
                + "            height: 100% !important;\n"
                + "            padding: 0 !important;\n"
                + "            margin: 0 !important;\n"
                + "        }\n"
                + "\n"
                + "        /**\n"
                + "     * Collapse table borders to avoid space between cells.\n"
                + "     */\n"
                + "        table {\n"
                + "            border-collapse: collapse !important;\n"
                + "        }\n"
                + "\n"
                + "        a {\n"
                + "            color: #1a82e2;\n"
                + "        }\n"
                + "\n"
                + "        img {\n"
                + "            height: auto;\n"
                + "            line-height: 100%;\n"
                + "            text-decoration: none;\n"
                + "            border: 0;\n"
                + "            outline: none;\n"
                + "        }\n"
                + "    </style>\n"
                + "</head>\n"
                + "\n"
                + "<body style=\"background-color: #7fcdf9\">\n"
                + "    <!-- start preheader -->\n"
                + "    <div class=\"preheader\" style=\"\n"
                + "        display: none;\n"
                + "        max-width: 0;\n"
                + "        max-height: 0;\n"
                + "        overflow: hidden;\n"
                + "        font-size: 1px;\n"
                + "        line-height: 1px;\n"
                + "        color: #fff;\n"
                + "        opacity: 0;\n"
                + "      \">\n"
                + "        A preheader is the short summary text that follows the subject line when\n"
                + "        an email is viewed in the inbox.\n"
                + "    </div>\n"
                + "    <!-- end preheader -->\n"
                + "\n"
                + "    <!-- start body -->\n"
                + "    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n"
                + "        <!-- start logo -->\n"
                + "        <tr>\n"
                + "            <td align=\"center\" bgcolor=\"#00CED1\">\n"
                + "                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px\">\n"
                + "                    <tr>\n"
                + "                        <td align=\"center\" valign=\"top\" style=\"padding: 6px 6px\">\n"
                + "                            <img src=\"https://github.com/andhuc/tourguideStorage/blob/main/tech_logo.png?raw=true\" alt=\"Logo\"\n"
                + "                                border=\"0\" width=\"68\" style=\"\n"
                + "                    display: block;\n"
                + "                    width: 68px;\n"
                + "                  \" />\n"
                + "                        </td>\n"
                + "                    </tr>\n"
                + "                </table>\n"
                + "                <!--[if (gte mso 9)|(IE)]>\n"
                + "        </td>\n"
                + "        </tr>\n"
                + "        </table>\n"
                + "        <![endif]-->\n"
                + "            </td>\n"
                + "        </tr>\n"
                + "        <!-- end logo -->\n"
                + "\n"
                + "        <!-- start hero -->\n"
                + "        <tr>\n"
                + "            <td align=\"center\" bgcolor=\"#00CED1\">\n"
                + "                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px\">\n"
                + "                    <tr>\n"
                + "                        <td align=\"left\" bgcolor=\"#ffffff\" style=\"\n"
                + "                  padding: 36px 24px 0;\n"
                + "                  font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif;\n"
                + "                  border-top: 3px solid #d4dadf;\n"
                + "                \">\n"
                + "                            <h1 style=\"\n"
                + "                    margin: 0;\n"
                + "                    font-size: 32px;\n"
                + "                    font-weight: 700;\n"
                + "                    letter-spacing: -1px;\n"
                + "                    line-height: 48px;\n"
                + "                  \">\n"
                + "                                Cảm ơn bạn đã mua hàng của chúng tôi!\n"
                + "                            </h1>\n"
                + "                        </td>\n"
                + "                    </tr>\n"
                + "                </table>\n"
                + "                <!--[if (gte mso 9)|(IE)]>\n"
                + "        </td>\n"
                + "        </tr>\n"
                + "        </table>\n"
                + "        <![endif]-->\n"
                + "            </td>\n"
                + "        </tr>\n"
                + "        <!-- end hero -->\n"
                + "\n"
                + "        <!-- start copy block -->\n"
                + "        <tr>\n"
                + "            <td align=\"center\" bgcolor=\"#00CED1\">\n"
                + "                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px\">\n"
                + "                    <!-- start copy -->\n"
                + "                    <tr>\n"
                + "                        <td align=\"left\" bgcolor=\"#ffffff\" style=\"\n"
                + "                  padding: 24px;\n"
                + "                  font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif;\n"
                + "                  font-size: 16px;\n"
                + "                  line-height: 24px;\n"
                + "                \">\n"
                + "                            <p style=\"margin: 0\">\n"
                + "                                Đây là chi tiết đơn hàng của bạn. Nếu bạn có thắc mắc liên hê\n"
                + "                                cho chúng tôi ở đây\n"
                + "                                <a href=\"http://localhost:" + request.getServerPort() + "/techworld/home\">contact us</a>.\n"
                + "                            </p>\n"
                + "                        </td>\n"
                + "                    </tr>\n"
                + "                    <!-- end copy -->\n"
                + "\n"
                + "                    <!-- start receipt table -->\n"
                + "                    <tr>\n"
                + "                        <td align=\"left\" bgcolor=\"#ffffff\" style=\"\n"
                + "                  padding: 24px;\n"
                + "                  font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif;\n"
                + "                  font-size: 16px;\n"
                + "                  line-height: 24px;\n"
                + "                \">\n"
                + "                            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\n"
                + "                                <tr>\n"
                + "                                    <td align=\"left\" bgcolor=\"#00CED1\" width=\"75%\" style=\"\n"
                + "                        padding: 12px;\n"
                + "                        font-family: 'Source Sans Pro', Helvetica, Arial,\n"
                + "                          sans-serif;\n"
                + "                        font-size: 16px;\n"
                + "                        line-height: 24px;\n"
                + "                      \">\n"
                + "                                        <strong>Mã đơn hàng </strong>\n"
                + "                                    </td>\n"
                + "                                    <td align=\"left\" bgcolor=\"#00CED1\" width=\"25%\" style=\"\n"
                + "                        padding: 12px;\n"
                + "                        font-family: 'Source Sans Pro', Helvetica, Arial,\n"
                + "                          sans-serif;\n"
                + "                        font-size: 16px;\n"
                + "                        line-height: 24px;\n"
                + "                      \">\n"
                + "                                        <strong>" + order.getOrderId() + "</strong>\n"
                + "                                    </td>\n"
                + "                                </tr>\n"
                + "                                <tr>\n"
                + "                                    <td align=\"left\" width=\"75%\" style=\"\n"
                + "                        padding: 12px;\n"
                + "                        font-family: 'Source Sans Pro', Helvetica, Arial,\n"
                + "                          sans-serif;\n"
                + "                        font-size: 16px;\n"
                + "                        line-height: 24px;\n"
                + "                        border-top: 2px dashed #d2c7ba;\n"
                + "                        border-bottom: 2px dashed #d2c7ba;\n"
                + "                      \">\n"
                + "                                        <strong>Tên của bạn</strong>\n"
                + "                                    </td>\n"
                + "                                    <td align=\"left\" width=\"25%\" style=\"\n"
                + "                        padding: 12px;\n"
                + "                        font-family: 'Source Sans Pro', Helvetica, Arial,\n"
                + "                          sans-serif;\n"
                + "                        font-size: 16px;\n"
                + "                        line-height: 24px;\n"
                + "                        border-top: 2px dashed #d2c7ba;\n"
                + "                        border-bottom: 2px dashed #d2c7ba;\n"
                + "                      \">\n"
                + "                                        <strong>" + userFullName + "</strong>\n"
                + "                                    </td>\n"
                + "                                </tr>\n"
                + "                                <tr>\n"
                + "                                    <td align=\"left\" width=\"75%\" style=\"\n"
                + "                        padding: 12px;\n"
                + "                        font-family: 'Source Sans Pro', Helvetica, Arial,\n"
                + "                          sans-serif;\n"
                + "                        font-size: 16px;\n"
                + "                        line-height: 24px;\n"
                + "                        border-top: 2px dashed #d2c7ba;\n"
                + "                        border-bottom: 2px dashed #d2c7ba;\n"
                + "                      \">\n"
                + "                                        <strong>Email</strong>\n"
                + "                                    </td>\n"
                + "                                    <td align=\"left\" width=\"25%\" style=\"\n"
                + "                        padding: 12px;\n"
                + "                        font-family: 'Source Sans Pro', Helvetica, Arial,\n"
                + "                          sans-serif;\n"
                + "                        font-size: 16px;\n"
                + "                        line-height: 24px;\n"
                + "                        border-top: 2px dashed #d2c7ba;\n"
                + "                        border-bottom: 2px dashed #d2c7ba;\n"
                + "                      \">\n"
                + "                                        <strong>" + email + "</strong>\n"
                + "                                    </td>\n"
                + "                                </tr>\n"
                + "                                <tr>\n"
                + "                                    <td align=\"left\" width=\"75%\" style=\"\n"
                + "                        padding: 12px;\n"
                + "                        font-family: 'Source Sans Pro', Helvetica, Arial,\n"
                + "                          sans-serif;\n"
                + "                        font-size: 16px;\n"
                + "                        line-height: 24px;\n"
                + "                        border-top: 2px dashed #d2c7ba;\n"
                + "                        border-bottom: 2px dashed #d2c7ba;\n"
                + "                      \">\n"
                + "                                        <strong>Địa chỉ</strong>\n"
                + "                                    </td>\n"
                + "                                    <td align=\"left\" width=\"25%\" style=\"\n"
                + "                        padding: 12px;\n"
                + "                        font-family: 'Source Sans Pro', Helvetica, Arial,\n"
                + "                          sans-serif;\n"
                + "                        font-size: 16px;\n"
                + "                        line-height: 24px;\n"
                + "                        border-top: 2px dashed #d2c7ba;\n"
                + "                        border-bottom: 2px dashed #d2c7ba;\n"
                + "                      \">\n"
                + "                                        <strong>" + userAddress + "</strong>\n"
                + "                                    </td>\n"
                + "                                </tr>\n"
                + "                                <tr>\n"
                + "                                    <td align=\"left\" width=\"75%\" style=\"\n"
                + "                        padding: 12px;\n"
                + "                        font-family: 'Source Sans Pro', Helvetica, Arial,\n"
                + "                          sans-serif;\n"
                + "                        font-size: 16px;\n"
                + "                        line-height: 24px;\n"
                + "                        border-top: 2px dashed #d2c7ba;\n"
                + "                        border-bottom: 2px dashed #d2c7ba;\n"
                + "                      \">\n"
                + "                                        <strong>Số điện thoại</strong>\n"
                + "                                    </td>\n"
                + "                                    <td align=\"left\" width=\"25%\" style=\"\n"
                + "                        padding: 12px;\n"
                + "                        font-family: 'Source Sans Pro', Helvetica, Arial,\n"
                + "                          sans-serif;\n"
                + "                        font-size: 16px;\n"
                + "                        line-height: 24px;\n"
                + "                        border-top: 2px dashed #d2c7ba;\n"
                + "                        border-bottom: 2px dashed #d2c7ba;\n"
                + "                      \">\n"
                + "                                        <strong>" + userMobile + "</strong>\n"
                + "                                    </td>\n"
                + "                                </tr>\n"
                + "                                <tr>\n"
                + "                                    <td align=\"left\" width=\"75%\" style=\"\n"
                + "                        padding: 12px;\n"
                + "                        font-family: 'Source Sans Pro', Helvetica, Arial,\n"
                + "                          sans-serif;\n"
                + "                        font-size: 16px;\n"
                + "                        line-height: 24px;\n"
                + "                        border-top: 2px dashed #d2c7ba;\n"
                + "                        border-bottom: 2px dashed #d2c7ba;\n"
                + "                      \">\n"
                + "                                        <strong>Tổng giá</strong>\n"
                + "                                    </td>\n"
                + "                                    <td align=\"left\" width=\"25%\" style=\"\n"
                + "                        padding: 12px;\n"
                + "                        font-family: 'Source Sans Pro', Helvetica, Arial,\n"
                + "                          sans-serif;\n"
                + "                        font-size: 16px;\n"
                + "                        line-height: 24px;\n"
                + "                        border-top: 2px dashed #d2c7ba;\n"
                + "                        border-bottom: 2px dashed #d2c7ba;\n"
                + "                      \">\n"
                + "                                        <strong>" + new DecimalFormat("#,###").format(total) + "</strong>\n"
                + "                                    </td>\n"
                + "                                </tr>\n"
                + "                            </table>\n"
                + "                        </td>\n"
                + "                    </tr>\n"
                + "                    <!-- end reeipt table -->\n"
                + "                </table>\n"
                + "                <!--[if (gte mso 9)|(IE)]>\n"
                + "        </td>\n"
                + "        </tr>\n"
                + "        </table>\n"
                + "        <![endif]-->\n"
                + "            </td>\n"
                + "        </tr>\n"
                + "        <!-- end copy block -->";

        for (CartItem item : listCartItem) {

            content += "<!-- start order details block -->\n"
                    + "        <tr>\n"
                    + "            <td align=\"center\" bgcolor=\"#00CED1\" valign=\"top\" width=\"100%\">\n"
                    + "                <table align=\"center\" bgcolor=\"#ffffff\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\"\n"
                    + "                    style=\"max-width: 600px\">\n"
                    + "                    <tr>\n"
                    + "                        <td align=\"center\" valign=\"top\" style=\"font-size: 0; border-bottom: 3px solid #d4dadf\">\n"
                    + "                            <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\"\n"
                    + "                                style=\"max-width: 600px\">\n"
                    + "                                <tr>\n"
                    + "                                    <td align=\"center\" valign=\"top\" style=\"font-size: 0; padding-top: 20px\">\n"
                    + "                                        <!--[if (gte mso 9)|(IE)]>\n"
                    + "                  <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"600\">\n"
                    + "                  <tr>\n"
                    + "                  <td align=\"center\" valign=\"top\" width=\"600\">\n"
                    + "                  <![endif]-->\n"
                    + "                                        <div style=\"\n"
                    + "                          display: inline-block;\n"
                    + "                          width: 100%;\n"
                    + "                          max-width: 600px;\n"
                    + "                          vertical-align: top;\n"
                    + "                        \">\n"
                    + "                                            <!-- start order details table -->\n"
                    + "                                            <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"\n"
                    + "                                                width=\"100%\" style=\"max-width: 600px\">\n"
                    + "                                                <!-- Add your table header here -->\n"
                    + "                                                <tr>\n"
                    + "                                                    <td align=\"left\" width=\"25%\" style=\"\n"
                    + "                                padding: 12px;\n"
                    + "                                font-family: 'Source Sans Pro', Helvetica, Arial,\n"
                    + "                                  sans-serif;\n"
                    + "                                font-size: 16px;\n"
                    + "                                line-height: 24px;\n"
                    + "                                border-top: 2px dashed #d2c7ba;\n"
                    + "                                border-bottom: 2px dashed #d2c7ba;\n"
                    + "                              \">\n"
                    + "                                                        <strong>" + item.getProduct().getProductName() + "</strong>\n"
                    + "                                                    </td>\n"
                    + "                                                    <td align=\"left\" width=\"25%\" style=\"\n"
                    + "                                padding: 12px;\n"
                    + "                                font-family: 'Source Sans Pro', Helvetica, Arial,\n"
                    + "                                  sans-serif;\n"
                    + "                                font-size: 16px;\n"
                    + "                                line-height: 24px;\n"
                    + "                                border-top: 2px dashed #d2c7ba;\n"
                    + "                                border-bottom: 2px dashed #d2c7ba;\n"
                    + "                              \">\n"
                    + "                                                        <strong>" + new DecimalFormat("#,###").format(item.getProduct().getPrice()) + "</strong>\n"
                    + "                                                    </td>\n"
                    + "                                                    <td align=\"left\" width=\"25%\" style=\"\n"
                    + "                                padding: 12px;\n"
                    + "                                font-family: 'Source Sans Pro', Helvetica, Arial,\n"
                    + "                                  sans-serif;\n"
                    + "                                font-size: 16px;\n"
                    + "                                line-height: 24px;\n"
                    + "                                border-top: 2px dashed #d2c7ba;\n"
                    + "                                border-bottom: 2px dashed #d2c7ba;\n"
                    + "                              \">\n"
                    + "                                                        <strong>" + item.getQuantity() + "</strong>\n"
                    + "                                                    </td>\n"
                    + "                                                </tr></table>\n";
        }

        content += "                                            <!-- end order details table -->\n"
                + "                                        </div>\n"
                + "                                        <!--[if (gte mso 9)|(IE)]>\n"
                + "                  </td>\n"
                + "                  </tr>\n"
                + "                  </table>\n"
                + "                  <![endif]-->\n"
                + "                                    </td>\n"
                + "                                </tr>\n"
                + "                            </table>\n"
                + "                        </td>\n"
                + "                    </tr>\n"
                + "                </table>\n"
                + "            </td>\n"
                + "        </tr>\n"
                + "        <!-- end order details block -->\n"
                + "\n"
                + "\n"
                + "        <!-- start footer -->\n"
                + "        <tr>\n"
                + "            <td align=\"center\" bgcolor=\"#00CED1\" style=\"padding: 24px\">\n"
                + "                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 600px\">\n"
                + "\n"
                + "\n"
                + "                    <!-- start buttons -->\n"
                + "                    <tr>\n"
                + "                        <td align=\"center\" bgcolor=\"#00CED1\" style=\"padding: 24px\">\n"
                + "                            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n"
                + "                                <tr>\n"
                + "                                    <td align=\"center\" bgcolor=\"#1a82e2\" style=\"border-radius: 6px\">\n"
                + "                                        <a href=\"http://localhost:" + request.getServerPort() + "/techworld/update-order?action=confirm&id=" + order.getOrderId() + "\" target=\"_blank\" style=\"\n"
                + "                          display: inline-block;\n"
                + "                          padding: 16px 36px;\n"
                + "                          font-family: 'Source Sans Pro', Helvetica, Arial,\n"
                + "                            sans-serif;\n"
                + "                          font-size: 16px;\n"
                + "                          color: #ffffff;\n"
                + "                          text-decoration: none;\n"
                + "                          border-radius: 6px;\n"
                + "                        \">Confirm Order</a>\n"
                + "                                    </td>\n"
                + "                                    <td align=\"center\" bgcolor=\"#FF00FF\" style=\"border-radius: 6px\">\n"
                + "                                        <a href=\"http://localhost:" + request.getServerPort() + "/techworld/update-order?action=cancel&id=" + order.getOrderId() + "\" target=\"_blank\" style=\"\n"
                + "                          display: inline-block;\n"
                + "                          padding: 16px 36px;\n"
                + "                          font-family: 'Source Sans Pro', Helvetica, Arial,\n"
                + "                            sans-serif;\n"
                + "                          font-size: 16px;\n"
                + "                          color: #ffffff;\n"
                + "                          text-decoration: none;\n"
                + "                          border-radius: 6px;\n"
                + "                        \">Cancel Order</a>\n"
                + "                                    </td>\n"
                + "                                </tr>\n"
                + "                            </table>\n"
                + "                        </td>\n"
                + "                    </tr>\n"
                + "                    <!-- end buttons -->\n"
                + "                </table>\n"
                + "                <!--[if (gte mso 9)|(IE)]>\n"
                + "        </td>\n"
                + "        </tr>\n"
                + "        </table>\n"
                + "        <![endif]-->\n"
                + "            </td>\n"
                + "        </tr>\n"
                + "        <!-- end footer -->\n"
                + "    </table>\n"
                + "    <!-- end body -->\n"
                + "</body>\n"
                + "\n"
                + "</html>";

        try {
            EmailConfig ec = new EmailConfig();
            ec.SendEmail(email, subject, content);
        } catch (MessagingException ex) {
            Logger.getLogger(CartController.class.getName()).log(Level.SEVERE, null, ex);
        }

        response.sendRedirect(request.getContextPath() + "/customer/order?id=" + order.getUserId());
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
