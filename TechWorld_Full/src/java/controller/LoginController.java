/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.CartDAO;
import dal.UserDAO;
import dal.UserDAO;
import jakarta.servlet.RequestDispatcher;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.User;

/**
 *
 * @author Admin
 */
@WebServlet(name = "LoginController", urlPatterns = {"/login"})
public class LoginController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet LoginController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet LoginController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        //b1: get user, pass from cookie    
        Cookie arr[] = request.getCookies();
        if (arr != null) {
            for (Cookie o : arr) {
                if (o.getName().equals("userC")) {
                    request.setAttribute("name", o.getValue());
                }
                if (o.getName().equals("passC")) {
                    request.setAttribute("password", o.getValue());
                }
            }
        } else {
// Set default values if cookies are not present
            request.setAttribute("name", "");
            request.setAttribute("password", "");
        }
        //b2: set user, pass to login form
        request.getRequestDispatcher("view/login.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        UserDAO userDao = new UserDAO();
        String name = request.getParameter("name");
        String password = request.getParameter("pass");
        String remember = request.getParameter("remember");
        if (name == "" || password == "") {
            request.setAttribute("loginMsg", "Tên đăng nhập hoặc mật khẩu trống");
            RequestDispatcher dispatcher = request.getRequestDispatcher("view/login.jsp");
            dispatcher.forward(request, response);

        } else {
            User user = userDao.login(name, password);

            if (user == null) {
                request.setAttribute("loginMsg", "Sai tên đăng nhập hoặc mật khẩu");
                RequestDispatcher dispatcher = request.getRequestDispatcher("view/login.jsp");
                dispatcher.forward(request, response);

            } else {
                // Check user status
                Boolean status = userDao.checkStatus(name);
                if (status != null && !status) {
                    request.setAttribute("loginMsg", "Tài khoản của bạn đã bị khóa. Vui lòng liên hệ admin để mở.");
                    RequestDispatcher dispatcher = request.getRequestDispatcher("view/login.jsp");
                    dispatcher.forward(request, response);
                    return; // Stop further processing
                }
                
                HttpSession session = request.getSession();
                session.setAttribute("user", user);
                session.setAttribute("size", 0);
                session.setAttribute("role_id", user.getRoleId());

                // luu acccount len tren cookie
                Cookie u = new Cookie("userC", name);
                Cookie p = new Cookie("passC", password);
                u.setMaxAge(60);

                if (remember != null) {
                    p.setMaxAge(60);
                } else {
                    p.setMaxAge(0);
                }

                response.addCookie(u); //luu u va p len tren edge
                response.addCookie(p);
                response.sendRedirect("home");
            }
        }
    }


    /**
     * Returns a short description of the servlet.
     *
* @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}