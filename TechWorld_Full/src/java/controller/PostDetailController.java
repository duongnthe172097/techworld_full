package controller;

import dal.PostDAO;
import model.Post;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "PostDetailController", urlPatterns = {"/marketing/post-detail"})
public class PostDetailController extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Retrieve post ID from request parameter
        int id = Integer.parseInt(request.getParameter("id"));

        // Initialize PostDAO
        PostDAO postDAO = new PostDAO();

        // Retrieve post details from database using the PostDAO
        Post post = postDAO.getById(id);

        // Set the post details as request attributes
        request.setAttribute("post", post);

        // Forward the request to the post-detail.jsp page for rendering
        request.getRequestDispatcher("/view/post-detail.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Retrieve post ID and updated post details from request parameters
        int id = Integer.parseInt(request.getParameter("id"));
        String title = request.getParameter("title");
        String briefInfo = request.getParameter("briefInfo");
        String content = request.getParameter("content");
        boolean status = Boolean.parseBoolean(request.getParameter("status"));

        // Initialize PostDAO
        PostDAO postDAO = new PostDAO();

        // Retrieve post details from database using the PostDAO
        Post post = postDAO.getById(id);

        // Update post details
        post.setTitle(title);
        post.setBriefInfo(briefInfo);
        post.setContent(content);
        post.setStatus(status);

        // Update post in the database
        boolean updateResult = postDAO.update(post);

        // Redirect the user to the post detail page with updated details
        if (updateResult) {
            response.sendRedirect(request.getContextPath() + "/marketing/post-detail?id=" + id + "&success");
        } else {
            // Handle update failure
            // For example, display an error message
            response.getWriter().println("Failed to update post details.");
        }
    }
}