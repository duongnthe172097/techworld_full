/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller;

import dal.OrderDAO;
import dal.OrderDetailDAO;
import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Order;
import model.OrderDetail;

/**
 *
 * @author lvhn1
 */
@WebServlet(name="OrderDetailController", urlPatterns={"/customer/order-detail"})
public class OrderDetailController extends HttpServlet {
   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        
        int orderId = Integer.parseInt(request.getParameter("id"));
        
        Order order = new OrderDAO().getOrderById(orderId);
        
        if (request.getParameter("action")!=null && request.getParameter("action").equals("cancel")) {
            
            order.setState(4);
            
            new OrderDAO().updateOrder(order);
            
            response.sendRedirect("/customer/order-detail?id=" + orderId);
            return;
        }
        
        List<OrderDetail> listOrderDetail = new OrderDetailDAO().getOrderDetailsByOrderId(orderId);
        double totalCost = 0;
        
        for (OrderDetail orderDetail : listOrderDetail)
            totalCost += orderDetail.getProduct().getPrice() * orderDetail.getQuantity();
        
        request.setAttribute("order", order);
        
        request.setAttribute("user", new UserDAO().getUserByID(order.getUserId()));
        request.setAttribute("orderDetails", listOrderDetail);
        request.setAttribute("totalCost", totalCost);
        
        request.getRequestDispatcher("/view/order-detail.jsp").forward(request, response);
        
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
