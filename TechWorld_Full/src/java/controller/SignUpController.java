/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import controller.SendEmail.EmailConfig;
import dal.UserDAO;
import jakarta.servlet.ServletContext;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import model.User;

/**
 *
 * @author HP
 */
@WebServlet(name = "SignUpController", urlPatterns = {"/signup"})
public class SignUpController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SignUpController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SignUpController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    private boolean validatePhone(String phone) {
        if(phone.length() != 10 || !phone.startsWith("0")) 
            return false;
        for(int i = 0; i < phone.length(); i++)
            if(!Character.isDigit(phone.charAt(i)))
                return false;
        return true;
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        UserDAO userDao = new UserDAO();
        String name = request.getParameter("name");
        String email = request.getParameter("email");
        String code = request.getParameter("code");
        String fullname = request.getParameter("fullname");
        String phone = request.getParameter("phone");
        String address = request.getParameter("address");
        String gender = request.getParameter("gender");
        String password = request.getParameter("pass");
        String repass = request.getParameter("repass");
        Date today = new Date(System.currentTimeMillis());
        final ServletContext servletContext = getServletContext();
        User user = new User(1, name, password, true, email, fullname, gender.equals("1"), phone, address, today, code);

        if (name.equals("") || email.equals("") || password.equals("") || repass.equals("")) {
            request.setAttribute("resMsg", "Xin vui lòng điền vào biểu mẫu hoàn toàn");
        } else if ((name.contains(" ") || name.length() > 50) || (password.contains(" ") || password.length() > 50) || (email.length() > 50)) {
            request.setAttribute("resMsg", "Tên người dùng, mật khẩu, email không được chứa dấu cách và có độ dài lớn hơn 50 ");
        } else if (password.length() < 8) {
            request.setAttribute("resMsg", "Mật khẩu phải dài ít nhất 8 ký tự");
        } else if (!validatePhone(phone)) {
            request.setAttribute("resMsg", "Số điện thoại chỉ bao gồm 10 chữ số và bắt đầu bằng 0");
        }
        else {
            if (userDao.checkExistUsername(name)) {
                request.setAttribute("resMsg", "Tên người dùng đã tồn tại");
            } else if (userDao.checkExistEmail(email)) {
                request.setAttribute("resMsg", "Email đã tồn tại");
            } else if (!password.equals(repass)) {
                request.setAttribute("resMsg", "Mật khẩu phải giống với Repassword");
            } else {
                EmailConfig ec = new EmailConfig();
                String cd = ec.randomCode();
                request.getSession().setAttribute("gui", user);
                servletContext.setAttribute("code", cd);

                try {
                    ec.SendEmail(email, "Tech World sent you OTP", cd);
                } catch (MessagingException ex) {
                    ex.printStackTrace();
                }
                TimerTask removeVariableTask = new TimerTask() {
                    @Override
                    public void run() {
                        servletContext.removeAttribute("code");
                    }
                };

                Timer timer = new Timer();
                timer.schedule(removeVariableTask, 60 * 1000);
                request.getRequestDispatcher("view/verify.jsp").forward(request, response);
                return; // Chấm dứt thực thi để tránh tiếp tục chuyển hướng đến "login.jsp"
            }
        }

// Chỉ chuyển hướng đến "login.jsp" khi tất cả các điều kiện đều không thỏa mãn
        request.getRequestDispatcher("view/login.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
