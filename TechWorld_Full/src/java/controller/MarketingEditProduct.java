/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.BrandDAO;
import dal.CategoryDAO;
import dal.ProductDAO;
import dal.ProductImageDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;
import model.Product;
import model.ProductImage;

/**
 *
 * @author izayo
 */
@WebServlet(name = "MarketingEditProduct", urlPatterns = {"/marketing/settings/edit"})
public class MarketingEditProduct extends HttpServlet {

    private static final String VIEW_PATH = "/view/marketing-productdetails.jsp";
    private final CategoryDAO categoryDAO = new CategoryDAO();
    private final BrandDAO brandDAO = new BrandDAO();
    private final ProductDAO productDAO = new ProductDAO();
    private final ProductImageDAO productImageDAO = new ProductImageDAO();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MarketingEditProduct</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MarketingEditProduct at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        Map<String, Object> ProductDetails = productDAO.getProductWithImageByProductId(id);
        Product p = (Product) ProductDetails.get("product");
        String brandName = (String) ProductDetails.get("brandName");
        String categoryName = (String) ProductDetails.get("categoryName");
        List<ProductImage> imageUrls = new ProductImageDAO().getAllImageOfProductById(id);

        request.setAttribute("product", p);
        request.setAttribute("productImage", imageUrls);
        request.setAttribute("brandName", brandName);
        request.setAttribute("categoryName", categoryName);
        request.getRequestDispatcher("/view/marketing-editform.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int id = Integer.parseInt(request.getParameter("id"));
        String productName = request.getParameter("name");
        String categoryName = request.getParameter("categoryName");
        String brandName = request.getParameter("brandName");
        int brandId = brandDAO.getBrandIdbyBrandName(brandName);
        int categoryId = categoryDAO.getCategoryIdbyCategoryName(categoryName);
        String description = request.getParameter("description");
        int quantityInStock = Integer.parseInt(request.getParameter("quantityInStock"));
        double price = Double.parseDouble(request.getParameter("price"));
        double discount = Double.parseDouble(request.getParameter("discount"));
        boolean status = Boolean.parseBoolean(request.getParameter("iActive"));

        if (quantityInStock < 0) {
            request.setAttribute("error", "Số lượng cần phải lớn hơn hoặc bằng 0");
            Map<String, Object> ProductDetails = productDAO.getProductWithImageByProductId(id);
            Product p = (Product) ProductDetails.get("product");
            String brandName_ = (String) ProductDetails.get("brandName");
            String categoryName_ = (String) ProductDetails.get("categoryName");
            List<ProductImage> imageUrls = new ProductImageDAO().getAllImageOfProductById(id);

            request.setAttribute("product", p);
            request.setAttribute("productImage", imageUrls);
            request.setAttribute("brandName", brandName_);
            request.setAttribute("categoryName", categoryName_);
            request.getRequestDispatcher("/view/marketing-editform.jsp?id=" + id).forward(request, response);
            return;
        }
        if (price <= 0) {
            request.setAttribute("error", "Giá cần phải lớn hơn 0");
            Map<String, Object> ProductDetails = productDAO.getProductWithImageByProductId(id);
            Product p = (Product) ProductDetails.get("product");
            String brandName_ = (String) ProductDetails.get("brandName");
            String categoryName_ = (String) ProductDetails.get("categoryName");
            List<ProductImage> imageUrls = new ProductImageDAO().getAllImageOfProductById(id);

            request.setAttribute("produ ct", p);
            request.setAttribute("productImage", imageUrls);
            request.setAttribute("brandName", brandName_);
            request.setAttribute("categoryName", categoryName_);
            request.getRequestDispatcher("/view/marketing-editform.jsp?id=" + id).forward(request, response);
            return;
        }
        if (discount < 0 || discount > 100) {
            request.setAttribute("error", "Giảm giá cần phải trong khoảng từ 0 đến 100");
            Map<String, Object> ProductDetails = productDAO.getProductWithImageByProductId(id);
            Product p = (Product) ProductDetails.get("product");
            String brandName_ = (String) ProductDetails.get("brandName");
            String categoryName_ = (String) ProductDetails.get("categoryName");
            List<ProductImage> imageUrls = new ProductImageDAO().getAllImageOfProductById(id);

            request.setAttribute("product", p);
            request.setAttribute("productImage", imageUrls);
            request.setAttribute("brandName", brandName_);
            request.setAttribute("categoryName", categoryName_);
            request.getRequestDispatcher("/view/marketing-editform.jsp?id=" + id).forward(request, response);
            return;
        }

        // Lấy ảnh được chọn từ dropdown
        String selectedImageUrl = request.getParameter("selectedImage");

        // Cập nhật thông tin sản phẩm
        Product product = new Product();
        product.setProductName(productName);

        product.setBrandId(brandId);
        product.setCategoryId(categoryId);
        product.setDescription(description);
        product.setQuantityInStock(quantityInStock);
        product.setPrice(price);
        product.setDiscount(discount);
        product.setStatus(status);
        product.setProductId(id);

        // Gọi phương thức cập nhật sản phẩm của DAO và nhận kết quả trả về
        int updateResult = productDAO.updateProduct(product);

        // Kiểm tra kết quả của việc cập nhật sản phẩm
        if (updateResult > 0) {

            ProductImage current = new ProductImageDAO().getAllImageOfProductById(id).get(0);
            ProductImage newImage = new ProductImageDAO().getImageById(Integer.parseInt(request.getParameter("selectedImage")));

            String temp = current.getImageUrl();
            current.setImageUrl(newImage.getImageUrl());
            newImage.setImageUrl(temp);

            new ProductImageDAO().updateProductImage(newImage);
            new ProductImageDAO().updateProductImage(current);

        }
        if (quantityInStock > 0 && price > 0 && discount > 0 && discount <= 100 && updateResult > 0) {
            Map<String, Object> ProductDetails = productDAO.getProductWithImageByProductId(id);
            Product p = (Product) ProductDetails.get("product");
            String brandName_ = (String) ProductDetails.get("brandName");
            String categoryName_ = (String) ProductDetails.get("categoryName");
            List<ProductImage> imageUrls = new ProductImageDAO().getAllImageOfProductById(id);

            request.setAttribute("product", p);
            request.setAttribute("productImage", imageUrls);
            request.setAttribute("brandName", brandName_);
            request.setAttribute("categoryName", categoryName_);

            request.setAttribute("success", "Cập nhật sản phẩm thành công");
            request.getRequestDispatcher("/view/marketing-editform.jsp").forward(request, response);
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
