/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller;


import dal.ProductDAO;
import dal.ProductImageDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import model.Product;
import model.ProductImage;
import model.ProductWithImage;

/**
 *
 * @author izayo
 */
@WebServlet(name="MarketingViewProductDetails", urlPatterns={"/marketing/settings/view"})
public class MarketingViewProductDetails extends HttpServlet {
    private static final String VIEW_PATH = "/view/marketing-productdetails.jsp";
    
    private final ProductImageDAO productImageDAO = new ProductImageDAO();
    private final ProductDAO productDAO = new ProductDAO();
    
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MarketingViewProductDetails</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MarketingViewProductDetails at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        // join information from 2 tables ProductDAO and ProductImageDAO
        List<ProductWithImage> productsWithImages = joinProductAndImage();
        
        // take information from 2 joined table into an attribute
        request.setAttribute("productsWithImages", productsWithImages);
        
        // redirect to JSP to show up products information
        request.getRequestDispatcher(VIEW_PATH).forward(request, response);
    } 
    private List<ProductWithImage> joinProductAndImage() {
        List<ProductWithImage> productsWithImages = new ArrayList<>();
        
        // take product list from product table
        List<Product> products = productDAO.getAllProducts();
        
        for (Product product : products) {
            // take image
            List<ProductImage> images = productImageDAO.getAllImageOfProductById(product.getProductId());
            
            // initialized variable to save products information and image
            
            String imageUrl="";
            if (!images.isEmpty()){
                imageUrl = images.get(0).getImageUrl();
            }
            // initialized a variable ProductWithImage which contains products information and products image
            ProductWithImage productWithImages = new ProductWithImage(
                    product.getProductId(),
                    imageUrl,
                    product.getProductName(),
                    product.getCategoryId(),
                    product.getDescription(),
                    product.getQuantityInStock(),
                    product.getPrice(),
                    product.getDiscount(),
                    product.isStatus()
                    
            );
            // add ProductWithImage into list
            productsWithImages.add(productWithImages);
        }
        return productsWithImages;
    }

    /** 
     * Handles the HTTP <code>POST</code> meth  od.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
      
     int id = Integer.parseInt(request.getParameter("productId"));
     //ý anh muon hoi la cai id nay dang tra ve gi? no no, gia tri cua no co, tuy id a, null cmnl a?, hien tai dang null do a
     //param sai ah :thinking:
     Map<String, Object> ProductDetails = productDAO.getProductWithImageByProductId(id);
     Product p = (Product) ProductDetails.get("product");
     String brandName= (String) ProductDetails.get("brandName");
     String categoryName =(String) ProductDetails.get("categoryName");
     List<String> imageUrls = (List<String>) ProductDetails.get("productImage");
     
     request.setAttribute("product", p);
     request.setAttribute("productImage",imageUrls);
     request.setAttribute("brandName",brandName);
     request.setAttribute("categoryName",categoryName);
     request.getRequestDispatcher("/view/marketing-settingform.jsp").forward(request, response);
     
    }
    //u sure?
    //sao biet no di vao doPost? nvm im blind
    //noi chung la chi dang mat id thoi a? vang, co id thi no fill het cac thong tin kia.
    
    

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
}
