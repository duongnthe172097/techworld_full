/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller;

import dal.CartDAO;
import dal.CartItemDAO;
import dal.OrderDAO;
import dal.OrderDetailDAO;
import dal.ProductDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.Date;
import java.util.List;
import model.CartItem;
import model.Order;
import model.OrderDetail;
import model.Product;

/**
 *
 * @author lvhn1
 */
@WebServlet(name="OrderController", urlPatterns={"/customer/order"})
public class OrderController extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet OrderController</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet OrderController at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.sendRedirect("my-order");
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        
        int cartId = Integer.parseInt(request.getParameter("cartId"));
        int userId = Integer.parseInt(request.getParameter("userId"));
        int deliveryId = Integer.parseInt(request.getParameter("deliveryId"));
        
        // create order
        Order order = new Order();
        
        order.setDeliveryId(deliveryId);
        order.setUserId(userId);
        order.setOrderDate(new Date(new java.util.Date().getTime()));
        order.setState(3);
        
        // get item in cart
        List<CartItem> listCartItem = new CartItemDAO().getCartItemsByCartId(cartId);
        
        // check exist item
        if (listCartItem.isEmpty()) {
            response.sendRedirect("cartController?error");
            return;
        }
        
        OrderDAO orderDAO = new OrderDAO();
        
        // add order to db
        order = orderDAO.addOrder(order);
        
        // add order detail
        for (CartItem cartItem : listCartItem) {
            
            OrderDetail orderDetail = new OrderDetail();
            
            orderDetail.setProductId(cartItem.getProductId());
            orderDetail.setQuantity(cartItem.getQuantity());
            orderDetail.setOrderId(order.getOrderId());
            
            new OrderDetailDAO().addOrderDetail(orderDetail);
            
        }
        
        // clear cart
        new CartItemDAO().deleteCartItemsByCartId(cartId);
        new CartDAO().deleteCart(cartId);
        
        // redirect to payment
        response.sendRedirect(request.getContextPath()+ "/customer/my-order");
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
