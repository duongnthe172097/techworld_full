/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.SettingDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Setting;

/**
 *
 * @author ns
 */
@WebServlet(name = "AdminDeleteSetting", urlPatterns = {"/admin/settings/delete"})
public class AdminDeleteSetting extends HttpServlet {

    private final SettingDAO settingDAO = new SettingDAO();

    private final AdminViewSettings adminViewSettings = new AdminViewSettings();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        Setting setting = settingDAO.getSettingById(id);
        if (setting == null) {
            request.setAttribute("deleteFailed", "Delete setting failed. Setting does not exist!");
            adminViewSettings.doGet(request, response);
            return;
        }
        Setting deletedSetting = settingDAO.deleteSetting(setting);
        if (deletedSetting == null) {
            request.setAttribute("deleteFailed", "Delete setting failed. Please try again!");
            adminViewSettings.doGet(request, response);
            return;
        }
        request.setAttribute("deleteSuccess", "Deleted setting successfully!");
        adminViewSettings.doGet(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

}
