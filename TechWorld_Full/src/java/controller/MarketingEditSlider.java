/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.SliderDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import model.Slider;
import utils.ImageUploader;

/**
 *
 * @author Admin
 */
@MultipartConfig()
@WebServlet(name = "MarketingEditSlider", urlPatterns = {"/marketing/marketing-editslider"})
public class MarketingEditSlider extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MarketingEditSlider</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MarketingEditSlider at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        SliderDAO sliderDAO = new SliderDAO();
        request.setAttribute("isEdit", true);
        int id = Integer.parseInt(request.getParameter("id").trim());
        Slider slider = sliderDAO.getSliderByID(id);
        if (slider == null) {
            response.sendRedirect(request.getContextPath() + "/view-slider");
            return;
        }
        request.setAttribute("oldId", id);
        request.setAttribute("slider", slider);
        request.getRequestDispatcher("view/slider-details.jsp").forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        SliderDAO sliderDAO = new SliderDAO();
        int slider_id = Integer.parseInt(request.getParameter("slider_id"));
        String title = request.getParameter("title");
        boolean status = request.getParameter("status").equals("1");
        String notes = request.getParameter("notes");
        Part part = request.getPart("imagePath");

        Slider slider = sliderDAO.getSliderByID(slider_id);
        slider.setTitle(title);
        slider.setStatus(status);
        slider.setNote(notes);
        if (part != null) {
            ImageUploader imageUploader = new ImageUploader();
            slider.setImage(imageUploader.uploadSliderImage(request, part, slider_id));
        }
        slider = sliderDAO.updateSlider(slider);
       
        request.setAttribute("sliderdetail", slider);
        request.setAttribute("updateSuccess", true);
        request.getRequestDispatcher("view/slidersList.jsp").forward(request, response);

//        // Lấy các thông tin từ request
//        int sliderId = Integer.parseInt(request.getParameter("slider_id"));
//        String title = request.getParameter("title");
//        boolean status = request.getParameter("status").equals("1");
//        String notes = request.getParameter("notes");
//        Part part = request.getPart("imagePath");
//
//        // Tạo đối tượng SliderDAO để thực hiện các thao tác với CSDL
//        SliderDAO sliderDAO = new SliderDAO();
//
//        // Lấy thông tin slider cần chỉnh sửa từ CSDL
//        Slider slider = sliderDAO.getSliderByID(sliderId);
//
//        // Cập nhật thông tin mới
//        slider.setTitle(title);
//        slider.setStatus(status);
//        slider.setNote(notes);
//
//        // Nếu người dùng chọn ảnh mới
//        if (part != null && part.getSize() > 0) {
//            // Upload ảnh mới và lưu đường dẫn vào slider
//            ImageUploader imageUploader = new ImageUploader();
//            String imagePath = imageUploader.uploadSliderImage(request, part, sliderId);
//            slider.setImage(imagePath);
//        }
//
//        // Cập nhật slider trong CSDL
//        sliderDAO.updateSlider(slider);
//        
//          request.setAttribute("updateSuccess", true);
//        // Chuyển hướng về trang chi tiết slider
//        response.sendRedirect(request.getContextPath() + "/view-sliderdetails?slider_id=" + sliderId);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
