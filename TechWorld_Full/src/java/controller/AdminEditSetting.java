/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.SettingDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Setting;

/**
 *
 * @author ns
 */
@WebServlet(name = "AdminEditSetting", urlPatterns = {"/admin/settings/edit"})
public class AdminEditSetting extends HttpServlet {

    private static final String FORM_PATH = "/view/setting-form.jsp";

    private final SettingDAO settingDAO = new SettingDAO();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setAttribute("isEdit", true);

        int id = Integer.parseInt(request.getParameter("id").trim());
        Setting setting = settingDAO.getSettingById(id);
        if (setting == null) {
            response.sendRedirect(request.getContextPath() + "/admin/settings/view");
            return;
        }
        request.setAttribute("oldId", id);
        request.setAttribute("setting", setting);
        request.getRequestDispatcher(FORM_PATH).forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setAttribute("isEdit", true);

        int oldId = Integer.parseInt(request.getParameter("oldId"));

        int id = Integer.parseInt(request.getParameter("id"));
        String name = request.getParameter("name").trim();
        String description = request.getParameter("description").trim();
        String value = request.getParameter("value");
        String type = request.getParameter("brandId");
        Boolean iActive = Boolean.valueOf(request.getParameter("isActive").trim());
        Setting newSetting = new Setting(id, name ,description , value, type, iActive);

        request.setAttribute("oldId", id);
        request.setAttribute("setting", newSetting);

        if (oldId != id && settingDAO.isIdExisted(id)) {
            request.setAttribute("error", "Id is already existed. Please choose another id!");
            request.getRequestDispatcher(FORM_PATH).forward(request, response);
            return;
        }
        int updatedSetting = settingDAO.updateSetting(oldId, newSetting);
        if (updatedSetting <= 0) {
            request.setAttribute("error", "Editing failed. Please try again!");
            request.getRequestDispatcher(FORM_PATH).forward(request, response);
            return;
        }
        request.setAttribute("success", "Updated setting successfully!");
        request.getRequestDispatcher(FORM_PATH).forward(request, response);
    }
}
