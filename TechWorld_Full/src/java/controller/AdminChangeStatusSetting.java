/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.BrandDAO;
import dal.CategoryDAO;
import dal.SettingDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Setting;

/**
 *
 * @author ns
 */
@WebServlet(name = "AdminChangeStatusSetting", urlPatterns = {"/admin/settings/change-status"})
public class AdminChangeStatusSetting extends HttpServlet {

    private final SettingDAO settingDAO = new SettingDAO();
    private final BrandDAO brandDAO = new BrandDAO();
    private final AdminViewSettings adminViewSettings = new AdminViewSettings();
    private final CategoryDAO categoryDAO = new CategoryDAO();
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        Setting setting = settingDAO.getSettingById(id);
//        int isSuccessSetting = settingDAO.updateSettingStatus(id, setting.isIsActive());
        int isSuccessSetting=0;
        int isSuccessTable = 0;
        if (setting == null) {
            request.setAttribute("deleteFailed", "Change setting status failed. Setting does not exist!");
            adminViewSettings.doGet(request, response);
            return;
        }
       
        if(setting.getType().contains("Brand")) {
            if (setting.isIsActive()==false) {
                isSuccessTable = brandDAO.updateBrandStatusById(Integer.parseInt(setting.getValue()), true);
                isSuccessSetting = settingDAO.updateSettingStatus(id, true);
            }
            else if ( setting.isIsActive() == true ) {
                isSuccessTable = brandDAO.updateBrandStatusById(Integer.parseInt(setting.getValue()),false);
                isSuccessSetting = settingDAO.updateSettingStatus(id, false);
                
            }
        }
            
            
            if (setting.getType().contains("Category")){
                if (setting.isIsActive()==false) {
                    isSuccessTable = categoryDAO.updateCategoryStatusById(Integer.parseInt(setting.getValue()), true);
                    isSuccessSetting = settingDAO.updateSettingStatus(id, true);
                }
            
                else if (setting.isIsActive()==true) {
                isSuccessTable = categoryDAO.updateCategoryStatusById(Integer.parseInt(setting.getValue()), false);
                isSuccessSetting = settingDAO.updateSettingStatus(id, false);
            }
            }                           
                
        if (isSuccessSetting <= 0 || isSuccessTable <=0) {
            request.setAttribute("deleteFailed", "Change setting status failed. Please try again!");
            adminViewSettings.doGet(request, response);
            return;
        }
        request.setAttribute("deleteSuccess", "Changed setting status successfully!");
        adminViewSettings.doGet(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }
}
