/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.ProductDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;
import model.Product;
import utils.ImageUploader;

/**
 *
 * @author admin
 */
@MultipartConfig()
@WebServlet(name = "MarketingAddNewProduct", urlPatterns = {"/marketing/product/add"})
public class MarketingAddNewProduct extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MarketingAddNewProduct</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MarketingAddNewProduct at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("/view/mkt-addProduct.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String productName = request.getParameter("productName");
        int quantityInStock = Integer.parseInt(request.getParameter("quantityInStock"));
        int categoryId = Integer.parseInt(request.getParameter("categoryId"));
        int brandId = Integer.parseInt(request.getParameter("brandId"));
        double price = Double.parseDouble(request.getParameter("price"));
        double discount = Double.parseDouble(request.getParameter("discount"));
        String description = request.getParameter("description");
        boolean status = request.getParameter("status").equals("1");
        Date updatedDate = new Date(System.currentTimeMillis());
        ProductDAO productDAO = new ProductDAO();

        //duplicate product
        if (productDAO.checkExistedProduct(productName)) {
            request.setAttribute("error", "Sản phẩm " + productName + " đã tồn tại, vui lòng kiểm tra lại");
            request.getRequestDispatcher("/view/mkt-addProduct.jsp").forward(request, response);
        } else {
            //insert product
            Product product = new Product(productName, categoryId, brandId, quantityInStock, price, discount, description, status, updatedDate);
            productDAO.insertProduct(product);

            //insert image
            Collection<Part> parts = request.getParts();
            Collection<Part> listParts = new ArrayList<>();
            for (Part part : parts) {
                if (part.getName().equals("productImage")) {
                    listParts.add(part);
                }
            }
            ImageUploader imageUploader = new ImageUploader();
            imageUploader.uploadImageProducts(request, listParts, productDAO.getLastProductId());

            request.setAttribute("msg", "Thêm mới sản phẩm " + productName + " thành công");
            request.getRequestDispatcher("/view/mkt-addProduct.jsp").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
