/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.RoleDAO;
import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import model.Role;
import model.User;

/**
 *
 * @author admin
 */
@WebServlet(name = "UserController", urlPatterns = {"/admin/user"})
public class UserController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UserController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UserController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    private boolean isChecked(int id, int[] ids) {
        if (ids == null) {
            return false;
        } else {
            for (int i = 0; i < ids.length; i++) {
                if (ids[i] == id) {
                    return true;
                }
            }
            return false;
        }
    }

    private List<User> sort(List<User> users, String sortType) {
        if (sortType.contains("1")) {
            Collections.sort(users, new Comparator<User>() {
                @Override
                public int compare(User o1, User o2) {
                    return o1.getUserId() - o2.getUserId();
                }

            });
        }
        if (sortType.contains("2")) {
            Collections.sort(users, new Comparator<User>() {
                @Override
                public int compare(User o1, User o2) {
                    return o1.getFullName().compareTo(o2.getFullName());
                }

            });
        }
        if (sortType.contains("3")) {
            Collections.sort(users, new Comparator<User>() {
                @Override
                public int compare(User o1, User o2) {
                    return Boolean.compare(o2.isGender(), o1.isGender());
                }

            });
        }
        if (sortType.contains("4")) {
            Collections.sort(users, new Comparator<User>() {
                @Override
                public int compare(User o1, User o2) {
                    return o1.getEmail().compareTo(o2.getEmail());
                }

            });
        }
        if (sortType.contains("5")) {
            Collections.sort(users, new Comparator<User>() {
                @Override
                public int compare(User o1, User o2) {
                    return o1.getMobile().compareTo(o2.getMobile());
                }

            });
        }
        if (sortType.contains("6")) {
            Collections.sort(users, new Comparator<User>() {
                @Override
                public int compare(User o1, User o2) {
                    return o1.getRoleId() - o2.getRoleId();
                }

            });
        }
        if (sortType.contains("7")) {
            Collections.sort(users, new Comparator<User>() {
                @Override
                public int compare(User o1, User o2) {
                    return Boolean.compare(o2.isStatus(), o1.isStatus());
                }
            });
        }
        return users;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        UserDAO userDAO = new UserDAO();
        String searchKey = request.getParameter("search");
        List<User> allUsers;

        //filter user
        if (searchKey == null || searchKey.equals("")) {
            //filter user by role 
            String[] roleIdRaw = request.getParameterValues("roleId");
            int[] roleIds = null;
            if (roleIdRaw != null) {
                roleIds = new int[roleIdRaw.length];
                for (int i = 0; i < roleIdRaw.length; i++) {
                    roleIds[i] = Integer.parseInt(roleIdRaw[i]);
                }
            }
            //get checked role
            RoleDAO roleDAO = new RoleDAO();
            List<Role> allRoles = roleDAO.getAllRoles();
            boolean[] checkedRoles = new boolean[allRoles.size()];
            for (int i = 0; i < checkedRoles.length; i++) {
                if (isChecked(allRoles.get(i).getRoleId(), roleIds)) {
                    checkedRoles[i] = true;
                } else {
                    checkedRoles[i] = false;
                }
            }
            request.setAttribute("checkedRoles", checkedRoles);

            //filter user by gender
            String gender = request.getParameter("gender");
            request.setAttribute("gender", gender);

            //filter user by status
            String status = request.getParameter("status");
            request.setAttribute("status", status);

            //filter user
            if (gender != null) {
                gender = gender.equals("2") ? null : gender;
            }
            if (status != null) {
                status = status.equals("2") ? null : status;
            }
            allUsers = userDAO.filterUsers(gender, status, roleIds);

            //sort user list
            String sortType = request.getParameter("sortType");
            if (sortType != null) {
                allUsers = sort(allUsers, sortType);
                request.setAttribute("sortType", sortType);
            }
        } 
        //search user
        else {
            allUsers = userDAO.searchUsers(searchKey);

            //sort user list
            String sortType = request.getParameter("sortType");
            if (sortType != null  && !sortType.isEmpty()) {
                allUsers = sort(allUsers, sortType);
                request.setAttribute("sortType", sortType);
            }
            request.setAttribute("searchKey", searchKey);
        }

        //paginate user
        int size = allUsers.size();
        int page, numberPerPage = 12;
        int numberOfPages = (size % numberPerPage == 0) ? (size / numberPerPage) : (size / numberPerPage + 1);
        String pageNo = request.getParameter("page");
        if (pageNo == null) {
            page = 1;
        } else {
            page = Integer.parseInt(pageNo);
        }
        int start, end;
        start = (page - 1) * numberPerPage;
        end = Math.min(page * numberPerPage, size);
        //get list user after paginating
        List<User> paginatedUser = userDAO.getPaginatedUsers(allUsers, start, end);
        request.setAttribute("allUsers", paginatedUser);
        request.setAttribute("page", page);
        request.setAttribute("numberOfPages", numberOfPages);

        request.getRequestDispatcher("/view/user-list.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
