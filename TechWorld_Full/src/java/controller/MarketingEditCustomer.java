/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller;

import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Slider;
import model.User;

/**
 *
 * @author Admin
 */
@WebServlet(name="MarketingEditCustomer", urlPatterns={"/marketing/marketing-editcustomer"})
public class MarketingEditCustomer extends HttpServlet {
   
    
      private final UserDAO userDAO = new UserDAO();
    
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MarketingEditCustomer</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MarketingEditCustomer at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
     //   UserDAO userDAO = new UserDAO();
          int id = Integer.parseInt(request.getParameter("id").trim());
        User customer = userDAO.getUserByID(id);
        if (customer == null) {
            response.sendRedirect(request.getContextPath() + "/marketing-viewcustomer");
            return;
        }
        request.setAttribute("oldId", id);
        request.setAttribute("customer", customer);
        request.getRequestDispatcher("view/customer-details.jsp").forward(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
         request.setAttribute("isEdit", true);
      // UserDAO userDAO = new UserDAO();
       int user_id = Integer.parseInt(request.getParameter("user_id").trim());
      // int role_id = Integer.parseInt(request.getParameter("role_id").trim());
       boolean status = request.getParameter("status").equals("1");
       String email = request.getParameter("email");
       String full_name=  request.getParameter("fullname");
       boolean gender = request.getParameter("gender").equals("1");
       String mobile =  request.getParameter("mobile");
       String address = request.getParameter("address");
       User user = userDAO.getUserByID(user_id);
     //  user.setRoleId(role_id);
       user.setStatus(status);
       user.setEmail(email);
       user.setFullName(full_name);
       user.setGender(gender);
       user.setMobile(mobile);
       user.setAddress(address);
       user = userDAO.updateCustomer(user);
       request.setAttribute("customerdetail", user);
       request.setAttribute("message", "Cập nhật thông tin thành công!");
       request.getRequestDispatcher("/view/customer-details.jsp").forward(request, response);
        
        
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}