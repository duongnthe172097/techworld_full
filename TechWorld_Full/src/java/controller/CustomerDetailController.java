/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.SliderDAO;
import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import model.Slider;
import model.User;

/**
 *
 * @author Admin
 */
@MultipartConfig
@WebServlet(name = "CustomerDetailController", urlPatterns = {"/marketing/customer-detail"})
public class CustomerDetailController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CustomerDetailController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CustomerDetailController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //Retrieve slider ID from request parameter
        int id = Integer.parseInt(request.getParameter("id"));

        //Initialize SliderDAO
        UserDAO userDAO = new UserDAO();

        //Retrieve slider details from database using SliderDAO
        User user = userDAO.getUserByID(id);

        //Set the slider details as request attributes
        request.setAttribute("user", user);

        //Set the slider details as request attributes
        request.getRequestDispatcher("/view/customer-detail.jsp").forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        String full_name = request.getParameter("full_name");
        boolean status = Boolean.parseBoolean(request.getParameter("status"));
        boolean gender = Boolean.parseBoolean(request.getParameter("gender"));
        String mobile = request.getParameter("mobile");
         Part filePart = request.getPart("newImage");
         UserDAO userDAO = new UserDAO();
         
         User user = userDAO.getUserByID(id);
        
         //Update user details
         user.setFullName(full_name);
         user.setStatus(status);
         user.setGender(gender);
         user.setMobile(mobile);
        
         // Check if a new image is uploaded
        if (filePart != null && filePart.getSize() > 0) {
            // Get filename
            String fileName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
            // Generate unique filename
            String uniqueFileName =  fileName;

            // Save the file to a directory on the server
            String uploadPath = getServletContext().getRealPath("") + File.separator + "view" + File.separator + "images" + File.separator + "user";
            File uploadDir = new File(uploadPath);
            if (!uploadDir.exists()) {
                uploadDir.mkdir();
            }
            File file = new File(uploadDir, uniqueFileName);
            try ( InputStream input = filePart.getInputStream()) {
                Files.copy(input, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                e.printStackTrace();
            }

            // Update the image URL in Slider object
            String image =   uniqueFileName;
           user.setImage(image);
        }
        
        if(mobile.length() > 11 || mobile.length() < 9) {
            request.setAttribute("error", "Số điện thoại của bạn cần phải có 10 hoặc 11 chữ số");
            request.getRequestDispatcher("/view/customer-detail.jsp").forward(request, response);
            return;
        }
        
         //Update slider in the database
        boolean updateResult = userDAO.update(user);

        if (updateResult) {
            response.sendRedirect(request.getContextPath() + "/marketing/customer-detail?id=" + id + "&success");
        } else {
            // Handle update failure
            // For example, display an error message
            response.getWriter().println("Failed to update post details.");
        }


         
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
