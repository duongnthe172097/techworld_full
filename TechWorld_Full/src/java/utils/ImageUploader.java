/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package utils;

import dal.PostImageDAO;
import dal.ProductImageDAO;
import dal.SliderDAO;
import dal.UserDAO;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.Part;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;

/**
 *
 * @author admin
 */
public class ImageUploader {

    public String uploadUserImage(HttpServletRequest request, Part part, int userId) {
        String folderName = "\\view\\images\\user";
        String realPath = request.getServletContext().getRealPath("") + "..\\..\\web" + folderName;
        String fileNameRaw = Paths.get(part.getSubmittedFileName()).getFileName().toString();
        String[] fileNameArray = fileNameRaw.split("\\.");
        String fileName = "user" + userId + "." + fileNameArray[1];
        Path filePath = Paths.get(realPath, fileName);

        try {
            // Kiểm tra nếu thư mục chứa hình ảnh không tồn tại, tạo mới
            if (!Files.exists(Paths.get(realPath))) {
                Files.createDirectories(Paths.get(realPath));
            }

            // Ghi tệp hình ảnh mới
            part.write(filePath.toString());

            // Tạo đối tượng UserDAO để thực hiện cập nhật hình ảnh trong cơ sở dữ liệu
            UserDAO userDAO = new UserDAO();

            // Lấy đường dẫn tệp hình ảnh cũ
            String oldImagePath = userDAO.getUserByID(userId).getImage();

            // Nếu có ảnh cũ, xóa tệp ảnh cũ
            if (oldImagePath != null && !oldImagePath.isEmpty()) {
                Path oldFilePath = Paths.get(request.getServletContext().getRealPath(oldImagePath));
                Files.deleteIfExists(oldFilePath);
            }

            // Cập nhật hình ảnh trong cơ sở dữ liệu
            int rowsAffected = userDAO.updateImage(userId, fileName);

            // Kiểm tra nếu cập nhật cơ sở dữ liệu thành công
            if (rowsAffected > 0) {
                // Trả về đường dẫn của hình ảnh đã tải lên
                return fileName;
            } else {
                // Nếu cập nhật cơ sở dữ liệu không thành công, trả về null
                return null;
            }
        } catch (IOException e) {
            e.printStackTrace();
            // Xử lý ngoại lệ và trả về thông báo hoặc thực hiện các bước phù hợp khác
            return null;
        }
    }

    public void uploadImagePosts(HttpServletRequest request, Collection<Part> parts, int postId) {
        String folderName = "\\view\\images\\post";
        String realPath = request.getServletContext().getRealPath("") + "..\\..\\web" + folderName;

        for (Part part : parts) {
            String fileName = part.getSubmittedFileName();
            System.out.println(fileName);

            // Kiểm tra xem tên tệp có null hay không
            if (fileName != null && !fileName.isEmpty()) {
                Path filePath = Paths.get(realPath, fileName);
                System.out.println(filePath.toString());
                try {
                    // Kiểm tra nếu thư mục chứa hình ảnh không tồn tại, tạo mới
                    if (!Files.exists(Paths.get(realPath))) {
                        Files.createDirectories(Paths.get(realPath));
                    }

                    // Ghi tệp hình ảnh mới
                    part.write(filePath.toString());

                    // Tạo đối tượng UserDAO để thực hiện cập nhật hình ảnh trong cơ sở dữ liệu
                    PostImageDAO postImageDAO = new PostImageDAO();
                    postImageDAO.insertPostImage(postId, fileName);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String uploadSliderImage(HttpServletRequest request, Part part, int sliderId) {
        String folderName = "\\view\\images\\slider";
        String realPath = request.getServletContext().getRealPath("") + "..\\..\\web" + folderName;
        String fileName = Paths.get(part.getSubmittedFileName()).getFileName().toString();
        Path filePath = Paths.get(realPath, fileName);

        try {
            // Kiểm tra nếu thư mục chứa hình ảnh không tồn tại, tạo mới
            if (!Files.exists(Paths.get(realPath))) {
                Files.createDirectories(Paths.get(realPath));
            }

            // Ghi tệp hình ảnh mới
            part.write(filePath.toString());

            // Tạo đối tượng UserDAO để thực hiện cập nhật hình ảnh trong cơ sở dữ liệu
            SliderDAO sliderDAO = new SliderDAO();

            // Lấy đường dẫn tệp hình ảnh cũ
            String oldImagePath = sliderDAO.getSliderByID(sliderId).getImage();

            // Nếu có ảnh cũ, xóa tệp ảnh cũ
            if (oldImagePath != null && !oldImagePath.isEmpty()) {
                Path oldFilePath = Paths.get(request.getServletContext().getRealPath(oldImagePath));
                Files.deleteIfExists(oldFilePath);
            }

            // Cập nhật hình ảnh trong cơ sở dữ liệu
            int rowsAffected = sliderDAO.updateImage(sliderId, fileName);

            // Kiểm tra nếu cập nhật cơ sở dữ liệu thành công
            if (rowsAffected > 0) {
                // Trả về đường dẫn của hình ảnh đã tải lên
                return fileName;
            } else {
                // Nếu cập nhật cơ sở dữ liệu không thành công, trả về null
                return null;
            }
        } catch (IOException e) {
            e.printStackTrace();
            // Xử lý ngoại lệ và trả về thông báo hoặc thực hiện các bước phù hợp khác
            return null;
        }
    }

    public void uploadImageProducts(HttpServletRequest request, Collection<Part> parts, int productId) {
        String folderName = "\\view\\images\\product";
        String realPath = request.getServletContext().getRealPath("") + "..\\..\\web" + folderName;

        for (Part part : parts) {
            String fileName = part.getSubmittedFileName();
            System.out.println(fileName);

            // Kiểm tra xem tên tệp có null hay không
            if (fileName != null && !fileName.isEmpty()) {
                Path filePath = Paths.get(realPath, fileName);
                System.out.println(filePath.toString());
                try {
                    // Kiểm tra nếu thư mục chứa hình ảnh không tồn tại, tạo mới
                    if (!Files.exists(Paths.get(realPath))) {
                        Files.createDirectories(Paths.get(realPath));
                    }

                    // Ghi tệp hình ảnh mới
                    part.write(filePath.toString());

                    // Tạo đối tượng UserDAO để thực hiện cập nhật hình ảnh trong cơ sở dữ liệu
                    ProductImageDAO productImageDAO = new ProductImageDAO();
                    productImageDAO.insertProductImage(productId, fileName);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
