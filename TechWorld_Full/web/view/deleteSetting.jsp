<%-- 
    Document   : deleteSetting
    Created on : Jan 17, 2024, 4:03:20 PM
    Author     : izayo
--%>

<%@ page import="java.sql.*"%>
<%@ page import="dal.DBContext"%>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Delete Setting</title>
    <!-- Add your CSS styles if needed -->
</head>
<body>

<%
    DBContext dbContext = new DBContext();
    Connection connection = null;
    PreparedStatement preparedStatement = null;
    
    try {
        connection = dbContext.getConnection();
        int userIdToDelete = Integer.parseInt(request.getParameter("user_id"));

        String sql = "DELETE FROM settings WHERE user_id=?";
        preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setInt(1, userIdToDelete);

        int rowsDeleted = preparedStatement.executeUpdate();

        if (rowsDeleted > 0) {
            out.println("<p>Setting with User ID " + userIdToDelete + " deleted successfully.</p>");
        } else {
            out.println("<p>Setting with User ID " + userIdToDelete + " not found or deletion failed.</p>");
        }
    } catch (SQLException e) {
        e.printStackTrace();
    } finally {
        try {
            if (preparedStatement != null) preparedStatement.close();
            if (connection != null) connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
%>

</body>
</html>
