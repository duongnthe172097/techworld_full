<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import = "model.*" %>
<%@page import = "dal.*" %>
<%@page import = "java.sql.Date" %>
<%@page import = "java.util.*" %>
<%@page import = "java.text.DecimalFormat" %>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Đơn hàng | Sale</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta name="description" content="Developed By M Abdur Rokib Promy">
        <meta name="keywords" content="Admin, Bootstrap 3, Template, Theme, Responsive">
        <!-- bootstrap 3.0.2 -->
        <link href="${pageContext.request.contextPath}/view/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="${pageContext.request.contextPath}/view/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="${pageContext.request.contextPath}/view/css/ionicons.min.css" rel="stylesheet" type="text/css" />

        <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
        <!-- Theme style -->
        <link href="${pageContext.request.contextPath}/view/css/style.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="https://cdn.datatables.net/1.13.7/css/jquery.dataTables.css" />
    </head>

    <%
        //userDAO
        UserDAO userDAO = new UserDAO();
        pageContext.setAttribute("userDAO", userDAO, PageContext.PAGE_SCOPE);
        
        //orderDAO
        OrderDAO orderDAO = new OrderDAO();
        pageContext.setAttribute("orderDAO", orderDAO, PageContext.PAGE_SCOPE);
        
        DecimalFormat decimalFormat = new DecimalFormat("#,###");
        pageContext.setAttribute("decimalFormat", decimalFormat, PageContext.PAGE_SCOPE);
    %>

    <body class="skin-black">

        <style type="text/css">
            #noti-box .alert {
                font-size: 18px;
            }

            .panel-body {
                max-height: 500px;
                overflow-y: auto;
                display: block;
            }

            .newly-feedback {
                max-height: 500px;
                min-height: 450px;
                overflow-y: auto;
                display: block;
            }

            .newly-feedback ul li {
                max-height: 150px;
            }

            .newly-feedback ul li div img {
                height: 10px;
                margin-left: 40px;
                margin-top: -20px;
            }

            .newly-feedback .feedback-info {
                font-size: 10px;
                color: gray;
            }

            .navbar-static-top form {
                padding: 8px;
            }

            section .date-form {
                background-color: #ffffff;
                padding: 20px;
                border-radius: 1px;
            }

            section .date-form div {
                font-size: 20px;
                font-weight: 600;
                margin-bottom: 10px;
            }

            section .date-form input {
                border: 1px solid gainsboro;
                height: 25px;
                padding: 15px;
                margin-right: 20px;
            }
        </style>

        <!-- header logo: style can be found in header.less -->
        <jsp:include page="admin-header.jsp"></jsp:include>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="${pageContext.request.contextPath}/view/img/26115.jpg" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Xin chào, ${sessionScope.user.userName}</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li>
                            <a href="${pageContext.request.contextPath}/sale/dashboard">
                                <i class="fa fa-dashboard"></i> <span>Thống kê</span>
                            </a>
                        </li>
                        <li class="active">
                            <c:if test="${sessionScope.user.roleId == 3}">
                                <c:set var="orderUrl" value="${pageContext.request.contextPath}/sale/order/list"/>
                            </c:if>
                            <c:if test="${sessionScope.user.roleId == 4}">
                                <c:set var="orderUrl" value="${pageContext.request.contextPath}/saleManager/order/list"/>
                            </c:if>
                            <a href="${orderUrl}">
                                <i class="fa fa-gavel"></i> <span>Đơn hàng</span>
                            </a>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <aside class="right-side">

                <!-- Main content -->
                <section class="content">
                    <form action="${pageContext.request.contextPath}/sale/order/list" class="date-form" style="margin-bottom:15px;">
                        <div>Ðơn hàng</div>
                        Từ: <input type="date" name="fromDate" value="${requestScope.fromDate}" onchange="this.form.submit()"/>
                        Đến: <input type="date" name="toDate" value="${requestScope.toDate}" onchange="this.form.submit()"/>
                    </form>

                    <div class="row" style="margin-bottom:5px;">
                        <div class="col-md-12">
                            <header class="panel-heading">
                                Danh sách đơn hàng
                            </header>
                            <!-- <div class="box-header"> -->
                            <!-- <h3 class="box-title">Responsive Hover Table</h3> -->

                            <!-- </div> -->
                            <div class="panel-body table-responsive" style="background-color: #ffffff">
                                <table id="setting-table" class="table cell-border row-border hover stripe">
                                    <thead>
                                        <tr>
                                            <th>Mã đơn</th>
                                            <th>Khách hàng</th>
                                            <th>Ngày đặt</th>
                                            <th>Số lượng</th>
                                            <th>Tổng tiền</th>
                                            <th>Ghi chú</th>
                                            <th>Trạng thái</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach var="order" items="${requestScope.orderList}">
                                            <tr>
                                                <td>${order.orderId}</td>
                                                <td>${userDAO.getUserByID(order.userId).fullName}</td>
                                                <td>${order.orderDate}</td>
                                                <td>${orderDAO.getTotalOfOrderById(order.orderId)[0]}</td>
                                                <td>${orderDAO.getTotalOfOrderById(order.orderId)[1]}</td>
                                                <td>${order.saleNote}</td>
                                                <td>
                                                    <c:choose>
                                                        <c:when test="${order.state eq 1}">
                                                            Đã nhận
                                                        </c:when>
                                                        <c:when test="${order.state eq 2}">
                                                            Đã hủy
                                                        </c:when>
                                                        <c:when test="${order.state eq 3}">
                                                            Đã đặt
                                                        </c:when>
                                                    </c:choose>
                                                </td>
                                                <td>
                                                    <a style="margin-right: 5px; margin-bottom: 5px;" class="btn btn-primary" 
                                                       href="${pageContext.request.contextPath}/sale/order/details?orderId=${order.orderId}">
                                                        Xem chi tiết
                                                    </a>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Mã đơn</th>
                                            <th>Khách hàng</th>
                                            <th>Ngày đặt</th>
                                            <th>Số lượng</th>
                                            <th>Tổng tiền</th>
                                            <th>Ghi chú</th>
                                            <th>Trạng thái</th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div><!-- /.box-body -->
                        </div>
                    </div>   

                    <!-- Main row -->

                    <!-- row end -->
                </section><!-- /.content -->
                <div class="footer-main">
                    Copyright &copy Director, 2014
                </div>
            </aside><!-- /.right-side -->

        </div><!-- ./wrapper -->


        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/jquery.min.js" type="text/javascript"></script>

        <!-- Bootstrap -->
        <script src="${pageContext.request.contextPath}/view/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- Director App -->
        <script src="${pageContext.request.contextPath}/view/js/Director/app.js" type="text/javascript"></script>

        <script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.js"></script>
        <script>
//            $(document).ready(function () {
//                $('#setting-table').DataTable();
//            });

                            var settingTable = new DataTable('#setting-table', {
                                initComplete: function () {
                                    this.api()
                                            .columns()
                                            .every(function () {
                                                let column = this;
                                                let title = column.footer().textContent;

                                                if (title === '')
                                                    return;

                                                // Create input element
                                                let input = document.createElement('input');
                                                input.placeholder = title;
                                                column.footer().replaceChildren(input);

                                                // Event listener for user input
                                                input.addEventListener('keyup', () => {
                                                    if (column.search() !== this.value) {
                                                        column.search(input.value).draw();
                                                    }
                                                });
                                            });
                                }
                            });

//            settingTable.column(4)
//                    .search("^" + $(this).val() + "$", true, false, true)
//                    .draw();

                            $('#setting-table tfoot > tr > th').css("border", "1px solid rgba(0, 0, 0, 0.15)");
                            $('#setting-table').css("border-collapse", "collapse");
                            $('#setting-table tfoot > tr > th > input').addClass('form-control');
                            $('#setting-table tfoot tr').appendTo('#setting-table thead');

        </script>
    </body>
</html>