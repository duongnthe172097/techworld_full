<%-- 
    Document   : admin
    Created on : Jan 19, 2024, 10:57:03 PM
    Author     : admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import = "model.*" %>
<%@page import = "dal.*" %>
<%@page import = "java.sql.Date" %>
<%@page import = "java.util.*" %>
<%@page import = "java.text.DecimalFormat" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Thống kê | Admin</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta name="description" content="Developed By M Abdur Rokib Promy">
        <meta name="keywords" content="Admin, Bootstrap 3, Template, Theme, Responsive">
        <!-- bootstrap 3.0.2 -->
        <link href="${pageContext.request.contextPath}/view/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="${pageContext.request.contextPath}/view/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="${pageContext.request.contextPath}/view/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="${pageContext.request.contextPath}/view/css/morris/morris.css" rel="stylesheet" type="text/css" />
        <!-- jvectormap -->
        <link href="${pageContext.request.contextPath}/view/css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
        <!-- Date Picker -->
        <link href="${pageContext.request.contextPath}/view/css/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
        <!-- fullCalendar -->
        <!-- <link href="${pageContext.request.contextPath}/view/css/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" /> -->
        <!-- Daterange picker -->
        <link href="${pageContext.request.contextPath}/view/css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
        <!-- iCheck for checkboxes and radio inputs -->
        <link href="${pageContext.request.contextPath}/view/css/iCheck/all.css" rel="stylesheet" type="text/css" />
        <!-- bootstrap wysihtml5 - text editor -->
        <!-- <link href="${pageContext.request.contextPath}/view/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" /> -->
        <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
        <!-- Theme style -->
        <link href="${pageContext.request.contextPath}/view/css/style.css" rel="stylesheet" type="text/css" />

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css" integrity="sha512-DTOQO9RWCH3ppGqcWaEA1BIZOC6xxalwEsw9c2QQeAIftl+Vegovlnee1c9QX4TctnWMn13TZye+giMm8e2LwA==" crossorigin="anonymous" referrerpolicy="no-referrer" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.${pageContext.request.contextPath}/view/js/1.3.0/respond.min.js"></script>
          <![endif]-->

    </head>
    <body class="skin-black">

        <%!
            public String limitString(String str, int maxLength) {
                if (str.length() > maxLength) {
                    return str.substring(0, maxLength) + "...";
                }
            return str;
            }
        %>

        <style type="text/css">
            #noti-box .alert {
                font-size: 18px;
            }

            .panel-body {
                max-height: 500px;
                overflow-y: auto;
                display: block;
            }

            .newly-feedback {
                max-height: 500px;
                min-height: 450px;
                overflow-y: auto;
                display: block;
            }

            .newly-feedback ul li {
                max-height: 150px;
            }

            .newly-feedback ul li div img {
                height: 10px;
                margin-left: 40px;
                margin-top: -20px;
            }

            .newly-feedback .feedback-info {
                font-size: 10px;
                color: gray;
            }

            .navbar-static-top form {
                padding: 8px;
            }

            section .date-form {
                background-color: #ffffff;
                padding: 20px;
                border-radius: 1px;
            }

            section .date-form div {
                font-size: 20px;
                font-weight: 600;
                margin-bottom: 10px;
            }

            section .date-form input {
                border: 1px solid gainsboro;
                height: 25px;
                padding: 15px;
                margin-right: 20px;
            }
        </style>

        <%
            
            DecimalFormat decimalFormat = new DecimalFormat("#,###");
            
            String customerType = (String)request.getAttribute("customerType");
            List<User> newlyRegisteredCustomers = (List<User>)request.getAttribute("newlyRegisteredCustomers");
            
            LinkedHashMap<User, String[]> newlyBoughtCustomers = (LinkedHashMap<User, String[]>)request.getAttribute("newlyBoughtCustomers");
            Set<User> userSet = newlyBoughtCustomers.keySet();
            
            LinkedHashMap<Feedback, String[]> newlyFeedbacks = (LinkedHashMap<Feedback, String[]>)request.getAttribute("newlyFeedbacks");
            Set<Feedback> feedbackSet = newlyFeedbacks.keySet();
            
            LinkedHashMap<Category, Double> recentRevenues = (LinkedHashMap<Category, Double>)request.getAttribute("recentRevenues");
            Set<Category> revenueSet = recentRevenues.keySet();
            
            LinkedHashMap<Category, Double> staticsFeedbackOfCategories = (LinkedHashMap<Category, Double>)request.getAttribute("staticsFeedbackOfCategories");
            Set<Category> staticFeedbackSet = staticsFeedbackOfCategories.keySet();
        %>
        <!-- header logo: style can be found in header.less -->
        <jsp:include page="admin-header.jsp"></jsp:include>

            <div class="wrapper row-offcanvas row-offcanvas-left">
                <!-- Left side column. contains the logo and sidebar -->
                <aside class="left-side sidebar-offcanvas">
                    <!-- sidebar: style can be found in sidebar.less -->
                    <section class="sidebar">
                        <!-- Sidebar user panel -->
                        <div class="user-panel">
                            <div class="pull-left image">
                                <img src="${pageContext.request.contextPath}/view/img/26115.jpg" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Xin chào, ${sessionScope.user.userName}</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>

                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="active">
                            <a href="${pageContext.request.contextPath}/admin/admin-dashboard">
                                <i class="fa fa-dashboard"></i> <span>Thống kê</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/admin/user">
                                <i class="fa fa-gavel"></i> <span>Người dùng</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/admin/settings/view">
                                <i class="fa fa-globe"></i> <span>Cài đặt</span>
                            </a>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <aside class="right-side">

                <!-- Main content -->
                <section class="content">
                    <form action="${pageContext.request.contextPath}/admin/admin-dashboard" class="date-form" style="margin-bottom:15px;">
                        <div>Thống kê dữ liệu</div>
                        Từ: <input type="date" name="fromDate" value="${requestScope.fromDate}" onchange="this.form.submit()"/>
                        Đến: <input type="date" name="toDate" value="${requestScope.toDate}" onchange="this.form.submit()"/>
                    </form>

                    <div class="row" style="margin-bottom:5px;">


                        <div class="col-md-3">
                            <div class="sm-st clearfix">
                                <span class="sm-st-icon st-red"><i class="fa-solid fa-money-bill"></i></span>
                                <div class="sm-st-info">
                                    <span>${requestScope.staticOrder[0] + requestScope.staticOrder[1] + requestScope.staticOrder[2]}</span>
                                    Đơn hàng mới
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="sm-st clearfix">
                                <span class="sm-st-icon st-violet"><i class="fa-solid fa-user"></i></span>
                                <div class="sm-st-info">
                                    <span><%=newlyRegisteredCustomers.size()%></span>
                                    Khách hàng mới
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="sm-st clearfix">
                                <span class="sm-st-icon st-blue"><i class="fa-solid fa-bag-shopping"></i></span>
                                <div class="sm-st-info">
                                    <span><%=newlyBoughtCustomers.size()%></span>
                                    Người mua hàng mới
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="sm-st clearfix">
                                <span class="sm-st-icon st-green"><i class="fa-solid fa-money-bill-trend-up"></i></span>
                                <div class="sm-st-info">
                                    <span><%= decimalFormat.format(recentRevenues.values().stream().mapToDouble(Double::doubleValue).sum()) %></span>
                                    Doanh thu
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Main row -->
                    <div class="row">


                        <div class="col-md-9">
                            <section class="panel">
                                <header class="panel-heading">
                                    Doanh thu gần đây: <%= decimalFormat.format(recentRevenues.values().stream().mapToDouble(Double::doubleValue).sum()) %>
                                </header>
                                <div class="panel-body">
                                    <canvas id="linechart" width="600" height="330"></canvas>
                                </div>
                            </section>
                            <!--earning graph end-->
                        </div>
                        <div class="col-lg-3">

                            <!--chat start-->
                            <section class="panel">
                                <header class="panel-heading">
                                    Đơn hàng gần đây (${requestScope.staticOrder[0] + requestScope.staticOrder[1] + requestScope.staticOrder[2]})
                                </header>
                                <div class="panel-body" id="noti-box">
                                    <div class="alert alert-success">
                                        Đơn hàng đã đặt: <strong>${requestScope.staticOrder[2]}</strong>
                                    </div>
                                    <div class="alert alert-block alert-danger"> 
                                        Đơn hàng đã hủy: <strong>${requestScope.staticOrder[1]}</strong>
                                    </div>
                                    <div class="alert alert-warning">
                                        Đơn hàng đã giao: <strong>${requestScope.staticOrder[0]}</strong>
                                    </div>
                                </div>
                            </section>

                        </div>


                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Newly customer -->
                            <section class="panel">
                                <header class="panel-heading">
                                    <form action="${pageContext.request.contextPath}/admin/admin-dashboard">
                                        <input type="hidden" name="fromDate" value="${requestScope.fromDate}"/>
                                        <input type="hidden" name="toDate" value="${requestScope.toDate}"/>
                                        <select style="border:none" name="customerType" onchange="this.form.submit()">
                                            <option 
                                                value="registered"
                                                ${requestScope.customerType.equals("registered") ? ' selected' : ''}
                                                >
                                                KHÁCH HÀNG MỚI (<%=newlyRegisteredCustomers.size()%>)
                                            </option>
                                            <option 
                                                value="bought"
                                                ${requestScope.customerType.equals("bought") ? ' selected' : ''}
                                                >
                                                NGƯỜI MUA HÀNG MỚI (<%=newlyBoughtCustomers.size()%>)
                                            </option>
                                        </select>
                                    </form>
                                </header>
                                <div class="panel-body table-responsive">
                                    <%if(customerType.equals("registered")){%>
                                    <%if(newlyRegisteredCustomers.size() > 0) {%>
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Tên</th>
                                                <th>Email</th>
                                                <th>Điện thoại</th>
                                                <th>Địa chỉ</th>
                                                <th>Giới tính</th>
                                                <th>Ngày đăng ký</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%for(int i = 0; i < newlyRegisteredCustomers.size(); i++) {%>
                                            <tr>
                                                <td><%=i + 1%></td>
                                                <td><%=newlyRegisteredCustomers.get(i).getFullName()%></td>
                                                <td><%=newlyRegisteredCustomers.get(i).getEmail()%></td>
                                                <td><%=newlyRegisteredCustomers.get(i).getMobile()%></td>
                                                <td><%=limitString(newlyRegisteredCustomers.get(i).getAddress(), 100)%></td>
                                                <td><span class="badge badge-info"></span><%=newlyRegisteredCustomers.get(i).isGender() ? "Nam" : "Nữ"%></td>
                                                <td><span class="badge badge-info"></span><%=newlyRegisteredCustomers.get(i).getUpdatedDate()%></td>
                                            </tr>
                                            <%}%>
                                        </tbody>
                                    </table>
                                    <%}else {%>
                                    Không có khách hàng mới.
                                    <%}%>
                                </div>
                                <%}%>

                                <!-- Newly bought -->
                                <%if(customerType.equals("bought")){%>
                                <div class="panel-body table-responsive">
                                    <%if(userSet.size() > 0) {%>
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Tên</th>
                                                <th>Điện thoại</th>
                                                <th>Ngày đặt hàng</th>
                                                <th>Tình trạng đơn</th>
                                                <th>Số lượng</th>
                                                <th>Tổng số tiền</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%int indexUserSet = 1;%>
                                            <%for(User u : userSet) {%>
                                            <tr>
                                                <td><%=indexUserSet%></td>
                                                <td><%=u.getFullName()%></td>
                                                <td><%=u.getMobile()%></td>
                                                <td><%=newlyBoughtCustomers.get(u)[0]%></td>
                                                <td>
                                                    <%=newlyBoughtCustomers.get(u)[1].equals("1") ? "Đã đặt" : ""%>
                                                    <%=newlyBoughtCustomers.get(u)[1].equals("2") ? "Đã hủy" : ""%>
                                                    <%=newlyBoughtCustomers.get(u)[1].equals("3") ? "Đã giao" : ""%>
                                                </td>
                                                <td><%=newlyBoughtCustomers.get(u)[2]%></td>
                                                <td><%=decimalFormat.format(Double.parseDouble(newlyBoughtCustomers.get(u)[3]))%></td>
                                                <%indexUserSet++;%>
                                            </tr>
                                            <%}%>
                                        </tbody>
                                    </table>
                                    <%}else {%>
                                    Không có người mua hàng mới.
                                    <%}%>
                                </div>
                                <%}%>
                            </section>

                        </div>

                        <div class="col-md-4">
                            <div class="panel newly-feedback">
                                <header class="panel-heading">
                                    Đánh giá gần đây (<%=newlyFeedbacks.size()%>)
                                </header>
                                <%if(feedbackSet.size() > 0) {%>
                                <ul class="list-group teammates">
                                    <%for(Feedback f : feedbackSet){%>
                                    <li class="list-group-item">
                                        <span>
                                            <a href=""><img src="${pageContext.request.contextPath}/view/images/user/<%=newlyFeedbacks.get(f)[1]%>" width="25" height="25"></a>
                                            <strong><%=newlyFeedbacks.get(f)[0]%></strong>
                                        </span> 
                                        <div>
                                            <img src="${pageContext.request.contextPath}/view/images/feedback/<%=f.getRatedStar()%>.png"/>
                                        </div>
                                        <p class="feedback-info"><%=f.getUpdatedDate()%> | Mặt hàng: <%=newlyFeedbacks.get(f)[2]%></p>
                                        <p class="feedback-content"><%=limitString(f.getContent(),100 )%></p>
                                    </li>
                                    <%}%>
                                </ul>
                                <%} else{%>
                                Không có đánh giá mới.
                                <%}%>
                            </div>
                        </div>

                        <div class="col-md-8">
                            <section class="panel">
                                <header class="panel-heading">
                                    Thống kê đánh giá sản phẩm
                                </header>
                                <div class="panel-body">
                                    <canvas id="statics-feedback" width="600" height="330"></canvas>
                                </div>
                            </section>
                        </div>

                        <div>

                        </div>
                    </div>
                    <!-- row end -->
                </section><!-- /.content -->
                <div class="footer-main">
                    Copyright &copy MinhNH, 2024
                </div>
            </aside><!-- /.right-side -->

        </div><!-- ./wrapper -->


        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/${pageContext.request.contextPath}/view/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/jquery.min.js" type="text/javascript"></script>

        <!-- jQuery UI 1.10.3 -->
        <script src="${pageContext.request.contextPath}/view/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="${pageContext.request.contextPath}/view/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- daterangepicker -->
        <script src="${pageContext.request.contextPath}/view/js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>

        <script src="${pageContext.request.contextPath}/view/js/plugins/chart.js" type="text/javascript"></script>

        <!-- datepicker
        <script src="${pageContext.request.contextPath}/view/js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>-->
        <!-- Bootstrap WYSIHTML5
        <script src="${pageContext.request.contextPath}/view/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>-->
        <!-- iCheck -->
        <script src="${pageContext.request.contextPath}/view/js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
        <!-- calendar -->
        <script src="${pageContext.request.contextPath}/view/js/plugins/fullcalendar/fullcalendar.js" type="text/javascript"></script>

        <!-- Director App -->
        <script src="${pageContext.request.contextPath}/view/js/Director/app.js" type="text/javascript"></script>

        <!-- Director dashboard demo (This is only for demo purposes) -->
        <script src="${pageContext.request.contextPath}/view/js/Director/dashboard.js" type="text/javascript"></script>

        <!-- Director for demo purposes -->
        <script type="text/javascript">
                                            $('input').on('ifChecked', function (event) {
                                                // var element = $(this).parent().find('input:checkbox:first');
                                                // element.parent().parent().parent().addClass('highlight');
                                                $(this).parents('li').addClass("task-done");
                                                console.log('ok');
                                            });
                                            $('input').on('ifUnchecked', function (event) {
                                                // var element = $(this).parent().find('input:checkbox:first');
                                                // element.parent().parent().parent().removeClass('highlight');
                                                $(this).parents('li').removeClass("task-done");
                                                console.log('not');
                                            });

        </script>
        <script>
            $('#noti-box').slimScroll({
                height: '400px',
                size: '5px',
                BorderRadius: '5px'
            });

            $('input[type="checkbox"].flat-grey, input[type="radio"].flat-grey').iCheck({
                checkboxClass: 'icheckbox_flat-grey',
                radioClass: 'iradio_flat-grey'
            });
        </script>
        <script type="text/javascript">
            <%
                List<Category> categoryListForRevenue = new ArrayList<>(revenueSet);
                String[] categoryNamesForRevenues = new String[categoryListForRevenue.size()];
                double[] revenues = new double[categoryListForRevenue.size()];

                for (int i = 0; i < categoryListForRevenue.size(); i++) {
                    Category category = categoryListForRevenue.get(i);
                    categoryNamesForRevenues[i] = "\"" + category.getCategoryName() + "\"";
                    revenues[i] = recentRevenues.get(category);
                }

                // Chuyển danh sách thành chuỗi JSON
                String jsonCategoryNamesForRevenues = "[" + String.join(",", categoryNamesForRevenues) + "]";
                String jsonRevenues = Arrays.toString(revenues);
            %>

            $(function () {
                "use strict";
                // BAR CHART
                var data = {
                    labels: <%= jsonCategoryNamesForRevenues %>,
                    datasets: [
                        {
                            label: "Revenue",
                            fillColor: "rgba(220,220,220,0.2)",
                            strokeColor: "rgba(220,220,220,1)",
                            pointColor: "rgba(220,220,220,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(220,220,220,1)",
                            data: <%= jsonRevenues %>
                        },
                                // Các datasets khác (nếu cần)
                    ]
                };

                new Chart(document.getElementById("linechart").getContext("2d")).Line(data, {
                    responsive: true,
                    maintainAspectRatio: false,
                });
            });
            // Chart.defaults.global.responsive = true;
        </script>

        <script type="text/javascript">
            <%
                List<Category> categoryListForFeedback = new ArrayList<>(staticFeedbackSet);
                String[] categoryNamesForFeedbacks = new String[categoryListForFeedback.size()];
                double[] stars = new double[categoryListForFeedback.size()];

                for (int i = 0; i < categoryListForFeedback.size(); i++) {
                    Category category = categoryListForFeedback.get(i);
                    categoryNamesForFeedbacks[i] = "\"" + category.getCategoryName() + "\"";
                    stars[i] = staticsFeedbackOfCategories.get(category);
                }

                // Chuyển danh sách thành chuỗi JSON
                String jsonCategoryNamesForFeedback = "[" + String.join(",", categoryNamesForFeedbacks) + "]";
                String jsonStars = Arrays.toString(stars);
            %>

            $(function () {
                "use strict";
                // BAR CHART
                var data = {
                    labels: <%= jsonCategoryNamesForFeedback %>,
                    datasets: [
                        {
                            label: "Stars",
                            fillColor: "rgba(220,220,220,0.2)",
                            strokeColor: "rgba(220,220,220,1)",
                            pointColor: "rgba(220,220,220,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(220,220,220,1)",
                            data: <%= jsonStars %>
                        },
                                // Các datasets khác (nếu cần)
                    ]
                };

                var options = {
                    scales: {
                        y: {
                            beginAtZero: true,
                            max: 5 // Đặt giá trị tối đa là 5, giả sử đánh giá từ 0 đến 5
                        }
                    }
                };

                new Chart(document.getElementById("statics-feedback").getContext("2d")).Bar(data, {
                    responsive: true,
                    maintainAspectRatio: false,
                    options: options
                });
            });
        </script>

    </body>
</html>
