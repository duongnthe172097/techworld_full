<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import = "model.*" %>
<%@page import = "dal.*" %>
<%@page import = "java.sql.Date" %>
<%@page import = "java.util.*" %>
<%@page import = "java.text.DecimalFormat" %>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Tech World | Admin Dashboard</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta name="description" content="Developed By M Abdur Rokib Promy">
        <meta name="keywords" content="Admin, Bootstrap 3, Template, Theme, Responsive">
        <!-- bootstrap 3.0.2 -->
        <link href="${pageContext.request.contextPath}/view/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="${pageContext.request.contextPath}/view/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="${pageContext.request.contextPath}/view/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="${pageContext.request.contextPath}/view/css/morris/morris.css" rel="stylesheet" type="text/css" />
        <!-- jvectormap -->
        <link href="${pageContext.request.contextPath}/view/css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
        <!-- Date Picker -->
        <link href="${pageContext.request.contextPath}/view/css/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
        <!-- fullCalendar -->
        <!-- <link href="${pageContext.request.contextPath}/view/css/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" /> -->
        <!-- Daterange picker -->
        <link href="${pageContext.request.contextPath}/view/css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
        <!-- iCheck for checkboxes and radio inputs -->
        <link href="${pageContext.request.contextPath}/view/css/iCheck/all.css" rel="stylesheet" type="text/css" />
        <!-- bootstrap wysihtml5 - text editor -->
        <!-- <link href="${pageContext.request.contextPath}/view/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" /> -->
        <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
        <!-- Theme style -->
        <link href="${pageContext.request.contextPath}/view/css/style.css" rel="stylesheet" type="text/css" />

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css" integrity="sha512-DTOQO9RWCH3ppGqcWaEA1BIZOC6xxalwEsw9c2QQeAIftl+Vegovlnee1c9QX4TctnWMn13TZye+giMm8e2LwA==" crossorigin="anonymous" referrerpolicy="no-referrer" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.${pageContext.request.contextPath}/view/js/1.3.0/respond.min.js"></script>
          <![endif]-->

    </head>
    <%
        DecimalFormat decimalFormat = new DecimalFormat("#,###");
        pageContext.setAttribute("decimalFormat", decimalFormat, PageContext.PAGE_SCOPE);
        //userDAO
        UserDAO userDAO = new UserDAO();
        pageContext.setAttribute("userDAO", userDAO, PageContext.PAGE_SCOPE);
        
    %>
    <body class="skin-black">
        <style type="text/css">
            #noti-box .alert {
                font-size: 18px;
            }

            .panel-body {
                max-height: 500px;
                overflow-y: auto;
                display: block;
            }

            .newly-feedback {
                max-height: 500px;
                min-height: 450px;
                overflow-y: auto;
                display: block;
            }

            .newly-feedback ul li {
                max-height: 150px;
            }

            .newly-feedback ul li div img {
                height: 10px;
                margin-left: 40px;
                margin-top: -20px;
            }

            .newly-feedback .feedback-info {
                font-size: 10px;
                color: gray;
            }

            .navbar-static-top form {
                padding: 8px;
            }

            section .date-form {
                background-color: #ffffff;
                padding: 20px;
                border-radius: 1px;
            }

            section .date-form div {
                font-size: 20px;
                font-weight: 600;
                margin-bottom: 10px;
            }

            section .date-form input {
                border: 1px solid gainsboro;
                height: 25px;
                padding: 15px;
                margin-right: 20px;
            }
        </style>

        <!-- header logo: style can be found in header.less -->
        <jsp:include page="admin-header.jsp"></jsp:include>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="${pageContext.request.contextPath}/view/img/26115.jpg" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Xin chào, ${sessionScope.user.userName}</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="active">
                            <a href="${pageContext.request.contextPath}/sale/dashboard">
                                <i class="fa fa-dashboard"></i> <span>Thống kê</span>
                            </a>
                        </li>
                        <li>
                            <c:if test="${sessionScope.user.roleId == 3}">
                                <c:set var="orderUrl" value="${pageContext.request.contextPath}/sale/order/list"/>
                            </c:if>
                            <c:if test="${sessionScope.user.roleId == 4}">
                                <c:set var="orderUrl" value="${pageContext.request.contextPath}/saleManager/order/list"/>
                            </c:if>
                            <a href="${orderUrl}">
                                <i class="fa fa-gavel"></i> <span>Đơn hàng</span>
                            </a>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <aside class="right-side">

                <!-- Main content -->
                <section class="content">

                    <form action="${pageContext.request.contextPath}/sale/dashboard" class="date-form" style="margin-bottom:15px;">
                        <div>Thống kê dữ liệu</div>
                        Từ: <input type="date" name="fromDate" value="${requestScope.fromDate}" onchange="this.form.submit()"/>
                        Đến: <input type="date" name="toDate" value="${requestScope.toDate}" onchange="this.form.submit()"/>
                    </form>

                    <!-- Main row -->
                    <div class="row">

                        <div class="col-lg-8">
                            <section class="panel">
                                <header class="panel-heading">
                                    Thống kê nhân viên bán hàng
                                </header>
                                <div class="panel-body">
                                    <div class="panel-body table-responsive">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Người bán hàng</th>
                                                    <th>Số đơn hàng</th>
                                                    <th>Số lượng hàng</th>
                                                    <th>Tổng số tiền</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:set var="saleIndex" value="1"/>
                                                <c:forEach items="${requestScope.saleStatistic}" var="item">
                                                    <tr>
                                                        <td>${saleIndex}</td>
                                                        <td>${userDAO.getUserByID(item[0].intValue()).fullName}</td>
                                                        <td>${item[1].intValue()}</td>
                                                        <td>${item[2].intValue()}</td>
                                                        <td>${decimalFormat.format(item[3])}</td>
                                                        <c:set var="saleIndex" value="${saleIndex + 1}"/>
                                                    </tr>
                                                </c:forEach>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </section>
                        </div>

                        <div class="col-lg-4">
                            <!--chat start-->
                            <section class="panel">
                                <header class="panel-heading">
                                    Đơn hàng gần đây (${requestScope.staticOrder[0] + requestScope.staticOrder[1] + requestScope.staticOrder[2]})
                                </header>
                                <div class="panel-body">
                                    <div class="alert alert-success">
                                        Đơn hàng đã đặt: <strong>${requestScope.staticOrder[2]}</strong>
                                    </div>
                                    <div class="alert alert-block alert-danger"> 
                                        Đơn hàng đã hủy: <strong>${requestScope.staticOrder[1]}</strong>
                                    </div>
                                    <div class="alert alert-warning">
                                        Đơn hàng đã giao: <strong>${requestScope.staticOrder[0]}</strong>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <div class="col-md-12">
                            <!--earning graph start-->
                            <section class="panel">
                                <header class="panel-heading">
                                    <c:set var="totalRevenue" value="0" />

                                    <c:forEach var="saleData" items="${saleStatistic}" varStatus="loop">
                                        <c:set var="revenue" value="${saleData[3]}" />
                                        <c:set var="totalRevenue" value="${totalRevenue + revenue}" />
                                    </c:forEach>
                                    Thống kê doanh thu: ${decimalFormat.format(totalRevenue)}
                                </header>
                                <div class="panel-body">
                                    <canvas id="linechart" width="600" height="330"></canvas>
                                </div>
                            </section>
                            <!--earning graph end-->

                        </div>

                    </div>
                    <!-- row end -->
                </section><!-- /.content -->
                <div class="footer-main">
                    Copyright &copy Director, 2014
                </div>
            </aside><!-- /.right-side -->

        </div><!-- ./wrapper -->


        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/${pageContext.request.contextPath}/view/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/jquery.min.js" type="text/javascript"></script>

        <!-- jQuery UI 1.10.3 -->
        <script src="${pageContext.request.contextPath}/view/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="${pageContext.request.contextPath}/view/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- daterangepicker -->
        <script src="${pageContext.request.contextPath}/view/js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>

        <script src="${pageContext.request.contextPath}/view/js/plugins/chart.js" type="text/javascript"></script>

        <!-- datepicker
        <script src="${pageContext.request.contextPath}/view/js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>-->
        <!-- Bootstrap WYSIHTML5
        <script src="${pageContext.request.contextPath}/view/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>-->
        <!-- iCheck -->
        <script src="${pageContext.request.contextPath}/view/js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
        <!-- calendar -->
        <script src="${pageContext.request.contextPath}/view/js/plugins/fullcalendar/fullcalendar.js" type="text/javascript"></script>

        <!-- Director App -->
        <script src="${pageContext.request.contextPath}/view/js/Director/app.js" type="text/javascript"></script>


        <!-- Director for demo purposes -->
        <script type="text/javascript">
                            $('input').on('ifChecked', function (event) {
                                // var element = $(this).parent().find('input:checkbox:first');
                                // element.parent().parent().parent().addClass('highlight');
                                $(this).parents('li').addClass("task-done");
                                console.log('ok');
                            });
                            $('input').on('ifUnchecked', function (event) {
                                // var element = $(this).parent().find('input:checkbox:first');
                                // element.parent().parent().parent().removeClass('highlight');
                                $(this).parents('li').removeClass("task-done");
                                console.log('not');
                            });

        </script>
        <script>
            $('#noti-box').slimScroll({
                height: '400px',
                size: '5px',
                BorderRadius: '5px'
            });

            $('input[type="checkbox"].flat-grey, input[type="radio"].flat-grey').iCheck({
                checkboxClass: 'icheckbox_flat-grey',
                radioClass: 'iradio_flat-grey'
            });
        </script>
        <script type="text/javascript">
            <%
                List<Double[]> saleStatistic = (List<Double[]>)request.getAttribute("saleStatistic");
                String[] saleNames = new String[saleStatistic.size()];
                double[] revenues = new double[saleStatistic.size()];
                
                for(int i = 0; i < saleStatistic.size(); i++) {
                    String saleName = userDAO.getUserByID((int)(double)saleStatistic.get(i)[0]).getFullName();
                    saleNames[i] = "\"" + saleName + "\"";
                    revenues[i] = saleStatistic.get(i)[3];
                }
                
                String labelSaleNames = "[" + String.join(",", saleNames) + "]";
                String dataRevenues = Arrays.toString(revenues);
            %>
            $(function () {
                "use strict";
                //BAR CHART
                var data = {
                    labels: <%=labelSaleNames%>,
                    datasets: [
                        {
                            label: "My First dataset",
                            fillColor: "rgba(220,220,220,0.2)",
                            strokeColor: "rgba(220,220,220,1)",
                            pointColor: "rgba(220,220,220,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(220,220,220,1)",
                            data: <%=dataRevenues%>
                        }
                    ]
                };
                new Chart(document.getElementById("linechart").getContext("2d")).Line(data, {
                    responsive: true,
                    maintainAspectRatio: false,
                });

            });
            // Chart.defaults.global.responsive = true;
        </script>
    </body>
</html>