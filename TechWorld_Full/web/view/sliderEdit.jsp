
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Admin | Dashboard</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta name="description" content="Developed By M Abdur Rokib Promy">
        <meta name="keywords" content="Admin, Bootstrap 3, Template, Theme, Responsive">
        <!-- bootstrap 3.0.2 -->
        <link href="${pageContext.request.contextPath}/view/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="${pageContext.request.contextPath}/view/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="${pageContext.request.contextPath}/view/css/ionicons.min.css" rel="stylesheet" type="text/css" />

        <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
        <!-- Theme style -->
        <link href="${pageContext.request.contextPath}/view/css/style.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->



    </head>





    <body class="skin-black">
        <!-- header logo: style can be found in header.less -->

        <jsp:include page="marketing-header.jsp"></jsp:include>
            <div class="wrapper row-offcanvas row-offcanvas-left">
                <!-- Left side column. contains the logo and sidebar -->
            <jsp:include page="marketing-aside.jsp"></jsp:include>

                <!-- Right side column. Contains the navbar and content of the page -->
                <aside class="right-side">
                    <!-- Content Header (Page header) -->


                    <!-- Main content -->
                    <section class="content" style="min-height: 90vh">
                        <div class="row" style="display: flex;
                             justify-content: center">
                            <div class="col-md-8">
                                <section class="panel">
                                    <header class="panel-heading">
                                    <c:choose>
                                        <c:when test="${isEdit}">
                                            Sửa cài đặt
                                        </c:when>
                                        <c:otherwise>
                                            Thêm cài đặt
                                        </c:otherwise>
                                    </c:choose>
                                </header>
                                <div class="panel-body">
                                    <c:if test="${not empty oldId}">
                                        <h3>Sửa Slider_ID :  ${oldId}</h3>
                                    </c:if>
                                    <form method="POST" role="form"
                                          <c:choose>
                                              <c:when test="${isEdit}">
                                                  action="${pageContext.request.contextPath}/marketing-editsliders"
                                              </c:when>
                                              <c:otherwise>
                                                  action="${pageContext.request.contextPath}/admin/settings/add"
                                              </c:otherwise>
                                          </c:choose>
                                          >
                                        <input type="hidden" name="oldId" value="${oldId}">
                                        <div class="form-group">
                                            <label>ID:</label>
                                            <input class="form-control" name="slider_id" type="text" value="${slider.sliderId}" placeholder="Id">
                                        </div>
                                        <div class="form-group">
                                            <label>Tên:</label>
                                            <input class="form-control" name="title" type="text" value="${slider.title}" placeholder="Tên">



                                            <div class="form-group" style="margin-bottom: 3rem;">
                                                <label>Trạng thái</label>
                                                <select id="status" class="form-control" name="status">
                                                    <option value="true">Hoạt động</option>
                                                    <option value="false">Không hoạt động</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Notes:</label>
                                                <input class="form-control" name="notes" type="text" value="${slider.note}" placeholder="Notes">
                                            </div>


                                            <input class="btn btn-primary" type="submit" 
                                                   <c:choose>
                                                       <c:when test="${isEdit}">
                                                           value="Sửa cài đặt"
                                                       </c:when>
                                                       <c:otherwise>
                                                           value="Thêm cài đặt"
                                                       </c:otherwise>
                                                   </c:choose>
                                                   >

                                            <a class="btn btn-success" 
                                               style="margin-left: 1rem"
                                               href="${pageContext.request.contextPath}/view-sliders">
                                                Quay lại
                                            </a>
                                    </form>
                                    <h4 class="text-danger">${error}</h4>
                                    <h4 class="text-success">${success}</h4>

                                    <script>
                                        <c:if test="${not empty slider.status}">
                                        document.getElementById("status").value = '${slider.status}';
                                        </c:if>
                                    </script>
                                </div>
                            </section>
                        </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
            <div class="footer-main">
                Copyright &copy Director, 2014
            </div>
        </div><!-- ./wrapper -->
        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/jquery.min.js" type="text/javascript"></script>

        <!-- Bootstrap -->
        <script src="${pageContext.request.contextPath}/view/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- Director App -->
        <script src="${pageContext.request.contextPath}/view/js/Director/app.js" type="text/javascript"></script>
    </body>
</html>
