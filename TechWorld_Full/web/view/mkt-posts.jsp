<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import = "model.*" %>
<%@page import = "dal.*" %>
<%@page import = "java.sql.Date" %>
<%@page import = "java.util.*" %>
<%@page import = "java.text.DecimalFormat" %>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Thống kê | Marketing</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta name="description" content="Developed By M Abdur Rokib Promy">
        <meta name="keywords" content="Admin, Bootstrap 3, Template, Theme, Responsive">
        <!-- bootstrap 3.0.2 -->
        <link href="${pageContext.request.contextPath}/view/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="${pageContext.request.contextPath}/view/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="${pageContext.request.contextPath}/view/css/ionicons.min.css" rel="stylesheet" type="text/css" />

        <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
        <!-- Theme style -->
        <link href="${pageContext.request.contextPath}/view/css/style.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="https://cdn.datatables.net/1.13.7/css/jquery.dataTables.css" />
    </head>

    <%
            CategoryDAO categoryDAO = new CategoryDAO();
            pageContext.setAttribute("categoryDAO", categoryDAO, PageContext.PAGE_SCOPE);
            
            BrandDAO brandDAO = new BrandDAO();
            pageContext.setAttribute("brandDAO", brandDAO, PageContext.PAGE_SCOPE);
            
            PostImageDAO postImageDAO = new PostImageDAO();
            pageContext.setAttribute("postImageDAO", postImageDAO, PageContext.PAGE_SCOPE);
            
            DecimalFormat decimalFormat = new DecimalFormat("#,###");
            pageContext.setAttribute("decimalFormat", decimalFormat, PageContext.PAGE_SCOPE);
    %>

    <body class="skin-black">

        <style type="text/css">
            #noti-box .alert {
                font-size: 18px;
            }

            .panel-body {
                max-height: 500px;
                overflow-y: auto;
                display: block;
            }

            .newly-feedback {
                max-height: 500px;
                min-height: 450px;
                overflow-y: auto;
                display: block;
            }

            .newly-feedback ul li {
                max-height: 150px;
            }

            .newly-feedback ul li div img {
                height: 10px;
                margin-left: 40px;
                margin-top: -20px;
            }

            .newly-feedback .feedback-info {
                font-size: 10px;
                color: gray;
            }

            .navbar-static-top form {
                padding: 8px;
            }

            section .date-form {
                background-color: #ffffff;
                padding: 20px;
                border-radius: 1px;
            }

            section .date-form div {
                font-size: 20px;
                font-weight: 600;
                margin-bottom: 10px;
            }

            section .date-form input {
                border: 1px solid gainsboro;
                height: 25px;
                padding: 15px;
                margin-right: 20px;
            }
        </style>

        <!-- header logo: style can be found in header.less -->
        <jsp:include page="admin-header.jsp"></jsp:include>
            <div class="wrapper row-offcanvas row-offcanvas-left">
                <!-- Left side column. contains the logo and sidebar -->
                <aside class="left-side sidebar-offcanvas">
                    <!-- sidebar: style can be found in sidebar.less -->
                    <section class="sidebar">
                        <!-- Sidebar user panel -->
                        <div class="user-panel">
                            <div class="pull-left image">
                                <img src="${pageContext.request.contextPath}/view/img/26115.jpg" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Xin chào, ${sessionScope.user.userName}</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>

                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="active">
                            <a href="${pageContext.request.contextPath}/marketing/marketing-dashboard">
                                <i class="fa fa-dashboard"></i> <span>Thống kê</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/posts">
                                <i class="fa fa-gavel"></i> <span>Bài đăng</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/view-sliders">
                                <i class="fa fa-gavel"></i> <span>Slider</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/product/list">
                                <i class="fa fa-gavel"></i> <span>Sản phẩm</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/marketing-viewcustomer">
                                <i class="fa fa-gavel"></i> <span>Khách hàng</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/feedbacks/list">
                                <i class="fa fa-gavel"></i> <span>Phản hồi</span>
                            </a>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <aside class="right-side">

                <!-- Main content -->
                <section class="content">
                    <form action="${pageContext.request.contextPath}/marketing/marketing-dashboard" class="date-form" style="margin-bottom:15px;">
                        <div>Sản phẩm</div>
                        <input type="hidden" name="view" value="posts"/>
                        Từ: <input type="date" name="fromDate" value="${requestScope.fromDate}" onchange="this.form.submit()"/>
                        Đến: <input type="date" name="toDate" value="${requestScope.toDate}" onchange="this.form.submit()"/>
                    </form>

                    <div class="row" style="margin-bottom:5px;">
                        <div class="col-md-12">
                            <ul class="nav nav-tabs"  style="margin-bottom:5px; background-color: #ffffff;">
                                <li class="nav-item">
                                    <a class="nav-link active" aria-current="page" 
                                       href="${pageContext.request.contextPath}/marketing/marketing-dashboard?view=statistic&fromDate=${requestScope.fromDate}&toDate=${requestScope.toDate}"
                                       >Thống kê
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" 
                                       href="${pageContext.request.contextPath}/marketing/marketing-dashboard?view=products&fromDate=${requestScope.fromDate}&toDate=${requestScope.toDate}"
                                       >Sản phẩm
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" 
                                       href="#" style="color: gray"
                                       >Bài đăng
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="row" style="margin-bottom:5px;">
                        <div class="col-md-12">
                            <header class="panel-heading">
                                Danh sách sản phẩm
                            </header>
                            <c:if test="${not empty deleteFailed}">
                                <p style="margin: 10px 15px 0px; font-size: 2rem;" class="text-success">${deleteFailed}</p>
                                <script>
                                    window.history.replaceState(null, '', "${pageContext.request.contextPath}/admin/settings/view");
                                </script>
                            </c:if>
                            <c:if test="${not empty deleteSuccess}">
                                <p style="margin: 10px 15px 0px; font-size: 2rem;" class="text-success">${deleteSuccess}</p>
                                <script>
                                    window.history.replaceState(null, '', "${pageContext.request.contextPath}/admin/settings/view");
                                </script>
                            </c:if>
                            <!-- <div class="box-header"> -->
                            <!-- <h3 class="box-title">Responsive Hover Table</h3> -->

                            <!-- </div> -->
                            <div class="panel-body table-responsive" style="background-color: #ffffff">
                                <table id="setting-table" class="table cell-border row-border hover stripe">
                                    <thead>
                                        <tr>
                                            <th style="min-width: 100px;">Ảnh</th>
                                            <th>Tiêu đề</th>
                                            <th>Mô tả ngắn</th>
                                            <th style="min-width: 100px;">Ngày</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach var="post" items="${requestScope.posts}">
                                            <tr>
                                                <c:set var="imageList" value="${postImageDAO.getAllImageOfPostById(post.postId)}"/>
                                                <c:set var="imageUrl" value="${(imageList.size() > 0) ? imageList.get(0).imageUrl : ''}"/>

                                                <td><img style="width: 100px" src="${pageContext.request.contextPath}/view/images/post/${imageUrl}" alt="image" /></td>
                                                <td>${post.title}</td>
                                                <td>${post.briefInfo}</td>
                                                <td>${post.updatedDate}</td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th></th>
                                            <th>Tiêu đề</th>
                                            <th>Mô tả ngắn</th>
                                            <th>Ngày</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div><!-- /.box-body -->
                        </div>
                    </div>   

                    <!-- Main row -->

                    <!-- row end -->
                </section><!-- /.content -->
                <div class="footer-main">
                    Copyright &copy Director, 2014
                </div>
            </aside><!-- /.right-side -->

        </div><!-- ./wrapper -->


        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/jquery.min.js" type="text/javascript"></script>

        <!-- Bootstrap -->
        <script src="${pageContext.request.contextPath}/view/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- Director App -->
        <script src="${pageContext.request.contextPath}/view/js/Director/app.js" type="text/javascript"></script>

        <script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.js"></script>
        <script>
                                    var settingTable = new DataTable('#setting-table', {
                                        initComplete: function () {
                                            this.api()
                                                    .columns()
                                                    .every(function () {
                                                        let column = this;
                                                        let title = column.footer().textContent;

                                                        if (title === '')
                                                            return;

                                                        // Create input element
                                                        let input = document.createElement('input');
                                                        input.placeholder = title;
                                                        column.footer().replaceChildren(input);

                                                        // Event listener for user input
                                                        input.addEventListener('keyup', () => {
                                                            if (column.search() !== this.value) {
                                                                column.search(input.value).draw();
                                                            }
                                                        });
                                                    });
                                        }
                                    });
                                    $('#setting-table tfoot > tr > th').css("border", "1px solid rgba(0, 0, 0, 0.15)");
                                    $('#setting-table').css("border-collapse", "collapse");
                                    $('#setting-table tfoot > tr > th > input').addClass('form-control');
                                    $('#setting-table tfoot tr').appendTo('#setting-table thead');

        </script>
    </body>
</html>