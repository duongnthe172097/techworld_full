<%-- 
    Document   : cart
    Created on : Jan 8, 2024, 4:20:43 PM
    Author     : admin
--%><%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Đơn hàng của tôi | Tech World</title>
        <link href="${pageContext.request.contextPath}/view/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/font-awesome.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/prettyPhoto.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/price-range.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/animate.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/main.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/responsive.css" rel="stylesheet">
        <!--[if lt IE 9]>
        <script src="${pageContext.request.contextPath}/view/js/html5shiv.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/respond.min.js"></script>
        <![endif]-->       
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/view/images/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="${pageContext.request.contextPath}/view/images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="${pageContext.request.contextPath}/view/images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="${pageContext.request.contextPath}/view/images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/view/images/ico/apple-touch-icon-57-precomposed.png">
    </head><!--/head-->

    <body>
        <style type="text/css">

            .product-homepage {
                background-color: #ffffff;
                border: none;
                box-shadow: rgba(0, 0, 0, 0.1) 0px 10px 15px -3px, rgba(0, 0, 0, 0.05) 0px 4px 6px -2px;
            }

            .product-homepage span {
                color: #f19d23;
                font-size: 20px;
                font-weight: 400;
            }

            .product-homepage span del {
                color: red;
                font-size: 12px;
            }

            .product-homepage img {
                box-shadow: rgba(255, 255, 255, 0.56) 0px 22px 70px 4px;
            }

            .product-homepage p {
                color: #000000;
                font-size: 18px;
                margin: 20px 0;
                min-height: 50px;
            }

            .post-homepage {
                background-color: #ffffff;
                border: none;
            }

            .post-homepage img {
                box-shadow: rgba(255, 255, 255, 0.56) 0px 22px 70px 4px;
            }

            #header {
                background-color: #7FCDF9;
                box-shadow: rgba(0, 0, 0, 0.1) 0px 4px 6px -1px, rgba(0, 0, 0, 0.06) 0px 2px 4px -1px;
            }

            .logo-techworld img{
                width: 120px;
                height: auto;
            }

            .header-searchform input{
                background-color: #ffffff;
                color: #333333;
            }

            @media (min-width: 768px) {

                #product-homepage {
                    min-height: 220vh;
                }

                .product-homepage {
                    height: 360px;
                    overflow: hidden;
                    padding: 3px;
                }
                .product-homepage img {
                    height: 150px;
                    width: fit-content;
                    max-width: 100%;
                }

                .post-homepage {
                    padding: 12px;
                    height: 430px;
                }

                .post-homepage img {
                    height: 150px;
                    width: fit-content;
                    width: 100%;
                    margin-bottom: 7px;
                }

                .post-homepage h5{
                    height: 25px;
                }

                .post-homepage p{
                    height: 150px;
                }
            }
        </style>
        <!-- ======= Header ======= -->
        <header id="header"><!--header-->
            <div class="header_top"><!--header_top-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="contactinfo">
                                <ul class="nav nav-pills">
                                    <li><a href="#"><i class="fa fa-phone"></i> +2 95 01 88 821</a></li>
                                    <li><a href="#"><i class="fa fa-envelope"></i> minhnhhe170924@fpt.edu.vn</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="social-icons pull-right">
                                <ul class="nav navbar-nav">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/header_top-->

            <div class="header-middle"><!--header-middle-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="logo pull-left logo-techworld">
                                <a href="${pageContext.request.contextPath}/home"><img src="${pageContext.request.contextPath}/view/images/home/tech_logo.png" alt="" /></a>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="shop-menu pull-right">
                                <ul class="nav navbar-nav">
                                    <c:if test="${sessionScope.user == null}">
                                        <li><a href="${pageContext.request.contextPath}/view/login.jsp"><i class="fa fa-user"></i> Tài khoản</a></li>
                                        <li><a href="${pageContext.request.contextPath}/view/login.jsp"><i class="fa fa-lock"></i> Đăng nhập</a></li>
                                        </c:if>
                                        <c:if test="${sessionScope.user != null}">
                                        <li><a href="${pageContext.request.contextPath}/profile"><i class="fa fa-user"></i> ${sessionScope.user.fullName}</a></li>
                                            <c:if test="${sessionScope.user.roleId == 5}">
                                            <li><a href="${pageContext.request.contextPath}/admin"><i class="fa fa-crosshairs"></i> Quản lý</a></li>
                                            </c:if>
                                            <c:if test="${sessionScope.user.roleId == 1}">
                                            <li><a href="${pageContext.request.contextPath}/cartController"><i class="fa fa-shopping-cart"></i>Giỏ hàng</a></li>
                                            <li><a href="${pageContext.request.contextPath}/customer/my-order" class="active"><i class="fa fa-crosshairs"></i> Đơn hàng của tôi</a></li>
                                            </c:if>
                                        <li><a href="${pageContext.request.contextPath}/logout"><i class="fa fa-lock"></i> Đăng xuất</a></li>
                                        </c:if>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/header-middle-->

            <div class="header-bottom"><!--header-bottom-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-9">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="mainmenu pull-left">
                                <ul class="nav navbar-nav collapse navbar-collapse">
                                    <li><a href="${pageContext.request.contextPath}/home">Trang chủ</a></li>
                                    <li><a href="${pageContext.request.contextPath}/products">Sản phẩm</a></li>
                                    <li><a href="${pageContext.request.contextPath}/blogs">Bài đăng</a></li>
                                    <li><a href="${pageContext.request.contextPath}/view/contact-us.jsp">Liên hệ</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <form action="${pageContext.request.contextPath}/home" class="search_box pull-right header-searchform">
                                <input type="text" name="search" placeholder="Tìm kiếm sản phẩm" value="${requestScope.searchKey}"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!--/header-bottom-->
        </header>

            <section id="cart_items">
                <div class="container">
                    <div class="breadcrumbs">
                        <ol class="breadcrumb">

                        </ol>
                    </div><!--/breadcrums-->

                    <div class="table-responsive cart_info">
                        <table class="table table-condensed">
                            <thead>
                                <tr class="cart_menu">
                                    <td class="description">Id</td>
                                    <td class="date">Ngày đặt hàng</td>
                                    <td class="description">Sản phẩm đầu tiên</td>
                                    <td class="description">Số sản phẩm</td>
                                    <td class="price">Tổng giá thành</td>
                                    <td class="status">Trạng thái</td>
                                    <td class="status">Feedback</td>
                                </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${listOrder}" var="o">
                                <tr>
                                    <td class="description">
                                        <a href="${pageContext.request.contextPath}/customer/order-detail?id=${o.orderId}">${o.orderId}</a>
                                    </td>
                                    <td class="date">
                                        <p>${o.orderDate}</p>
                                    </td>
                                    <td class="description">
                                        <c:if test="${not empty o.orderDetailList and not empty o.orderDetailList.get(0).product and not empty o.orderDetailList.get(0).product.productId}">
                                            <c:set var="productId" value="${o.orderDetailList.get(0).product.productId}" />
                                            <c:set var="productImage" value="${not empty o.orderDetailList.get(0).product.productImage and not empty o.orderDetailList.get(0).product.productImage.get(0) ? o.orderDetailList.get(0).product.productImage.get(0).imageUrl : ''}" />
                                            <c:if test="${not empty productImage}">
                                                <a href="product?id=${productId}">
                                                    <img style="width: 100px" src="${pageContext.request.contextPath}/view/images/product/${productImage}" alt="">
                                                </a>
                                            </c:if>
                                        </c:if>


                                        <c:if test="${not empty o.orderDetailList and not empty o.orderDetailList.get(0).product and not empty o.orderDetailList.get(0).product.productName}">
                                            <h4>${o.orderDetailList.get(0).product.productName}</h4>
                                        </c:if>


                                    </td>
                                    <td class="cart_description">
                                        <p>${o.orderDetailList.size()} Products</p>
                                    </td>
                                    <td class="cart_price">
                                        <p><fmt:formatNumber value="${o.totalCost}" pattern="#,##0" /> VND</p>
                                    </td>
                                    <td class="cart_status">
                                        <p>${o.stateString}</p>
                                    </td>
                                    <td>
                                        <a class="btn btn-warning btn-sm" href="${pageContext.request.contextPath}/customer/order-detail?id=${o.orderId}">Đánh giá</a>
                                    </td>

                                </tr>
                            </c:forEach>



                        </tbody>
                    </table>              
                </div>   
                <ul class="pagination">
                    <li class="page-item <c:if test='${page <= 1}'>disabled</c:if>'">
                        <a class="page-link" href="?index=${page - 1}" tabindex="-1" aria-disabled="true">Previous</a>
                    </li>

                    <c:if test="${page > 2}">
                        <li class="page-item"><a class="page-link" href="?index=${page - 2}">${page - 2}</a></li>
                        </c:if>

                    <c:if test="${page > 1}">
                        <li class="page-item"><a class="page-link" href="?index=${page - 1}">${page - 1}</a></li>
                        </c:if>

                    <li class="page-item active"><a class="page-link" href="?index=${page}">${page}</a></li>

                    <c:if test="${page+1 <= total}">
                        <li class="page-item"><a class="page-link" href="?index=${page + 1}">${page + 1}</a></li>
                        </c:if>

                    <c:if test="${page+2 <= total}">
                        <li class="page-item"><a class="page-link" href="?index=${page + 2}">${page + 2}</a></li>
                        </c:if>

                    <li class="page-item">
                        <a class="page-link" href="?index=${page + 1}" aria-disabled="false">Next</a>
                    </li>
                </ul>
            </div>  
        </section> 



        <!-- ======= Footer ======= -->
        <jsp:include page="footer.jsp"></jsp:include>



            <script src="${pageContext.request.contextPath}/view/js/jquery.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/jquery.scrollUp.min.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/jquery.prettyPhoto.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/main.js"></script>
    </body>
</html>