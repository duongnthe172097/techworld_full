<%-- 
    Document   : amin-header
    Created on : Jan 21, 2024, 8:01:54 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<header class="header">
    <a href="#" class="logo">
        ${sessionScope.user.roleId == 5 ? 'Trang quản trị' : ''} 
        ${sessionScope.user.roleId == 2 ? 'Trang marketing' : ''} 
        ${sessionScope.user.roleId == 3 || sessionScope.user.roleId == 4 ? 'Trang sale' : ''} 
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        <div class="navbar-right">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->

                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-user"></i>
                        <span>${sessionScope.user.fullName} <i class="caret"></i></span>
                    </a>
                    <ul class="dropdown-menu dropdown-custom dropdown-menu-right">
                        <li>
                            <a href="${pageContext.request.contextPath}/profile">
                                <i class="fa fa-user fa-fw pull-right"></i>
                                Profile
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/home">
                                <i class="fa fa-glass fa-fw pull-right"></i>
                                Trang chủ
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/logout"><i class="fa fa-ban fa-fw pull-right"></i> Đăng xuất</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
