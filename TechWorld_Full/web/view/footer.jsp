<%-- 
    Document   : footer
    Created on : Jan 11, 2024, 12:35:23 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import = "model.*" %>
<%@page import = "dal.*" %>
<%@page import = "java.util.List" %>
<%@page import = "java.util.ArrayList" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    //Get all categories
    CategoryDAO categoryDAO = new CategoryDAO();
    List<Category> allCategories = categoryDAO.getAllCategories();
    pageContext.setAttribute("allCategories", allCategories, PageContext.PAGE_SCOPE);
%>
<style>
    .video-gallery ul li {
        margin: -10px 0;
    }
</style>
<footer id="footer"><!--Footer-->
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="companyinfo">
                        <h2><span>TECH</span>-WORLD</h2>
                        <p>Khám phá Tech World ngay hôm nay để trải nghiệm mua sắm công nghệ tuyệt vời. Sản phẩm đa dạng và chất lượng, đảm bảo là điểm đến yêu thích của bạn.</p>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="col-sm-6">
                        <div class="video-gallery">
                            <h2>Tìm mua</h2>
                            <ul class="nav nav-pills nav-stacked">
                                <c:forEach items="${allCategories}" var="item">
                                    <li><a href="#">${item.categoryName}</a></li>
                                    </c:forEach>
                            </ul>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="video-gallery">
                            <h2>Về chúng tôi</h2>
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="#">Thông tin công ty</a></li>
                                <li><a href="#">Nhân sự</a></li>
                                <li><a href="#">Địa điểm</a></li>
                                <li><a href="#">Bản quyền</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="address">
                        <img src="${pageContext.request.contextPath}/view/images/home/map.png" alt="" />
                        <p>Khu Công nghệ cao Hòa Lạc – Km29 Đại lộ Thăng Long, H. Thạch Thất, TP. Hà Nội</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <p class="pull-left">Copyright © 2024 TECH-WORLD Inc. All rights reserved.</p>
            </div>
        </div>
    </div>

</footer><!--/Footer-->
