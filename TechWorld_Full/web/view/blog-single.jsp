<%-- 
    Document   : blog-single
    Created on : Jan 8, 2024, 4:19:59 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import = "model.*" %>
<%@page import = "dal.*" %>
<%@page import = "java.util.List" %>
<%@page import = "java.util.ArrayList" %>
<%@page import = "java.text.DecimalFormat" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Blog Single | Tech World</title>
        <link href="${pageContext.request.contextPath}/view/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/font-awesome.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/prettyPhoto.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/price-range.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/animate.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/main.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/responsive.css" rel="stylesheet">
        <!--[if lt IE 9]>
        <script src="${pageContext.request.contextPath}/view/js/html5shiv.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/respond.min.js"></script>
        <![endif]-->       
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/view/images/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="${pageContext.request.contextPath}/view/images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="${pageContext.request.contextPath}/view/images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="${pageContext.request.contextPath}/view/images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/view/images/ico/apple-touch-icon-57-precomposed.png">
    </head><!--/head-->

    <%
        //Get all categories
        CategoryDAO categoryDAO = new CategoryDAO();
        List<Category> allActiveCategories = categoryDAO.getAllActiveCategories();
        pageContext.setAttribute("allActiveCategories", allActiveCategories, PageContext.PAGE_SCOPE);

        //Get all brands
        BrandDAO brandDAO = new BrandDAO();
        List<Brand> allActiveBrands = brandDAO.getAllActiveBrands();
        pageContext.setAttribute("allActiveBrands", allActiveBrands, PageContext.PAGE_SCOPE);

        //post image dao
        PostImageDAO postImageDAO = new PostImageDAO();
        pageContext.setAttribute("postImageDAO", postImageDAO, PageContext.PAGE_SCOPE);

        // user dao
        UserDAO userDAO = new UserDAO();
        pageContext.setAttribute("userDAO", userDAO, PageContext.PAGE_SCOPE);

        // comment dao
        CommentDAO commentDAO = new CommentDAO();
        pageContext.setAttribute("commentDAO", commentDAO, PageContext.PAGE_SCOPE);
    %>

    <body>
        <style type="text/css">
            .left-sidebar {
                background-color: #ffffff;
                padding-top: 20px;
                margin-left: -15px;
            }

            .left-sidebar .panel-title {
                color: #333333;
                font-weight: 500;
                padding-left: 10px;
                font-size: 18px;
            }

            .left-sidebar ul li {
                padding: 5px 10px;
                font-weight: 400;
                font-size: 16px;
            }

            .left-sidebar ul li input {
                margin-right: 10px;
            }

            #header {
                background-color: #7FCDF9;
                box-shadow: rgba(0, 0, 0, 0.1) 0px 4px 6px -1px, rgba(0, 0, 0, 0.06) 0px 2px 4px -1px;
            }

            .logo-techworld img{
                width: 120px;
                height: auto;
            }

            .header-searchform input{
                background-color: #ffffff;
                color: #333333;
            }

            .response-area {
                max-height: 60vh;
                overflow: auto;
            }
        </style>

        <!-- ======= Header ======= -->
        <header id="header"><!--header-->
            <div class="header_top"><!--header_top-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="contactinfo">
                                <ul class="nav nav-pills">
                                    <li><a href="#"><i class="fa fa-phone"></i> +2 95 01 88 821</a></li>
                                    <li><a href="#"><i class="fa fa-envelope"></i> minhnhhe170924@fpt.edu.vn</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="social-icons pull-right">
                                <ul class="nav navbar-nav">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/header_top-->

            <div class="header-middle"><!--header-middle-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="logo pull-left logo-techworld">
                                <a href="${pageContext.request.contextPath}/home"><img src="${pageContext.request.contextPath}/view/images/home/tech_logo.png" alt="" /></a>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="shop-menu pull-right">
                                <ul class="nav navbar-nav">
                                    <c:if test="${sessionScope.user == null}">
                                        <li><a href="${pageContext.request.contextPath}/view/login.jsp"><i class="fa fa-user"></i> Tài khoản</a></li>
                                        <li><a href="${pageContext.request.contextPath}/view/login.jsp"><i class="fa fa-lock"></i> Đăng nhập</a></li>
                                        </c:if>
                                        <c:if test="${sessionScope.user != null}">
                                        <li><a href="${pageContext.request.contextPath}/profile"><i class="fa fa-user"></i> ${sessionScope.user.fullName}</a></li>
                                            <c:if test="${sessionScope.user.roleId == 5}">
                                            <li><a href="${pageContext.request.contextPath}/admin"><i class="fa fa-crosshairs"></i> Quản lý</a></li>
                                            </c:if>
                                            <c:if test="${sessionScope.user.roleId == 1}">
                                            <li><a href="${pageContext.request.contextPath}/cartController"><i class="fa fa-shopping-cart"></i>Giỏ hàng</a></li>
                                            <li><a href="${pageContext.request.contextPath}/customer/my-order"><i class="fa fa-crosshairs"></i> Đơn hàng của tôi</a></li>
                                            </c:if>
                                        <li><a href="${pageContext.request.contextPath}/logout"><i class="fa fa-lock"></i> Đăng xuất</a></li>
                                        </c:if>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/header-middle-->

            <div class="header-bottom"><!--header-bottom-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-9">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="mainmenu pull-left">
                                <ul class="nav navbar-nav collapse navbar-collapse">
                                    <li><a href="${pageContext.request.contextPath}/home">Trang chủ</a></li>
                                    <li><a href="${pageContext.request.contextPath}/products">Sản phẩm</a></li>
                                    <li><a href="${pageContext.request.contextPath}/blogs" class="active">Bài đăng</a></li>
                                    <li><a href="${pageContext.request.contextPath}/view/contact-us.jsp">Liên hệ</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <form action="${pageContext.request.contextPath}/home" class="search_box pull-right header-searchform">
                                <input type="text" name="search" placeholder="Tìm kiếm sản phẩm" value="${requestScope.searchKey}"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!--/header-bottom-->
        </header><!--/header-->

        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <!-- ======= Sidebar ======= -->
                        <div class="left-sidebar">

                            <form action="${pageContext.request.contextPath}/blogs">
                                <h2>Danh mục</h2>
                                <div class="panel-group category-products" id="accordian"><!--category-productsr-->
                                    <c:forEach begin="0" end="${allActiveCategories.size() - 1}" var="i">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <input 
                                                    type="checkbox" 
                                                    name="categoryId" 
                                                    value="${allActiveCategories.get(i).getCategoryId()}"
                                                    ${requestScope.checkedCategories[i] ? 'checked' : ''}
                                                    onclick="this.form.submit()"
                                                    />
                                                <span class="panel-title">${allActiveCategories.get(i).getCategoryName()}</span>
                                            </div>
                                        </div>
                                    </c:forEach>
                                    <!--/category-products-->

                                </div>
                                <div class="brands_products"><!--brands_products-->
                                    <h2>Thương hiệu</h2>
                                    <div class="brands-name">
                                        <ul class="nav nav-pills nav-stacked">
                                            <c:forEach begin="0" end="${allActiveBrands.size() - 1}" var="i">
                                                <li>
                                                    <input 
                                                        type="checkbox" 
                                                        name="brandId" 
                                                        value="${allActiveBrands.get(i).getBrandId()}"
                                                        ${requestScope.checkedBrands[i] ? 'checked' : ''}
                                                        onclick="this.form.submit()"
                                                        />
                                                    ${allActiveBrands.get(i).getBrandName()}
                                                </li>
                                            </c:forEach>
                                        </ul>
                                    </div>
                                </div>
                                <!--/brands_products-->
                            </form>
                            <div class="price-range"><!--price-range-->
                                <h2>Tìm kiếm</h2>
                                <div class="well text-center" style="padding: 0px;display: flex;justify-content: center;">
                                    <form action="${pageContext.request.contextPath}/blogs" class="search_box pull-right header-searchform">
                                        <input type="text" name="search" placeholder="Tìm kiếm" value="${requestScope.searchKey}"
                                               style="padding: 10px; border: 1px solid burlywood; width: 100%"/>
                                    </form>
                                </div>
                            </div><!--/price-range-->


                            <div class="shipping text-center"><!--shipping-->
                                <img src="${pageContext.request.contextPath}/view/images/home/shipping.jpg" alt="" />
                            </div><!--/shipping-->

                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="blog-post-area">
                            <h2 class="title text-center">Chi tiết bài đăng</h2>
                            <div class="single-blog-post">
                                <h3>${requestScope.postDetails.title}</h3>
                                <div class="post-meta">
                                    <ul>
                                        <li><i class="fa fa-user"></i> ${userDAO.getUserByID(postDetails.getUserId()).getFullName()}</li>
                                        <li><i class="fa fa-calendar"></i>${postDetails.updatedDate}</li>
                                    </ul>
                                </div>
                                <div id="slider-carousel" class="carousel slide" data-ride="carousel">
                                    <c:set var="imageList" value="${postImageDAO.getAllImageOfPostById(postDetails.getPostId())}"/>
                                    <div class="carousel-inner">
                                        <c:forEach items="${imageList}" var="image">
                                            <div class="item ${imageList.indexOf(image) == 0 ? 'active' : ''}">
                                                <a href="">
                                                    <img style="width: 90%;" src="${pageContext.request.contextPath}/view/images/post/${image.imageUrl}" alt="">
                                                </a>
                                            </div>
                                        </c:forEach>
                                    </div>
                                    <a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
                                        <i class="fa fa-angle-left"></i>
                                    </a>
                                    <a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </div>
                                <p style="margin-top: 40px;">
                                    ${requestScope.postDetails.content}
                                </p>
                            </div>
                        </div><!--/blog-post-area-->

                        <div class="socials-share">
                            <a href=""><img src="${pageContext.request.contextPath}/view/images/blog/socials.png" alt=""></a>
                        </div><!--/socials-share-->

                        <div class="media commnets">
                            <div class="media-body">
                                <h4 class="media-heading">Viết bởi: ${userDAO.getUserByID(postDetails.getUserId()).fullName}</h4>
                                <p>${postDetails.briefInfo}</p>
                                <div class="blog-socials">
                                    <ul>
                                        <li><a href=""><i class="fa fa-facebook"></i></a></li>
                                        <li><a href=""><i class="fa fa-twitter"></i></a></li>
                                        <li><a href=""><i class="fa fa-dribbble"></i></a></li>
                                        <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div><!--Comments-->
                        <div class="response-area">
                            <h2>BÌNH LUẬN</h2>
                            <ul class="media-list">
                                <c:set var="commentList" value="${commentDAO.getCommentsByPostId(postDetails.getPostId())}"/>
                                <c:forEach items="${commentList}" var="comment">
                                    <li class="media">
                                        <a class="pull-left" href="#">
                                            <img class="media-object" src="${pageContext.request.contextPath}/view/images/user/${userDAO.getUserByID(comment.userId).image}" alt="">
                                        </a>
                                        <div class="media-body">
                                            <ul class="sinlge-post-meta">
                                                <li><i class="fa fa-user"></i>${userDAO.getUserByID(comment.userId).fullName}</li>
                                                <li><i class="fa fa-calendar"></i> ${comment.updatedDate}</li>
                                            </ul>
                                            <p>${comment.content}</p>
                                        </div>
                                    </li>
                                    <c:set var="replyList" value="${commentDAO.getReplyByCommentId(comment.commentId)}"/>
                                    <c:forEach items="${replyList}" var="reply">
                                        <li class="media second-media" style="padding: 0;">
                                            <a class="pull-left" href="#">
                                                <img class="media-object" src="${pageContext.request.contextPath}/view/images/user/${userDAO.getUserByID(reply.userId).image}" alt="">
                                            </a>
                                            <div class="media-body">
                                                <ul class="sinlge-post-meta">
                                                    <li><i class="fa fa-user"></i>${userDAO.getUserByID(reply.userId).fullName}</li>
                                                    <li><i class="fa fa-calendar"></i> ${reply.updatedDate}</li>
                                                </ul>
                                                <p>${reply.content}</p>
                                            </div>
                                        </li>
                                    </c:forEach>
                                    <div class="text-area media second-media" style="padding: 0; margin-top: -10px;">
                                        <form action="${pageContext.request.contextPath}/post-comment?postId=${postDetails.postId}" method="post">
                                            <input type="hidden" name="service" value="reply"/>
                                            <input type="hidden" name="replyOf" value="${comment.commentId}"/>
                                            <textarea name="content" rows="2" required placeholder="Viết câu trả lời của bạn..."></textarea>
                                            <button class="btn btn-primary" type="submit">
                                                <i class="fa fa-reply"></i>Trả lời
                                            </button>
                                        </form>
                                    </div>
                                </c:forEach>
                            </ul>
                            <c:if test="${commentList.size() == 0}">
                                Hãy là người bình luận đầu tiên.
                            </c:if>
                        </div><!--/Response-area-->
                        <div class="replay-box">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="text-area">
                                        <form action="${pageContext.request.contextPath}/post-comment?postId=${postDetails.postId}" method="post">
                                            <input type="hidden" name="service" value="comment"/>
                                            <div class="blank-arrow">
                                                <label>Ý kiến của bạn</label>
                                            </div>
                                            <span>*</span>
                                            <textarea name="content" rows="6" required placeholder="Bình luận về bài viết."></textarea>
                                            <button class="btn btn-primary" type="submit">
                                                Đăng
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div><!--/Repaly Box-->
                    </div>	
                </div>
            </div>
        </section>

        <!-- ======= Footer ======= -->
        <jsp:include page="footer.jsp"></jsp:include>



            <script src="${pageContext.request.contextPath}/view/js/jquery.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/price-range.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/jquery.scrollUp.min.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/jquery.prettyPhoto.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/main.js"></script>
    </body>
</html>