<%-- 
    Document   : header
    Created on : Jan 11, 2024, 12:31:22 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<style type="text/css">
    #header {
        background-color: #7FCDF9;
        box-shadow: rgba(0, 0, 0, 0.1) 0px 4px 6px -1px, rgba(0, 0, 0, 0.06) 0px 2px 4px -1px;
    }

    .logo-techworld img{
        width: 120px;
        height: auto;
    }

    .header-searchform input{
        background-color: #ffffff;
        color: #333333;
    }

</style>

<header id="header"><!--header-->
    <div class="header_top"><!--header_top-->
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="contactinfo">
                        <ul class="nav nav-pills">
                            <li><a href="#"><i class="fa fa-phone"></i> +2 95 01 88 821</a></li>
                            <li><a href="#"><i class="fa fa-envelope"></i> minhnhhe170924@fpt.edu.vn</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="social-icons pull-right">
                        <ul class="nav navbar-nav">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/header_top-->

    <div class="header-middle"><!--header-middle-->
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="logo pull-left logo-techworld">
                        <a href="${pageContext.request.contextPath}/home"><img src="${pageContext.request.contextPath}/view/images/home/tech_logo.png" alt="" /></a>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="shop-menu pull-right">
                        <ul class="nav navbar-nav">
                            <c:if test="${sessionScope.user == null}">
                                <li><a href="${pageContext.request.contextPath}/view/login.jsp"><i class="fa fa-user"></i> Tài khoản</a></li>
                                <li><a href="${pageContext.request.contextPath}/view/login.jsp"><i class="fa fa-lock"></i> Đăng nhập</a></li>
                                </c:if>
                                <c:if test="${sessionScope.user != null}">
                                <li><a href="${pageContext.request.contextPath}/profile"><i class="fa fa-user"></i> ${sessionScope.user.fullName}</a></li>
                                    <c:if test="${sessionScope.user.roleId == 5}">
                                    <li><a href="${pageContext.request.contextPath}/admin"><i class="fa fa-crosshairs"></i> Quản lý</a></li>
                                    </c:if>
                                    <c:if test="${sessionScope.user.roleId == 1}">
                                    <li><a href="${pageContext.request.contextPath}/cartController"><i class="fa fa-shopping-cart"></i>Giỏ hàng</a></li>
                                    <li><a href="${pageContext.request.contextPath}/customer/my-order"><i class="fa fa-crosshairs"></i> Đơn hàng của tôi</a></li>
                                    </c:if>
                                <li><a href="${pageContext.request.contextPath}/logout"><i class="fa fa-lock"></i> Đăng xuất</a></li>
                                </c:if>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/header-middle-->

    <div class="header-bottom"><!--header-bottom-->
        <div class="container">
            <div class="row">
                <div class="col-sm-9">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="mainmenu pull-left">
                        <ul class="nav navbar-nav collapse navbar-collapse">
                            <li><a href="${pageContext.request.contextPath}/home" class="active">Trang chủ</a></li>
                            <li><a href="${pageContext.request.contextPath}/products">Sản phẩm</a></li>
                            <li><a href="${pageContext.request.contextPath}/blogs">Danh sách bài đăng</a></li>
                            <li><a href="${pageContext.request.contextPath}/view/contact-us.jsp">Liên hệ</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3">
                    <form action="${pageContext.request.contextPath}/home" class="search_box pull-right header-searchform">
                        <input type="text" name="search" placeholder="Tìm kiếm sản phẩm" value="${requestScope.searchKey}"/>
                    </form>
                </div>
            </div>
        </div>
    </div><!--/header-bottom-->
</header><!--/header-->
