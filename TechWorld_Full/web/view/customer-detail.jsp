<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Sale</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta name="description" content="Developed By M Abdur Rokib Promy">
        <meta name="keywords" content="Admin, Bootstrap 3, Template, Theme, Responsive">
        <!-- bootstrap 3.0.2 -->
        <link href="${pageContext.request.contextPath}/view/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="${pageContext.request.contextPath}/view/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="${pageContext.request.contextPath}/view/css/ionicons.min.css" rel="stylesheet" type="text/css" />

        <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
        <!-- Theme style -->
        <link href="${pageContext.request.contextPath}/view/css/style.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <link rel="stylesheet" href="https://cdn.datatables.net/1.13.7/css/jquery.dataTables.css" />
        <style>
            /*            tfoot input {
                            width: 100%;
                            padding: 3px;
                            box-sizing: border-box;
                            border: 1px solid #aaa;
                            border-radius: 3px;
                            padding: 5px;
                            background-color: transparent;
                            color: inherit;
                            margin-left: 3px;
                        }*/
        </style>
    </head>

    <body class="skin-black">
        <!-- header logo: style can be found in header.less -->
        <jsp:include page="admin-header.jsp"></jsp:include>
            <div class="wrapper row-offcanvas row-offcanvas-left">
                <!-- Left side column. contains the logo and sidebar -->
                <aside class="left-side sidebar-offcanvas">
                    <!-- sidebar: style can be found in sidebar.less -->
                    <section class="sidebar">
                        <!-- Sidebar user panel -->
                        <div class="user-panel">
                            <div class="pull-left image">
                                <img src="${pageContext.request.contextPath}/view/img/26115.jpg" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Xin chào, ${sessionScope.user.userName}</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>

                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/marketing-dashboard">
                                <i class="fa fa-dashboard"></i> <span>Thống kê</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/posts">
                                <i class="fa fa-gavel"></i> <span>Bài đăng</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/view-sliders">
                                <i class="fa fa-gavel"></i> <span>Slider</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/product/list">
                                <i class="fa fa-gavel"></i> <span>Sản phẩm</span>
                            </a>
                        </li>
                        <li class="active">
                            <a href="${pageContext.request.contextPath}/marketing/marketing-viewcustomer">
                                <i class="fa fa-gavel"></i> <span>Khách hàng</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/feedbacks/list">
                                <i class="fa fa-gavel"></i> <span>Phản hồi</span>
                            </a>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->


                <div class="container mt-5" style="background-color: #ffffff">
                    <!-- Display post details -->
                    <h1>Chi tiết Slider</h1>

                    <c:if test="${param.success ne null}">
                        <div class="alert alert-success" role="alert">
                            Thành công!
                        </div>
                    </c:if>

                    <form action="${pageContext.request.contextPath}/marketing/customer-detail" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="id" value="${user.userId}">


                        <div class="form-group">
                            <label for="full_name">Tên:</label>
                            <input type="text" class="form-control" id="full_name" name="full_name" value="${user.fullName}">
                        </div>


                        <div class="form-group">
                            <label for="status">Trạng thái:</label>
                            <select class="form-control" id="status" name="status">
                                <option value="true" ${user.status ? 'selected' : ''}>Hợp lệ</option>
                                <option value="false" ${!user.status ? 'selected' : ''}>Bị cấm</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="gender">Giới tính:</label>
                            <select class="form-control" id="status" name="gender">
                                <option value="true" ${user.status ? 'selected' : ''}>Nam</option>
                                <option value="false" ${!user.status ? 'selected' : ''}>Nữ</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input type="text" class="form-control" id="email" name="email" value="${user.email}" readonly>
                        </div>

                        <div class="form-group">
                            <label for="mobile">Mobile:</label>
                            <input type="text" class="form-control" id="mobile" name="mobile" value="${user.mobile}">
                        </div>
                        <!-- Existing image display -->
                        <div class="form-group">
                            <label for="image">Ảnh hiện tại:</label>
                            <img src="${pageContext.request.contextPath}/view/images/user/${user.image}" alt="Current Image" style="max-width: 200px; max-height: 200px;">
                        </div>

                        <!-- Input field for selecting new image -->
                        <div class="form-group">
                            <label for="newImage">Chọn ảnh mới:</label>
                            <input type="file" id="newImage" name="newImage" onchange="previewImage(this)">
                        </div> 
                        <div id="imagePreview"></div>

                        <button type="submit" class="btn btn-primary">Cập nhật</button>
                        <footer style="color: red;" class="panel-heading">
                            ${error}
                        </footer>
                    </form>



            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/jquery.min.js" type="text/javascript"></script>

        <!-- Bootstrap -->
        <script src="${pageContext.request.contextPath}/view/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- Director App -->
        <script src="${pageContext.request.contextPath}/view/js/Director/app.js" type="text/javascript"></script>

        <script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.js"></script>



        <script type="text/javascript">

                                function previewImage(input) {
                                    var preview = document.getElementById('imagePreview');
                                    preview.innerHTML = ''; // Clear previous preview

                                    if (input.files && input.files[0]) {
                                        var reader = new FileReader();

                                        reader.onload = function (e) {
                                            var img = document.createElement('img');
                                            img.src = e.target.result;
                                            img.style.maxWidth = '200px'; // Set maximum width for preview image
                                            img.style.maxHeight = '200px'; // Set maximum height for preview image
                                            preview.appendChild(img); // Append preview image to container
                                        }

                                        reader.readAsDataURL(input.files[0]); // Read the selected file as data URL
                                    }
                                }

        </script>


        <!-- JavaScript code to handle image selection event -->
        <script type="text/javascript">
            document.getElementById("newImage").addEventListener("change", function (event) {
                var selectedFile = event.target.files[0]; // Get the selected file
                var fileSize = selectedFile.size; // Get the size of the file in bytes
                var maxSize = 5 * 1024 * 1024; // 5 MB

                // Check if file size exceeds the maximum allowed size
                if (fileSize > maxSize) {
                    alert("File size exceeds the maximum limit of 5MB.");
                    // Clear the file input field
                    document.getElementById("newImage").value = "";
                }
            }
            );
        </script>


    </body>
</html>
