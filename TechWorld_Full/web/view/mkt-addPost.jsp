<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import = "model.*" %>
<%@page import = "dal.*" %>
<%@page import = "java.sql.Date" %>
<%@page import = "java.util.*" %>
<%@page import = "java.text.DecimalFormat" %>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Marketing | Thêm sản phẩm</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta name="description" content="Developed By M Abdur Rokib Promy">
        <meta name="keywords" content="Admin, Bootstrap 3, Template, Theme, Responsive">
        <!-- bootstrap 3.0.2 -->
        <link href="${pageContext.request.contextPath}/view/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="${pageContext.request.contextPath}/view/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="${pageContext.request.contextPath}/view/css/ionicons.min.css" rel="stylesheet" type="text/css" />

        <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
        <!-- Theme style -->
        <link href="${pageContext.request.contextPath}/view/css/style.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="https://cdn.datatables.net/1.13.7/css/jquery.dataTables.css" />
    </head>

    <%
        CategoryDAO categoryDAO = new CategoryDAO();
        pageContext.setAttribute("categoryDAO", categoryDAO, PageContext.PAGE_SCOPE);
            
        BrandDAO brandDAO = new BrandDAO();
        pageContext.setAttribute("brandDAO", brandDAO, PageContext.PAGE_SCOPE);
        
         ProductDAO productDAO = new ProductDAO();
        pageContext.setAttribute("productDAO", productDAO, PageContext.PAGE_SCOPE);
    %>

    <body class="skin-black">


        <!-- header logo: style can be found in header.less -->
        <jsp:include page="admin-header.jsp"></jsp:include>
            <div class="wrapper row-offcanvas row-offcanvas-left">
                <!-- Left side column. contains the logo and sidebar -->
                <aside class="left-side sidebar-offcanvas">
                    <!-- sidebar: style can be found in sidebar.less -->
                    <section class="sidebar">
                        <!-- Sidebar user panel -->
                        <div class="user-panel">
                            <div class="pull-left image">
                                <img src="${pageContext.request.contextPath}/view/img/26115.jpg" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Xin chào, ${sessionScope.user.userName}</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>

                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/marketing-dashboard">
                                <i class="fa fa-dashboard"></i> <span>Thống kê</span>
                            </a>
                        </li>
                        <li class="active">
                            <a href="${pageContext.request.contextPath}/marketing/posts">
                                <i class="fa fa-gavel"></i> <span>Bài đăng</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/view-sliders">
                                <i class="fa fa-gavel"></i> <span>Slider</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/product/list">
                                <i class="fa fa-gavel"></i> <span>Sản phẩm</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/marketing-viewcustomer">
                                <i class="fa fa-gavel"></i> <span>Khách hàng</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/feedbacks/list">
                                <i class="fa fa-gavel"></i> <span>Phản hồi</span>
                            </a>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <aside class="right-side">

                <!-- Main content -->
                <section class="content">

                    <div class="row"> 
                        <div class="col-md-12">
                            <a class="btn btn-primary" href="${pageContext.request.contextPath}/marketing/posts" style="margin: 10px 0;">Quay lại</a>
                            <section class="panel">
                                <header style="color: green;" class="panel-heading">
                                    ${msg}
                                </header>
                                <header class="panel-heading">
                                    Thêm sản phẩm
                                </header>
                                <div class="panel-body">
                                    <form action="${pageContext.request.contextPath}/marketing/post/add" role="form" method="post" enctype="multipart/form-data">
                                        <div class="form-group col-md-6">
                                            <label for="title">Tên bài đăng</label>
                                            <input name="title" type="text" class="form-control" id="title" placeholder="Nhập tên" required>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="brief_info">Miêu tả ngắn</label>
                                            <input name="brief_info" type="text" class="form-control" id="brief_info" placeholder="Nhập miêu tả ngắn" required>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="content">Nội dung</label>
                                            <br/>
                                            <textarea 
                                                id="content" 
                                                rows="3" 
                                                placeholder="Nhập mô tả"
                                                style="width: 100%; border: 1px solid gainsboro; border-radius: 5px;"
                                                name="content"
                                                >
                                            </textarea>
                                        </div>
                                        <div class="form-group col-md-2" >
                                            <label style="margin-top: 10px;">Trạng thái</label>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="status" value="1" checked>Hiện 
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="status" value="0" checked="">Ẩn
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">

                                            <label for="product_id" >Đây là bài đăng cho sản phẩm  </label>
                                            <select class="form-control m-b-10" id="product_id" name="product_id" >
                                                <c:forEach items="${productDAO.getAllProducts()}" var="item"> 

                                                    <option value="${item.productId}">${item.productName}</option>
                                                </c:forEach>



                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="postImage">Chọn ảnh</label>
                                            <input type="file" name="postImage" accept="image/*" id="postImage" multiple onchange="ImagesFileAsURL()" required>
                                            <div id="displayImg"></div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <button type="submit" class="btn btn-info">Thêm</button>
                                        </div>
                                        <script type="text/javascript">
                                            function ImagesFileAsURL() {
                                                var fileSelected = document.getElementById('postImage').files;
                                                if (fileSelected.length > 0) {
                                                    for (var i = 0; i < fileSelected.length; i++) {
                                                        var fileToLoad = fileSelected[i];
                                                        var fileReader = new FileReader();
                                                        fileReader.onload = function (fileLoaderEvent) {
                                                            var srcData = fileLoaderEvent.target.result;
                                                            var newImage = document.createElement('img');
                                                            newImage.src = srcData;
                                                            newImage.style.width = "100px";
                                                            document.getElementById('displayImg').innerHTML += newImage.outerHTML;
                                                        }
                                                        fileReader.readAsDataURL(fileToLoad);
                                                    }
                                                }

                                            }
                                        </script>
                                    </form>

                                </div>
                            </section>

                        </div>
                    </div>
                    <!-- row end -->
                </section><!-- /.content -->
                <div class="footer-main">
                    Copyright &copy Director, 2014
                </div>
            </aside><!-- /.right-side -->

        </div><!-- ./wrapper -->


        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/${pageContext.request.contextPath}/view/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/jquery.min.js" type="text/javascript"></script>

        <!-- jQuery UI 1.10.3 -->
        <script src="${pageContext.request.contextPath}/view/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="${pageContext.request.contextPath}/view/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- daterangepicker -->
        <script src="${pageContext.request.contextPath}/view/js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>

        <script src="${pageContext.request.contextPath}/view/js/plugins/chart.js" type="text/javascript"></script>

        <!-- datepicker
        <script src="${pageContext.request.contextPath}/view/js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>-->
        <!-- Bootstrap WYSIHTML5
        <script src="${pageContext.request.contextPath}/view/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>-->
        <!-- iCheck -->
        <script src="${pageContext.request.contextPath}/view/js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
        <!-- calendar -->
        <script src="${pageContext.request.contextPath}/view/js/plugins/fullcalendar/fullcalendar.js" type="text/javascript"></script>

        <!-- Director App -->
        <script src="${pageContext.request.contextPath}/view/js/Director/app.js" type="text/javascript"></script>

        <!-- Director dashboard demo (This is only for demo purposes) -->
        <script src="${pageContext.request.contextPath}/view/js/Director/dashboard.js" type="text/javascript"></script>


    </body>
</html>

