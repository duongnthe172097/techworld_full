<%-- 
    Document   : updateActiveMode
    Created on : Jan 17, 2024, 7:50:10 AM
    Author     : izayo
--%>
<%@ page import="java.sql.*"%>
<%@ page import="dal.DBContext"%> 
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <title>Update Active Mode</title>
        <!-- Add your CSS styles if needed -->

    </head>
    <body>
       <%
           DBContext dbContext = new DBContext();
           Connection connection = null;
           PreparedStatement preparedStatement = null;
           
           try {
           connection = dbContext.getConnection();
           int userIdToUpdate = Integer.parseInt(request.getParameter("user_id"));
           boolean newActiveMode = request.getParameter("active_mode") !=null;
           
           String sql = "UPDATE settings SET iactive=? WHERE user_id=?";
           preparedStatement = connection.prepareStatement(sql);
           preparedStatement.setBoolean(1,newActiveMode);
           preparedStatement.setInt(2,userIdToUpdate);
           
           int rowUpdated = preparedStatement.executeUpdate();
           
           if (rowUpdated > 0) {
                out.println("<p> Active mode changed completed for this userId " + userIdToUpdate + ".</p>");
           } else {
                out.println("<p> Active mode haven't changed completed for this userId" + userIdToUpdate + ".</p>");
           }
           } catch (SQLException e) {
                e.printStackTrace();
           }
           %>
    </body>
</html>
