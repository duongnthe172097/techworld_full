<%-- 
    Document   : user-list
    Created on : Jan 21, 2024, 7:49:36 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import = "model.*" %>
<%@page import = "dal.*" %>
<%@page import = "java.sql.Date" %>
<%@page import = "java.util.*" %>
<%@page import = "java.text.DecimalFormat" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Quản lý người dùng | Admin</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta name="description" content="Developed By M Abdur Rokib Promy">
        <meta name="keywords" content="Admin, Bootstrap 3, Template, Theme, Responsive">
        <!-- bootstrap 3.0.2 -->
        <link href="${pageContext.request.contextPath}/view/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="${pageContext.request.contextPath}/view/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="${pageContext.request.contextPath}/view/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="${pageContext.request.contextPath}/view/css/morris/morris.css" rel="stylesheet" type="text/css" />
        <!-- jvectormap -->
        <link href="${pageContext.request.contextPath}/view/css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
        <!-- Date Picker -->
        <link href="${pageContext.request.contextPath}/view/css/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
        <!-- fullCalendar -->
        <!-- <link href="${pageContext.request.contextPath}/view/css/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" /> -->
        <!-- Daterange picker -->
        <link href="${pageContext.request.contextPath}/view/css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
        <!-- iCheck for checkboxes and radio inputs -->
        <link href="${pageContext.request.contextPath}/view/css/iCheck/all.css" rel="stylesheet" type="text/css" />
        <!-- bootstrap wysihtml5 - text editor -->
        <!-- <link href="${pageContext.request.contextPath}/view/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" /> -->
        <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
        <!-- Theme style -->
        <link href="${pageContext.request.contextPath}/view/css/style.css" rel="stylesheet" type="text/css" />



        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.${pageContext.request.contextPath}/view/js/1.3.0/respond.min.js"></script>
          <![endif]-->

        <style type="text/css">

        </style>
    </head>
    <%
        List<User> allUsers = (List<User>)request.getAttribute("allUsers");
        RoleDAO roleDAO = new RoleDAO();
        List<Role> allRoles = roleDAO.getAllRoles();
        
        int numberOfPages = (int)request.getAttribute("numberOfPages");
        int pageNo = (int)request.getAttribute("page");
        
        String searchKey = (String)request.getAttribute("searchKey");
        
        String sortType = (String)request.getAttribute("sortType");
        if(sortType == null) {
            sortType = "";
        }
        
        boolean[] checkedRoles = (boolean[])request.getAttribute("checkedRoles");
        String gender = (String)request.getAttribute("gender") == null ? "" : (String)request.getAttribute("gender");
        String status = (String)request.getAttribute("status") == null ? "" : (String)request.getAttribute("status");
    %>

    <body class="skin-black">
        <style>
            .filter-form {
                display: flex;
            }

            .sort-form {
                display: flex;
                justify-content: space-around;
            }
        </style>
        <!-- header logo: style can be found in header.less -->
        <jsp:include page="admin-header.jsp"></jsp:include>
            <div class="wrapper row-offcanvas row-offcanvas-left">
                <!-- Left side column. contains the logo and sidebar -->
                <aside class="left-side sidebar-offcanvas">
                    <!-- sidebar: style can be found in sidebar.less -->
                    <section class="sidebar">
                        <!-- Sidebar user panel -->
                        <div class="user-panel">
                            <div class="pull-left image">
                                <img src="${pageContext.request.contextPath}/view/img/26115.jpg" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Xin chào, ${sessionScope.user.userName}</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>

                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li>
                            <a href="${pageContext.request.contextPath}/admin/admin-dashboard">
                                <i class="fa fa-dashboard"></i> <span>Thống kê</span>
                            </a>
                        </li>
                        <li class="active">
                            <a href="${pageContext.request.contextPath}/admin/user">
                                <i class="fa fa-gavel"></i> <span>Người dùng</span>
                            </a>
                        </li>

                        <li>
                            <a href="${pageContext.request.contextPath}/admin/settings/view">
                                <i class="fa fa-globe"></i> <span>Cài đặt</span>
                            </a>
                        </li>

                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>
            <aside class="right-side">

                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <section class="panel">
                                    <header class="panel-heading">
                                        Bộ lọc
                                    </header>
                                    <div class="panel-body">
                                        <form action="${pageContext.request.contextPath}/admin/user" class="filter-form">
                                            <div class="col-lg-6">
                                                <h4>Vai trò</h4>
                                                <%
                                                    for(int i = 0; i < allRoles.size(); i++) {
                                                %>
                                                <div>
                                                    <input 
                                                        type="checkbox" 
                                                        name="roleId" 
                                                        value="<%=allRoles.get(i).getRoleId()%>"
                                                        <%= checkedRoles!= null && checkedRoles[i] ? "checked" : "" %>
                                                        onclick="this.form.submit()"
                                                        /> <%=allRoles.get(i).getRoleName()%>
                                                </div>
                                                <%}%>
                                            </div>
                                            <div class="col-lg-6">
                                                <div>
                                                    <h4>Giới tính</h4>
                                                    <div>
                                                        <input 
                                                            type="radio" 
                                                            name="gender" 
                                                            value="1"
                                                            <%= gender != null && gender.equals("1") ? "checked" : "" %>
                                                            onclick="this.form.submit()"
                                                            /> Nam
                                                    </div>
                                                    <div>
                                                        <input 
                                                            type="radio" 
                                                            name="gender" 
                                                            value="0"
                                                            <%= gender != null && gender.equals("0") ? "checked" : "" %>
                                                            onclick="this.form.submit()"
                                                            /> Nữ
                                                    </div>
                                                    <div>
                                                        <input 
                                                            type="radio" 
                                                            name="gender" 
                                                            value="2"
                                                            <%= gender != null && gender.equals("2") ? "checked" : "" %>
                                                            onclick="this.form.submit()"
                                                            /> Cả hai
                                                    </div>
                                                </div>
                                                <div>
                                                    <h4>Trạng thái</h4>
                                                    <div>
                                                        <input 
                                                            type="radio" 
                                                            name="status" 
                                                            value="1"
                                                            <%= status != null && status.equals("1") ? "checked" : "" %>
                                                            onclick="this.form.submit()"
                                                            /> 
                                                        Hợp lệ
                                                    </div>
                                                    <div>
                                                        <input 
                                                            type="radio" 
                                                            name="status" 
                                                            value="0"
                                                            <%= status != null && status.equals("0") ? "checked" : "" %>
                                                            onclick="this.form.submit()"
                                                            />
                                                        Bị cấm
                                                    </div>
                                                    <div>
                                                        <input 
                                                            type="radio" 
                                                            name="status" 
                                                            value="2"
                                                            <%= status != null && status.equals("2") ? "checked" : "" %>
                                                            onclick="this.form.submit()"
                                                            /> Cả hai
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                </section>
                            </div>

                            <div class="col-md-6">
                                <section class="panel">
                                    <header class="panel-heading ">
                                        Sắp xếp
                                    </header>
                                    <div class="panel-body sort-form">
                                        <span>
                                            <input 
                                                type="radio" name="sortType" value="1" 
                                                <%= sortType.contains("1") ? "checked" : ""%> 
                                                onclick="this.form.submit()"
                                                /> ID
                                        </span>
                                        <span>
                                            <input type="radio" name="sortType" value="2" 
                                                   <%= sortType.contains("2") ? "checked" : ""%> 
                                                   onclick="this.form.submit()"
                                                   />Tên
                                        </span>
                                        <span>
                                            <input 
                                                type="radio" name="sortType" value="3" 
                                                <%= sortType.contains("3") ? "checked" : ""%> 
                                                onclick="this.form.submit()"
                                                />Giới tính
                                        </span>
                                        <span>
                                            <input type="radio" name="sortType" value="4" 
                                                   <%= sortType.contains("4") ? "checked" : ""%> 
                                                   onclick="this.form.submit()"
                                                   />Email
                                        </span>
                                        <span>
                                            <input type="radio" name="sortType" value="5" 
                                                   <%= sortType.contains("5") ? "checked" : ""%> 
                                                   onclick="this.form.submit()"
                                                   />Điện thoại
                                        </span>
                                        <span>
                                            <input type="radio" name="sortType" value="6" 
                                                   <%= sortType.contains("6") ? "checked" : ""%> 
                                                   onclick="this.form.submit()"
                                                   />Vai trò
                                        </span>
                                        <span>
                                            <input type="radio" name="sortType" value="7" 
                                                   <%= sortType.contains("7") ? "checked" : ""%> 
                                                   onclick="this.form.submit()"
                                                   />Trạng thái
                                        </span>
                                        </form>
                                    </div>
                                </section>
                                <section class="panel">
                                    <header class="panel-heading">
                                        Tìm kiếm
                                    </header>
                                    <div class="panel-body">
                                        <form action="${pageContext.request.contextPath}/admin/user">
                                            <input type="text" name="search" placeholder="Nhập từ khóa" value="<%= searchKey != null ? searchKey : ""%>" style="padding: 4px;"/>
                                            <input class="btn btn-primary" type="submit" value="Tìm kiếm"/>
                                        </form>
                                    </div>
                                </section>
                                <div class="panel">
                                    <div class="panel-body">
                                        <a href="${pageContext.request.contextPath}/view/add-userform.jsp">+ Thêm người dùng</a>
                                    </div>
                                </div>
                            </div>                

                        </div>
                        <div class="col-md-12">
                            <section class="panel">
                                <header class="panel-heading">
                                    Danh sách người dùng
                                </header>
                                <div class="panel-body table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>STT</th>
                                                <th>ID</th>
                                                <th>Tên</th>
                                                <th>Giới tính</th>
                                                <th>Email</th> 
                                                <th>Điện thoại</th>
                                                <th>Vai trò</th> 
                                                <th>Trạng thái</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%
                                                int index = 1;
                                                for(User user : allUsers) {
                                            %>
                                            <tr>
                                                <td><%=index%></td>
                                                <td><%=user.getUserId()%></td>
                                                <td><%=user.getFullName()%></td>
                                                <td><%=user.isGender() ? "Nam" : "Nữ"%></td>
                                                <td><%=user.getEmail()%></td>
                                                <td><%=user.getMobile()%></td> 
                                                <td><%=roleDAO.getRoleNameById(user.getRoleId())%></td>
                                                <td>
                                                    <span class="label <%=user.isStatus() ? "label-success" : "label-danger"%> ">
                                                        <%=user.isStatus() ? "Hợp lệ" : "Bị cấm"%>
                                                    </span>
                                                </td>
                                                <td><a href="${pageContext.request.contextPath}/view-userdetails?user_id=<%=user.getUserId()%>">Xem chi tiết</a></td>
                                                <% index++; %>
                                            </tr>
                                            <%}%>
                                        </tbody>
                                    </table>
                                    <div class="pagination-area col-sm-12">
                                        <ul class="pagination">
                                            <%
                                                for(int i = 0; i < numberOfPages; i++) {
                                            %>
                                            <li><a href="${pageContext.request.contextPath}/admin/user?page=<%= i + 1%>" class="<%= i == pageNo ? "active" : ""%>"><%= i + 1%></a></li>
                                                <%}%>
                                            <li><a href=""><i class="fa fa-angle-double-right"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </section>


                        </div><!--end col-6 -->


                    </div>

                    <!-- row end -->
                </section><!-- /.content -->
                <div class="footer-main">
                    Copyright &copy Director, 2014
                </div>
            </aside><!-- /.right-side -->

        </div><!-- ./wrapper -->


        <!-- jQuery 2.0.2 -->
        <script src="${pageContext.request.contextPath}/view/js/jquery.min.js" type="text/javascript"></script>

        <!-- jQuery UI 1.10.3 -->
        <script src="${pageContext.request.contextPath}/view/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="${pageContext.request.contextPath}/view/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- daterangepicker -->
        <script src="${pageContext.request.contextPath}/view/js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>

        <script src="${pageContext.request.contextPath}/view/js/plugins/chart.js" type="text/javascript"></script>

        <!-- datepicker
        <script src="${pageContext.request.contextPath}/view/js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>-->
        <!-- Bootstrap WYSIHTML5
        <script src="${pageContext.request.contextPath}/view/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>-->
        <!-- iCheck -->
        <script src="${pageContext.request.contextPath}/view/js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
        <!-- calendar -->
        <script src="${pageContext.request.contextPath}/view/js/plugins/fullcalendar/fullcalendar.js" type="text/javascript"></script>

        <!-- Director App -->
        <script src="${pageContext.request.contextPath}/view/js/Director/app.js" type="text/javascript"></script>

        <!-- Director dashboard demo (This is only for demo purposes) -->
        <script src="${pageContext.request.contextPath}/view/js/Director/dashboard.js" type="text/javascript"></script>

        <!-- Director for demo purposes -->
        <script type="text/javascript">
                                                       $('input').on('ifChecked', function (event) {
                                                           // var element = $(this).parent().find('input:checkbox:first');
                                                           // element.parent().parent().parent().addClass('highlight');
                                                           $(this).parents('li').addClass("task-done");
                                                           console.log('ok');
                                                       });
                                                       $('input').on('ifUnchecked', function (event) {
                                                           // var element = $(this).parent().find('input:checkbox:first');
                                                           // element.parent().parent().parent().removeClass('highlight');
                                                           $(this).parents('li').removeClass("task-done");
                                                           console.log('not');
                                                       });

        </script>
        <script>
            $('#noti-box').slimScroll({
                height: '400px',
                size: '5px',
                BorderRadius: '5px'
            });

            $('input[type="checkbox"].flat-grey, input[type="radio"].flat-grey').iCheck({
                checkboxClass: 'icheckbox_flat-grey',
                radioClass: 'iradio_flat-grey'
            });
        </script>
    </body>
</html>