<%-- 
    Document   : cart
    Created on : Jan 8, 2024, 4:20:43 PM
    Author     : admin
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Chi tiết đơn hàng | Tech World</title>
        <link href="${pageContext.request.contextPath}/view/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/font-awesome.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/prettyPhoto.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/price-range.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/animate.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/main.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/responsive.css" rel="stylesheet">

    </head><!--/head-->

    <body>
        <!-- ======= Header ======= -->
        <style type="text/css">

            .product-homepage {
                background-color: #ffffff;
                border: none;
                box-shadow: rgba(0, 0, 0, 0.1) 0px 10px 15px -3px, rgba(0, 0, 0, 0.05) 0px 4px 6px -2px;
            }

            .product-homepage span {
                color: #f19d23;
                font-size: 20px;
                font-weight: 400;
            }

            .product-homepage span del {
                color: red;
                font-size: 12px;
            }

            .product-homepage img {
                box-shadow: rgba(255, 255, 255, 0.56) 0px 22px 70px 4px;
            }

            .product-homepage p {
                color: #000000;
                font-size: 18px;
                margin: 20px 0;
                min-height: 50px;
            }

            .post-homepage {
                background-color: #ffffff;
                border: none;
            }

            .post-homepage img {
                box-shadow: rgba(255, 255, 255, 0.56) 0px 22px 70px 4px;
            }

            #header {
                background-color: #7FCDF9;
                box-shadow: rgba(0, 0, 0, 0.1) 0px 4px 6px -1px, rgba(0, 0, 0, 0.06) 0px 2px 4px -1px;
            }

            .logo-techworld img{
                width: 120px;
                height: auto;
            }

            .header-searchform input{
                background-color: #ffffff;
                color: #333333;
            }

            @media (min-width: 768px) {

                #product-homepage {
                    min-height: 220vh;
                }
                .product-homepage {
                    height: 360px;
                    overflow: hidden;
                    padding: 3px;
                }
                .product-homepage img {
                    height: 150px;
                    width: fit-content;
                    max-width: 100%;
                }

                .post-homepage {
                    padding: 12px;
                    height: 430px;
                }

                .post-homepage img {
                    height: 150px;
                    width: fit-content;
                    width: 100%;
                    margin-bottom: 7px;
                }

                .post-homepage h5{
                    height: 25px;
                }

                .post-homepage p{
                    height: 150px;
                }
            }
        </style>
        <!-- ======= Header ======= -->
        <header id="header"><!--header-->
            <div class="header_top"><!--header_top-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="contactinfo">
                                <ul class="nav nav-pills">
                                    <li><a href="#"><i class="fa fa-phone"></i> +2 95 01 88 821</a></li>
                                    <li><a href="#"><i class="fa fa-envelope"></i> minhnhhe170924@fpt.edu.vn</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="social-icons pull-right">
                                <ul class="nav navbar-nav">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/header_top-->

            <div class="header-middle"><!--header-middle-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="logo pull-left logo-techworld">
                                <a href="${pageContext.request.contextPath}/home"><img src="${pageContext.request.contextPath}/view/images/home/tech_logo.png" alt="" /></a>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="shop-menu pull-right">
                                <ul class="nav navbar-nav">
                                    <c:if test="${sessionScope.user == null}">
                                        <li><a href="${pageContext.request.contextPath}/view/login.jsp"><i class="fa fa-user"></i> Tài khoản</a></li>
                                        <li><a href="${pageContext.request.contextPath}/view/login.jsp"><i class="fa fa-lock"></i> Đăng nhập</a></li>
                                        </c:if>
                                        <c:if test="${sessionScope.user != null}">
                                        <li><a href="${pageContext.request.contextPath}/profile"><i class="fa fa-user"></i> ${sessionScope.user.fullName}</a></li>
                                            <c:if test="${sessionScope.user.roleId == 5}">
                                            <li><a href="${pageContext.request.contextPath}/admin"><i class="fa fa-crosshairs"></i> Quản lý</a></li>
                                            </c:if>
                                            <c:if test="${sessionScope.user.roleId == 1}">
                                            <li><a href="${pageContext.request.contextPath}/cartController"><i class="fa fa-shopping-cart"></i>Giỏ hàng</a></li>
                                            <li><a href="${pageContext.request.contextPath}/customer/my-order" class="active"><i class="fa fa-crosshairs"></i> Đơn hàng của tôi</a></li>
                                            </c:if>
                                        <li><a href="${pageContext.request.contextPath}/logout"><i class="fa fa-lock"></i> Đăng xuất</a></li>
                                        </c:if>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/header-middle-->

            <div class="header-bottom"><!--header-bottom-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-9">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="mainmenu pull-left">
                                <ul class="nav navbar-nav collapse navbar-collapse">
                                    <li><a href="${pageContext.request.contextPath}/home">Trang chủ</a></li>
                                    <li><a href="${pageContext.request.contextPath}/products">Sản phẩm</a></li>
                                    <li><a href="${pageContext.request.contextPath}/blogs">Bài đăng</a></li>
                                    <li><a href="${pageContext.request.contextPath}/view/contact-us.jsp">Liên hệ</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <form action="${pageContext.request.contextPath}/home" class="search_box pull-right header-searchform">
                                <input type="text" name="search" placeholder="Tìm kiếm sản phẩm" value="${requestScope.searchKey}"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!--/header-bottom-->
        </header>


        <div class="container">

            <h2 class="text-center mb-4">Chi tiết đặt hàng</h2>
            <div class="container">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">

                    </ol>
                </div><!--/breadcrums-->

                <!-- Display Order Information -->
                <div class="mb-4 row">
                    <div class="col-md-6">
                        <p><strong>ID đơn hàng:</strong> ${order.orderId}</p>
                        <p><strong>Ngày đặt hàng:</strong> <fmt:formatDate value="${order.orderDate}" pattern="yyyy-MM-dd HH:mm:ss"/></p>
                        <p><strong>Tổng giá thành:</strong> <fmt:formatNumber value="${totalCost}" pattern="#,##0" /></p>
                        <p><strong>Trạng thái:</strong> ${order.stateString} </p>
                    </div>
                    <div class="col-md-6">
                        <p><strong>Tên người nhận:</strong> ${user.fullName}</p>
                        <p><strong>Email:</strong> ${user.email}</p>
                        <p><strong>Di động:</strong> ${user.mobile}</p>
                        <p><strong>Địa chỉ:</strong> ${user.address}</p>
                    </div>         
                </div>

                <!-- Display Order Details Table -->
                <table class="table table-bordered mt-5">
                    <thead class="thead-dark">
                        <tr>
                            <th>ID</th>
                            <th>Tên sản phẩm</th>
                            <th>Giá</th>
                            <th>Số lượng</th>
                            <th>Tổng giá thành</th>
                            <th>Trạng thái</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="orderDetail" items="${orderDetails}">
                            <tr>
                                <td>${orderDetail.productId}</td>
                                <td>${orderDetail.product.productName}</td>
                                <td><fmt:formatNumber value="${orderDetail.product.price}" pattern="#,##0" /></td>
                                <td>${orderDetail.quantity}</td>
                                <td><fmt:formatNumber value="${orderDetail.totalCost}" pattern="#,##0" /></td>
                                <td>
                                    <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#productModal${orderDetail.productId}">
                                        Xem
                                    </button>
                                    <a class="btn btn-secondary" target="_blank" href="${pageContext.request.contextPath}/product?id=${orderDetail.productId}">Mua lại</a>
                                    <c:if test = "${!orderDetail.getIsFeedback(user) && order.state eq 1}">
                                        <a class="btn btn-warning" data-toggle="modal" data-target="#feedbackModal${orderDetail.orderId * 1000 + orderDetail.productId}">Đánh giá</a>
                                    </c:if>

                                </td>
                            </tr>

                            <!-- Product Modal -->
                        <div class="modal fade" id="productModal${orderDetail.productId}" tabindex="-1" role="dialog" aria-labelledby="productModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="productModalLabel">Thông tin sản phẩm</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <!-- Display product information here -->
                                        <div class="w-100 text-center mb-5">
                                            <img style="width: 100%; object-fit: cover;" src="${pageContext.request.contextPath}/view/images/product/${orderDetail.product.productImage.get(0).imageUrl}" alt="">
                                        </div>
                                        <p><strong>Tên sản phẩm:</strong> ${orderDetail.product.productName}</p>
                                        <p><strong>Tóm tắt sản phẩm:</strong> ${orderDetail.product.description}</p>
                                        <p><strong>Giá:</strong> <fmt:formatNumber value="${orderDetail.product.price}" pattern="#,##0" /></p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="feedbackModal${orderDetail.orderId * 1000 + orderDetail.productId}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Đánh giá</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form id="feedbackForm" action="feedback" method="post">
                                            <input type="hidden" name="id" value="${orderDetail.productId}">
                                            <input type="hidden" name="orderId" value="${order.orderId}">
                                            <div class="mb-3">
                                                <label for="feedbackContent" class="form-label">Nội dung đánh giá</label>
                                                <textarea class="form-control" id="feedbackContent" name="content" rows="3" required></textarea>
                                            </div>
                                            <div class="mb-3">
                                                <label for="feedbackRating" class="form-label">Sao</label>
                                                <input type="hidden" id="rating" name="rate" value="0" required/>
                                                <div class="star-rating">
                                                    <span class="star" data-value="1" onclick="setRating(1)" style="font-size: 24px; cursor: pointer;">☆</span>
                                                    <span class="star" data-value="2" onclick="setRating(2)" style="font-size: 24px; cursor: pointer;">☆</span>
                                                    <span class="star" data-value="3" onclick="setRating(3)" style="font-size: 24px; cursor: pointer;">☆</span>
                                                    <span class="star" data-value="4" onclick="setRating(4)" style="font-size: 24px; cursor: pointer;">☆</span>
                                                    <span class="star" data-value="5" onclick="setRating(5)" style="font-size: 24px; cursor: pointer;">☆</span>
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-primary">Gửi đánh giá</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </c:forEach>
                    </tbody>
                </table>

                <!-- Order Actions -->
                <div class="text-center mt-5">
                    <!-- Cancel button (if not "Delivered") -->
                    <c:if test="${order.state eq 3}">
                        <a class="btn btn-danger" href="/customer/order-detail?id=${order.orderId}&action=cancel">Cancel Order</a>
                    </c:if>
                </div>

            </div>
        </div>

        <!-- ======= Footer ======= -->
        <jsp:include page="footer.jsp"></jsp:include>



            <script src="${pageContext.request.contextPath}/view/js/jquery.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/jquery.scrollUp.min.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/jquery.prettyPhoto.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/main.js"></script>

        <script>
                                                        function setRating(value) {
                                                            const stars = document.querySelectorAll('.star');
                                                            const ratingInput = document.getElementById('rating');
                                                            ratingInput.value = value;
                                                            stars.forEach(function (star) {
                                                                const starValue = parseInt(star.getAttribute('data-value'));
                                                                if (starValue <= value) {
                                                                    star.innerHTML = '⭐';
                                                                } else {
                                                                    star.innerHTML = '☆';
                                                                }
                                                            });
                                                        }

        </script>

    </body>
</html>