<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import = "model.*" %>
<%@page import = "dal.*" %>
<%@page import = "java.sql.Date" %>
<%@page import = "java.util.*" %>
<%@page import = "java.text.DecimalFormat" %>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Thống kê | Marketing</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta name="description" content="Developed By M Abdur Rokib Promy">
        <meta name="keywords" content="Admin, Bootstrap 3, Template, Theme, Responsive">
        <!-- bootstrap 3.0.2 -->
        <link href="${pageContext.request.contextPath}/view/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="${pageContext.request.contextPath}/view/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="${pageContext.request.contextPath}/view/css/ionicons.min.css" rel="stylesheet" type="text/css" />

        <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
        <!-- Theme style -->
        <link href="${pageContext.request.contextPath}/view/css/style.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="https://cdn.datatables.net/1.13.7/css/jquery.dataTables.css" />
    </head>

    <%
            DecimalFormat decimalFormat = new DecimalFormat("#,###");
        
            String customerType = (String)request.getAttribute("customerType");
            List<User> newlyRegisteredCustomers = (List<User>)request.getAttribute("newlyRegisteredCustomers");
            
            LinkedHashMap<User, String[]> newlyBoughtCustomers = (LinkedHashMap<User, String[]>)request.getAttribute("newlyBoughtCustomers");
            Set<User> userSet = newlyBoughtCustomers.keySet();
            
            LinkedHashMap<Feedback, String[]> newlyFeedbacks = (LinkedHashMap<Feedback, String[]>)request.getAttribute("newlyFeedbacks");
            Set<Feedback> feedbackSet = newlyFeedbacks.keySet();
    %>

    <body class="skin-black">

        <%!
            public String limitString(String str, int maxLength) {
                if (str.length() > maxLength) {
                    return str.substring(0, maxLength) + "...";
                }
            return str;
            }
        %>
        <style type="text/css">
            #noti-box .alert {
                font-size: 18px;
            }

            .panel-body {
                max-height: 500px;
                overflow-y: auto;
                display: block;
            }

            .newly-feedback {
                max-height: 500px;
                min-height: 450px;
                overflow-y: auto;
                display: block;
            }

            .newly-feedback ul li {
                max-height: 150px;
            }

            .newly-feedback ul li div img {
                height: 10px;
                margin-left: 40px;
                margin-top: -20px;
            }

            .newly-feedback .feedback-info {
                font-size: 10px;
                color: gray;
            }

            .navbar-static-top form {
                padding: 8px;
            }

            section .date-form {
                background-color: #ffffff;
                padding: 20px;
                border-radius: 1px;
            }

            section .date-form div {
                font-size: 20px;
                font-weight: 600;
                margin-bottom: 10px;
            }

            section .date-form input {
                border: 1px solid gainsboro;
                height: 25px;
                padding: 15px;
                margin-right: 20px;
            }
        </style>

        <!-- header logo: style can be found in header.less -->
        <jsp:include page="admin-header.jsp"></jsp:include>
            <div class="wrapper row-offcanvas row-offcanvas-left">
                <!-- Left side column. contains the logo and sidebar -->
                <aside class="left-side sidebar-offcanvas">
                    <!-- sidebar: style can be found in sidebar.less -->
                    <section class="sidebar">
                        <!-- Sidebar user panel -->
                        <div class="user-panel">
                            <div class="pull-left image">
                                <img src="${pageContext.request.contextPath}/view/img/26115.jpg" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Xin chào, ${sessionScope.user.userName}</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>

                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="active">
                            <a href="${pageContext.request.contextPath}/marketing/marketing-dashboard">
                                <i class="fa fa-dashboard"></i> <span>Thống kê</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/posts">
                                <i class="fa fa-gavel"></i> <span>Bài đăng</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/view-sliders">
                                <i class="fa fa-gavel"></i> <span>Slider</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/product/list">
                                <i class="fa fa-gavel"></i> <span>Sản phẩm</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/marketing-viewcustomer">
                                <i class="fa fa-gavel"></i> <span>Khách hàng</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/feedbacks/list">
                                <i class="fa fa-gavel"></i> <span>Phản hồi</span>
                            </a>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <aside class="right-side">

                <!-- Main content -->
                <section class="content">
                    <form action="${pageContext.request.contextPath}/marketing/marketing-dashboard" class="date-form" style="margin-bottom:15px;">
                        <div>Thống kê dữ liệu</div>
                        <input type="hidden" name="view" value="statistic"/>
                        Từ: <input type="date" name="fromDate" value="${requestScope.fromDate}" onchange="this.form.submit()"/>
                        Đến: <input type="date" name="toDate" value="${requestScope.toDate}" onchange="this.form.submit()"/>
                    </form>

                    <div class="row" style="margin-bottom:5px;">
                        <div class="col-md-12">
                            <ul class="nav nav-tabs"  style="margin-bottom:5px; background-color: #ffffff;">
                                <li class="nav-item">
                                    <a class="nav-link active" aria-current="page" style="color: gray"
                                       href="#">Thống kê
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" 
                                       href="${pageContext.request.contextPath}/marketing/marketing-dashboard?view=products&fromDate=${requestScope.fromDate}&toDate=${requestScope.toDate}"
                                       >Sản phẩm
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" 
                                       href="${pageContext.request.contextPath}/marketing/marketing-dashboard?view=posts&fromDate=${requestScope.fromDate}&toDate=${requestScope.toDate}"
                                       >Bài đăng
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="row" style="margin-bottom:5px;">
                        <div class="col-md-4">
                            <div class="sm-st clearfix">
                                <span class="sm-st-icon st-red"><i class="fa fa-check-square-o"></i></span>
                                <div class="sm-st-info">
                                    <span>${requestScope.staticOrder[0]}</span>
                                    Đơn hàng đã giao 
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="sm-st clearfix">
                                <span class="sm-st-icon st-violet"><i class="fa fa-envelope-o"></i></span>
                                <div class="sm-st-info">
                                    <span>${requestScope.staticOrder[1]}</span>
                                    Đơn hàng đã hủy
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="sm-st clearfix">
                                <span class="sm-st-icon st-blue"><i class="fa fa-dollar"></i></span>
                                <div class="sm-st-info">
                                    <span>${requestScope.staticOrder[2]}</span>
                                    Đơn hàng đã đặt
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Main row -->

                    <div class="row">
                        <div class="col-md-8">
                            <!-- Newly customer -->
                            <section class="panel">
                                <header class="panel-heading">
                                    <form action="${pageContext.request.contextPath}/marketing/marketing-dashboard?view=statistic">
                                        <input type="hidden" name="view" value="statistic"/>
                                        <input type="hidden" name="fromDate" value="${requestScope.fromDate}"/>
                                        <input type="hidden" name="toDate" value="${requestScope.toDate}"/>
                                        <select style="border:none" name="customerType" onchange="this.form.submit()">
                                            <option 
                                                value="registered"
                                                ${requestScope.customerType.equals("registered") ? ' selected' : ''}
                                                >
                                                KHÁCH HÀNG MỚI (<%=newlyRegisteredCustomers.size()%>)
                                            </option>
                                            <option 
                                                value="bought"
                                                ${requestScope.customerType.equals("bought") ? ' selected' : ''}
                                                >
                                                NGƯỜI MUA HÀNG MỚI (<%=newlyBoughtCustomers.size()%>)
                                            </option>
                                        </select>
                                    </form>
                                </header>
                                <div class="panel-body table-responsive">
                                    <%if(customerType.equals("registered")){%>
                                    <%if(newlyRegisteredCustomers.size() > 0) {%>
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Tên</th>
                                                <th>Email</th>
                                                <th>Điện thoại</th>
                                                <th>Địa chỉ</th>
                                                <th>Giới tính</th>
                                                <th>Ngày đăng ký</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%for(int i = 0; i < newlyRegisteredCustomers.size(); i++) {%>
                                            <tr>
                                                <td><%=i + 1%></td>
                                                <td><%=newlyRegisteredCustomers.get(i).getFullName()%></td>
                                                <td><%=newlyRegisteredCustomers.get(i).getEmail()%></td>
                                                <td><%=newlyRegisteredCustomers.get(i).getMobile()%></td>
                                                <td><%=limitString(newlyRegisteredCustomers.get(i).getAddress(), 100)%></td>
                                                <td><span class="badge badge-info"></span><%=newlyRegisteredCustomers.get(i).isGender() ? "Nam" : "Nữ"%></td>
                                                <td><span class="badge badge-info"></span><%=newlyRegisteredCustomers.get(i).getUpdatedDate()%></td>
                                            </tr>
                                            <%}%>
                                        </tbody>
                                    </table>
                                    <%}else {%>
                                    Không có khách hàng mới.
                                    <%}%>
                                </div>
                                <%}%>

                                <!-- Newly bought -->
                                <%if(customerType.equals("bought")){%>
                                <div class="panel-body table-responsive">
                                    <%if(userSet.size() > 0) {%>
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Tên</th>
                                                <th>Điện thoại</th>
                                                <th>Ngày đặt hàng</th>
                                                <th>Tình trạng đơn</th>
                                                <th>Số lượng</th>
                                                <th>Tổng số tiền</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%int indexUserSet = 1;%>
                                            <%for(User u : userSet) {%>
                                            <tr>
                                                <td><%=indexUserSet%></td>
                                                <td><%=u.getFullName()%></td>
                                                <td><%=u.getMobile()%></td>
                                                <td><%=newlyBoughtCustomers.get(u)[0]%></td>
                                                <td>
                                                    <%=newlyBoughtCustomers.get(u)[1].equals("1") ? "Đã đặt" : ""%>
                                                    <%=newlyBoughtCustomers.get(u)[1].equals("2") ? "Đã hủy" : ""%>
                                                    <%=newlyBoughtCustomers.get(u)[1].equals("3") ? "Đã giao" : ""%>
                                                </td>
                                                <td><%=newlyBoughtCustomers.get(u)[2]%></td>
                                                <td><%=decimalFormat.format(Double.parseDouble(newlyBoughtCustomers.get(u)[3]))%></td>
                                                <%indexUserSet++;%>
                                            </tr>
                                            <%}%>
                                        </tbody>
                                    </table>
                                    <%}else {%>
                                    Không có người mua hàng mới.
                                    <%}%>
                                </div>
                                <%}%>
                            </section>

                        </div>

                        <div class="col-md-4">
                            <div class="panel newly-feedback">
                                <header class="panel-heading">
                                    Đánh giá gần đây (<%=newlyFeedbacks.size()%>)
                                </header>
                                <%if(feedbackSet.size() > 0) {%>
                                <ul class="list-group teammates">
                                    <%for(Feedback f : feedbackSet){%>
                                    <li class="list-group-item">
                                        <span>
                                            <a href=""><img src="${pageContext.request.contextPath}/view/images/user/<%=newlyFeedbacks.get(f)[1]%>" width="25" height="25"></a>
                                            <strong><%=newlyFeedbacks.get(f)[0]%></strong>
                                        </span> 
                                        <div>
                                            <img src="${pageContext.request.contextPath}/view/images/feedback/<%=f.getRatedStar()%>.png"/>
                                        </div>
                                        <p class="feedback-info"><%=f.getUpdatedDate()%> | Mặt hàng: <%=newlyFeedbacks.get(f)[2]%></p>
                                        <p class="feedback-content"><%=limitString(f.getContent(),100 )%></p>
                                    </li>
                                    <%}%>
                                </ul>
                                <%} else{%>
                                Không có đánh giá mới.
                                <%}%>
                            </div>
                        </div>

                        <div>

                        </div>
                    </div>
                    <!-- row end -->
                </section><!-- /.content -->
                <div class="footer-main">
                    Copyright &copy Director, 2014
                </div>
            </aside><!-- /.right-side -->

        </div><!-- ./wrapper -->


        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/${pageContext.request.contextPath}/view/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/jquery.min.js" type="text/javascript"></script>

        <!-- jQuery UI 1.10.3 -->
        <script src="${pageContext.request.contextPath}/view/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="${pageContext.request.contextPath}/view/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- daterangepicker -->
        <script src="${pageContext.request.contextPath}/view/js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>

        <script src="${pageContext.request.contextPath}/view/js/plugins/chart.js" type="text/javascript"></script>

        <!-- datepicker
        <script src="${pageContext.request.contextPath}/view/js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>-->
        <!-- Bootstrap WYSIHTML5
        <script src="${pageContext.request.contextPath}/view/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>-->
        <!-- iCheck -->
        <script src="${pageContext.request.contextPath}/view/js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
        <!-- calendar -->
        <script src="${pageContext.request.contextPath}/view/js/plugins/fullcalendar/fullcalendar.js" type="text/javascript"></script>

        <!-- Director App -->
        <script src="${pageContext.request.contextPath}/view/js/Director/app.js" type="text/javascript"></script>

        <!-- Director dashboard demo (This is only for demo purposes) -->
        <script src="${pageContext.request.contextPath}/view/js/Director/dashboard.js" type="text/javascript"></script>

        <!-- Director for demo purposes -->
        <script type="text/javascript">
                                            $('input').on('ifChecked', function (event) {
                                                // var element = $(this).parent().find('input:checkbox:first');
                                                // element.parent().parent().parent().addClass('highlight');
                                                $(this).parents('li').addClass("task-done");
                                                console.log('ok');
                                            });
                                            $('input').on('ifUnchecked', function (event) {
                                                // var element = $(this).parent().find('input:checkbox:first');
                                                // element.parent().parent().parent().removeClass('highlight');
                                                $(this).parents('li').removeClass("task-done");
                                                console.log('not');
                                            });

        </script>
        <script>
            $('#noti-box').slimScroll({
                height: '400px',
                size: '5px',
                BorderRadius: '5px'
            });

            $('input[type="checkbox"].flat-grey, input[type="radio"].flat-grey').iCheck({
                checkboxClass: 'icheckbox_flat-grey',
                radioClass: 'iradio_flat-grey'
            });
        </script>

    </body>
</html>