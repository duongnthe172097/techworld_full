<%-- 
    Document   : left-sidebar
    Created on : Jan 11, 2024, 1:21:35 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import = "model.*" %>
<%@page import = "dal.*" %>
<%@page import = "java.util.List" %>
<%@page import = "java.util.ArrayList" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%
    //Get all categories
    CategoryDAO categoryDAO = new CategoryDAO();
    List<Category> allActiveCategories = categoryDAO.getAllActiveCategories();
    pageContext.setAttribute("allActiveCategories", allActiveCategories, PageContext.PAGE_SCOPE);
    
    //Get all brands
    BrandDAO brandDAO = new BrandDAO();
    List<Brand> allActiveBrands = brandDAO.getAllActiveBrands();
    pageContext.setAttribute("allActiveBrands", allActiveBrands, PageContext.PAGE_SCOPE);
%>

<!DOCTYPE html>
<style>
    .left-sidebar {
        background-color: #ffffff;
        padding-top: 20px;
        margin-left: -15px;
    }
    
    .left-sidebar .panel-title {
        color: #333333;
        font-weight: 500;
        padding-left: 10px;
        font-size: 18px;
    }
    
    .left-sidebar ul li {
        padding: 5px 10px;
        font-weight: 400;
        font-size: 16px;
    }
    
    .left-sidebar ul li input {
        margin-right: 10px;
    }
    
</style>

<div class="left-sidebar">

    <form action="${pageContext.request.contextPath}/home">
        <h2>Danh mục</h2>
        <div class="panel-group category-products" id="accordian"><!--category-productsr-->
            <c:forEach begin="0" end="${allActiveCategories.size() - 1}" var="i">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <input 
                            type="checkbox" 
                            name="categoryId" 
                            value="${allActiveCategories.get(i).getCategoryId()}"
                            ${requestScope.checkedCategories[i] ? 'checked' : ''}
                            onclick="this.form.submit()"
                            />
                        <span class="panel-title">${allActiveCategories.get(i).getCategoryName()}</span>
                    </div>
                </div>
            </c:forEach>
            <!--/category-products-->

        </div>
        <div class="brands_products"><!--brands_products-->
            <h2>Thương hiệu</h2>
            <div class="brands-name">
                <ul class="nav nav-pills nav-stacked">
                    <c:forEach begin="0" end="${allActiveBrands.size() - 1}" var="i">
                        <li>
                            <input 
                                type="checkbox" 
                                name="brandId" 
                                value="${allActiveBrands.get(i).getBrandId()}"
                                ${requestScope.checkedBrands[i] ? 'checked' : ''}
                                onclick="this.form.submit()"
                                />
                            ${allActiveBrands.get(i).getBrandName()}
                        </li>
                        </c:forEach>
                </ul>
            </div>
        </div>
        <!--/brands_products-->

        
    </form>

    <div class="shipping text-center"><!--shipping-->
        <img src="${pageContext.request.contextPath}/view/images/home/shipping.jpg" alt="" />
    </div><!--/shipping-->

</div>

