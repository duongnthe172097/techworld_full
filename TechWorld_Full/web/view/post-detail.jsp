<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Chi tiết bài đăng | Marketing</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta name="description" content="Developed By M Abdur Rokib Promy">
        <meta name="keywords" content="Admin, Bootstrap 3, Template, Theme, Responsive">
        <!-- bootstrap 3.0.2 -->
        <link href="${pageContext.request.contextPath}/view/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="${pageContext.request.contextPath}/view/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="${pageContext.request.contextPath}/view/css/ionicons.min.css" rel="stylesheet" type="text/css" />

        <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
        <!-- Theme style -->
        <link href="${pageContext.request.contextPath}/view/css/style.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <link rel="stylesheet" href="https://cdn.datatables.net/1.13.7/css/jquery.dataTables.css" />
        <style>
            /*            tfoot input {
                            width: 100%;
                            padding: 3px;
                            box-sizing: border-box;
                            border: 1px solid #aaa;
                            border-radius: 3px;
                            padding: 5px;
                            background-color: transparent;
                            color: inherit;
                            margin-left: 3px;
                        }*/
        </style>
    </head>

    <body class="skin-black">
        <!-- header logo: style can be found in header.less -->
        <jsp:include page="admin-header.jsp"></jsp:include>
            <div class="wrapper row-offcanvas row-offcanvas-left">
                <!-- Left side column. contains the logo and sidebar -->
                <aside class="left-side sidebar-offcanvas">
                    <!-- sidebar: style can be found in sidebar.less -->
                    <section class="sidebar">
                        <!-- Sidebar user panel -->
                        <div class="user-panel">
                            <div class="pull-left image">
                                <img src="${pageContext.request.contextPath}/view/img/26115.jpg" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Xin chào, ${sessionScope.user.userName}</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>

                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/marketing-dashboard">
                                <i class="fa fa-dashboard"></i> <span>Thống kê</span>
                            </a>
                        </li>
                        <li class="active">
                            <a href="${pageContext.request.contextPath}/marketing/posts">
                                <i class="fa fa-gavel"></i> <span>Bài đăng</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/view-sliders">
                                <i class="fa fa-gavel"></i> <span>Slider</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/product/list">
                                <i class="fa fa-gavel"></i> <span>Sản phẩm</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/marketing-viewcustomer">
                                <i class="fa fa-gavel"></i> <span>Khách hàng</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/feedbacks/list">
                                <i class="fa fa-gavel"></i> <span>Phản hồi</span>
                            </a>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->

                
                <div class="container mt-5" style="background-color: #ffffff; padding: 10px;">
                    <a class="btn btn-primary" href="${pageContext.request.contextPath}/marketing/posts">Quay lại</a>
                    <!-- Display post details -->
                    <h1>Chi tiết bài đăng</h1>

                    <c:if test="${param.success ne null}">
                        <div class="alert alert-success" role="alert">
                            Thành công!
                        </div>
                    </c:if>

                    <form action="post-detail" method="post">
                        <input type="hidden" name="id" value="${post.postId}">
                        <div class="form-group">
                            <label for="title">Tiêu đề:</label>
                            <input type="text" class="form-control" id="title" name="title" value="${post.title}">
                        </div>
                        <div class="form-group">
                            <label for="briefInfo">Thông tin tóm tắt:</label>
                            <textarea class="form-control" id="briefInfo" name="briefInfo" rows="3">${post.briefInfo}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="content">Nội dung:</label>
                            <textarea class="form-control" id="content" name="content" rows="5">${post.content}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="status">Trạng thái:</label>
                            <select class="form-control" id="status" name="status">
                                <option value="true" ${post.status ? 'selected' : ''}>Hiện</option>
                                <option value="false" ${!post.status ? 'selected' : ''}>Ẩn</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary">Lưu thay đổi</button>
                    </form>


                    <div>
                        <!-- Display list of post images -->
                        <h2>Danh sách hình ảnh bài viết</h2>
                        <button type="button" class="btn btn-primary mt-3" data-toggle="modal" data-target="#addImageModal">
                            Thêm hình ảnh
                        </button>
                        <div class="row">
                            <c:forEach var="image" items="${post.postImages}">
                                <div class="col-md-4">
                                    <div class="thumbnail">
                                        <img src="${pageContext.request.contextPath}/view/images/post/${image.imageUrl}" alt="Post Image">
                                        <!-- Optionally, display other details of the image -->
                                        <div class="caption">
                                            <p>ID Ảnh: ${image.imageId}</p>

                                            <form action="${pageContext.request.contextPath}/postimage" method="post">
                                                <input type="hidden" name="action" value="delete">
                                                <input type="hidden" name="id" value="${image.imageId}">
                                                <input type="hidden" name="postId" value="${post.postId}">
                                                <button type="submit" class="btn btn-danger">Xóa</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>


                    </div>
                </div>

                <div class="modal fade" id="addImageModal" tabindex="-1" role="dialog" aria-labelledby="addImageModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="addImageModalLabel">Thêm hình ảnh</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body">
                                <!-- Add form for uploading image -->
                                <form action="${pageContext.request.contextPath}/postimage" method="post" enctype="multipart/form-data">
                                    <input type="hidden" name="action" value="add">
                                    <input type="hidden" name="postId" value="${post.postId}">
                                    <div class="form-group">
                                        <label for="image">Chọn ảnh:</label>
                                        <input type="file" class="form-control form-select" id="image" name="file">
                                    </div>
                                    <button type="submit" class="btn btn-primary">Tải lên</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>


            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->
        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/jquery.min.js" type="text/javascript"></script>

        <!-- Bootstrap -->
        <script src="${pageContext.request.contextPath}/view/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- Director App -->
        <script src="${pageContext.request.contextPath}/view/js/Director/app.js" type="text/javascript"></script>

        <script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.js"></script>

    </script>
</body>
</html>