<%-- 
    Document   : blog
    Created on : Jan 8, 2024, 4:20:36 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import = "model.*" %>
<%@page import = "dal.*" %>
<%@page import = "java.util.List" %>
<%@page import = "java.util.ArrayList" %>
<%@page import = "java.text.DecimalFormat" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Blog | Tech World</title>
        <link href="${pageContext.request.contextPath}/view/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/font-awesome.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/prettyPhoto.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/price-range.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/animate.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/main.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/responsive.css" rel="stylesheet">
        <!--[if lt IE 9]>
        <script src="${pageContext.request.contextPath}/view/js/html5shiv.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/respond.min.js"></script>
        <![endif]-->       
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/view/images/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="${pageContext.request.contextPath}/view/images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="${pageContext.request.contextPath}/view/images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="${pageContext.request.contextPath}/view/images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/view/images/ico/apple-touch-icon-57-precomposed.png">
    </head><!--/head-->

    <%
    //Get all categories
    CategoryDAO categoryDAO = new CategoryDAO();
    List<Category> allActiveCategories = categoryDAO.getAllActiveCategories();
    pageContext.setAttribute("allActiveCategories", allActiveCategories, PageContext.PAGE_SCOPE);
    
    //Get all brands
    BrandDAO brandDAO = new BrandDAO();
    List<Brand> allActiveBrands = brandDAO.getAllActiveBrands();
    pageContext.setAttribute("allActiveBrands", allActiveBrands, PageContext.PAGE_SCOPE);
    
    //post image dao
    PostImageDAO postImageDAO = new PostImageDAO();
    pageContext.setAttribute("postImageDAO", postImageDAO, PageContext.PAGE_SCOPE);
    
    // user dao
    UserDAO userDAO = new UserDAO();
    pageContext.setAttribute("userDAO", userDAO, PageContext.PAGE_SCOPE);
    
    %>

    <body>
        <style type="text/css">
            .left-sidebar {
                background-color: #ffffff;
                padding-top: 20px;
                margin-left: -15px;
            }

            .left-sidebar .panel-title {
                color: #333333;
                font-weight: 500;
                padding-left: 10px;
                font-size: 18px;
            }

            .left-sidebar ul li {
                padding: 5px 10px;
                font-weight: 400;
                font-size: 16px;
            }

            .left-sidebar ul li input {
                margin-right: 10px;
            }

            #header {
                background-color: #7FCDF9;
                box-shadow: rgba(0, 0, 0, 0.1) 0px 4px 6px -1px, rgba(0, 0, 0, 0.06) 0px 2px 4px -1px;
            }

            .logo-techworld img{
                width: 120px;
                height: auto;
            }

            .header-searchform input{
                background-color: #ffffff;
                color: #333333;
            }

        </style>

        <!-- ======= Header ======= -->
        <header id="header"><!--header-->
            <div class="header_top"><!--header_top-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="contactinfo">
                                <ul class="nav nav-pills">
                                    <li><a href="#"><i class="fa fa-phone"></i> +2 95 01 88 821</a></li>
                                    <li><a href="#"><i class="fa fa-envelope"></i> minhnhhe170924@fpt.edu.vn</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="social-icons pull-right">
                                <ul class="nav navbar-nav">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/header_top-->

            <div class="header-middle"><!--header-middle-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="logo pull-left logo-techworld">
                                <a href="${pageContext.request.contextPath}/home"><img src="${pageContext.request.contextPath}/view/images/home/tech_logo.png" alt="" /></a>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="shop-menu pull-right">
                                <ul class="nav navbar-nav">
                                    <c:if test="${sessionScope.user == null}">
                                        <li><a href="${pageContext.request.contextPath}/view/login.jsp"><i class="fa fa-user"></i> Tài khoản</a></li>
                                        <li><a href="${pageContext.request.contextPath}/view/login.jsp"><i class="fa fa-lock"></i> Đăng nhập</a></li>
                                        </c:if>
                                        <c:if test="${sessionScope.user != null}">
                                        <li><a href="${pageContext.request.contextPath}/profile"><i class="fa fa-user"></i> ${sessionScope.user.fullName}</a></li>
                                            <c:if test="${sessionScope.user.roleId == 5}">
                                            <li><a href="${pageContext.request.contextPath}/admin/admin-dashboard"><i class="fa fa-crosshairs"></i> Trang quản trị</a></li>
                                            </c:if>
                                            <c:if test="${sessionScope.user.roleId == 3 || sessionScope.user.roleId == 4}">
                                            <li><a href="${pageContext.request.contextPath}/sale/dashboard"><i class="fa fa-crosshairs"></i> Trang sale</a></li>
                                            </c:if>
                                            <c:if test="${sessionScope.user.roleId == 2}">
                                            <li><a href="${pageContext.request.contextPath}/marketing/marketing-dashboard"><i class="fa fa-crosshairs"></i> Trang marketing</a></li>
                                            </c:if>
                                            <c:if test="${sessionScope.user.roleId == 1}">
                                            <li><a href="${pageContext.request.contextPath}/cartController"><i class="fa fa-shopping-cart"></i>Giỏ hàng</a></li>
                                            <li><a href="${pageContext.request.contextPath}/customer/my-order"><i class="fa fa-crosshairs"></i> Đơn hàng của tôi</a></li>
                                            </c:if>
                                        <li><a href="${pageContext.request.contextPath}/logout"><i class="fa fa-lock"></i> Đăng xuất</a></li>
                                        </c:if>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/header-middle-->

            <div class="header-bottom"><!--header-bottom-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-9">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="mainmenu pull-left">
                                <ul class="nav navbar-nav collapse navbar-collapse">
                                    <li><a href="${pageContext.request.contextPath}/home">Trang chủ</a></li>
                                    <li><a href="${pageContext.request.contextPath}/products">Sản phẩm</a></li>
                                    <li><a href="${pageContext.request.contextPath}/blogs" class="active">Bài đăng</a></li>
                                    <li><a href="${pageContext.request.contextPath}/view/contact-us.jsp">Liên hệ</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <form action="${pageContext.request.contextPath}/home" class="search_box pull-right header-searchform">
                                <input type="text" name="search" placeholder="Tìm kiếm sản phẩm" value="${requestScope.searchKey}"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!--/header-bottom-->
        </header><!--/header-->

        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <!-- ======= Sidebar ======= -->
                        <div class="left-sidebar">

                            <form action="${pageContext.request.contextPath}/blogs">
                                <h2>Danh mục</h2>
                                <div class="panel-group category-products" id="accordian"><!--category-productsr-->
                                    <c:forEach begin="0" end="${allActiveCategories.size() - 1}" var="i">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <input 
                                                    type="checkbox" 
                                                    name="categoryId" 
                                                    value="${allActiveCategories.get(i).getCategoryId()}"
                                                    ${requestScope.checkedCategories[i] ? 'checked' : ''}
                                                    onclick="this.form.submit()"
                                                    />
                                                <span class="panel-title">${allActiveCategories.get(i).getCategoryName()}</span>
                                            </div>
                                        </div>
                                    </c:forEach>
                                    <!--/category-products-->

                                </div>
                                <div class="brands_products"><!--brands_products-->
                                    <h2>Thương hiệu</h2>
                                    <div class="brands-name">
                                        <ul class="nav nav-pills nav-stacked">
                                            <c:forEach begin="0" end="${allActiveBrands.size() - 1}" var="i">
                                                <li>
                                                    <input 
                                                        type="checkbox" 
                                                        name="brandId" 
                                                        value="${allActiveBrands.get(i).getBrandId()}"
                                                        ${requestScope.checkedBrands[i] ? 'checked' : ''}
                                                        onclick="this.form.submit()"
                                                        />
                                                    ${allActiveBrands.get(i).getBrandName()}
                                                </li>
                                            </c:forEach>
                                        </ul>
                                    </div>
                                </div>
                                <!--/brands_products-->
                            </form>
                            <div class="price-range"><!--price-range-->
                                <h2>Tìm kiếm</h2>
                                <div class="well text-center" style="padding: 0px;display: flex;justify-content: center;">
                                    <form action="${pageContext.request.contextPath}/blogs" class="search_box pull-right header-searchform">
                                        <input type="text" name="search" placeholder="Tìm kiếm bài đăng" value="${requestScope.searchKey}"
                                               style="padding: 10px; border: 1px solid burlywood; width: 100%"/>
                                    </form>
                                </div>
                            </div><!--/price-range-->


                            <div class="shipping text-center"><!--shipping-->
                                <img src="${pageContext.request.contextPath}/view/images/home/shipping.jpg" alt="" />
                            </div><!--/shipping-->

                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="blog-post-area">
                            <h2 class="title text-center">Bài đăng mới nhất</h2>
                            <c:forEach items="${requestScope.allPosts}" var="post">
                                <div class="single-blog-post">
                                    <h3>${post.title}</h3>
                                    <div class="post-meta">
                                        <ul>
                                            <li><i class="fa fa-user"></i> ${userDAO.getUserByID(post.getUserId()).getFullName()}</li>
                                            <li><i class="fa fa-calendar"></i>${post.updatedDate}</li>
                                        </ul>
                                    </div>
                                    <c:set var="imageList" value="${postImageDAO.getAllImageOfPostById(post.getPostId())}"/>
                                    <c:set var="imageUrl" value="${(imageList.size() > 0) ? imageList.get(0).imageUrl : ''}"/>
                                    <a href="${pageContext.request.contextPath}/blog-details?id=${post.postId}">
                                        <img src="${pageContext.request.contextPath}/view/images/post/${imageUrl}" alt="image" />
                                    </a>
                                    <p>${post.briefInfo}</p>
                                    <a  class="btn btn-primary" href="${pageContext.request.contextPath}/blog-details?id=${post.postId}">Xem chi tiết</a>
                                </div>
                            </c:forEach>
                            <c:if test="${requestScope.allPosts.size() == 0}">
                                <h4>Không có bài đăng nào.</h4>
                            </c:if>
                            <c:set var="page" value="${requestScope.page}"/>
                            <div class="pagination-area col-sm-12">
                                <ul class="pagination">
                                    <c:forEach begin="1" end="${requestScope.numberOfPages}" var="index">
                                        <li><a href="blogs?page=${index}" class="${index == page ? "active" : ""}">${index}</a></li>
                                        </c:forEach>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- ======= Footer ======= -->
    <jsp:include page="footer.jsp"></jsp:include>



        <script src="${pageContext.request.contextPath}/view/js/jquery.js"></script>
    <script src="${pageContext.request.contextPath}/view/js/price-range.js"></script>
    <script src="${pageContext.request.contextPath}/view/js/jquery.scrollUp.min.js"></script>
    <script src="${pageContext.request.contextPath}/view/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/view/js/jquery.prettyPhoto.js"></script>
    <script src="${pageContext.request.contextPath}/view/js/main.js"></script>
</body>
</html>
