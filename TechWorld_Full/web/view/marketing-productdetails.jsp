<%-- 
Document   : add-setting
Created on : Jan 18, 2024, 11:18:35 PM
Author     : ns
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page import = "model.*" %>
<%@page import = "dal.*" %>
<%@page import = "java.sql.Date" %>
<%@page import = "java.util.*" %>
<%@page import = "java.text.DecimalFormat" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Admin | Dashboard</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta name="description" content="Developed By M Abdur Rokib Promy">
        <meta name="keywords" content="Admin, Bootstrap 3, Template, Theme, Responsive">
        <!-- bootstrap 3.0.2 -->
        <link href="${pageContext.request.contextPath}/view/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="${pageContext.request.contextPath}/view/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="${pageContext.request.contextPath}/view/css/ionicons.min.css" rel="stylesheet" type="text/css" />

        <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
        <!-- Theme style -->
        <link href="${pageContext.request.contextPath}/view/css/style.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <link rel="stylesheet" href="https://cdn.datatables.net/1.13.7/css/jquery.dataTables.css" />
        <style>
            /*            tfoot input {
                            width: 100%;
                            padding: 3px;
                            box-sizing: border-box;
                            border: 1px solid #aaa;
                            border-radius: 3px;
                            padding: 5px;
                            background-color: transparent;
                            color: inherit;
                            margin-left: 3px;
                        }*/
        </style>
    </head>
    <body class="skin-black">
        <!-- header logo: style can be found in header.less -->
        <jsp:include page="admin-header.jsp"></jsp:include>
            <div class="wrapper row-offcanvas row-offcanvas-left">
                <!-- Left side column. contains the logo and sidebar -->
                <aside class="left-side sidebar-offcanvas">
                    <!-- sidebar: style can be found in sidebar.less -->
                    <section class="sidebar">
                        <!-- Sidebar user panel -->
                        <div class="user-panel">
                            <div class="pull-left image">
                                <img src="${pageContext.request.contextPath}/view/img/26115.jpg" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Xin chào, ${sessionScope.user.userName}</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>

                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/marketing-dashboard">
                                <i class="fa fa-dashboard"></i> <span>Thống kê</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/posts">
                                <i class="fa fa-gavel"></i> <span>Bài đăng</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/view-sliders">
                                <i class="fa fa-gavel"></i> <span>Slider</span>
                            </a>
                        </li>
                        <li class="active">
                            <a href="${pageContext.request.contextPath}/marketing/product/list">
                                <i class="fa fa-gavel"></i> <span>Sản phẩm</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/marketing-viewcustomer">
                                <i class="fa fa-gavel"></i> <span>Khách hàng</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/feedbacks/list">
                                <i class="fa fa-gavel"></i> <span>Phản hồi</span>
                            </a>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->


                <!-- Main content -->
                <section class="content" style="min-height: 90vh">
                    <div class="row" style="display: flex; justify-content: center">
                        <div class="col-md-12">
                            <a href="${pageContext.request.contextPath}/admin/settings/add" class="btn btn-primary" style="margin-bottom: 1rem;">Thêm cài đặt</a>
                            <div class="panel">
                                <header class="panel-heading">
                                    Danh sách cài đặt
                                </header>
                                <c:if test="${not empty deleteFailed}">
                                    <p style="margin: 10px 15px 0px; font-size: 2rem;" class="text-success">${deleteFailed}</p>
                                    <script>
                                        window.history.replaceState(null, '', "${pageContext.request.contextPath}/admin/settings/view");
                                    </script>
                                </c:if>
                                <c:if test="${not empty deleteSuccess}">
                                    <p style="margin: 10px 15px 0px; font-size: 2rem;" class="text-success">${deleteSuccess}</p>
                                    <script>
                                        window.history.replaceState(null, '', "${pageContext.request.contextPath}/admin/settings/view");
                                    </script>
                                </c:if>

                                <!-- <div class="box-header"> -->
                                <!-- <h3 class="box-title">Responsive Hover Table</h3> -->

                                <!-- </div> -->

                                <% 
                                ProductImageDAO productImageIDAO = new ProductImageDAO();
                                pageContext.setAttribute("productImageIDAO", productImageIDAO, PageContext.PAGE_SCOPE);
                                    
                                DecimalFormat decimalFormat = new DecimalFormat("#,###");
                                pageContext.setAttribute("decimalFormat", decimalFormat, PageContext.PAGE_SCOPE);
                                %>

                                <div class="panel-body table-responsive">

                                    <table id="setting-table" class="table cell-border row-border hover stripe">
                                        <thead>
                                            <tr>

                                                <th>Ảnh</th>
                                                <th>Tên</th>
                                                <th>Loại</th>
                                                <th>Số lượng</th>
                                                <th>Mô tả</th>
                                                <th>Giá tiền</th>
                                                <th>Khuyến mãi</th>
                                                <th>Trạng thái</th>
                                                <th>Hành động</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <c:forEach var="item" items="${productsWithImages}">
                                                <tr>
                                                    <c:set var="imageList" value="${productImageIDAO.getAllImageOfProductById(item.productId)}"/>
                                                    <c:set var="imageUrl" value="${(imageList.size() > 0) ? imageList.get(0).imageUrl : ''}"/>
                                                    <td><img style="width: 200px !important;" src="${pageContext.request.contextPath}/view/images/product/${imageUrl}" alt="Product Image"></td>
                                                    <td>${item.productName}</td>
                                                    <td>${item.categoryId}</td>
                                                    <td>${item.quantityInStock}</td>
                                                    <td>${item.description}</td>                                       
                                                    <td>${decimalFormat.format(item.price - (item.price * item.discount * 0.01))}</td>
                                                    <td>${item.discount}%</td>
                                                    <td>${item.isStatus() ? "Hien" : "An"}</td>
                                                    <td style="width: 150px; text-align: center;">
                                                        <form action="${pageContext.request.contextPath}/marketing/settings/view" method="post">
                                                            <input type="hidden" name="productId" value="${item.productId}"> <!-- la dang thieu o day a?, cho nay van co ID, a thay cai value=1 khong, nhung em loi ve servlet voi keo' sang cai edit thi` mat -->
                                                            <button type="submit" class="btn btn-primary" style="margin-right: 5px; margin-bottom: 5px;">Xem/Sửa</button> <!-- submit nay di vao dau? -->
                                                        </form>                                                          
                                                        <a style="margin-right: 5px; margin-bottom: 5px;" class="btn btn-info" href="${pageContext.request.contextPath}/marketing/settings/change-status?id=${item.productId}">Đổi trạng thái</a><br/>
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                        <tfoot>
                                            <tr>

                                                <th></th>
                                                <th>Tên</th>
                                                <th>Loại</th>
                                                <th>Số lượng</th>
                                                <th>Mô tả</th>
                                                <th>Giá tiền</th>
                                                <th>Khuyến mãi</th>
                                                <th>Trạng thái</th>
                                                <th>Hành động</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div><!-- /.box-body -->
                            </div>
                        </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
            <div class="footer-main">
                Copyright &copy Director, 2014
            </div>
        </div><!-- ./wrapper -->
        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/jquery.min.js" type="text/javascript"></script>

        <!-- Bootstrap -->
        <script src="${pageContext.request.contextPath}/view/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- Director App -->
        <script src="${pageContext.request.contextPath}/view/js/Director/app.js" type="text/javascript"></script>

        <script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.js"></script>
        <script>
//            $(document).ready(function () {
//                $('#setting-table').DataTable();
//            });

                                        var settingTable = new DataTable('#setting-table', {
                                            initComplete: function () {
                                                this.api()
                                                        .columns()
                                                        .every(function () {
                                                            let column = this;
                                                            let title = column.footer().textContent;

                                                            if (title === '')
                                                                return;

                                                            // Create input element
                                                            let input = document.createElement('input');
                                                            input.placeholder = title;
                                                            column.footer().replaceChildren(input);

                                                            // Event listener for user input
                                                            input.addEventListener('keyup', () => {
                                                                if (column.search() !== this.value) {
                                                                    column.search(input.value).draw();
                                                                }
                                                            });
                                                        });
                                            }
                                        });

//            settingTable.column(4)
//                    .search("^" + $(this).val() + "$", true, false, true)
//                    .draw();

                                        $('#setting-table tfoot > tr > th').css("border", "1px solid rgba(0, 0, 0, 0.15)");
                                        $('#setting-table').css("border-collapse", "collapse");
                                        $('#setting-table tfoot > tr > th > input').addClass('form-control');
                                        $('#setting-table tfoot tr').appendTo('#setting-table thead');

        </script>
    </body>
</html>
