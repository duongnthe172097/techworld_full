<%-- 
    Document   : index
    Created on : Jan 8, 2024, 4:21:22 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Profile | Tech World</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.1/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/font-awesome.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/prettyPhoto.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/price-range.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/animate.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/main.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/responsive.css" rel="stylesheet">
        <!--[if lt IE 9]>
        <script src="${pageContext.request.contextPath}/view/js/html5shiv.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/respond.min.js"></script>
        <![endif]-->       
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/view/images/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="${pageContext.request.contextPath}/view/images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="${pageContext.request.contextPath}/view/images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="${pageContext.request.contextPath}/view/images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/view/images/ico/apple-touch-icon-57-precomposed.png">

        <title>bs4 edit profile page - Bootdey.com</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <style type="text/css">
            header .header_top ul{
                display: contents;
            }

            div.shop-menu.pull-right ul{
                display: contents;
            }
        </style>
    </head><!--/head-->


    <!-- ======= Header ======= -->

    <body>
        <style type="text/css">

            .product-homepage {
                background-color: #ffffff;
                border: none;
                box-shadow: rgba(0, 0, 0, 0.1) 0px 10px 15px -3px, rgba(0, 0, 0, 0.05) 0px 4px 6px -2px;
            }

            .product-homepage span {
                color: #f19d23;
                font-size: 20px;
                font-weight: 400;
            }

            .product-homepage span del {
                color: red;
                font-size: 12px;
            }

            .product-homepage img {
                box-shadow: rgba(255, 255, 255, 0.56) 0px 22px 70px 4px;
            }

            .product-homepage p {
                color: #000000;
                font-size: 18px;
                margin: 20px 0;
                min-height: 50px;
            }

            .post-homepage {
                background-color: #ffffff;
                border: none;
            }

            .post-homepage img {
                box-shadow: rgba(255, 255, 255, 0.56) 0px 22px 70px 4px;
            }

            #header {
                background-color: #7FCDF9;
                box-shadow: rgba(0, 0, 0, 0.1) 0px 4px 6px -1px, rgba(0, 0, 0, 0.06) 0px 2px 4px -1px;
            }

            .logo-techworld img{
                width: 120px;
                height: auto;
            }

            .header-searchform input{
                background-color: #ffffff;
                color: #333333;
            }

            @media (min-width: 768px) {

                #product-homepage {
                    min-height: 220vh;
                }

                .product-homepage {
                    height: 360px;
                    overflow: hidden;
                    padding: 3px;
                }
                .product-homepage img {
                    height: 150px;
                    width: fit-content;
                    max-width: 100%;
                }

                .post-homepage {
                    padding: 12px;
                    height: 430px;
                }

                .post-homepage img {
                    height: 150px;
                    width: fit-content;
                    width: 100%;
                    margin-bottom: 7px;
                }

                .post-homepage h5{
                    height: 25px;
                }

                .post-homepage p{
                    height: 150px;
                }
            }
        </style>
        <!-- ======= Header ======= -->
        <header id="header"><!--header-->
            <div class="header_top"><!--header_top-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="contactinfo">
                                <ul class="nav nav-pills">
                                    <li><a href="#"><i class="fa fa-phone"></i> +2 95 01 88 821</a></li>
                                    <li><a href="#"><i class="fa fa-envelope"></i> minhnhhe170924@fpt.edu.vn</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="social-icons pull-right">
                                <ul class="nav navbar-nav">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/header_top-->

            <div class="header-middle"><!--header-middle-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="logo pull-left logo-techworld">
                                <a href="${pageContext.request.contextPath}/home"><img src="${pageContext.request.contextPath}/view/images/home/tech_logo.png" alt="" /></a>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="shop-menu pull-right">
                                <ul class="nav navbar-nav">
                                    <c:if test="${sessionScope.user == null}">
                                        <li><a href="${pageContext.request.contextPath}/view/login.jsp"><i class="fa fa-user"></i> Tài khoản</a></li>
                                        <li><a href="${pageContext.request.contextPath}/view/login.jsp"><i class="fa fa-lock"></i> Đăng nhập</a></li>
                                        </c:if>
                                        <c:if test="${sessionScope.user != null}">
                                        <li><a href="${pageContext.request.contextPath}/profile" class="active"><i class="fa fa-user"></i> ${sessionScope.user.fullName}</a></li>
                                            <c:if test="${sessionScope.user.roleId == 5}">
                                            <li><a href="${pageContext.request.contextPath}/admin/admin-dashboard"><i class="fa fa-crosshairs"></i> Trang quản trị</a></li>
                                            </c:if>
                                            <c:if test="${sessionScope.user.roleId == 3 || sessionScope.user.roleId == 4}">
                                            <li><a href="${pageContext.request.contextPath}/sale/dashboard"><i class="fa fa-crosshairs"></i> Trang sale</a></li>
                                            </c:if>
                                            <c:if test="${sessionScope.user.roleId == 2}">
                                            <li><a href="${pageContext.request.contextPath}/marketing/marketing-dashboard"><i class="fa fa-crosshairs"></i> Trang marketing</a></li>
                                            </c:if>
                                            <c:if test="${sessionScope.user.roleId == 1}">
                                            <li><a href="${pageContext.request.contextPath}/cartController"><i class="fa fa-shopping-cart"></i>Giỏ hàng</a></li>
                                            <li><a href="${pageContext.request.contextPath}/customer/my-order"><i class="fa fa-crosshairs"></i> Đơn hàng của tôi</a></li>
                                            </c:if>
                                        <li><a href="${pageContext.request.contextPath}/logout"><i class="fa fa-lock"></i> Đăng xuất</a></li>
                                        </c:if>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/header-middle-->

            <div class="header-bottom"><!--header-bottom-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-9">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="mainmenu pull-left">
                                <ul class="nav navbar-nav collapse navbar-collapse">
                                    <li><a href="${pageContext.request.contextPath}/home">Trang chủ</a></li>
                                    <li><a href="${pageContext.request.contextPath}/products">Sản phẩm</a></li>
                                    <li><a href="${pageContext.request.contextPath}/blogs">Bài đăng</a></li>
                                    <li><a href="${pageContext.request.contextPath}/view/contact-us.jsp">Liên hệ</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <form action="${pageContext.request.contextPath}/home" class="search_box pull-right header-searchform">
                                <input type="text" name="search" placeholder="Tìm kiếm sản phẩm" value="${requestScope.searchKey}"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!--/header-bottom-->
        </header>
        <div class="container"  style="margin-top: 10vh;">
            <div class="row flex-lg-nowrap">
                <div class="col">
                    <div class="row">
                        <div class="col mb-3">
                            <div class="card">
                                <div class="card-body">
                                    <div class="e-profile">
                                        <h3 style="color: green;">${requestScope.message}</h3>
                                        <h3 style="color: red;">${requestScope.error}</h3>
                                        <div class="row">
                                            <div class="col-12 col-sm-auto mb-3">
                                                <div class="mx-auto" style="width: 140px;">
                                                    <div class="d-flex justify-content-center align-items-center rounded" style="height: 140px; background-color: rgb(233, 236, 239);">
                                                        <span style="color: rgb(166, 168, 170); font: bold 8pt Arial;">
                                                            <img src="${pageContext.request.contextPath}/view/images/user/${user.getImage()}" style="height: 140px; width: 140px">
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col d-flex flex-column flex-sm-row justify-content-between mb-3">
                                                <div class="text-center text-sm-left mb-2 mb-sm-0">
                                                    <h4 class="pt-sm-2 pb-1 mb-0 text-nowrap">${sessionScope.ur.getUserName()}</h4>
                                                    <div class="mt-2">
                                                        <div class="pass-enter mt-16">
                                                            <a href="${pageContext.request.contextPath}/profile" class="btn btn-primary">
                                                                <span>Quay lại</span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <ul class="nav nav-tabs">
                                            <li class="nav-item"><a class="active nav-link" active" onclick="showTab('settings')">Thông tin người dùng</a></li>
                                            <li class="nav-item"><a class="active nav-link" onclick="showTab('changePassword')">Đổi mật khẩu</a></li>
                                        </ul>
                                        <div class="tab-content pt-3">
                                            <div id="settings" class="tab-pane active">
                                                <form action="${pageContext.request.contextPath}/editprofile" method="post" >
                                                    <input type="hidden" name="service" value="update-info"/>
                                                    <div class="row">
                                                        <div class="col">
                                                            <div class="row">
                                                                <div class="col">
                                                                    <div class="form-group">
                                                                        <label>Họ và tên</label>
                                                                        <input class="form-control" type="text" name="fullname" value="${sessionScope.user.fullName}">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">

                                                                <div class="col">
                                                                    <div class="form-group">
                                                                        <label>Số điện thoại</label>
                                                                        <input class="form-control" id="phone" type="text" name="mobile" value="${sessionScope.user.mobile}">
                                                                    </div>
                                                                    <div id="err"> </div> 
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col">
                                                                    <div class="form-group">
                                                                        <label>Địa chỉ</label>
                                                                        <input class="form-control" type="text" name="address" value="${sessionScope.user.address}">
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col d-flex justify-content-end">
                                                                    <button class="btn btn-primary" id="profile-btn" type="submit">Lưu thay đổi</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div id="changePassword" class="tab-pane active">
                                                <form action="${pageContext.request.contextPath}/changepass" method="post">
                                                    <div class="row">
                                                        <div class="col-12 col-sm-6 mb-3">
                                                            <div class="col">
                                                                <div class="form-group">
                                                                    <input class="form-control" type="text" name="username" placeholder="User Name" value="${sessionScope.user.getUserName()}" readonly hidden>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col">
                                                                    <div class="form-group">
                                                                        <label>Mật khẩu cũ</label>
                                                                        <input class="form-control" name="pass" type="password" placeholder="••••••">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col">
                                                                    <div class="form-group">
                                                                        <label>Mật khẩu mới</label>
                                                                        <input class="form-control" id="pass" name="newpass" type="password" placeholder="••••••">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="mess">${mess} </div> 
                                                            <div class="row">
                                                                <div class="col">
                                                                    <div class="form-group">
                                                                        <label>Xác nhận mật khẩu mới <span class="d-none d-xl-inline"></span></label>
                                                                        <input class="form-control" id="repass" name="repass" type="password" placeholder="••••••"></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="col d-flex justify-content-end">
                                                            <button class="btn btn-primary" id="change-btn" type="submit">Lưu thay đổi</button>
                                                        </div>
                                                    </div>

                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.1/dist/js/bootstrap.bundle.min.js"></script>
        <script>
                                                let match = false;
                                                function checkPassword() {
                                                    var password1 = document.getElementById("pass").value;
                                                    var password2 = document.getElementById("repass").value;
                                                    var message = document.getElementById("mess");
                                                    var btnSignUp = document.getElementById('change-btn');

                                                    if (password1 === password2) {
                                                        message.innerHTML = "Mật khẩu trùng khớp!";
                                                        message.style.color = "green";
                                                        btnSignUp.disabled = false;
                                                        btnSignUp.style.filter = "contrast(100%)";
                                                    } else {
                                                        message.innerHTML = "Mật khẩu không trùng khớp!";
                                                        message.style.color = "#E06565";

                                                        btnSignUp.disabled = true;
                                                        btnSignUp.style.filter = "contrast(10%)";
                                                    }
                                                }
                                                document.getElementById("pass").addEventListener("input", checkPassword);
                                                document.getElementById("repass").addEventListener("input", checkPassword);
        </script>
        <script>
            function validatePhoneNumber() {
                var phoneInput = document.getElementById("phone");
                var message = document.getElementById("err");
                var btnProfile = document.getElementById('profile-btn');
                var phoneNumber = phoneInput.value.trim();
                var phoneRegex = /^0\d{9}$/;
                if (!phoneRegex.test(phoneNumber)) {
                    message.innerHTML = "Số điện thoại không hợp lệ!";
                    message.style.color = "#E06565";
                    btnProfile.disabled = true;
                    btnProfile.style.filter = "contrast(10%)";
                } else {
                    message.innerHTML = "Số điện thoại hợp lệ!";
                    message.style.color = "green";
                    btnProfile.disabled = false;
                    btnProfile.style.filter = "contrast(100%)";
                }
            }
            document.getElementById("phone").addEventListener("input", validatePhoneNumber);
        </script>
        <script>
            document.addEventListener("DOMContentLoaded", function () {
                document.getElementById("changePassword").style.display = "none";
            });

            function showTab(tabName) {
                var i;
                var x = document.getElementsByClassName("tab-pane");
                for (i = 0; i < x.length; i++) {
                    x[i].style.display = "none";
                }
                document.getElementById(tabName).style.display = "block";
            }
        </script>
        <script type="text/javascript">

        </script>



        <!-- ======= Footer ======= -->
        <jsp:include page="footer.jsp"></jsp:include>



            <script src="${pageContext.request.contextPath}/view/js/jquery.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/jquery.scrollUp.min.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/price-range.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/jquery.prettyPhoto.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/main.js"></script>
    </body>
</html>