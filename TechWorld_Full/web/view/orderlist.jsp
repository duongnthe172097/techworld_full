<%-- 
    Document   : orderlist
    Created on : Feb 21, 2024, 7:43:19 AM
    Author     : Admin
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Sale</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta name="description" content="Developed By M Abdur Rokib Promy">
        <meta name="keywords" content="Admin, Bootstrap 3, Template, Theme, Responsive">
        <!-- bootstrap 3.0.2 -->
        <link href="${pageContext.request.contextPath}/view/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="${pageContext.request.contextPath}/view/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="${pageContext.request.contextPath}/view/css/ionicons.min.css" rel="stylesheet" type="text/css" />

        <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
        <!-- Theme style -->
        <link href="${pageContext.request.contextPath}/view/css/style.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <link rel="stylesheet" href="https://cdn.datatables.net/1.13.7/css/jquery.dataTables.css" />
        <style>
            /*            tfoot input {
                            width: 100%;
                            padding: 3px;
                            box-sizing: border-box;
                            border: 1px solid #aaa;
                            border-radius: 3px;
                            padding: 5px;
                            background-color: transparent;
                            color: inherit;
                            margin-left: 3px;
                        }*/
        </style>
    </head>

    <body class="skin-black">
        <!-- header logo: style can be found in header.less -->
        <jsp:include page="admin-header.jsp"></jsp:include>
            <div class="wrapper row-offcanvas row-offcanvas-left">
                <!-- Left side column. contains the logo and sidebar -->
            <jsp:include page="admin-aside.jsp"></jsp:include>

                <!-- Right side column. Contains the navbar and content of the page -->
                <aside class="right-side">
                    <!-- Content Header (Page header) -->


                    <!-- Main content -->
                    <section class="content" style="min-height: 90vh">
                        <div class="row" style="display: flex; justify-content: center">
                            <div class="col-md-12">
                                <div class="panel">
                                    <header class="panel-heading">
                                        Danh sách đơn hàng
                                    </header>

                                    <!-- </div> -->
                                    <div class="panel-body">
                                        <table class="table">
                                            <thead class="thead-dark">
                                                <tr>
                                                    <th>Order ID</th>
                                                    <th>Order Date</th>
                                                    <th>Total Cost</th>
                                                    <th>Product</th>
                                                    <th>Receiver Name</th>
                                                    <th>Status</th>
                                                    <th>Details</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <c:forEach var="order" items="${orderList}">
                                                <tr>
                                                    <td>${order.orderId}</td>
                                                    <td><fmt:formatDate value="${order.orderDate}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
                                                    <td><fmt:formatNumber value="${order.totalCost}" pattern="#,##0" /></td>
                                                    <td>
                                                        ${order.orderDetailList.size()} Products
                                                    </td>
                                                    <td>${order.user.fullName}</td>
                                                    <td>${order.stateString}</td>
                                                    <th>
                                                        <a class="btn btn-secondary" href="/order/order-detail?id=${order.orderId}">Details</a>
                                                    </th>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </div><!-- /.box-body -->
                            </div>
                        </div>
                    </div>

                    <ul class="pagination">
                        <li class="page-item <c:if test='${page <= 1}'>disabled</c:if>'">
                            <a class="page-link" href="?page=${page - 1}" tabindex="-1" aria-disabled="true">Previous</a>
                        </li>

                        <c:if test="${page > 2}">
                            <li class="page-item"><a class="page-link" href="?page=${page - 2}">${page - 2}</a></li>
                            </c:if>

                        <c:if test="${page > 1}">
                            <li class="page-item"><a class="page-link" href="?page=${page - 1}">${page - 1}</a></li>
                            </c:if>

                        <li class="page-item active"><a class="page-link" href="?page=${page}">${page}</a></li>

                        <li class="page-item"><a class="page-link" href="?page=${page + 1}">${page + 1}</a></li>

                        <li class="page-item">
                            <a class="page-link" href="?page=${page + 1}" aria-disabled="false">Next</a>
                        </li>
                    </ul>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
            <div class="footer-main">
                Copyright &copy Director, 2014
            </div>
        </div><!-- ./wrapper -->
        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/jquery.min.js" type="text/javascript"></script>

        <!-- Bootstrap -->
        <script src="${pageContext.request.contextPath}/view/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- Director App -->
        <script src="${pageContext.request.contextPath}/view/js/Director/app.js" type="text/javascript"></script>

        <script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.js"></script>

    </script>
</body>
</html>