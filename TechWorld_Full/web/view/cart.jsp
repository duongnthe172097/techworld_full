<%-- 
    Document   : cart
    Created on : Jan 8, 2024, 4:20:43 PM
    Author     : admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Checkout | Tech World</title>
        <link href="${pageContext.request.contextPath}/view/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/font-awesome.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/prettyPhoto.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/price-range.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/animate.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/main.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/responsive.css" rel="stylesheet">
        <!--[if lt IE 9]>
        <script src="${pageContext.request.contextPath}/view/js/html5shiv.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/respond.min.js"></script>
        <![endif]-->       
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/view/images/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="${pageContext.request.contextPath}/view/images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="${pageContext.request.contextPath}/view/images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="${pageContext.request.contextPath}/view/images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/view/images/ico/apple-touch-icon-57-precomposed.png">
    </head><!--/head-->

    <body>
        <style type="text/css">

            .product-homepage {
                background-color: #ffffff;
                border: none;
                box-shadow: rgba(0, 0, 0, 0.1) 0px 10px 15px -3px, rgba(0, 0, 0, 0.05) 0px 4px 6px -2px;
            }

            .product-homepage span {
                color: #f19d23;
                font-size: 20px;
                font-weight: 400;
            }

            .product-homepage span del {
                color: red;
                font-size: 12px;
            }

            .product-homepage img {
                box-shadow: rgba(255, 255, 255, 0.56) 0px 22px 70px 4px;
            }

            .product-homepage p {
                color: #000000;
                font-size: 18px;
                margin: 20px 0;
                min-height: 50px;
            }

            .post-homepage {
                background-color: #ffffff;
                border: none;
            }

            .post-homepage img {
                box-shadow: rgba(255, 255, 255, 0.56) 0px 22px 70px 4px;
            }

            #header {
                background-color: #7FCDF9;
                box-shadow: rgba(0, 0, 0, 0.1) 0px 4px 6px -1px, rgba(0, 0, 0, 0.06) 0px 2px 4px -1px;
            }

            .logo-techworld img{
                width: 120px;
                height: auto;
            }

            .header-searchform input{
                background-color: #ffffff;
                color: #333333;
            }

            @media (min-width: 768px) {

                #product-homepage {
                    min-height: 220vh;
                }

                .product-homepage {
                    height: 360px;
                    overflow: hidden;
                    padding: 3px;
                }
                .product-homepage img {
                    height: 150px;
                    width: fit-content;
                    max-width: 100%;
                }

                .post-homepage {
                    padding: 12px;
                    height: 430px;
                }

                .post-homepage img {
                    height: 150px;
                    width: fit-content;
                    width: 100%;
                    margin-bottom: 7px;
                }

                .post-homepage h5{
                    height: 25px;
                }

                .post-homepage p{
                    height: 150px;
                }
            }
        </style>
        <!-- ======= Header ======= -->
        <header id="header"><!--header-->
            <div class="header_top"><!--header_top-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="contactinfo">
                                <ul class="nav nav-pills">
                                    <li><a href="#"><i class="fa fa-phone"></i> +2 95 01 88 821</a></li>
                                    <li><a href="#"><i class="fa fa-envelope"></i> minhnhhe170924@fpt.edu.vn</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="social-icons pull-right">
                                <ul class="nav navbar-nav">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/header_top-->

            <div class="header-middle"><!--header-middle-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="logo pull-left logo-techworld">
                                <a href="${pageContext.request.contextPath}/home"><img src="${pageContext.request.contextPath}/view/images/home/tech_logo.png" alt="" /></a>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="shop-menu pull-right">
                                <ul class="nav navbar-nav">
                                    <c:if test="${sessionScope.user == null}">
                                        <li><a href="${pageContext.request.contextPath}/view/login.jsp"><i class="fa fa-user"></i> Tài khoản</a></li>
                                        <li><a href="${pageContext.request.contextPath}/view/login.jsp"><i class="fa fa-lock"></i> Đăng nhập</a></li>
                                        </c:if>
                                        <c:if test="${sessionScope.user != null}">
                                        <li><a href="${pageContext.request.contextPath}/profile"><i class="fa fa-user"></i> ${sessionScope.user.fullName}</a></li>
                                            <c:if test="${sessionScope.user.roleId == 5}">
                                            <li><a href="${pageContext.request.contextPath}/admin"><i class="fa fa-crosshairs"></i> Quản lý</a></li>
                                            </c:if>
                                            <c:if test="${sessionScope.user.roleId == 1}">
                                            <li><a href="${pageContext.request.contextPath}/cartController" class="active"><i class="fa fa-shopping-cart"></i>Giỏ hàng</a></li>
                                            <li><a href="${pageContext.request.contextPath}/customer/my-order"><i class="fa fa-crosshairs"></i> Đơn hàng của tôi</a></li>
                                            </c:if>
                                        <li><a href="${pageContext.request.contextPath}/logout"><i class="fa fa-lock"></i> Đăng xuất</a></li>
                                        </c:if>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/header-middle-->

            <div class="header-bottom"><!--header-bottom-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-9">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="mainmenu pull-left">
                                <ul class="nav navbar-nav collapse navbar-collapse">
                                    <li><a href="${pageContext.request.contextPath}/home">Trang chủ</a></li>
                                    <li><a href="${pageContext.request.contextPath}/products">Sản phẩm</a></li>
                                    <li><a href="${pageContext.request.contextPath}/blogs">Bài đăng</a></li>
                                    <li><a href="${pageContext.request.contextPath}/view/contact-us.jsp">Liên hệ</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <form action="${pageContext.request.contextPath}/home" class="search_box pull-right header-searchform">
                                <input type="text" name="search" placeholder="Tìm kiếm sản phẩm" value="${requestScope.searchKey}"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!--/header-bottom-->
        </header>

        <section id="cart_items">
            <div class="container">
                <div class="breadcrumbs">
                    <ol class="breadcrumb">

                    </ol>
                </div><!--/breadcrums-->

                <c:if test="${param.success ne null}">
                    <div class="alert alert-success" role="alert">
                        Thành công!
                    </div>
                </c:if>
                <c:if test="${param.out_of_stock ne null}">
                    <div class="alert alert-danger" role="alert">
                        Hết số lượng!
                    </div>
                </c:if>
                <c:if test="${param.removed ne null}">
                    <div class="alert alert-danger" role="alert">
                        Đã xóa
                    </div>
                </c:if>
                <c:if test="${param.error ne null}">
                    <div class="alert alert-danger" role="alert">
                        Lỗi
                    </div>
                </c:if>

                <div class="table-responsive cart_info">
                    <table class="table table-condensed">
                        <thead>
                            <tr class="cart_menu">
                                <td class="image" style="width: 20%;">Sản phẩm</td>
                                <td class="price" style="padding-left: 10%">Giá</td>
                                <td class="quantity">Số lượng</td>
                                <td class="total">Tổng giá</td>
                                <td>Action</td>
                            </tr>
                        </thead>
                        <tbody>

                            <c:forEach items="${listItem}" var="item">
                                <tr>
                                    <td class="cart_product">
                                        <img style="width: 100%; object-fit: cover;" src="view/images/product/${item.product.productImage.get(0).imageUrl}" alt="">
                                    </td>

                                    <td class="cart_price" style="padding-left: 10%">
                                        <p><fmt:formatNumber value="${item.product.price}" pattern="#,##0" /></p>
                                    </td>

                                    <td class="cart_quantity">
                                        <div class="cart_quantity_button">
                                            <a class="cart_quantity_up" href="addToCart?productId=${item.productId}&quantity=1"> + </a>
                                            <input class="cart_quantity_input" type="text" name="quantity" value="${item.quantity}" autocomplete="off" size="2">
                                            <a class="cart_quantity_down" href="addToCart?productId=${item.productId}&quantity=-1"> - </a>
                                        </div>
                                    </td>

                                    <td class="cart_total">
                                        <p class="cart_total_price"><fmt:formatNumber value="${item.totalCost}" pattern="#,##0" /></p>
                                    </td>

                                    <td class="cart_delete">
                                        <a class="cart_quantity_delete" href="addToCart?productId=${item.productId}&quantity=0&action=remove"><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>
                            </c:forEach>


                        </tbody>
                    </table>
                </div>
                <div class="payment-options text-center">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#confirmOrderModal">Xác nhận đơn hàng</button>
                </div>
            </div>
        </section> <!--/#cart_items-->

        <!-- Bootstrap Modal -->
        <div class="modal fade" id="confirmOrderModal" tabindex="-1" role="dialog" aria-labelledby="confirmOrderModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="confirmOrderModalLabel">Xác nhận đơn hàng của bạn</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="confirmOrderForm" method="post" action="cartController">
                            <input type="hidden" value="${userId}" name="userId">
                            <input type="hidden" value="${cartId}" name="cartId">
                            <div class="form-group">
                                <label for="deliveryOption">Phương thức giao hàng:</label>
                                <select class="form-control" id="deliveryOption" name="deliveryId">
                                    <c:forEach items="${listDelivery}" var="delivery">
                                        <option value="${delivery.delivery}">${delivery.companyName}</option>
                                    </c:forEach>
                                </select>
                            </div>

                            <input type="text" name="userEmail" class="form-control mt-3" value="${user.email}" >
                            <br>
                            <input type="text" name="userFullName" class="form-control mt-3" value="${user.fullName}" >
                            <br>
                            <input type="text" name="userMobile" class="form-control mt-3" value="${user.mobile}" >
                            <br>
                            <input type="text" name="userAddress" class="form-control mt-3" value="${user.address}" >



                        </form>

                        <form id="confirmOrderForm" method="post" action="cartController">
                            <input type="hidden" value="${userId}" name="userId">
                            <input type="hidden" value="${cartId}" name="cartId">


                            <input type="hidden" name="userEmail" class="form-control mt-3" value="${user.email}" >
                            <br>
                            <input type="hidden" name="userFullName" class="form-control mt-3" value="${user.fullName}" >
                            <br>
                            <input type="hidden" name="userMobile" class="form-control mt-3" value="${user.mobile}" >
                            <br>    
                            <input type="hidden" name="userAddress" class="form-control mt-3" value="${user.address}" >

                        </form>

                    </div>

                    <div class="modal-footer">
                        <p>Total cost: <fmt:formatNumber value="${totalCost}" pattern="#,##0" /></p>
                        <button type="submit" form="confirmOrderForm" class="btn btn-primary" action="POST">Xác nhận đơn hàng</button>
                    </div>
                </div>
            </div>
        </div>


        <!-- ======= Footer ======= -->
        <jsp:include page="footer.jsp"></jsp:include>



            <script src="${pageContext.request.contextPath}/view/js/jquery.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/jquery.scrollUp.min.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/jquery.prettyPhoto.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/main.js"></script>
    </body>
</html>