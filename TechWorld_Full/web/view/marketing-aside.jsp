<%-- 
    Document   : admin-aside
    Created on : Jan 21, 2024, 8:05:16 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<aside class="left-side sidebar-offcanvas">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="${pageContext.request.contextPath}/view/img/26115.jpg" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>Xin chào, ${sessionScope.user.userName}</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="active">
                <a href="${pageContext.request.contextPath}/marketing/view-sliders">
                    <i class="fa fa-dashboard"></i> <span>Slider</span>
                </a>
            </li>
            <li>
                <a href="${pageContext.request.contextPath}/marketing/marketing-viewcustomer">
                    <i class="fa fa-gavel"></i> <span>Người dùng</span>
                </a>
            </li>
             <li>
                <a href="${pageContext.request.contextPath}/marketing/posts">
                    <i class="fa fa-gavel"></i> <span>Bài dang</span>
                </a>
            </li>

            <li>
                <a href="${pageContext.request.contextPath}/admin/settings/view">
                    <i class="fa fa-globe"></i> <span>Cài đặt</span>
                </a>
            </li>

            <li>
                <a href="${pageContext.request.contextPath}/home">
                    <i class="fa fa-glass"></i> <span>Trang chủ</span>
                </a>
            </li>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
