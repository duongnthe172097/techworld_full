<%-- 
    Document   : add-setting
    Created on : Jan 18, 2024, 11:18:35 PM
    Author     : ns
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Cài đặt | Admin</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta name="description" content="Developed By M Abdur Rokib Promy">
        <meta name="keywords" content="Admin, Bootstrap 3, Template, Theme, Responsive">
        <!-- bootstrap 3.0.2 -->
        <link href="${pageContext.request.contextPath}/view/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="${pageContext.request.contextPath}/view/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="${pageContext.request.contextPath}/view/css/ionicons.min.css" rel="stylesheet" type="text/css" />

        <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
        <!-- Theme style -->
        <link href="${pageContext.request.contextPath}/view/css/style.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-black">
        <!-- header logo: style can be found in header.less -->
        <jsp:include page="admin-header.jsp"></jsp:include>
            <div class="wrapper row-offcanvas row-offcanvas-left">
                <!-- Left side column. contains the logo and sidebar -->
                <aside class="left-side sidebar-offcanvas">
                    <!-- sidebar: style can be found in sidebar.less -->
                    <section class="sidebar">
                        <!-- Sidebar user panel -->
                        <div class="user-panel">
                            <div class="pull-left image">
                                <img src="${pageContext.request.contextPath}/view/img/26115.jpg" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Xin chào, ${sessionScope.user.userName}</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>

                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li>
                            <a href="${pageContext.request.contextPath}/admin/admin-dashboard">
                                <i class="fa fa-dashboard"></i> <span>Thống kê</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/admin/user">
                                <i class="fa fa-gavel"></i> <span>Người dùng</span>
                            </a>
                        </li>

                        <li class="active">
                            <a href="${pageContext.request.contextPath}/admin/settings/view">
                                <i class="fa fa-globe"></i> <span>Cài đặt</span>
                            </a>
                        </li>

                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->


                <!-- Main content -->
                <section class="content" style="min-height: 90vh">
                    <div class="row" style="display: flex; justify-content: center">
                        <div class="col-md-8">
                            <section class="panel">
                                <header class="panel-heading">
                                    <c:choose>
                                        <c:when test="${isEdit}">
                                            Xem/Sửa cài đặt
                                        </c:when>
                                        <c:otherwise>
                                            Thêm cài đặt
                                        </c:otherwise>
                                    </c:choose>
                                </header>
                                <div class="panel-body">
                                    <c:if test="${not empty oldId}">
                                        <h3>Xem/Sửa ID :  ${oldId}</h3>
                                    </c:if>
                                    <form method="POST" role="form"
                                          <c:choose>
                                              <c:when test="${isEdit}">
                                                  action="${pageContext.request.contextPath}/admin/settings/edit"
                                              </c:when>
                                              <c:otherwise>
                                                  action="${pageContext.request.contextPath}/admin/settings/add"
                                              </c:otherwise>
                                          </c:choose>
                                          >
                                        <input type="hidden" name="oldId" value="${oldId}"     
                                               <div class="form-group">
                                        <label>ID:</label>
                                        <input class="form-control" name="id" type="text" value="${setting.id}" placeholder="Id" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label>Name:</label>
                                            <input class="form-control" name="name" type="text" value="${setting.name}" placeholder="Name" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label>Description:</label>
                                            <input class="form-control" name="description" type="text" value="${setting.description}" placeholder="Description" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label>Mô tả:</label>
                                            <input class="form-control" name="value" type="text" value="${setting.value}" placeholder="Type" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label>Tên:</label>
                                            <input class="form-control" name="type" type="text" value="${setting.type}" placeholder="Value" readonly>
                                        </div>
                                        <div class="form-group" style="margin-bottom: 3rem;">
                                            <label>Trạng thái</label>
                                            <select id="status" class="form-control" name="isActive" readonly>
                                                <option value="true">Bật</option>
                                                <option value="false">Tắt</option>
                                            </select>
                                        </div>

                                        <a class="btn btn-success" 
                                           style="margin-left: 1rem"
                                           href="${pageContext.request.contextPath}/admin/settings/view">
                                            Quay lại
                                        </a>
                                    </form>
                                    <h4 class="text-danger">${error}</h4>
                                    <h4 class="text-success">${success}</h4>

                                    <script>
                                        <c:if test="${not empty setting.isActive}">
                                        document.getElementById("status").value = '${setting.isActive}';
                                        </c:if>
                                    </script>
                                </div>
                            </section>
                        </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
            <div class="footer-main">
                Copyright &copy Director, 2014
            </div>
        </div><!-- ./wrapper -->
        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/jquery.min.js" type="text/javascript"></script>

        <!-- Bootstrap -->
        <script src="${pageContext.request.contextPath}/view/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- Director App -->
        <script src="${pageContext.request.contextPath}/view/js/Director/app.js" type="text/javascript"></script>
    </body>
</html>
