<%-- 
    Document   : login
    Created on : Jan 8, 2024, 4:21:33 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Login | Tech World</title>
        <link href="${pageContext.request.contextPath}/view/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/font-awesome.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/prettyPhoto.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/price-range.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/animate.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/main.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/responsive.css" rel="stylesheet">
        <!--[if lt IE 9]>
        <script src="${pageContext.request.contextPath}/view/js/html5shiv.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/respond.min.js"></script>
        <![endif]-->       
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/view/images/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="${pageContext.request.contextPath}/view/images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="${pageContext.request.contextPath}/view/images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="${pageContext.request.contextPath}/view/images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/view/images/ico/apple-touch-icon-57-precomposed.png">
    </head><!--/head-->

    <body>
        <!-- ======= Header ======= -->
        <jsp:include page="header.jsp"></jsp:include>

            <style>

                .form-input-gender {
                    display: flex;
                    align-items: center;
                }

                .form-input-gender div {
                    display: flex;
                    align-items: center;
                    margin-right: 30px;
                }

                .form-input-gender input[type="radio"] {
                    width: auto;
                    margin-right: 5px;
                }
            </style>

            <section id="form"><!--form-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4 col-sm-offset-1">
                            <div class="login-form"><!--login form-->
                                <h2>Đăng nhập tài khoản</h2>
                                <form action="${pageContext.request.contextPath}/login" method="post">
                                <input name="name" value="${name}" placeholder="Tên đăng nhập" required/>
                                <input name="pass" value="${password}" type="password" placeholder="Mật khẩu" required/>
                                <span>
                                    <input name="remember" type="checkbox" class="checkbox"> 
                                    Lưu đăng nhập
                                </span>
                                <button name="remember" value="1" type="submit" class="btn btn-default">Đăng nhập</button>
                                <p><a href="${pageContext.request.contextPath}/view/forgotpass.jsp">Quên mật khẩu?</a></p>
                                <p style="color: red" >${loginMsg}</p>
                            </form>
                        </div><!--/login form-->
                    </div>
                    <div class="col-sm-1">
                        <h2 class="or">HOẶC</h2>
                    </div>
                    <div class="col-sm-4">
                        <div class="signup-form"><!--sign up form-->
                            <h2>Đăng ký tài khoản</h2>
                            <form action="${pageContext.request.contextPath}/signup" method="post">
                                <input value="${name}" name="name" placeholder="Tên đăng nhập" required/>
                                <input value="${email}" name="email" type="email" placeholder="Nhập email" required/>
                                <c:if test = "${hidden == true}"> 
                                    <input name="code" type="text" placeholder="Verify Code"/>
                                </c:if>
                                <input value="${fullname}" name="fullname" placeholder="Nhập tên" required/>
                                <input value="${phone}" name="phone" type="phone" placeholder="Số điện thoại" required/>
                                <input value="${address}" name="address" type="address" placeholder="Địa chỉ" required/>
                                <input value="${pass}" name="pass" type="password" placeholder="Mật khẩu" required/>
                                <input value="${repass}" name="repass" type="password" placeholder="Nhập lại mật khẩu" required/>
                                <div class="form-input-gender">
                                    <div><input name="gender" type="radio" value="1" required/>Nam</div>
                                    <div><input name="gender" type="radio" value="0" required/>Nữ</div>
                                </div>
                                <button type="submit" class="btn btn-default">Đăng ký</button>
                                <b style="color:red">${resMsg}</b>
                            </form>
                        </div><!--/sign up form-->
                    </div>
                </div>
        </section><!--/form-->


        <!-- ======= Footer ======= -->
        <jsp:include page="footer.jsp"></jsp:include>



            <script src="${pageContext.request.contextPath}/view/js/jquery.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/price-range.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/jquery.scrollUp.min.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/jquery.prettyPhoto.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/main.js"></script>
    </body>
</html>
