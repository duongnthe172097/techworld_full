<%-- 
    Document   : marketing-settingform
    Created on : Mar 4, 2024, 9:52:45 AM
    Author     : izayo
--%>

<%-- 
    Document   : add-setting
    Created on : Jan 18, 2024, 11:18:35 PM
    Author     : ns
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page import = "model.*" %>
<%@page import = "dal.*" %>
<%@page import = "java.sql.Date" %>
<%@page import = "java.util.*" %>
<%@page import = "java.text.DecimalFormat" %>  
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Chi tiết sản phẩm | Marketing</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta name="description" content="Developed By M Abdur Rokib Promy">
        <meta name="keywords" content="Admin, Bootstrap 3, Template, Theme, Responsive">
        <!-- bootstrap 3.0.2 -->
        <link href="${pageContext.request.contextPath}/view/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="${pageContext.request.contextPath}/view/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="${pageContext.request.contextPath}/view/css/ionicons.min.css" rel="stylesheet" type="text/css" />

        <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
        <!-- Theme style -->
        <link href="${pageContext.request.contextPath}/view/css/style.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-black">
        <!-- header logo: style can be found in header.less -->
        <jsp:include page="admin-header.jsp"></jsp:include>
            <div class="wrapper row-offcanvas row-offcanvas-left">
                <!-- Left side column. contains the logo and sidebar -->
                <aside class="left-side sidebar-offcanvas">
                    <!-- sidebar: style can be found in sidebar.less -->
                    <section class="sidebar">
                        <!-- Sidebar user panel -->
                        <div class="user-panel">
                            <div class="pull-left image">
                                <img src="${pageContext.request.contextPath}/view/img/26115.jpg" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Xin chào, ${sessionScope.user.userName}</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>

                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/marketing-dashboard">
                                <i class="fa fa-dashboard"></i> <span>Thống kê</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/posts">
                                <i class="fa fa-gavel"></i> <span>Bài đăng</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/view-sliders">
                                <i class="fa fa-gavel"></i> <span>Slider</span>
                            </a>
                        </li>
                        <li class="active">
                            <a href="${pageContext.request.contextPath}/marketing/product/list">
                                <i class="fa fa-gavel"></i> <span>Sản phẩm</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/marketing-viewcustomer">
                                <i class="fa fa-gavel"></i> <span>Khách hàng</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/feedbacks/list">
                                <i class="fa fa-gavel"></i> <span>Phản hồi</span>
                            </a>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <% 
                ProductImageDAO productImageIDAO = new ProductImageDAO();
                pageContext.setAttribute("productImageIDAO", productImageIDAO, PageContext.PAGE_SCOPE);
                                    
                DecimalFormat decimalFormat = new DecimalFormat("#.###");
                pageContext.setAttribute("decimalFormat", decimalFormat, PageContext.PAGE_SCOPE);
                %>

                <!-- Main content -->
                <section class="content" style="min-height: 90vh">
                    <div class="row" style="display: flex; justify-content: center">
                        <div class="col-md-8">
                            <section class="panel">
                                <header class="panel-heading">
                                    <c:choose>
                                        <c:when test="${isEdit}">
                                            Xem/Sửa sản phẩm
                                        </c:when>

                                    </c:choose>
                                </header>
                                <div class="panel-body">
                                    <c:if test="${not empty oldId}">
                                        <h3>Xem/Sửa ID :  ${oldId}</h3>
                                    </c:if>
                                    <form method="POST" role="form"
                                          <c:choose>
                                              <c:when test="${isEdit}">
                                                  action="${pageContext.request.contextPath}/marketing/settings/edit"
                                              </c:when>                                             
                                          </c:choose>
                                          >
                                        <input type="hidden" name="oldId" value="${oldId}">
                                        <div class="form-group">
                                            <label>Chọn ảnh mặc định:</label>
                                            <select class="form-control" name="selectedImage" onchange="displayImage(this)">
                                                <%-- Sử dụng forEach để lặp qua tất cả các URL hình ảnh --%>
                                                <c:forEach var="imageUrl" items="${productImage}">
                                                    <option value="${imageUrl}">${imageUrl}</option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Image:</label>
                                            <img id="selectedImageDisplay" style="width: 200px !important;" src="" alt="Product Image">
                                        </div>
                                        <script>
                                            function displayImage(select) {
                                                var selectedImageUrl = select.value;
                                                var imageDisplay = document.getElementById("selectedImageDisplay");
                                                imageDisplay.src = "${pageContext.request.contextPath}/view/images/product/" + selectedImageUrl;
                                            }
                                        </script>

                                        <div class="form-group">
                                            <label>ID:</label>
                                            <input class="form-control" name="id" type="text" value="${product.productId}" placeholder="Id" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Tên:</label>
                                            <input class="form-control" name="name" type="text" value="${product.productName}" placeholder="Value" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Danh mục</label>
                                            <input class="form-control" name="categoryName" type="text" value="${categoryName}" placeholder="Type" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Nhãn hàng:</label>
                                            <input class="form-control" name="brandName" type="text" value="${brandName}" placeholder="Type" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Mô tả:</label>
                                            <input class="form-control" name="description" type="text" value="${product.description}" placeholder="Type" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Số hàng còn trong kho</label>
                                            <input class="form-control" name="quantityInStock" type="text" value="${product.quantityInStock}" placeholder="Type" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Giá money</label>
                                            <input class="form-control" name="price" type="text" value="${decimalFormat.format(product.price)}" placeholder="Type" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Discount</label>
                                            <input class="form-control" name="discount" type="text" value="${product.discount}" placeholder="Type" disabled>
                                        </div>
                                        <div class="form-group" style="margin-bottom: 3rem;">
                                            <label>Trạng thái</label>
                                            <select id="status" class="form-control" name="iActive" readonly>
                                                <option value="true" ${product.isStatus() ? ' selected' : ''}>Hiện</option>
                                                <option value="false" ${product.isStatus() ? '' : ' selected'}>Ẩn</option>
                                            </select>
                                        </div>

                                        <a class="btn btn-primary" 
                                           style="margin-left: 1rem"
                                           href="${pageContext.request.contextPath}/marketing/settings/edit?id=${product.productId}" >
                                            Sửa
                                        </a> 
                                        <a class="btn btn-success" 
                                           style="margin-left: 1rem"
                                           href="${pageContext.request.contextPath}/marketing/product/list">
                                            Quay lại
                                        </a>
                                    </form>
                                    <h4 class="text-danger">${error}</h4>
                                    <h4 class="text-success">${success}</h4>

                                    <script>
                                        <c:if test="${not empty setting.iActive}">
                                        document.getElementById("status").value = '${setting.iActive}';
                                        </c:if>
                                    </script>
                                </div>
                            </section>
                        </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
            <div class="footer-main">
                Copyright &copy Director, 2014
            </div>
        </div><!-- ./wrapper -->
        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/jquery.min.js" type="text/javascript"></script>

        <!-- Bootstrap -->
        <script src="${pageContext.request.contextPath}/view/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- Director App -->
        <script src="${pageContext.request.contextPath}/view/js/Director/app.js" type="text/javascript"></script>
    </body>
</html>

