<%-- 
    Document   : product-detaiks
    Created on : Jan 8, 2024, 4:21:43 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import = "model.*" %>
<%@page import = "dal.*" %>
<%@page import = "java.sql.Date" %>
<%@page import = "java.util.*" %>
<%@page import = "java.text.DecimalFormat" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Chi tiết sản phẩm | Tech World</title>
        <link href="${pageContext.request.contextPath}/view/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/font-awesome.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/prettyPhoto.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/price-range.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/animate.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/main.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/responsive.css" rel="stylesheet">
        <!--[if lt IE 9]>
        <script src="${pageContext.request.contextPath}/view/js/html5shiv.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/respond.min.js"></script>
        <![endif]-->       
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/view/images/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="${pageContext.request.contextPath}/view/images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="${pageContext.request.contextPath}/view/images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="${pageContext.request.contextPath}/view/images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/view/images/ico/apple-touch-icon-57-precomposed.png">
    </head><!--/head-->

    <%
    //Get all categories
    CategoryDAO categoryDAO = new CategoryDAO();
    List<Category> allActiveCategories = categoryDAO.getAllActiveCategories();
    pageContext.setAttribute("allActiveCategories", allActiveCategories, PageContext.PAGE_SCOPE);
    pageContext.setAttribute("categoryDAO", categoryDAO, PageContext.PAGE_SCOPE);
    
    //Get all brands
    BrandDAO brandDAO = new BrandDAO();
    List<Brand> allActiveBrands = brandDAO.getAllActiveBrands();
    pageContext.setAttribute("allActiveBrands", allActiveBrands, PageContext.PAGE_SCOPE);
    pageContext.setAttribute("brandDAO", brandDAO, PageContext.PAGE_SCOPE);
    
    ProductImageDAO productImageDAO = new ProductImageDAO();
    pageContext.setAttribute("productImageDAO", productImageDAO, PageContext.PAGE_SCOPE);
    
    DecimalFormat decimalFormat = new DecimalFormat("#,###");
    pageContext.setAttribute("decimalFormat", decimalFormat, PageContext.PAGE_SCOPE);
    
    %>
    <body>
        <style>

            .carousel-inner {
                display: contents;
            }

            .carousel-inner a img {
                height: 84px !important;
                width: 84px;
            }

            .left-sidebar {
                background-color: #ffffff;
                padding-top: 20px;
                margin-left: -15px;
                margin-top: 10px;
            }

            .left-sidebar .panel-title {
                color: #333333;
                font-weight: 500;
                padding-left: 10px;
                font-size: 18px;
            }

            .left-sidebar ul li {
                padding: 5px 10px;
                font-weight: 400;
                font-size: 16px;
            }

            .left-sidebar ul li input {
                margin-right: 10px;
            }

            #header {
                background-color: #7FCDF9;
                box-shadow: rgba(0, 0, 0, 0.1) 0px 4px 6px -1px, rgba(0, 0, 0, 0.06) 0px 2px 4px -1px;
            }

            .logo-techworld img{
                width: 120px;
                height: auto;
            }

            .header-searchform input{
                background-color: #ffffff;
                color: #333333;
            }

            body {
                background-color: #F8F8F8;
            }

            .product-homepage {
                background-color: #ffffff;
                border: none;
                box-shadow: rgba(0, 0, 0, 0.1) 0px 10px 15px -3px, rgba(0, 0, 0, 0.05) 0px 4px 6px -2px;
            }

            .product-homepage span {
                color: #f19d23;
                font-size: 20px;
                font-weight: 400;
            }

            .product-homepage span del {
                color: red;
                font-size: 12px;
            }

            .product-homepage img {
                box-shadow: rgba(255, 255, 255, 0.56) 0px 22px 70px 4px;
            }

            .product-homepage p {
                color: #000000;
                font-size: 18px;
                margin: 20px 0;
                min-height: 50px;
            }

            @media (min-width: 768px) {

                #product-homepage {
                    min-height: 220vh;
                }

                .product-homepage {
                    height: 360px;
                    overflow: hidden;
                    padding: 3px;
                }
                .product-homepage img {
                    height: 150px;
                    width: fit-content;
                    max-width: 100%;
                }

                .feedback-header img {
                    width: 100px;
                }
            }
        </style>
        <!-- ======= Header ======= -->
        <header id="header"><!--header-->
            <div class="header_top"><!--header_top-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="contactinfo">
                                <ul class="nav nav-pills">
                                    <li><a href="#"><i class="fa fa-phone"></i> +2 95 01 88 821</a></li>
                                    <li><a href="#"><i class="fa fa-envelope"></i> minhnhhe170924@fpt.edu.vn</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="social-icons pull-right">
                                <ul class="nav navbar-nav">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/header_top-->

            <div class="header-middle"><!--header-middle-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="logo pull-left logo-techworld">
                                <a href="${pageContext.request.contextPath}/home"><img src="${pageContext.request.contextPath}/view/images/home/tech_logo.png" alt="" /></a>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="shop-menu pull-right">
                                <ul class="nav navbar-nav">
                                    <c:if test="${sessionScope.user == null}">
                                        <li><a href="${pageContext.request.contextPath}/view/login.jsp"><i class="fa fa-user"></i> Tài khoản</a></li>
                                        <li><a href="${pageContext.request.contextPath}/view/login.jsp"><i class="fa fa-lock"></i> Đăng nhập</a></li>
                                        </c:if>
                                        <c:if test="${sessionScope.user != null}">
                                        <li><a href="${pageContext.request.contextPath}/profile"><i class="fa fa-user"></i> ${sessionScope.user.fullName}</a></li>
                                            <c:if test="${sessionScope.user.roleId == 5}">
                                            <li><a href="${pageContext.request.contextPath}/admin/admin-dashboard"><i class="fa fa-crosshairs"></i> Trang quản trị</a></li>
                                            </c:if>
                                            <c:if test="${sessionScope.user.roleId == 3 || sessionScope.user.roleId == 4}">
                                            <li><a href="${pageContext.request.contextPath}/sale/dashboard"><i class="fa fa-crosshairs"></i> Trang sale</a></li>
                                            </c:if>
                                            <c:if test="${sessionScope.user.roleId == 2}">
                                            <li><a href="${pageContext.request.contextPath}/marketing/marketing-dashboard"><i class="fa fa-crosshairs"></i> Trang marketing</a></li>
                                            </c:if>
                                            <c:if test="${sessionScope.user.roleId == 1}">
                                            <li><a href="${pageContext.request.contextPath}/cartController"><i class="fa fa-shopping-cart"></i>Giỏ hàng</a></li>
                                            <li><a href="${pageContext.request.contextPath}/customer/my-order"><i class="fa fa-crosshairs"></i> Đơn hàng của tôi</a></li>
                                            </c:if>
                                        <li><a href="${pageContext.request.contextPath}/logout"><i class="fa fa-lock"></i> Đăng xuất</a></li>
                                        </c:if>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/header-middle-->

            <div class="header-bottom"><!--header-bottom-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-9">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="mainmenu pull-left">
                                <ul class="nav navbar-nav collapse navbar-collapse">
                                    <li><a href="${pageContext.request.contextPath}/home">Trang chủ</a></li>
                                    <li><a href="${pageContext.request.contextPath}/products" class="active">Sản phẩm</a></li>
                                    <li><a href="${pageContext.request.contextPath}/blogs">Bài đăng</a></li>
                                    <li><a href="${pageContext.request.contextPath}/view/contact-us.jsp">Liên hệ</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <form action="${pageContext.request.contextPath}/products" class="search_box pull-right header-searchform">
                                <input type="text" name="search" placeholder="Tìm kiếm sản phẩm" value="${requestScope.searchKey}"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!--/header-bottom-->
        </header><!--/header-->

        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <!-- ======= Sidebar ======= -->
                        <div class="left-sidebar">

                            <form action="${pageContext.request.contextPath}/products">
                                <h2>Danh mục</h2>
                                <div class="panel-group category-products" id="accordian"><!--category-productsr-->
                                    <c:forEach begin="0" end="${allActiveCategories.size() - 1}" var="i">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <input 
                                                    type="checkbox" 
                                                    name="categoryId" 
                                                    value="${allActiveCategories.get(i).getCategoryId()}"
                                                    ${requestScope.checkedCategories[i] ? 'checked' : ''}
                                                    onclick="this.form.submit()"
                                                    />
                                                <span class="panel-title">${allActiveCategories.get(i).getCategoryName()}</span>
                                            </div>
                                        </div>
                                    </c:forEach>
                                    <!--/category-products-->

                                </div>
                                <div class="brands_products"><!--brands_products-->
                                    <h2>Thương hiệu</h2>
                                    <div class="brands-name">
                                        <ul class="nav nav-pills nav-stacked">
                                            <c:forEach begin="0" end="${allActiveBrands.size() - 1}" var="i">
                                                <li>
                                                    <input 
                                                        type="checkbox" 
                                                        name="brandId" 
                                                        value="${allActiveBrands.get(i).getBrandId()}"
                                                        ${requestScope.checkedBrands[i] ? 'checked' : ''}
                                                        onclick="this.form.submit()"
                                                        />
                                                    ${allActiveBrands.get(i).getBrandName()}
                                                </li>
                                            </c:forEach>
                                        </ul>
                                    </div>
                                </div>
                                <!--/brands_products-->
                            </form>
                            <div class="price-range"><!--price-range-->
                                <h2>Tìm kiếm</h2>
                                <div class="well text-center" style="padding: 0px;display: flex;justify-content: center;">
                                    <form action="${pageContext.request.contextPath}/products" class="search_box pull-right header-searchform">
                                        <input type="text" name="search" placeholder="Tìm kiếm sản phẩm" value="${requestScope.searchKey}"
                                               style="padding: 10px; border: 1px solid burlywood; width: 100%"/>
                                    </form>
                                </div>
                            </div><!--/price-range-->


                            <div class="shipping text-center"><!--shipping-->
                                <img src="${pageContext.request.contextPath}/view/images/home/shipping.jpg" alt="" />
                            </div><!--/shipping-->

                        </div>
                    </div>

                    <div class="col-sm-9 padding-right">
                        <div class="product-details"><!--product-details-->
                            <div class="col-sm-5">
                                <div class="view-product">
                                    <c:set var="imageList" value="${productImageDAO.getAllImageOfProductById(productDetails.productId)}"/>
                                    <c:set var="imageUrl" value="${(imageList.size() > 0) ? imageList.get(imgIndex).imageUrl : ''}"/>
                                    <img src="${pageContext.request.contextPath}/view/images/product/${imageUrl}" alt="image" />
                                </div>
                                <div id="similar-product" class="carousel slide" data-ride="carousel">

                                    <!-- Wrapper for slides -->
                                    <div class="carousel-inner">
                                        <div class="item active">
                                            <c:forEach begin="0" end="2" var="index">
                                                <c:if test="${imageList.size() > index}">
                                                    <a href="${pageContext.request.contextPath}/product?id=${productDetails.productId}&img=${index}">
                                                        <img src="${pageContext.request.contextPath}/view/images/product/${imageList.get(index).imageUrl}" alt="">
                                                    </a>
                                                </c:if>
                                            </c:forEach>
                                        </div>
                                        <c:if test="${imageList.size() > 3}">
                                            <div class="item">
                                                <c:forEach begin="3" end="5" var="index">
                                                    <c:if test="${imageList.size() > index}">
                                                        <a href="${pageContext.request.contextPath}/product?id=${productDetails.productId}&img=${index}">
                                                            <img src="${pageContext.request.contextPath}/view/images/product/${imageList.get(index).imageUrl}" alt="">
                                                        </a>
                                                    </c:if>
                                                </c:forEach>
                                            </div>
                                        </c:if>
                                        <c:if test="${imageList.size() > 6}">
                                            <div class="item">
                                                <c:forEach begin="6" end="8" var="index">
                                                    <c:if test="${imageList.size() > index}">
                                                        <a href="${pageContext.request.contextPath}/product?id=${productDetails.productId}&img=${index}">
                                                            <img src="${pageContext.request.contextPath}/view/images/product/${imageList.get(index).imageUrl}" alt="">
                                                        </a>
                                                    </c:if>
                                                </c:forEach>
                                            </div>
                                        </c:if>
                                    </div>

                                    <!-- Controls -->
                                    <a class="left item-control" href="#similar-product" data-slide="prev">
                                        <i class="fa fa-angle-left"></i>
                                    </a>
                                    <a class="right item-control" href="#similar-product" data-slide="next">
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </div>

                            </div>
                            <div class="col-sm-7">
                                <div class="product-information"><!--/product-information-->
                                    <img src="${pageContext.request.contextPath}/view/images/product-details/new.jpg" class="newarrival" alt="" />
                                    <h2>${requestScope.productDetails.productName}</h2>
                                    <p>Loại hàng: ${categoryDAO.getCategoryNameById(productDetails.categoryId)}</p>
                                    <img src="${pageContext.request.contextPath}/view/images/product-details/rating.png" alt="" />
                                    <span>
                                        <c:set var="newPrice" value="${decimalFormat.format(productDetails.price * (1 - productDetails.discount/100))}"/>
                                        <c:set var="oldPrice" value="${decimalFormat.format(productDetails.price)}"/>
                                        <span style="font-size: 20px;">
                                            ${newPrice}đ
                                            <br/>
                                            <del style="font-size: 13px; font-weight: lighter; color: red;">
                                                <c:if test="${oldPrice != newPrice}">
                                                    ${oldPrice}đ
                                                </c:if>
                                            </del>
                                        </span>
                                        <form action="${pageContext.request.contextPath}/addToCart" method="get" style="display: flex; align-items: center;">
                                            <label>Số lượng:</label>
                                            <input name="productId" value="${productDetails.productId}" type="hidden"/>
                                            <input type="number" name="quantity" value="1" min="1" max="${productDetails.quantityInStock}"/>
                                            <c:set var="roleId" value="${sessionScope.user.roleId}"/>
                                             <c:if test="${roleId == 1 || empty roleId}">
                                                <button type="submit" class="btn btn-fefault cart">
                                                    <i class="fa fa-shopping-cart"></i>
                                                    Thêm vào giỏ
                                                </button>
                                            </c:if>

                                        </form>

                                    </span>
                                    <p><b>Còn hàng:</b> Trong kho (${productDetails.quantityInStock})</p>
                                    <p><b>Thương hiệu:</b> ${brandDAO.getBrandNameById(productDetails.categoryId)}</p>
                                    <a href=""><img src="${pageContext.request.contextPath}/view/images/product-details/share.png" class="share img-responsive"  alt="" /></a>
                                </div><!--/product-information-->
                            </div>
                        </div><!--/product-details-->

                        <div class="category-tab shop-details-tab"><!--category-tab-->
                            <div class="col-sm-12">
                                <ul class="nav nav-tabs">
                                    <li><a href="#details" data-toggle="tab">Mô tả</a></li>
                                    <li class="active"><a href="#reviews" data-toggle="tab">Phản hồi (${requestScope.numberOfFeedbacks})</a></li>
                                </ul>
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane fade" id="details" >
                                    <p>${requestScope.productDetails.description}</p>
                                </div>

                                <div class="tab-pane fade active in" id="reviews" >
                                    <div class="col-sm-12 feedback-header">
                                        <c:choose>
                                            <c:when test="${requestScope.avgRating == 5}">
                                                <c:set var="imgRating" value="5"/>
                                            </c:when>
                                            <c:when test="${requestScope.avgRating < 5 && requestScope.avgRating > 4}">
                                                <c:set var="imgRating" value="4.5"/>
                                            </c:when>
                                            <c:when test="${requestScope.avgRating == 4}">
                                                <c:set var="imgRating" value="4"/>
                                            </c:when>
                                            <c:when test="${requestScope.avgRating < 4 && requestScope.avgRating > 3}">
                                                <c:set var="imgRating" value="3.5"/>
                                            </c:when>
                                            <c:when test="${requestScope.avgRating == 3}">
                                                <c:set var="imgRating" value="3"/>
                                            </c:when>
                                            <c:when test="${requestScope.avgRating < 3 && requestScope.avgRating > 2}">
                                                <c:set var="imgRating" value="2.5"/>
                                            </c:when>
                                            <c:when test="${requestScope.avgRating == 2}">
                                                <c:set var="imgRating" value="2"/>
                                            </c:when>
                                            <c:when test="${requestScope.avgRating < 2 && requestScope.avgRating > 1}">
                                                <c:set var="imgRating" value="1.5"/>
                                            </c:when>
                                            <c:when test="${requestScope.avgRating == 1}">
                                                <c:set var="imgRating" value="1"/>
                                            </c:when>
                                            <c:otherwise>
                                                <c:set var="imgRating" value="0.5"/>
                                            </c:otherwise>
                                        </c:choose>
                                        <img src="${pageContext.request.contextPath}/view/images/feedback/${imgRating}.png" alt="rating"/>
                                        <h6>Trung bình: ${requestScope.avgRating}</h6>
                                    </div>
                                    <c:set var="feedbackSet" value="${requestScope.feedbacksOfProduct.keySet()}"/>
                                    <c:forEach items="${feedbackSet}" var="feedback">
                                        <div class="col-sm-12" style="margin-top: 10px; margin-bottom: 10px; border: 1px solid gainsboro; padding: 10px;">
                                            <ul>
                                                <li><a href=""><i class="fa fa-user"></i>${requestScope.feedbacksOfProduct.get(feedback)}</a></li>
                                                <li><a href=""><i class="fa fa-calendar-o"></i>${feedback.updatedDate}</a></li>
                                            </ul>
                                            <p>${feedback.content}</p>
                                            <b>Đánh giá: </b> 
                                            <img 
                                                src="${pageContext.request.contextPath}/view/images/feedback/${feedback.ratedStar}.png" 
                                                alt="" 
                                                style="width: 70px;"
                                                />
                                        </div>
                                    </c:forEach>
                                </div>

                            </div>
                        </div><!--/category-tab-->

                        <div class="recommended_items"><!--recommended_items-->
                            <h2 class="title text-center">Sản phẩm mới</h2>

                            <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
                                <div>
                                    <c:forEach items="${requestScope.lastestProducts}" var="product">
                                        <div class="col-sm-4">
                                            <div class="product-image-wrapper">
                                                <div class="single-products product-homepage">
                                                    <div class="productinfo text-center">
                                                        <a href="${pageContext.request.contextPath}/product?id=${product.productId}">
                                                            <c:set var="imageList" value="${productImageDAO.getAllImageOfProductById(product.productId)}"/>
                                                            <c:set var="imageUrl" value="${(imageList.size() > 0) ? imageList.get(0).imageUrl : ''}"/>
                                                            <img src="${pageContext.request.contextPath}/view/images/product/${imageUrl}" alt="image" />
                                                            <c:set var="newPrice" value="${decimalFormat.format(product.price * (1 - product.discount/100))}"/>
                                                            <h2>
                                                                <span>${newPrice}đ</span>
                                                                <c:set var="oldPrice" value="${decimalFormat.format(product.price)}"/>
                                                                <span>
                                                                    <del>
                                                                        <c:if test="${oldPrice != newPrice}">
                                                                            ${oldPrice}đ
                                                                        </c:if>
                                                                    </del>
                                                                </span>
                                                            </h2>
                                                            <p>${product.productName}</p>
                                                        </a>
                                                        <button class="btn btn-default add-to-cart"></i>Mới</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </c:forEach>
                                </div>			
                            </div>
                        </div><!--/recommended_items-->

                    </div>
                </div>
            </div>
        </section>

        <!-- ======= Footer ======= -->
        <jsp:include page="footer.jsp"></jsp:include>



            <script src="${pageContext.request.contextPath}/view/js/jquery.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/price-range.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/jquery.scrollUp.min.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/jquery.prettyPhoto.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/main.js"></script>
    </body>
</html>
