<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import = "model.*" %>
<%@page import = "dal.*" %>
<%@page import = "java.sql.Date" %>
<%@page import = "java.util.*" %>
<%@page import = "java.text.DecimalFormat" %>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Sale | Đơn hàng</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta name="description" content="Developed By M Abdur Rokib Promy">
        <meta name="keywords" content="Admin, Bootstrap 3, Template, Theme, Responsive">
        <!-- bootstrap 3.0.2 -->
        <link href="${pageContext.request.contextPath}/view/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="${pageContext.request.contextPath}/view/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="${pageContext.request.contextPath}/view/css/ionicons.min.css" rel="stylesheet" type="text/css" />

        <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
        <!-- Theme style -->
        <link href="${pageContext.request.contextPath}/view/css/style.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>

    <%
        //order information
        Order orderInfo = (Order)request.getAttribute("orderInfo");
        pageContext.setAttribute("orderInfo", orderInfo, PageContext.PAGE_SCOPE);
        
        //userDAO
        UserDAO userDAO = new UserDAO();
        pageContext.setAttribute("userDAO", userDAO, PageContext.PAGE_SCOPE);
        
        //orderDAO
        OrderDAO orderDAO = new OrderDAO();
        pageContext.setAttribute("orderDAO", orderDAO, PageContext.PAGE_SCOPE);
        
        //orderDetails
        List<OrderDetail> orderDetails = orderDAO.getOrderDetailsByOrderId(orderInfo.getOrderId());
        pageContext.setAttribute("orderDetails", orderDetails, PageContext.PAGE_SCOPE);
        
        //product image dao
        ProductImageDAO productImageDAO = new ProductImageDAO();
        pageContext.setAttribute("productImageDAO", productImageDAO, PageContext.PAGE_SCOPE);
        
        //product dao
        ProductDAO productDAO = new ProductDAO();
        pageContext.setAttribute("productDAO", productDAO, PageContext.PAGE_SCOPE);
        
        //category dao
        CategoryDAO categoryDAO = new CategoryDAO();
        pageContext.setAttribute("categoryDAO", categoryDAO, PageContext.PAGE_SCOPE);
        
        //brand dao
        BrandDAO brandDAO = new BrandDAO();
        pageContext.setAttribute("brandDAO", brandDAO, PageContext.PAGE_SCOPE);
        
        //decimal format
        DecimalFormat decimalFormat = new DecimalFormat("#,###");
        pageContext.setAttribute("decimalFormat", decimalFormat, PageContext.PAGE_SCOPE);
    %>

    <body class="skin-black">


        <!-- header logo: style can be found in header.less -->
        <jsp:include page="admin-header.jsp"></jsp:include>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="${pageContext.request.contextPath}/view/img/26115.jpg" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Xin chào, ${sessionScope.user.userName}</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li>
                            <a href="${pageContext.request.contextPath}/sale/dashboard">
                                <i class="fa fa-dashboard"></i> <span>Thống kê</span>
                            </a>
                        </li>
                        <li class="active">
                            <c:if test="${sessionScope.user.roleId == 3}">
                                <c:set var="orderUrl" value="${pageContext.request.contextPath}/sale/order/list"/>
                            </c:if>
                            <c:if test="${sessionScope.user.roleId == 4}">
                                <c:set var="orderUrl" value="${pageContext.request.contextPath}/saleManager/order/list"/>
                            </c:if>
                            <a href="${orderUrl}">
                                <i class="fa fa-gavel"></i> <span>Đơn hàng</span>
                            </a>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <aside class="right-side">

                <!-- Main content -->
                <section class="content">
                    <div class="row" style="margin-bottom:5px;">
                        <div class="col-md-12">
                            <a class="btn btn-primary" href="${pageContext.request.contextPath}/saleManager/order/list">Quay lại</a>
                        </div>
                    </div>
                    <div class="row" style="margin-bottom:5px;">
                        <div class="col-md-7">
                            <header class="panel-heading">
                                Thông tin người nhận
                            </header>
                            <div class="panel-body" style="background-color: #ffffff;">
                                <c:set var="customerInfo" value="${userDAO.getUserByID(orderInfo.userId)}"/>
                                <div class="col-md-5">
                                    <img style="border-radius: 50%; width: 200px;" src="${pageContext.request.contextPath}/view/images/user/${customerInfo.image}"/>
                                </div>
                                <div class="col-md-7">
                                    <ul class="list-group teammates">
                                        <li class="list-group-item" style="border: none"><strong>Tên:</strong>  ${customerInfo.fullName}</li>
                                        <li class="list-group-item" style="border: none"><strong>Email:</strong>    ${customerInfo.email}</li>
                                        <li class="list-group-item" style="border: none"><strong>Giới tính:</strong>    ${customerInfo.isGender() ? 'Nam' : 'Nữ' }</li>
                                        <li class="list-group-item" style="border: none"><strong>Điện thoại:</strong>   ${customerInfo.mobile}</li>
                                        <li class="list-group-item" style="border: none"><strong>Địa chỉ:</strong>  ${customerInfo.address}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <header class="panel-heading">
                                Quản lý đơn
                            </header>
                            <div class="panel-body" style="background-color: #ffffff;">
                                <h4 style="color: green;">${requestScope.msg}</h4>
                                <form action="${pageContext.request.contextPath}/saleManager/order/update" method="get">
                                    <input type="hidden" name="orderId" value="${orderInfo.orderId}"/>
                                    <ul class="list-group teammates">
                                        <li class="list-group-item" style="border: none">
                                            <label for="#saleName">Người phụ trách:</label>
                                            <c:set var="saleList" value="${userDAO.getUsersByRoleId(3)}"/>
                                            <select id="saleName" name="saleId" style="border: 1px solid gainsboro; padding: 5px;">
                                                <c:forEach items="${saleList}" var="saleChoice">
                                                    <option 
                                                        value="${saleChoice.userId}"
                                                        ${orderInfo.saleId == saleChoice.userId ? ' selected' : ''}
                                                        >
                                                        ${saleChoice.fullName}
                                                    </option>
                                                </c:forEach>
                                            </select>
                                        </li>
                                        <li class="list-group-item" style="border: none">
                                            <label for="#orderStatus">Trạng thái:</label>
                                            <c:if test="${orderInfo.state != 3}">
                                                <input type="hidden" name="state" value="${orderInfo.state}"/>
                                            </c:if>
                                            <select id="orderStatus" name="state" style="border: 1px solid gainsboro; padding: 5px;" ${orderInfo.state != 3 ? ' disabled' : '' }>
                                                <option value="1" ${orderInfo.state == 1 ? ' selected' : ''}>
                                                    Ðã giao
                                                </option>
                                                <option value="2"  ${orderInfo.state == 2 ? ' selected' : ''}>
                                                    Ðã hủy
                                                </option>
                                                <option value="3"  ${orderInfo.state == 3 ? ' selected' : ''}>
                                                    Ðã đặt
                                                </option>
                                            </select>
                                        </li>
                                        <li class="list-group-item" style="border: none">
                                            <label for="#saleNote">Ghi chú:</label>
                                            <input id="saleNote" type="text" name="saleNote" value="${orderInfo.saleNote}" style="border: 1px solid gainsboro; padding: 5px; width: 100%;"/>
                                        </li>
                                        <li class="list-group-item" style="border: none">
                                            <button type="submit" class="btn btn-primary">Cập nhật</button>
                                        </li>
                                    </ul>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="row" style="margin-bottom:5px;">
                        <div class="col-md-12">
                            <header class="panel-heading">
                                Danh sách sản phẩm
                            </header>
                            <div class="panel-body" style="background-color: #ffffff;">
                                <div class="panel-body table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Ảnh</th>
                                                <th>Sản phẩm</th>
                                                <th>Phân loại</th>
                                                <th>Thương hiệu</th>
                                                <th>Đơn giá</th>
                                                <th>Giảm giá</th>
                                                <th>Số lượng</th>
                                                <th>Tạm tính</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:set var="productIndex" value="1"/>
                                            <c:set var="amountTotal" value="0"/>
                                            <c:forEach items="${orderDetails}" var="item">
                                                <tr>
                                                    <td>${productIndex}</td>
                                                    <c:set var="productIndex" value="${productIndex + 1}"/>
                                                    <c:set var="imageList" value="${productImageDAO.getAllImageOfProductById(item.productId)}"/>
                                                    <c:set var="imageUrl" value="${(imageList.size() > 0) ? imageList.get(0).imageUrl : ''}"/>
                                                    <td><img style="width: 100px;" src="${pageContext.request.contextPath}/view/images/product/${imageUrl}" alt="image" /></td>
                                                    <td>${productDAO.getProductById(item.productId).productName}</td>
                                                    <td>${categoryDAO.getCategoryNameById(productDAO.getProductById(item.productId).categoryId)}</td>
                                                    <td>${brandDAO.getBrandNameById(productDAO.getProductById(item.productId).brandId)}</td>
                                                    <c:set var="productPrice" value="${productDAO.getProductById(item.productId).price}"/>
                                                    <td>${decimalFormat.format(productPrice)}</td>
                                                    <c:set var="productDiscount" value="${productDAO.getProductById(item.productId).discount}"/>
                                                    <td>${productDiscount}%</td>
                                                    <td>${item.quantity}</td>
                                                    <c:set var="subTotal" value="${item.quantity*(productPrice * (1 - productDiscount/100))}"/>
                                                    <c:set var="amountTotal" value="${amountTotal + subTotal}"/>
                                                    <td>${decimalFormat.format(subTotal)}</td>
                                                    <c:set var="saleIndex" value="${productIndex + 1}"/>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                        <tfoot>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <th>Thành tiền</th>
                                        <th>${decimalFormat.format(amountTotal)}</th>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>

                            <!-- </div> -->
                        </div>
                    </div>   

                    <!-- Main row -->

                    <!-- row end -->
                </section><!-- /.content -->
                <div class="footer-main">
                    Copyright &copy Director, 2014
                </div>
            </aside><!-- /.right-side -->

        </div><!-- ./wrapper -->


        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/jquery.min.js" type="text/javascript"></script>

        <!-- Bootstrap -->
        <script src="${pageContext.request.contextPath}/view/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- Director App -->
        <script src="${pageContext.request.contextPath}/view/js/Director/app.js" type="text/javascript"></script>

    </body>
</html>