<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import = "model.*" %>
<%@page import = "dal.*" %>
<%@page import = "java.sql.Date" %>
<%@page import = "java.util.*" %>
<%@page import = "java.text.DecimalFormat" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Chi tiết phản hồi | Marketing</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta name="description" content="Developed By M Abdur Rokib Promy">
        <meta name="keywords" content="Admin, Bootstrap 3, Template, Theme, Responsive">
        <!-- bootstrap 3.0.2 -->
        <link href="${pageContext.request.contextPath}/view/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="${pageContext.request.contextPath}/view/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="${pageContext.request.contextPath}/view/css/ionicons.min.css" rel="stylesheet" type="text/css" />

        <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
        <!-- Theme style -->
        <link href="${pageContext.request.contextPath}/view/css/style.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.${pageContext.request.contextPath}/view/js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script type="text/javascript">
            // Hàm để ẩn thông báo sau một khoảng thời gian
            function hideAlert() {
                var alertDiv = document.getElementById("updateSuccessAlert");
                if (alertDiv) {
                    setTimeout(function () {
                        alertDiv.style.display = "none";
                    }, 5000); // 5 giây
                }
            }

            // Gọi hàm hideAlert khi trang tải xong
            window.onload = hideAlert;
        </script>

    </head>

    <style>
        .user-brief-info {
            text-align: center;
        }
        .user-brief-info img{
            width: 200px;
            height: 200px;
            border-radius: 50%;
        }

        .user-brief-info h4,h3 {
            color: grey;
        }

        .user-brief-info h4 {
            padding: 20px;
        }

        .form-update-user select{
            border: 1px solid wheat;
            padding: 10px;
        }

        .form-update-user span {
            padding: 25px;
        }

        @media(min-width: 998px) {
            .user-brief-info section{
                min-height: 460px;
            }
        }
    </style>

    <%
        FeedbackDetail feedbackDetail = (FeedbackDetail)request.getAttribute("feedbackdetail");    
    %>

    <!-- Phần hiển thị thông báo cập nhật thành công -->

    <body class="skin-black">
        <!-- header logo: style can be found in header.less -->
        <jsp:include page="admin-header.jsp"></jsp:include>
            <div class="wrapper row-offcanvas row-offcanvas-left">
                <!-- Left side column. contains the logo and sidebar -->
                <aside class="left-side sidebar-offcanvas">
                    <!-- sidebar: style can be found in sidebar.less -->
                    <section class="sidebar">
                        <!-- Sidebar user panel -->
                        <div class="user-panel">
                            <div class="pull-left image">
                                <img src="${pageContext.request.contextPath}/view/img/26115.jpg" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Xin chào, ${sessionScope.user.userName}</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>

                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/marketing-dashboard">
                                <i class="fa fa-dashboard"></i> <span>Thống kê</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/posts">
                                <i class="fa fa-gavel"></i> <span>Bài đăng</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/view-sliders">
                                <i class="fa fa-gavel"></i> <span>Slider</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/product/list">
                                <i class="fa fa-gavel"></i> <span>Sản phẩm</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/marketing-viewcustomer">
                                <i class="fa fa-gavel"></i> <span>Khách hàng</span>
                            </a>
                        </li>
                        <li class="active">
                            <a href="${pageContext.request.contextPath}/marketing/feedbacks/list">
                                <i class="fa fa-gavel"></i> <span>Phản hồi</span>
                            </a>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->


                <!-- Main content -->
                <section class="content" style="min-height: 90vh">
                    <div class="row">

                        
                        <div class="col-lg-12">
                            <a class="btn btn-primary" href="${pageContext.request.contextPath}/marketing/feedbacks/list" style="margin: 10px 0;">Quay lại</a>
                            <section class="panel">
                                <header class="panel-heading">
                                    Chi tiết đánh giá
                                </header>
                                <div class="panel-body">
                                    <form action="${pageContext.request.contextPath}/marketing/feedback/change-status" method="get" >

                                        <div class="row">
                                            <div class="col-md-6">
                                                <img src="${pageContext.request.contextPath}/view/images/product/<%=feedbackDetail.getImageUrl()%>" id="profile-image" class="img-responsive" alt="Slider Image">

                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <h3 style="color: green">${msg}</h3>
                                                    <input type="hidden" name="id" value="<%=feedbackDetail.getFeedbackId()%>"/>
                                                    <label class="control-label">Tên:</label>
                                                    <input type="text" class="form-control" value="<%=feedbackDetail.getFullName()%>"  readonly/>

                                                    <label class="control-label">Email:</label>
                                                    <input type="text" class="form-control" value="<%=feedbackDetail.getEmail()%>"  readonly/>

                                                    <label class="control-label">Số điện thoại:</label>
                                                    <input type="text" class="form-control" value="<%=feedbackDetail.getMobile()%>"  readonly/>

                                                    <label class="control-label">Đánh giá:</label>

                                                    <c:choose>
                                                        <c:when test="${feedbackDetail.ratedStar == 5}">
                                                            <c:set var="imgRating" value="5"/>
                                                        </c:when>
                                                        <c:when test="${feedbackDetail.ratedStar > 4}">
                                                            <c:set var="imgRating" value="4.5"/>
                                                        </c:when>
                                                        <c:when test="${feedbackDetail.ratedStar == 4}">
                                                            <c:set var="imgRating" value="4"/>
                                                        </c:when>
                                                        <c:when test="${feedbackDetail.ratedStar > 3}">
                                                            <c:set var="imgRating" value="3.5"/>
                                                        </c:when>
                                                        <c:when test="${feedbackDetail.ratedStar == 3}">
                                                            <c:set var="imgRating" value="3"/>
                                                        </c:when>
                                                        <c:when test="${feedbackDetail.ratedStar > 2}">
                                                            <c:set var="imgRating" value="2.5"/>
                                                        </c:when>
                                                        <c:when test="${feedbackDetail.ratedStar == 2}">
                                                            <c:set var="imgRating" value="2"/>
                                                        </c:when>
                                                        <c:when test="${feedbackDetail.ratedStar > 1}">
                                                            <c:set var="imgRating" value="1.5"/>
                                                        </c:when>
                                                        <c:when test="${feedbackDetail.ratedStar == 1}">
                                                            <c:set var="imgRating" value="1"/>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <c:set var="imgRating" value="0.5"/>
                                                        </c:otherwise>
                                                    </c:choose>

                                                    <img src="${pageContext.request.contextPath}/view/images/feedback/${feedbackdetail.ratedStar}.png" alt="rating"/>


                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Trạng thái:</label>
                                                    <select class="form-control" name="status" onchange="this.form.submit()"/>
                                                    <option value="0" <%=feedbackDetail.isStatus() ? "selected" : ""%>>Hiện</option>
                                                    <option value="1" <%=!feedbackDetail.isStatus() ? "selected" : ""%>>Ẩn</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Nội dung:</label>
                                                    <textarea class="form-control" name="notes" readonly/><%=feedbackDetail.getContent()%></textarea>
                                                </div>
                                            </div>

                                        </div>
                                        <input type="hidden" name="slider_id" value="<%=feedbackDetail.getFeedbackId()%>"/>
                                    </form>
                                </div>

                        </div>




                </section>
        </div>
    </div><!--row1-->

</section><!-- /.content -->
</aside><!-- /.right-side -->
<div class="footer-main">
    Copyright &copy Director, 2014
</div>
</div><!-- ./wrapper -->
<!-- jQuery 2.0.2 -->
<script src="http://ajax.googleapis.com/${pageContext.request.contextPath}/view/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/view/js/jquery.min.js" type="text/javascript"></script>

<!-- Bootstrap -->
<script src="${pageContext.request.contextPath}/view/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Director App -->
<script src="${pageContext.request.contextPath}/view/js/Director/app.js" type="text/javascript"></script>
</body>
</html>
