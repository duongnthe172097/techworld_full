<%-- 
    Document   : user-details
    Created on : Jan 24, 2024, 9:23:35 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import = "model.*" %>
<%@page import = "dal.*" %>
<%@page import = "java.sql.Date" %>
<%@page import = "java.util.*" %>
<%@page import = "java.text.DecimalFormat" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Chi tiết người dùng | Admin</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta name="description" content="Developed By M Abdur Rokib Promy">
        <meta name="keywords" content="Admin, Bootstrap 3, Template, Theme, Responsive">
        <!-- bootstrap 3.0.2 -->
        <link href="${pageContext.request.contextPath}/view/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="${pageContext.request.contextPath}/view/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="${pageContext.request.contextPath}/view/css/ionicons.min.css" rel="stylesheet" type="text/css" />

        <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
        <!-- Theme style -->
        <link href="${pageContext.request.contextPath}/view/css/style.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.${pageContext.request.contextPath}/view/js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <%
           User userDetail = (User)request.getAttribute("userdetail");
           RoleDAO roleDAO = new RoleDAO();
           List<Role> allRoles =  roleDAO.getAllRoles();
           String roleName = roleDAO.getRoleNameById(userDetail.getUserId());
    %>
    <style>
        .user-brief-info {
            text-align: center;
        }
        .user-brief-info img{
            width: 200px;
            height: 200px;
            border-radius: 50%;
        }

        .user-brief-info h4,h3 {
            color: grey;
        }

        .user-brief-info h4 {
            padding: 20px;
        }

        .form-update-user select{
            border: 1px solid wheat;
            padding: 10px;
        }

        .form-update-user span {
            padding: 25px;
        }

        @media(min-width: 998px) {
            .user-brief-info section{
                min-height: 460px;
            }
        }
    </style>

    <body class="skin-black">
        <!-- header logo: style can be found in header.less -->
        <jsp:include page="admin-header.jsp"></jsp:include>
            <div class="wrapper row-offcanvas row-offcanvas-left">
                <!-- Left side column. contains the logo and sidebar -->
                <aside class="left-side sidebar-offcanvas">
                    <!-- sidebar: style can be found in sidebar.less -->
                    <section class="sidebar">
                        <!-- Sidebar user panel -->
                        <div class="user-panel">
                            <div class="pull-left image">
                                <img src="${pageContext.request.contextPath}/view/img/26115.jpg" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Xin chào, ${sessionScope.user.userName}</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>

                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li>
                            <a href="${pageContext.request.contextPath}/admin/admin-dashboard">
                                <i class="fa fa-dashboard"></i> <span>Thống kê</span>
                            </a>
                        </li>
                        <li class="active">
                            <a href="${pageContext.request.contextPath}/admin/user">
                                <i class="fa fa-gavel"></i> <span>Người dùng</span>
                            </a>
                        </li>

                        <li>
                            <a href="${pageContext.request.contextPath}/admin/settings/view">
                                <i class="fa fa-globe"></i> <span>Cài đặt</span>
                            </a>
                        </li>

                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->


                <!-- Main content -->
                <section class="content" style="min-height: 90vh">
                    <h3 style="color: green">${requestScope.msg}</h3>
                    <div class="row">
                        <div class="col-lg-4 user-brief-info">
                            <section class="panel">
                                <div class="panel-body">
                                    <img src="${pageContext.request.contextPath}/view/images/user/<%=userDetail.getImage()%>" alt=""/>
                                </div>
                                <h3><%=userDetail.getFullName()%></h3>
                                <h4><%=userDetail.getAddress()%></h4>
                            </section>
                        </div>
                        <div class="col-lg-8">
                            <section class="panel">
                                <header class="panel-heading">
                                    Thông tin về <%=userDetail.getFullName()%>
                                </header>
                                <div class="panel-body form-update-user">
                                    <form class="form-horizontal" role="form" action="${pageContext.request.contextPath}/admin/admin-edituser" method="post">
                                        <input type="hidden" name="user_id" value="<%=userDetail.getUserId()%>"/>
                                        <div class="form-group">
                                            <label class="col-lg-2 col-sm-2 control-label">Họ và tên</label>
                                            <div class="col-lg-10">
                                                <input type="text" class="form-control" id="inputEmail1" value="<%=userDetail.getFullName()%>" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 col-sm-2 control-label">Giới tính</label>
                                            <div class="col-lg-10">
                                                <input class="form-control" id="inputPassword1" value="<%=userDetail.isGender() ? "Nam" : "Nữ"%>" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 col-sm-2 control-label">Địa chỉ</label>
                                            <div class="col-lg-10">
                                                <input class="form-control" id="inputPassword1" value="<%=userDetail.getAddress()%>" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 col-sm-2 control-label">Email</label>
                                            <div class="col-lg-10">
                                                <input class="form-control" id="inputPassword1" value="<%=userDetail.getEmail()%>" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 col-sm-2 control-label">Điện thoại</label>
                                            <div class="col-lg-10">
                                                <input class="form-control" id="inputPassword1" value="<%=userDetail.getMobile()%>" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 col-sm-2 control-label">Vai trò</label>
                                            <div class="col-lg-10">
                                                <select name="role_id">
                                                    <%
                                                        for(Role role : allRoles) {
                                                    %>
                                                    <option value="<%=role.getRoleId()%>" <%=role.getRoleId() == userDetail.getRoleId() ? "selected" : ""%>><%=role.getRoleName()%><option>
                                                        <%}%>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-lg-2 col-sm-2 control-label">Trạng thái</label>
                                            <span><input name="status" type="radio" value="1" <%=userDetail.isStatus() ? "checked" : ""%>/>Hợp lệ</span>
                                            <span><input name="status" type="radio" value="0" <%=!userDetail.isStatus() ? "checked" : ""%>/>Bị cấm</span>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-offset-2 col-lg-10">
                                                <button type="submit" class="btn btn-danger">Cập nhật</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </section>
                        </div>
                    </div><!--row1-->

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
            <div class="footer-main">
                Copyright &copy Director, 2014
            </div>
        </div><!-- ./wrapper -->
        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/${pageContext.request.contextPath}/view/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/jquery.min.js" type="text/javascript"></script>

        <!-- Bootstrap -->
        <script src="${pageContext.request.contextPath}/view/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- Director App -->
        <script src="${pageContext.request.contextPath}/view/js/Director/app.js" type="text/javascript"></script>
    </body>
</html>
