<%-- 
    Document   : index
    Created on : Jan 8, 2024, 4:21:22 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import = "model.*" %>
<%@page import = "dal.*" %>
<%@page import = "java.util.List" %>
<%@page import = "java.util.ArrayList" %>
<%@page import = "java.text.DecimalFormat" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Home | Tech World</title>
        <link href="${pageContext.request.contextPath}/view/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/font-awesome.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/prettyPhoto.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/price-range.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/animate.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/main.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/view/css/responsive.css" rel="stylesheet">
        <!--[if lt IE 9]>
        <script src="${pageContext.request.contextPath}/view/js/html5shiv.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/respond.min.js"></script>
        <![endif]-->       
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/view/images/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="${pageContext.request.contextPath}/view/images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="${pageContext.request.contextPath}/view/images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="${pageContext.request.contextPath}/view/images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/view/images/ico/apple-touch-icon-57-precomposed.png">
    </head><!--/head-->

    <body>
        <style type="text/css">

            body {
                background-color: #F8F8F8;
            }

            .product-homepage {
                background-color: #ffffff;
                border: none;
                box-shadow: rgba(0, 0, 0, 0.1) 0px 10px 15px -3px, rgba(0, 0, 0, 0.05) 0px 4px 6px -2px;
            }

            .product-homepage span {
                color: #f19d23;
                font-size: 20px;
                font-weight: 400;
            }

            .product-homepage span del {
                color: red;
                font-size: 12px;
            }

            .product-homepage img {
                box-shadow: rgba(255, 255, 255, 0.56) 0px 22px 70px 4px;
            }

            .product-homepage p {
                color: #000000;
                font-size: 18px;
                margin: 20px 0;
                min-height: 50px;
            }

            .post-homepage {
                background-color: #ffffff;
                border: none;
            }

            .post-homepage img {
                box-shadow: rgba(255, 255, 255, 0.56) 0px 22px 70px 4px;
            }

            #header {
                background-color: #7FCDF9;
                box-shadow: rgba(0, 0, 0, 0.1) 0px 4px 6px -1px, rgba(0, 0, 0, 0.06) 0px 2px 4px -1px;
            }

            .logo-techworld img{
                width: 120px;
                height: auto;
            }

            .header-searchform input{
                background-color: #ffffff;
                color: #333333;
            }

            @media (min-width: 768px) {

                #product-homepage {
                    min-height: 220vh;
                }

                .product-homepage {
                    height: 360px;
                    overflow: hidden;
                    padding: 3px;
                }
                .product-homepage img {
                    height: 150px;
                    width: fit-content;
                    max-width: 100%;
                }

                .post-homepage {
                    padding: 12px;
                    height: 430px;
                }

                .post-homepage img {
                    height: 150px;
                    width: fit-content;
                    width: 100%;
                    margin-bottom: 7px;
                }

                .post-homepage h5{
                    height: 25px;
                }

                .post-homepage p{
                    height: 150px;
                }
            }
        </style>
        <!-- ======= Header ======= -->
        <header id="header"><!--header-->
            <div class="header_top"><!--header_top-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="contactinfo">
                                <ul class="nav nav-pills">
                                    <li><a href="#"><i class="fa fa-phone"></i> +2 95 01 88 821</a></li>
                                    <li><a href="#"><i class="fa fa-envelope"></i> minhnhhe170924@fpt.edu.vn</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="social-icons pull-right">
                                <ul class="nav navbar-nav">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/header_top-->

            <div class="header-middle"><!--header-middle-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="logo pull-left logo-techworld">
                                <a href="${pageContext.request.contextPath}/home"><img src="${pageContext.request.contextPath}/view/images/home/tech_logo.png" alt="" /></a>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="shop-menu pull-right">
                                <ul class="nav navbar-nav">
                                    <c:if test="${sessionScope.user == null}">
                                        <li><a href="${pageContext.request.contextPath}/view/login.jsp"><i class="fa fa-user"></i> Tài khoản</a></li>
                                        <li><a href="${pageContext.request.contextPath}/view/login.jsp"><i class="fa fa-lock"></i> Đăng nhập</a></li>
                                        </c:if>
                                        <c:if test="${sessionScope.user != null}">
                                        <li><a href="${pageContext.request.contextPath}/profile"><i class="fa fa-user"></i> ${sessionScope.user.fullName}</a></li>
                                            <c:if test="${sessionScope.user.roleId == 5}">
                                            <li><a href="${pageContext.request.contextPath}/admin/admin-dashboard"><i class="fa fa-crosshairs"></i> Trang quản trị</a></li>
                                            </c:if>
                                            <c:if test="${sessionScope.user.roleId == 3 || sessionScope.user.roleId == 4}">
                                            <li><a href="${pageContext.request.contextPath}/sale/dashboard"><i class="fa fa-crosshairs"></i> Trang sale</a></li>
                                            </c:if>
                                            <c:if test="${sessionScope.user.roleId == 2}">
                                            <li><a href="${pageContext.request.contextPath}/marketing/marketing-dashboard"><i class="fa fa-crosshairs"></i> Trang marketing</a></li>
                                            </c:if>
                                            <c:if test="${sessionScope.user.roleId == 1}">
                                            <li><a href="${pageContext.request.contextPath}/cartController"><i class="fa fa-shopping-cart"></i>Giỏ hàng</a></li>
                                            <li><a href="${pageContext.request.contextPath}/customer/my-order"><i class="fa fa-crosshairs"></i> Đơn hàng của tôi</a></li>
                                            </c:if>
                                        <li><a href="${pageContext.request.contextPath}/logout"><i class="fa fa-lock"></i> Đăng xuất</a></li>
                                        </c:if>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/header-middle-->

            <div class="header-bottom"><!--header-bottom-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-9">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="mainmenu pull-left">
                                <ul class="nav navbar-nav collapse navbar-collapse">
                                    <li><a href="${pageContext.request.contextPath}/home" class="active">Trang chủ</a></li>
                                    <li><a href="${pageContext.request.contextPath}/products">Sản phẩm</a></li>
                                    <li><a href="${pageContext.request.contextPath}/blogs">Bài đăng</a></li>
                                    <li><a href="${pageContext.request.contextPath}/view/contact-us.jsp">Liên hệ</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <form action="${pageContext.request.contextPath}/home" class="search_box pull-right header-searchform">
                                <input type="text" name="search" placeholder="Tìm kiếm sản phẩm" value="${requestScope.searchKey}"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div><!--/header-bottom-->
        </header><!--/header-->
        <%
            ProductImageDAO productImageDAO = new ProductImageDAO();
            pageContext.setAttribute("productImageDAO", productImageDAO, PageContext.PAGE_SCOPE);
            
            PostImageDAO postImageDAO = new PostImageDAO();
            pageContext.setAttribute("postImageDAO", postImageDAO, PageContext.PAGE_SCOPE);
            
            SliderDAO sliderDAO = new SliderDAO();
            List<Slider> allActiveSliders = sliderDAO.getActiveSliders();
            pageContext.setAttribute("allActiveSliders", allActiveSliders, PageContext.PAGE_SCOPE);
            
            PostDAO postDAO = new PostDAO();
            List<Post> allActivePosts = postDAO.getAllActivePosts();
            pageContext.setAttribute("allActivePosts", allActivePosts, PageContext.PAGE_SCOPE);
            
            DecimalFormat decimalFormat = new DecimalFormat("#,###");
            pageContext.setAttribute("decimalFormat", decimalFormat, PageContext.PAGE_SCOPE);
        %>
        <section id="slider"><!--slider-->
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <c:if test="${allActiveSliders.size() > 0}">
                            <div id="slider-carousel" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
                                        <c:forEach begin="1" end="${allActiveSliders.size() - 1}" var="index">
                                        <li data-target="#slider-carousel" data-slide-to="${index}"></li>
                                        </c:forEach>
                                </ol>
                                <div class="carousel-inner">
                                    <c:forEach items="${allActiveSliders}" var="item" varStatus="status">
                                        <div class="item ${status.index == 0 ? 'active' : ''}">
                                            <div class="col-sm-6">
                                                <h1><span>TECH</span>-WORLD</h1>
                                                <h2>${item.title}</h2>
                                                <p>${item.note}</p>
                                                <a href="${pageContext.request.contextPath}/product?id=${item.productId}"><button type="button" class="btn btn-default get">Mua ngay</button></a>
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="${pageContext.request.contextPath}/view/images/slider/${item.image}" class="girl img-responsive" alt=""/>
                                                <img src="${pageContext.request.contextPath}/view/images/home/pricing.png"  class="pricing" alt="" />
                                            </div>
                                        </div>
                                    </c:forEach>
                                </div>

                                <a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
                                    <i class="fa fa-angle-left"></i>
                                </a>
                                <a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                        </c:if>
                    </div>
                </div>
            </div>
        </section><!--/slider-->

        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <!-- ======= Sidebar ======= -->
                        <jsp:include page="left-sidebar.jsp"></jsp:include>
                        </div>

                        <!--features_items-->
                        <div class="col-sm-9 padding-right" id="product-homepage">
                            <div class="features_items"><!--features_items-->
                                <h2 class="title text-center">Sản phẩm</h2>
                                <div class="tab-content">
                                <c:forEach items="${requestScope.allProducts}" var="product">
                                    <div class="tab-pane fade active in">
                                        <c:if test="${requestScope.allProducts.size() > 0}">
                                            <div class="col-sm-4">
                                                <div class="product-image-wrapper">
                                                    <div class="single-products product-homepage">
                                                        <div class="productinfo text-center">
                                                            <a href="${pageContext.request.contextPath}/product?id=${product.productId}">
                                                                <c:set var="imageList" value="${productImageDAO.getAllImageOfProductById(product.productId)}"/>
                                                                <c:set var="imageUrl" value="${(imageList.size() > 0) ? imageList.get(0).imageUrl : ''}"/>
                                                                <img src="${pageContext.request.contextPath}/view/images/product/${imageUrl}" alt="image" />
                                                                <c:set var="newPrice" value="${decimalFormat.format(product.price * (1 - product.discount/100))}"/>
                                                                <h2>
                                                                    <span>${newPrice}đ</span>
                                                                    <c:set var="oldPrice" value="${decimalFormat.format(product.price)}"/>
                                                                    <span>
                                                                        <del>
                                                                            <c:if test="${oldPrice != newPrice}">
                                                                                ${oldPrice}đ
                                                                            </c:if>
                                                                        </del>
                                                                    </span>
                                                                </h2>
                                                                <p>${product.productName}</p>
                                                            </a>
                                                            <c:set var="roleId" value="${sessionScope.user.roleId}"/>
                                                            <c:if test="${roleId == 1 || empty roleId}">
                                                                <button class="btn btn-default add-to-cart" onclick="addToCart('${product.productId}')"><i class="fa fa-shopping-cart"></i>Thêm vào giỏ</button>
                                                            </c:if>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </c:if>
                                        <c:if test="${requestScope.allProducts == null || requestScope.allProducts.size() == 0}">
                                            Không có sản phẩm.
                                        </c:if>
                                    </div>
                                </c:forEach>
                                <c:set var="page" value="${requestScope.page}"/>
                                <div class="pagination-area col-sm-12">
                                    <ul class="pagination">
                                        <c:forEach begin="1" end="${requestScope.numberOfPages}" var="index">
                                            <li><a href="home?page=${index}" class="${index == page ? "active" : ""}">${index}</a></li>
                                            </c:forEach>
                                    </ul>
                                </div>
                            </div>

                        </div><!--features_items-->

                        <script>
                            <c:choose>
                                <c:when test="${empty sessionScope.user}">
                            function addToCart(id) {
                                window.location.href = "login";
                                alert('Login required');
                            }
                                </c:when>
                                <c:otherwise>
                            function addToCart(id) {
                                fetch('addToCart?productId=' + id + '&quantity=1')
                                        .then(response => alert('Thêm thành công!'))
                                        .catch(error => console.error('Error:', error));
                            }
                                </c:otherwise>
                            </c:choose>
                        </script>



                        <div class="recommended_items"><!--recommended_items-->
                            <h2 class="title text-center">Bài đăng</h2>
                            <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <div class="item active">	
                                        <c:forEach begin="0" end="2" var="index">
                                            <c:if test="${allActivePosts.size() >= index}">
                                                <div class="col-sm-4">
                                                    <div class="product-image-wrapper">
                                                        <div class="single-products post-homepage">
                                                            <div class="productinfo text-center">
                                                                <c:set var="imageList" value="${postImageDAO.getAllImageOfPostById(index + 1)}"/>
                                                                <c:set var="imageUrl" value="${(imageList.size() > 0) ? imageList.get(0).imageUrl : ''}"/>
                                                                <img src="${pageContext.request.contextPath}/view/images/post/${imageUrl}" alt="image" />
                                                                <h5 style="color: #00B4D8; margin: 12px;">
                                                                    ${allActivePosts.get(index).title}
                                                                </h5>
                                                                <p style="text-align: left; font-size: 12px;">
                                                                    ${allActivePosts.get(index).briefInfo}
                                                                </p>
                                                                <a href="${pageContext.request.contextPath}/blog-details?id=${index + 1}" class="btn btn-default add-to-cart">
                                                                    Xem chi tiết
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </c:if>
                                        </c:forEach>
                                    </div>
                                    <div class="item">	
                                        <c:forEach begin="3" end="${allActivePosts.size() - 1}" var="index">
                                            <c:if test="${allActivePosts.size() >= index}">
                                                <div class="col-sm-4">
                                                    <div class="product-image-wrapper">
                                                        <div class="single-products post-homepage">
                                                            <div class="productinfo text-center">
                                                                <c:set var="imageList" value="${postImageDAO.getAllImageOfPostById(index + 1)}"/>
                                                                <c:set var="imageUrl" value="${(imageList.size() > 0) ? imageList.get(0).imageUrl : ''}"/>
                                                                <img src="${pageContext.request.contextPath}/view/images/post/${imageUrl}" alt="image" />
                                                                <h5 style="color: #00B4D8; margin: 12px;">
                                                                    ${allActivePosts.get(index).title}
                                                                </h5>
                                                                <p style="text-align: left; font-size: 12px;">
                                                                    ${allActivePosts.get(index).briefInfo}
                                                                </p>
                                                                <a href="${pageContext.request.contextPath}/blog-details?id=${index + 1}" class="btn btn-default add-to-cart">
                                                                    Xem chi tiết
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </c:if>
                                        </c:forEach>
                                    </div>
                                </div>
                                <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
                                    <i class="fa fa-angle-left"></i>
                                </a>
                                <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
                                    <i class="fa fa-angle-right"></i>
                                </a>			
                            </div>
                        </div><!--/recommended_items-->
                    </div>
                </div>
            </div>
        </section>


        <!-- ======= Footer ======= -->
        <jsp:include page="footer.jsp"></jsp:include>

            <script src="${pageContext.request.contextPath}/view/js/jquery.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/jquery.scrollUp.min.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/price-range.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/jquery.prettyPhoto.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/main.js"></script>
    </body>
</html>
