<%-- 
    Document   : add-setting
    Created on : Jan 18, 2024, 11:18:35 PM
    Author     : ns
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page import = "model.*" %>
<%@page import = "dal.*" %>
<%@page import = "java.util.List" %>
<%@page import = "java.util.ArrayList" %>
<%@ page import="java.util.Map" %>
<%@page import = "java.text.DecimalFormat" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Slider | Marketing</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta name="description" content="Developed By M Abdur Rokib Promy">
        <meta name="keywords" content="Admin, Bootstrap 3, Template, Theme, Responsive">
        <!-- bootstrap 3.0.2 -->
        <link href="${pageContext.request.contextPath}/view/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="${pageContext.request.contextPath}/view/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="${pageContext.request.contextPath}/view/css/ionicons.min.css" rel="stylesheet" type="text/css" />

        <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
        <!-- Theme style -->
        <link href="${pageContext.request.contextPath}/view/css/style.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <link rel="stylesheet" href="https://cdn.datatables.net/1.13.7/css/jquery.dataTables.css" />
        <style>
            /*            tfoot input {
                            width: 100%;
                            padding: 3px;
                            box-sizing: border-box;
                            border: 1px solid #aaa;
                            border-radius: 3px;
                            padding: 5px;
                            background-color: transparent;
                            color: inherit;
                            margin-left: 3px;
                        }*/

            /* CSS cho ô filter */
            #status-filter-container {
                margin-bottom: 10px;
            }

            #status-filter {
                width: 120px;
            }

            /* CSS cho phần hiển thị kết quả lọc */
            #filtered-results {
                margin-top: 10px;
                font-style: italic;
            }

            /* CSS cho hàng chứa các ô filter */
            #filter-row {
                display: flex;
                justify-content: space-between;
                margin-bottom: 10px;
            }

            /* CSS cho ô filter */
            #status-filter-container,
            #author-filter-container {
                flex: 1;
                margin-right: 10px;
            }
            /* CSS cho ô filter theo tác giả */
            #author-filter-container {
                margin-bottom: 10px;
            }

            #author-filter {
                width: 25%;
            }


        </style>
    </head>
    <%
        //post image dao
            PostImageDAO postImageDAO = new PostImageDAO();
             pageContext.setAttribute("postImageDAO", postImageDAO, PageContext.PAGE_SCOPE);
    
            CategoryDAO categoryDAO = new CategoryDAO();
            pageContext.setAttribute("categoryDAO", categoryDAO, PageContext.PAGE_SCOPE);

            PostDAO postDAO = new PostDAO();
            pageContext.setAttribute("postDAO", postDAO, PageContext.PAGE_SCOPE);

           UserDAO userDAO = new UserDAO();
            pageContext.setAttribute("userDAO", userDAO, PageContext.PAGE_SCOPE);
            
             ProductDAO productDAO = new ProductDAO();
            pageContext.setAttribute("productDAO", productDAO, PageContext.PAGE_SCOPE);
    %>



    <body class="skin-black">
        <!-- header logo: style can be found in header.less -->
        <jsp:include page="admin-header.jsp"></jsp:include>
            <div class="wrapper row-offcanvas row-offcanvas-left">
                <!-- Left side column. contains the logo and sidebar -->
                <aside class="left-side sidebar-offcanvas">
                    <!-- sidebar: style can be found in sidebar.less -->
                    <section class="sidebar">
                        <!-- Sidebar user panel -->
                        <div class="user-panel">
                            <div class="pull-left image">
                                <img src="${pageContext.request.contextPath}/view/img/26115.jpg" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Xin chào, ${sessionScope.user.userName}</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>

                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/marketing-dashboard">
                                <i class="fa fa-dashboard"></i> <span>Thống kê</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/posts">
                                <i class="fa fa-gavel"></i> <span>Bài đăng</span>
                            </a>
                        </li>
                        <li class="active">
                            <a href="${pageContext.request.contextPath}/marketing/view-sliders">
                                <i class="fa fa-gavel"></i> <span>Slider</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/product/list">
                                <i class="fa fa-gavel"></i> <span>Sản phẩm</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/marketing-viewcustomer">
                                <i class="fa fa-gavel"></i> <span>Khách hàng</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/marketing/feedbacks/list">
                                <i class="fa fa-gavel"></i> <span>Phản hồi</span>
                            </a>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->


                <!-- Main content -->
                <section class="content" style="min-height: 1000vh">
                    <h3 style="color: green">${msg}</h3>
                    <div class="row" style="display: flex; justify-content: center; ">
                        <div class="col-md-12"> 
                            <header class="panel-heading">
                                Danh sách slider
                            </header>

                            <!-- <div class="box-header"> -->
                            <!-- <h3 class="box-title">Responsive Hover Table</h3> -->

                            <!-- </div> -->

                            <div class="panel-body table-responsive" style="background-color: #ffffff">

                                <div id="filter-row" class="row">
                                    <div id="status-filter-container" class="col-md-4">
                                        <label for="status-filter">Lọc theo trạng thái:</label>
                                        <select id="status-filter">
                                            <option value="">Tất cả</option>
                                            <option value="active">Hiện</option>
                                            <option value="inactive">Ẩn</option>
                                        </select>
                                    </div>  

                                </div>
                                <!-- Ô filter -->


                                <!-- Phần hiển thị kết quả lọc -->
                                <div id="filtered-results"></div>
                                <table id="setting-table" class="table cell-border row-border hover stripe">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Ảnh</th>
                                            <th>Tên</th>
                                            <th>Mô tả ngắn</th>                          
                                            <th>Trạng thái</th>
                                            <th>Đổi trạng thái</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach var="sliders" items="${sliders}">
                                            <tr>
                                                <td>${sliders.sliderId}</td>
                                                <td> 
                                                    <img style="width:400px;" src="${pageContext.request.contextPath}/view/images/slider/${sliders.image}" alt="image" />
                                                </td>
                                                <td>${sliders.title}</td>
                                                <td>${sliders.note} </td>
                                                <td>${sliders.status ? "Hiện" : "Ẩn"}</td>
                                                <td>
                                                    <a style="text-align: center" class="btn ${sliders.isStatus() ? 'btn-danger' : 'btn-success'}" style="margin-right: 5px; margin-bottom: 5px;" class="btn btn-info" 
                                                       href="${pageContext.request.contextPath}/marketing/slider/change-status?id=${sliders.sliderId}&status=${sliders.isStatus() ? 1 : 0}">
                                                        ${!sliders.isStatus() ? 'Hiện' : 'Ẩn'}
                                                    </a>
                                                </td>
                                                <td style="width: 30px; text-align: center;">
                                                    <a style="margin-right: 5px; margin-bottom: 5px;" class="btn btn-primary" href="${pageContext.request.contextPath}/marketing/slider-detail?id=${sliders.sliderId}">Xem</a>

                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>ID</th>
                                            <th>Ảnh</th>
                                            <th>Tên</th>  
                                            <th>Mô tả ngắn</th>
                                            <th>Trạng thái</th>
                                            <th></th><!-- comment -->
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div><!-- /.box-body -->
                        </div>
                    </div>
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
            <div class="footer-main">
                Copyright &copy Director, 2014
            </div>
        </div><!-- ./wrapper -->
        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <script src="${pageContext.request.contextPath}/view/js/jquery.min.js" type="text/javascript"></script>

        <!-- Bootstrap -->
        <script src="${pageContext.request.contextPath}/view/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- Director App -->
        <script src="${pageContext.request.contextPath}/view/js/Director/app.js" type="text/javascript"></script>

        <script src="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.js"></script>
        <script>
//            $(document).ready(function () {
//                $('#setting-table').DataTable();
//            });

            var settingTable = new DataTable('#setting-table', {
                initComplete: function () {
                    this.api()
                            .columns()
                            .every(function () {
                                let column = this;
                                let title = column.footer().textContent;

                                if (title === '')
                                    return;

                                // Create input element
                                let input = document.createElement('input');
                                input.placeholder = title;
                                column.footer().replaceChildren(input);

                                // Event listener for user input
                                input.addEventListener('keyup', () => {
                                    if (column.search() !== this.value) {
                                        column.search(input.value).draw();
                                    }
                                });
                            });
                }
            });

//            settingTable.column(4)
//                    .search("^" + $(this).val() + "$", true, false, true)
//                    .draw();

            $('#setting-table tfoot > tr > th').css("border", "1px solid rgba(0, 0, 0, 0.15)");
            $('#setting-table').css("border-collapse", "collapse");
            $('#setting-table tfoot > tr > th > input').addClass('form-control');
            $('#setting-table tfoot tr').appendTo('#setting-table thead');




        </script>

        <script>
            document.addEventListener('DOMContentLoaded', function () {
                // Lắng nghe sự kiện thay đổi trạng thái của ô filter
                document.getElementById('status-filter').addEventListener('change', function () {
                    var status = this.value;
                    var rows = document.querySelectorAll('#setting-table tbody tr');
                    var filteredResults = document.getElementById('filtered-results');
                    var count = 0;

                    rows.forEach(function (row) {
                        var cell = row.querySelector('td:nth-child(5)'); // Lấy ô chứa trạng thái

                        // Lấy trạng thái từ textContent của cell
                        var cellStatus = cell.textContent.trim() === "Hiện" ? "active" : "inactive";

                        if (status === '' || cellStatus === status) { // So sánh trạng thái của bài đăng với trạng thái đã chọn
                            row.style.display = '';
                            count++;
                        } else {
                            row.style.display = 'none';
                        }
                    });

                    // Hiển thị số lượng kết quả lọc được
                    filteredResults.textContent = count + " kết quả được tìm thấy.";
                });
            });


        </script>
        <script>

            document.addEventListener('DOMContentLoaded', function () {
                var authorFilter = document.getElementById('author-filter');

                // Lấy danh sách tên tác giả từ các ô thẻ td trong cột "Tác giả"
                var authorCells = document.querySelectorAll('#setting-table tbody td:nth-child(5)');
                var authors = [];

                // Lặp qua từng ô td chứa tên tác giả và thêm vào danh sách tác giả (nếu chưa có)
                authorCells.forEach(function (cell) {
                    var authorName = cell.textContent.trim();
                    if (!authors.includes(authorName)) {
                        authors.push(authorName);
                    }
                });

                // Sắp xếp danh sách tác giả theo thứ tự abc
                authors.sort();

                // Thêm tùy chọn "Tất cả" vào đầu danh sách
                var allOption = document.createElement('option');
                allOption.value = '';
                allOption.textContent = 'Tất cả';
                authorFilter.appendChild(allOption);

                // Thêm các tùy chọn tác giả vào ô filter
                authors.forEach(function (author) {
                    var option = document.createElement('option');
                    option.value = author;
                    option.textContent = author;
                    authorFilter.appendChild(option);
                });

                // Lắng nghe sự kiện khi người dùng thay đổi giá trị của ô select
                authorFilter.addEventListener('change', function () {
                    var selectedAuthor = this.value;
                    var rows = document.querySelectorAll('#setting-table tbody tr');
                    var filteredResults = document.getElementById('filtered-results');
                    var count = 0;

                    rows.forEach(function (row) {
                        var cell = row.querySelector('td:nth-child(5)'); // Lấy ô chứa tên tác giả

                        if (selectedAuthor === '' || cell.textContent.trim() === selectedAuthor) {
                            row.style.display = '';
                            count++;
                        } else {
                            row.style.display = 'none';
                        }
                    });

                    // Hiển thị số lượng kết quả lọc được
                    filteredResults.textContent = count + " kết quả được tìm thấy.";
                });
            });

        </script>
    </body>
</html>

